#!/usr/bin/python
import os
import sys
import multiprocessing


def worker(cmd):
	os.system(cmd)


todolist = ['1a0s', '1bxw', '1e54', '1ek9', '1fep', '1i78', '1k24', '1kmo', '1nqe', '1p4t', '1prn', '1qd6', '1qj8', '1t16', '1thq', '1tly', '1uyn', '1xkw', '1yc9', '2erv', '2f1c', '2f1t', '2fcp', '2gr8', '2lhf', '2lme', '2mlh', '2mpr', '2o4v', '2omf', '2por', '2qdz', '2vqi', '2wjr', '2ynk', '3aeh', '3bs0', '3csl', '3dwo', '3dzm', '3fid', '3kvn', '3pik', '3rbh', '3rfz', '3syb', '3szv', '3v8x', '3vzt', '4c00', '4e1s', '4gey', '4k3c', '4pr7', '4q35', '7ahl', '3b07', '3o44']


pool = multiprocessing.Pool(38)

for todo in todolist:
	todo = todo.strip()
	#resf = open('inputs/'+todo+'/'+todo+'.res')
	#resnum = len(resf.readlines())
	#resf.close()
	#print todo, resnum
	if not os.path.exists('inputs/'+todo):
		continue
	if not os.path.exists('inputs/'+todo+'/'+todo+'.cen.topo'):
		continue

	centopof = open('inputs/'+todo+'/'+todo+'.cen.topo')
	lines = centopof.readlines()
	centopof.close() 
	strandnum = len(open('inputs/'+todo+'/'+todo+'.triplet.con').readlines())
	#if strandnum > 8:
	#if strandnum <= 8 or strandnum > 12:
	#if strandnum <= 12:
	#	print todo
	#	continue

	if strandnum > 20:
		continue

	if strandnum > 12:
		exeprog = './hyall_p2_ir.out'
	else:
		#exeprog = './hyall_p3_aa_ir.out'
		#exeprog = './hyall_p3_aa.out'
		exeprog = './hyall_p4.out'
		#exeprog = './hyall_p3.out'
		#exeprog = './hyall_p2.out'
		#exeprog = './hyall_p1.out'
		#exeprog = './hyall_p0.out'
	restotest = []
	for line in lines:
		splt = line.split()
		seq,top = int(splt[0]),int(splt[1])
		if top == 2:
			restotest += [seq-4, seq-2, seq, seq+2, seq+4]
		else:
			restotest += [seq-3, seq-1, seq+1, seq+3]

	if not os.path.exists('summary_buried_only/'+todo):
		os.mkdir('summary_buried_only/'+todo)

	reslist = open('summary_buried_only/'+todo+'/'+todo+'.out.list','w')
	for i in restotest:
		reslist.write(str(i)+'\n')
	reslist.close()

	for i in restotest:
		if os.path.exists('summary_buried_only/'+todo+'_'+str(i)+'_1.0.0.0.0.0.0.0.8.energy'):
			continue
		if os.path.exists('summary_buried_only/'+todo+'/'+todo+'_'+str(i)+'_1.0.0.0.0.0.0.0.8.energy'):
			continue

		cmd =  exeprog+' '+todo+' odds_asym_old 1 0.0 0.0 0.0 0.8 '+str(i)+' >> summary_buried_only/last.log\n'
		pool.apply_async(worker, (cmd,))

pool.close()
pool.join()
