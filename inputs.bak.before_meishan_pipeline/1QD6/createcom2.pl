###### Create PDB.com file for calculating strand energy/PPI region of a protein#######
###### input file: pdb.tri.center which is info of center residue numbers of strand triplet ###
###### output file: pdb.com which is calculating the strand starting residue number from the center residue number of strand 
###### triplet, followed by adding bash command to annalysis the strand energy/ppi region
 
use strict;
use warnings;
use Cwd;

my $pr=shift @ARGV;
my $dir=getcwd();
open(IN,"<$dir/inputs/$pr/$pr.tri.center") || die "Couldn't open $pr.tri.center!\n";
open(OUT,">$dir/$pr.com") || die "Couldn't open $pr.com!\n";
open(CON,">$dir/inputs/$pr/$pr.triplet.con") || die "Couldn't open $pr.com!\n";

print OUT "if [ ! -d $dir/summary/$pr ]\n";
print OUT "then\n";
print OUT "cd summary/\n";
print OUT "mkdir $pr\n";
print OUT "cp $dir/predictppi.pl $dir/summary/$pr\n";
print OUT "cd ..\n";
print OUT "fi\n";
my $i=1;

while (my $line=<IN>)
{ chomp $line;
  my @arr=split("\t",$line);
  if ($i%2==1)
  {
    printf OUT "perl\tmyPPI.pl\t%s\t%d\t%d\t%d\t1\t2\t1\n",$pr,$arr[0]-8,$arr[1]-9,$arr[2]-8;
     printf CON "%d\t%d\t%d\t1\t2\t1\n",$arr[0]-9,$arr[1]-8,$arr[2]-9;
  }
   else
  {
    printf OUT "perl\tmyPPI.pl\t%s\t%d\t%d\t%d\t2\t1\t2\n",$pr,$arr[0]-9,$arr[1]-8,$arr[2]-9;
    printf CON "%d\t%d\t%d\t2\t1\t2\n",$arr[0]-8,$arr[1]-9,$arr[2]-8;
  }
  $i++;
}

print OUT "cd summary/$pr\n";
print OUT "cat *.summ >$pr.summary\n";
print OUT "awk '{if(NR%9==2) printf \"%d\\t\",\$2; else if (NR%9==0) print \$1}' $pr.summary >>tmp\n";
print OUT "sort -n tmp >>$pr.triple\n";
print OUT "perl predictppi.pl $pr\n";
print OUT "rm tmp\n";
print OUT "awk '{if(NR%9==2) printf \"%d\\t\",\$2; else if (NR%9==5) print \$1}' $pr.summary >>tmp\n";
print OUT "sort -n tmp >>$pr.triple.can\n";
print OUT "rm tmp\n";
print OUT "awk '{if(NR%9==2) printf \"%d\\t\",\$2; else if (NR%9==0) print \$0}' $pr.summary >>tmp\n";
print OUT "sort -n tmp >>$pr.avg\n";
print OUT "rm tmp\n";
print OUT "awk '{if(NR%9==2) printf \"%d\\t\",\$2; else if (NR%9==5) print \$0}' $pr.summary >>tmp\n";
print OUT "sort -n tmp >>$pr.can\n";
print OUT "rm tmp\n";

