This folder contains data not got from pdb.org, and "9thq" is also not a real name.

The information is got from a [plasmic PagP](https://www.ncbi.nlm.nih.gov/protein/896030722?report=fasta),
which has some functional site different from membrane PagP, according to a private conversation with Russell Bishop.

The alignment between the two is followed, calculated by EMBOSS Needle

> 9thq seq           1 MQRCLRVAWGIVFIFTFSVQAETKIYGEQRVSGWWNWLKNDVSQTWNEPQ     50
> 
>                                                  .....|....:.:::|||.:|:
> 
> 1thq seq           1 ----------------------------MNADEWMTTFRENIAQTWQQPE     22
> 
> 

> 9thq seq          51 NYDLYLPFLSWHNRFMYDKEKTDNYNEMPWGGGFGVSRYNQEGNWSSLYA    100
> 
>                      :||||:|.::||.||.|||||||.|||.|||||||:||::::|||..|||
> 
> 1thq seq          23 HYDLYIPAITWHARFAYDKEKTDRYNERPWGGGFGLSRWDEKGNWHGLYA     72
> 
> 

> 9thq seq         101 MMFKDSHNEWQPIIGYGWEKGWYLDSRRDFRLGLGVTAGITARDDFANYV    150
> 
>                      |.||||.|:|:||.|||||..|...:..:|.||||.|||:||||:: ||:
> 
> 1thq seq          73 MAFKDSWNKWEPIAGYGWESTWRPLADENFHLGLGFTAGVTARDNW-NYI    121
> 
> 

> 9thq seq         151 PLPIILPLFSASYRRLSVQFTYIPGTYNNGNVLFAWLRYGF--------    191
> 
>                      |||::|||.|..|..::.|.||||||||||||.|||:|:.|        
> 
> 1thq seq         122 PLPVLLPLASVGYGPVTFQMTYIPGTYNNGNVYFAWMRFQFLEHHHHHH    170
> 

To simplify the calculation, we use 9thq sequence starting from "RVSG...",
so that we can use all data from 1thq except .res file.
The sequence starting from "RVSG..." is used to construct 9thq.res file.
There is also a deletion of "A" in the segment of "...DDFANY...".
