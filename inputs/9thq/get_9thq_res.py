seq = "RVSGWWNWLKNDVSQTWNEPQNYDLYLPFLSWHNRFMYDKEKTDNYNEMPWGGGFGVSRYNQEGNWSSLYAMMFKDSHNEWQPIIGYGWEKGWYLDSRRDFRLGLGVTAGITARDDFNYVPLPIILPLFSASYRRLSVQFTYIPGTYNNGNVLFAWLRYGF"

one_list = [ 'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V' ]

index_list = range(20)

one_to_index_dict = dict( zip(one_list, index_list) )

with open('9thq.res','w') as f:
	for aa in seq:
		f.write( str(one_to_index_dict[aa])+'\n')

