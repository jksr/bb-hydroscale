#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <memory>
#include <new>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <cmath>

using namespace std;


string testtag = "";

#define window 7
#define T 298
#define Kb 0.0019859

#ifdef P0
	#define PRECISION 1
#elif P1
	#define PRECISION 10
#elif P2
	#define PRECISION 100
#elif P3
	#define PRECISION 1000
#else
	#define PRECISION 10000
#endif

#ifdef ALLAPP
	#define APPCRIT 2
#else
	#define APPCRIT 12
#endif


// reverse the strand.
void reverse(int *strand);

// jernigan
float intrastrand (int *strand,int index);

// determin facing, t for 1 2 3 in the triplet
int facing(int *strand, int t);

float singlebody (int *strand, int index, int face);

// SH/NH/WH
float pairwise_ab (int *strand_a,int *strand_b,int dir1,int dir2,int face);
float pairwise_bc (int *strand_b,int *strand_c,int dir2,int dir3,int face);

void load_odds(string& fname, float* arr, float val, bool needlog=true);
int aa_pair(int a, int b);
char *aa_char(int a);

float energy_exct(int TotalStrands, int& statenum);
float energy_app(int TotalStrands, int& statenum);

float strong[211];
float vdw[211];
float weak[211];
float intra[211];

float self[360];
float self0[360];

float* PeriIn = self0+20;
float* PeriOut = self0+40;
float* CoreIn = self0+60;
float* CoreOut = self0+80;
float* ExtraIn = self0+100;
float* ExtraOut = self0+120;

float *semin;
float *semax;
float ***se;
float ***sel;
float ***ser;
float **sb;

int main(int argc,char*argv[]) {  
 
	if (argc != 9) {
		cerr << "# of arguments incorrect"<<endl;
		exit(0);
	}
	printf ("Program Started\n");

	string path = argv[0];
	path = path.substr(0,path.rfind("/"));

	string protein = argv[1];

	string oddsfolder = path+"/"+string(argv[2])+"/";

	double wself = atof(argv[3]);
	string selfweight = argv[3];    
	double wsh = atof(argv[4]);
	string shweight = argv[4];
	double wnh = atof(argv[5]);
	string nhweight = argv[5];
	double wwh = atof(argv[6]);
	string whweight = argv[6];
	double wintra = atof(argv[7]);
	string intraweight = argv[7];       
	int mutpos = atoi(argv[8]);
	string mutationposition = argv[8];


	float strong0[211];
	float vdw0[211];
	float weak0[211];
	float intra0[211];
	  
	string inputsfolder = path+"/inputs/";
	string proteinfolder = inputsfolder+protein+"/";
	string fname;
	ifstream fin;
	string line;

	fname = proteinfolder + protein + ".res";
	fin.open(fname.c_str());	
	if(fin.fail()) {
		cerr << "error opening file" << fname <<endl;
		exit(1);
	}
	// total line number, i.e. total residue number
	int nuRes = 0;
	while ( getline(fin,line) ) {
		nuRes++;
	}
	fin.clear();
	fin.seekg(0,ios::beg);
	//cout <<"line number " <<  nuRes << endl;

	int *res = new int[nuRes+1];
	res[0]=200;
	for( int i = 1; i < nuRes+1; i++ ){
		fin>>res[i];
	}
	fin.close();


	fname = proteinfolder + protein + ".triplet.con";
	fin.open(fname.c_str());
	if(fin.fail()) {
		cerr << "error opening file" << fname <<endl;
		exit(1);
	}

	int TotalStrands = 0;
	while ( getline(fin,line) ) {
		TotalStrands++;
	}
	fin.clear();
	fin.seekg(0,ios::beg);



	int *starts_a_rot = new int[TotalStrands * 3];
	int *starts_b_rot = new int[TotalStrands * 3];
	int *starts_c_rot = new int[TotalStrands * 3];
	int *direction_a_rot = new int[TotalStrands * 3];
	int *direction_b_rot = new int[TotalStrands * 3];
	int *direction_c_rot = new int[TotalStrands * 3];

	int rotateto = 0;
	int mutstrand = 0;
	for( int i = 0; i < TotalStrands; i++ ){
		getline (fin,line);
		istringstream iss(line);
		iss >> starts_a_rot[i] >> starts_b_rot[i] >> starts_c_rot[i]
			>> direction_a_rot[i] >> direction_b_rot[i] >> direction_c_rot[i];
		// find out mutation happens on which strand
		if(starts_b_rot[i]<mutpos && mutpos<starts_b_rot[i]+18){
			mutstrand = i;
			rotateto = i+TotalStrands/4;
		}
	}
	for( int i = TotalStrands; i < 3*TotalStrands; i++ ){
		starts_a_rot[i] = starts_a_rot[i-TotalStrands];
		starts_b_rot[i] = starts_b_rot[i-TotalStrands];
		starts_c_rot[i] = starts_c_rot[i-TotalStrands];
		direction_a_rot[i] = direction_a_rot[i-TotalStrands];
		direction_b_rot[i] = direction_b_rot[i-TotalStrands];
		direction_c_rot[i] = direction_c_rot[i-TotalStrands];
	}
	fin.close();

	#ifndef INTERFACE_ROTATION
	rotateto = 0;
	#endif


	int *starts_a = &starts_a_rot[rotateto];
	int *starts_b = &starts_b_rot[rotateto];
	int *starts_c = &starts_c_rot[rotateto];
	int *direction_a = &direction_a_rot[rotateto];
	int *direction_b = &direction_b_rot[rotateto];
	int *direction_c = &direction_c_rot[rotateto];


	fname = oddsfolder+"all.self";
	load_odds(fname, self0, -1.4);
	fname = oddsfolder + "strong.my.odds";
	load_odds(fname, strong0, -3);
	fname = oddsfolder + "vdw.my.odds";
	load_odds(fname, vdw0, -3);
	fname = oddsfolder + "weak.my.odds";
	load_odds(fname, weak0, -1.72);
	fname = oddsfolder+"all.jernigan";
	load_odds(fname, intra0, nanf(""), false);

	//fname = inputsfolder+"CoreIn.odds";
	//load_odds(fname, CoreIn, -3.91);
	//fname = inputsfolder+"CoreOut.odds";
	//load_odds(fname, CoreOut, -3.91);
	//fname = inputsfolder+"PeriIn.odds";
	//load_odds(fname, PeriIn, -3.91);
	//fname = inputsfolder+"PeriOut.odds";
	//load_odds(fname, PeriOut, -3.91);
	//fname = inputsfolder+"ExtraIn.odds";
	//load_odds(fname, ExtraIn, -3.91);
	//fname = inputsfolder+"ExtraOut.odds";
	//load_odds(fname, ExtraOut, -3.91);
	
	FILE *STATE;
	string temp = path+"/summary/" + protein+"_"+mutationposition+"_"+selfweight+"."+shweight+"."+nhweight+"."+whweight+"."+intraweight+".energy" + testtag;
	STATE =fopen(temp.c_str(),"a");	
	if(STATE==NULL) {
		cerr << "error opening file" << temp <<endl;
		exit(1);
	}

    for (int i =0; i< 160; i++) { 
		self[i] =(float) (-Kb*T*wself*self0[i]);
	}
	self[200] = 0;
	self[220] = 0;
	self[240] = 0;
	self[260] = 0;
	self[280] = 0;
	self[300] = 0;
	self[320] = 0;
	self[340] = 0;

    for (int i=0; i<210; i++) {
		strong[i] = (float)(-Kb*T*wsh*strong0[i]);
		vdw[i] = (float)(-Kb*T*wnh*vdw0[i]);
		weak[i] =(float)(-Kb*T*wwh*weak0[i]);
		intra[i] = (float)(-Kb*T*wintra*intra0[i]);
	} 
	   
	strong[210]=0;
	vdw[210]=0;
	weak[210]=0;
	intra[210]=0;

	semin = (float *)malloc(sizeof(float) * TotalStrands);
	semax = (float *)malloc(sizeof(float) * TotalStrands); 
	se = (float ***)malloc(sizeof(float **) * TotalStrands); 
	sel = (float ***)malloc(sizeof(float **) * TotalStrands); 
	ser = (float ***)malloc(sizeof(float **) * TotalStrands); 
	sb = (float **)malloc(sizeof(float *) * TotalStrands); 
	float deltaG[20];

	fprintf(STATE, "## Position : %d\n", mutpos);
	fprintf(STATE, "## Precision : %d\n", PRECISION);
	fprintf(STATE, "## Method : %s\n", TotalStrands>APPCRIT ? "approx" : "exact");

 	int hb = window/2;
	int lb = -hb;

	clock_t startclock = clock();
	for( int mut = 0; mut < 20; mut++){

		for (int i = 0 ;  i < TotalStrands; i++) {
			semin[i] = 1000;
			semax[i] = -1000;
			se[i] = (float **)malloc(sizeof(float *) * window);
			sel[i] = (float **)malloc(sizeof(float *) * window);
			ser[i] = (float **)malloc(sizeof(float *) * window);
			sb[i] = (float *)malloc(sizeof(float) * window);
			for (int j = 0; j < window; j++){
				se[i][j] = (float *)calloc( window*window, sizeof(float) );
				sel[i][j] = (float *)calloc( window*window, sizeof(float) );
				ser[i][j] = (float *)calloc( window*window, sizeof(float) );
			}
		}

		int strand_a[18];
		int strand_b[18];
		int strand_c[18];

		for(int l = 0; l<TotalStrands; l++) {
			int a= starts_a[l];
			int b= starts_b[l];
			int c= starts_c[l];
    
			for(int i = 0 ; i < 18 ; i++) {
				strand_a[i] = i+a>nuRes ? 200 : res[i+a];
				strand_b[i] = i+b>nuRes ? 200 : res[i+b];
				strand_c[i] = i+c>nuRes ? 200 : res[i+c];
			}
    
			if (direction_a[l] ==2) {reverse(strand_a);}
			if (direction_b[l] ==2) {reverse(strand_b);}
			if (direction_c[l] ==2) {reverse(strand_c);}
    
			//int facing_a = facing(strand_a,1);
			int facing_b = facing(strand_b,2);
			//int facing_c = facing(strand_c,3);
    
			/*
			// paralell triplet
			if (((facing_a+facing_b) != 3) || ((facing_c+facing_b) !=3)) {
				if (facing_a == facing_c) { facing_b=3-facing_a; }
				else if ((facing_a+facing_b)==3) { facing_c=facing_a; }
				else { facing_a=facing_c; }
			}
			*/
				
			res[mutpos] = mut;        //mutation is happening here!!!!
                     
			for(int m = lb ; m <= hb ; m++) {   // Side chain orientation pattern of non-cannonical conformation
				if ( m%2 !=0 ){
					facing_b = 3-facing_b;
				}
				for(int j = lb ; j <= hb ; j++) {
					for(int k = lb ; k <= hb ; k++) {
						for(int i = 0 ; i < 18 ; i++) {
							if( j+i+a<1 || j+i+a>nuRes ) { strand_a[i] = 200; }
							else { strand_a[i] = res[j+i+a]; }
							if( m+i+b<1 || m+i+b>nuRes ) { strand_b[i] = 200; }
							else { strand_b[i] = res[i+b+m]; }
							if( k+i+c<1 || k+i+c>nuRes ) { strand_c[i] = 200; }
							else { strand_c[i] = res[k+i+c]; } 
						}
						if (direction_a[l]==2) {reverse(strand_a);}
						if (direction_b[l]==2) {reverse(strand_b);}
						if (direction_c[l]==2) {reverse(strand_c);}
    
						//double aself=singlebody(strand_a,1,3-facing_b);
						float bself=singlebody(strand_b,2,facing_b);
						//double cself=singlebody(strand_c,3,3-facing_b);
						float pinterab = pairwise_ab(strand_a,strand_b,direction_a[l],direction_b[l],facing_b);
						float pinterbc = pairwise_bc(strand_b,strand_c,direction_b[l],direction_c[l],facing_b);
						float pintra = intrastrand (strand_b,2);
    
						// interab interbc double counted
						float total = bself+pinterab+pinterbc+pintra;
    
						sb[l][m+hb] = bself+pintra;
						se[l][m+hb][((j+hb)*window)+(k+hb)] = total;
						sel[l][m+hb][((j+hb)*window)+(k+hb)] = total+pinterab;
						ser[l][m+hb][((j+hb)*window)+(k+hb)] = total+pinterbc;
						float tmpextreme = max( max(total,total+pinterab), total+pinterbc );
						if(tmpextreme > semax[l]) { semax[l] = tmpextreme; }
						tmpextreme = min( min(total,total+pinterab), total+pinterbc );
						if(tmpextreme < semin[l]) { semin[l] = tmpextreme; }
					}
				}
			}
		}
		printf ("Triplets calculation done!\t%s:%d\tmut:%d\n",protein.c_str(), mutpos, mut);
		
		int statenum;
		if(TotalStrands > APPCRIT){
			deltaG[mut] = energy_app(TotalStrands, statenum);
		}
		else{
			deltaG[mut] = energy_exct(TotalStrands, statenum);
		}
		printf ("%s\tpos:%d\tmut:%d\tdG%f\n",protein.c_str(), mutpos, mut,  deltaG[mut]);

	}//for mut
	clock_t endclock = clock();
	fprintf(STATE, "## Time : %f\n", ((double) (endclock - startclock)) / CLOCKS_PER_SEC );
	for( int mut = 0; mut < 20; mut++){
		fprintf (STATE,"%d\t%.4f\t%.4f\n",mut,deltaG[mut],deltaG[mut]-deltaG[0]);
	}

} //for main


void reverse(int *strand) {
	int tmp;
	for(int i=0; i<18/2; i++) {
		tmp = strand[i];
		strand[i] = strand[17-i];
		strand[17-i] = tmp;
	}
}

int facing(int *strand, int t) {
	//Strand 1: TM residues 4-12: Extra HeadGroup:4,5; Core:6-10; Peri HeadGroup:11,12
	//Strand 2: TM residues 5-13: Extra HeadGroup:5,6; Core:7-11; Peri HeadGroup:12,13
	//Strand 3: TM residues 6-14: Extra HeadGroup:6,7; Core:8-12; Peri HeadGroup:13,14
	double SumOdd = ExtraIn[strand[3+t]]+ExtraOut[strand[4+t]]+
		CoreIn[strand[5+t]]+CoreOut[strand[6+t]]+CoreIn[strand[7+t]]+CoreOut[strand[8+t]]+CoreIn[strand[9+t]]+
		PeriOut[strand[10+t]]+PeriIn[strand[11+t]];
	double SumEven = ExtraOut[strand[3+t]]+ExtraIn[strand[4+t]]+
		CoreOut[strand[5+t]]+CoreIn[strand[6+t]]+CoreOut[strand[7+t]]+CoreIn[strand[8+t]]+CoreOut[strand[9+t]]+
		PeriIn[strand[10+t]]+PeriOut[strand[11+t]];
	if (SumOdd > SumEven) { return 1; }
	else { return 2; }
}

float singlebody (int *strand,int index, int face) {
	float single=0;
	int zz=index-1;
	//Strand 1: residues 0-15: Extra Cap:0-3; Extra HeadGroup:4,5; Core:6-10; Peri HeadGroup:11,12; Peri Cap:13-15
	//Strand 2: TM residues 1-16: Extra Cap:1-4; Extra HeadGroup:5,6; Core:7-11; Peri HeadGroup:12,13 Peri Cap:14-16
	//Strand 3: TM residues 2-17: Extra Cap:2-5; Extra HeadGroup:6,7; Core:8-12; Peri HeadGroup:13,14 Peri Cap:15-17
	if (face == 1) {
		single=self[strand[0+zz]+140]+self[strand[1+zz]+140]+self[strand[2+zz]+140]+self[strand[3+zz]+140]+
			self[strand[4+zz]+100]+self[strand[5+zz]+120]+
			self[strand[6+zz]+60] +self[strand[7+zz]+80] +self[strand[8+zz]+60] +self[strand[9+zz]+80] +self[strand[10+zz]+60]+
			self[strand[11+zz]+40]+self[strand[12+zz]+20]+
			self[strand[13+zz]+0] +self[strand[14+zz]+0] +self[strand[15+zz]+0];
	if((self0[strand[3 +zz]+140] < -1) && (self0[strand[3 +zz]+120] > 0)) { single += (self[strand[3 +zz]+120]-self[strand[3 +zz]+140]); }
	if((self0[strand[4 +zz]+100] < -1) && (self0[strand[4 +zz]+140] > 0)) { single += (self[strand[4 +zz]+140]-self[strand[4 +zz]+100]); }
	if((self0[strand[5 +zz]+120] < -1) && (self0[strand[5 +zz]+80 ] > 0)) { single += (self[strand[5 +zz]+80 ]-self[strand[5 +zz]+120]); }
	if((self0[strand[6 +zz]+60 ] < -1) && (self0[strand[6 +zz]+100] > 0)) { single += (self[strand[6 +zz]+100]-self[strand[6 +zz]+60]); }
	if((self0[strand[10+zz]+60 ] < -1) && (self0[strand[10+zz]+20 ] > 0)) { single += (self[strand[10+zz]+20 ]-self[strand[10+zz]+60]); }
	if((self0[strand[11+zz]+40 ] < -1) && (self0[strand[11+zz]+80 ] > 0)) { single += (self[strand[11+zz]+40 ]-self[strand[11+zz]+80]); }
	if((self0[strand[12+zz]+20 ] < -1) && (self0[strand[12+zz]+ 0 ] > 0)) { single += (self[strand[12+zz]+0  ]-self[strand[12+zz]+20]); }
	if((self0[strand[13+zz]+0  ] < -1) && (self0[strand[13+zz]+40 ] > 0)) { single += (self[strand[13+zz]+40 ]-self[strand[11+zz]+0]); }
	}
	else {  
		single=self[strand[0+zz]+140]+self[strand[1+zz]+140]+self[strand[2+zz]+140]+self[strand[3+zz]+140]+
			self[strand[4+zz]+120]+self[strand[5+zz]+100]+
			self[strand[6+zz]+80] +self[strand[7+zz]+60] +self[strand[8+zz]+80] +self[strand[9+zz]+60] +self[strand[10+zz]+80]+
			self[strand[11+zz]+20]+self[strand[12+zz]+40]+
			self[strand[13+zz]+0] +self[strand[14+zz]+0] +self[strand[15+zz]+0];
	if((self0[strand[3 +zz]+140] < -1) && (self0[strand[3 +zz]+100] > 0)) { single += (self[strand[3 +zz]+100]-self[strand[3 +zz]+140]);}
	if((self0[strand[4 +zz]+120] < -1) && (self0[strand[4 +zz]+140] > 0)) { single += (self[strand[4 +zz]+140]-self[strand[4 +zz]+120]);}
	if((self0[strand[5 +zz]+100] < -1) && (self0[strand[5 +zz]+60 ] > 0)) { single += (self[strand[5 +zz]+60 ]-self[strand[5 +zz]+100]);}
	if((self0[strand[6 +zz]+80 ] < -1) && (self0[strand[6 +zz]+120] > 0)) { single += (self[strand[6 +zz]+120]-self[strand[6 +zz]+80]); }
	if((self0[strand[10+zz]+80 ] < -1) && (self0[strand[10+zz]+40 ] > 0)) { single += (self[strand[10+zz]+40 ]-self[strand[10+zz]+80]);}
	if((self0[strand[11+zz]+20 ] < -1) && (self0[strand[11+zz]+60 ] > 0)) { single += (self[strand[11+zz]+60 ]-self[strand[11+zz]+20]);}
	if((self0[strand[12+zz]+40 ] < -1) && (self0[strand[12+zz]+ 0 ] > 0)) { single += (self[strand[12+zz]+0  ]-self[strand[12+zz]+40]);}
	if((self0[strand[13+zz]+0  ] < -1) && (self0[strand[13+zz]+20 ] > 0)) { single += (self[strand[13+zz]+20 ]-self[strand[11+zz]+0]);}
	}
	return single; 
} 


float pairwise_ab(int *strand_a,int *strand_b,int dir1,int dir2,int face) {
	float pstrong =0;
	float pvdw=0;
	float pweak=0;
	if ( dir1==1 && dir2==2 ) {
		if (face==2) {
			for (int i=5;i<=13;i++) {
				pweak += weak[aa_pair(strand_a[i-1],strand_b[i])];
			}
			for (int i=5;i<12;i+=2) {
				pstrong += strong[aa_pair(strand_a[i],strand_b[i])];
				pvdw +=vdw[aa_pair(strand_a[i+1],strand_b[i+1])];
			}
		}
		if (face==1) {
			for (int i=5;i<=13;i++) {
				pweak += weak[aa_pair(strand_a[i-1],strand_b[i])];
			}
			for (int i=5;i<12;i+=2) {
				pstrong +=strong[aa_pair(strand_a[i+1],strand_b[i+1])];
				pvdw += vdw[aa_pair(strand_a[i],strand_b[i])];
			}
		}
	}
	else if ( dir1==2 && dir2==1 ) {
		if (face==2) {
			for (int i=5;i<12;i++) {
				pweak += weak[aa_pair(strand_a[i+1],strand_b[i])];
			 }
			for (int i=5;i<12;i+=2) {
				pvdw += vdw[aa_pair(strand_a[i],strand_b[i])];
				pstrong +=strong[aa_pair(strand_a[i+1],strand_b[i+1])];
			}
		}
		if (face==1) {
			for (int i=5;i<12;i++) {
				pweak += weak[aa_pair(strand_a[i+1],strand_b[i])];
			} 
			for (int i=5;i<12;i+=2) {
				pvdw +=vdw[aa_pair(strand_a[i+1],strand_b[i+1])];
				pstrong += strong[aa_pair(strand_a[i],strand_b[i])];
			}
		}
	}
	//else if ( dir1==1 && dir2==1 ) { }
	//else if ( dir1==2 && dir2==2 ) { }
	return pstrong+pvdw+pweak;;
}

float pairwise_bc(int *strand_b,int *strand_c,int dir2,int dir3,int face) {
	float pstrong =0;
	float pvdw =0;
	float pweak =0;
	if ((dir2==1)&&(dir3==2)) {
		if (face==2) {
			for (int i=5;i<=13;i++) {
				pweak += weak[aa_pair(strand_b[i],strand_c[i+1])];
			}
			for (int i=6;i<=13;i+=2) {
				pvdw+=vdw[aa_pair(strand_b[i],strand_c[i])];
				pstrong += strong[aa_pair(strand_b[i+1],strand_c[i+1])];
			}
		}
		if (face==1) {
			for (int i=5;i<=13;i++) {
				pweak += weak[aa_pair(strand_b[i],strand_c[i+1])];
			}
			for (int i=6;i<=12;i+=2) {
				pstrong+=strong[aa_pair(strand_b[i],strand_c[i])];
				pvdw += vdw[aa_pair(strand_b[i+1],strand_c[i+1])];
			}
		}
	}
	if ((dir2==2)&&(dir3==1)) {
		if (face==2) {
			for (int i=7;i<=13;i++) {
				pweak += weak[aa_pair(strand_b[i],strand_c[i-1])];
			}
			for (int i=6;i<=12;i+=2) {
				pstrong+=strong[aa_pair(strand_b[i],strand_c[i])];
				pvdw += vdw[aa_pair(strand_b[i+1],strand_c[i+1])];
			}
		}
		if (face==1) {
			for (int i=7;i<=13;i++) {
				pweak +=weak[aa_pair(strand_b[i],strand_c[i-1])];
			}
			for (int i=6;i<=13;i+=2) {
				pvdw+=vdw[aa_pair(strand_b[i],strand_c[i])];
				pstrong += strong[aa_pair(strand_b[i+1],strand_c[i+1])];
			}
		}
	}
	//if ((dir2==1)&&(dir3==1)) { }
	//if ((dir2==2)&&(dir3==2)) { }
	return pstrong+pvdw+pweak;;
}


float intrastrand (int *strand,int index) {
	float sum =0;
	for (int i=3+index; i<10+index; i+=2) {
		sum += intra[aa_pair(strand[i],strand[i+2])];
	}
	return sum;
}

char *aa_char(int aa) {   
	switch(aa){
		case  0 : return "a";
		case  1 : return "r";
		case  2 : return "n";
		case  3 : return "d";
		case  4 : return "c";
		case  5 : return "q";
		case  6 : return "e";
		case  7 : return "g";
		case  8 : return "h";
		case  9 : return "i";
		case 10 : return "l";
		case 11 : return "k";
		case 12 : return "m";
		case 13 : return "f";
		case 14 : return "p";
		case 15 : return "s";
		case 16 : return "t";
		case 17 : return "w";
		case 18 : return "y";
		case 19 : return "v";
		default : return "x";
	}
}

int aa_pair(int aa1, int aa2) {
	if ( aa1==200 || aa2==200 ) { return 210; }
	else if(aa1 <= aa2) { return (aa1 * 20 + aa2 - (aa1*(aa1+1)/2)); }
	else { return (aa2 * 20 + aa1 - (aa2*(aa2+1)/2)); }
}

void load_odds(string& fname, float* arr, float val, bool needlog){
	ifstream fin(fname.c_str());
	if(fin.fail()) {
		cerr << "error opening file" << fname <<endl;
		exit(1);
	}
	string line;
	float tmpodds;
	int i = 0;
	while( getline(fin,line) ){
		tmpodds = atof(line.c_str());
		if(tmpodds == 0) { arr[i] =val; }
		else { arr[i] = needlog ? (float)log(tmpodds) : tmpodds; }
		i++;
	}
	fin.close();
}

float energy_exct(int TotalStrands, int& statenum){
 	int hb = window/2;
	int lb = -hb;
	float prmin=0;
	float prmax=0;
	for (int i=0; i<TotalStrands; i++) {
		prmin += semin[i];
		prmax += semax[i];
	}
	int emin = prmin<0 ? (int)(-prmin+1) : 1;
	int emax = prmax>0 ? (int)(prmax+1) : 1;
	//cout<<"states size"<<" "<<emax<<" "<<emin<<endl;
	vector<vector<int> > states_pos(emax, vector<int>(PRECISION,0));
	vector<vector<int> > states_neg(emin, vector<int>(PRECISION,0));

	int first = 0,second = 0;
	int *var = new int[TotalStrands];
	for(int i=0; i<TotalStrands; i++) {
		var[i] = lb;
	}

	int left = 0, right = 0;
	int gcount =0;
	while(var[TotalStrands-1]<hb+1) {
		float pk=0;
		for(int i=0; i<TotalStrands; i++) {
			if(i==0) {
				left = var[TotalStrands-1];
				right = var[i+1];
			}
			else if(i==TotalStrands-1) {
				left = var[i-1];
				right = var[0];
			}
			else {
				left = var[i-1];
				right = var[i+1];
			}
			pk += se[i][var[i]+hb][((left+hb)*window)+right+hb];
		}
		gcount++;
		pk = round(pk*PRECISION)/PRECISION;
		if(pk >= 0) {
			first = (int)pk;
			second = (int)((pk-first)*PRECISION);
			states_pos[first][second]++;
		}	
		else {
			pk = abs(pk);
			first = (int)pk;
			second = (int)((pk-first)*PRECISION);
			states_neg[first][second]++;
		}
		
		int j=0;
		var[j]++;
		while(var[j]>hb) {
			if(j == TotalStrands-1) {
				break;
			}
			var[j]=lb;
			j++;
			var[j]++;
		}
	}

	double factor =0;
	for(int i=0; i<emax; i++) {
		for(int j=0; j<PRECISION; j++) {
			if(states_pos[i][j]>0) { factor += states_pos[i][j]*exp( -((float)i+(float)j/PRECISION)/Kb/T ); }
		}
	}
	for(int i=0; i<emin; i++) {
		for(int j=0; j<PRECISION; j++) {
			if(states_neg[i][j]>0) { factor += states_neg[i][j]*exp( ((float)i+(float)j/PRECISION)/Kb/T ); }
		}
	}
 
	delete[] var;
	statenum = gcount;
	return (float)(-Kb*T*log(factor));
}

float energy_app(int TotalStrands, int& statenum){
 	int hb = window/2;
	int lb = -hb;

	float prmin_l=0;
	float prmax_l=0;
	for (int i=1; i<TotalStrands/2; i++) {
		prmin_l += semin[i];
		prmax_l += semax[i];
	}
	int emin_l = prmin_l<0 ? (int)(-prmin_l+1) : 1;
	int emax_l = prmax_l>0 ? (int)(prmax_l+1) : 1;

	float prmin_r=0;
	float prmax_r=0;
	for (int i=TotalStrands/2+1; i<TotalStrands; i++) {
		prmin_r += semin[i];
		prmax_r += semax[i];
	}
	int emin_r = prmin_r<0 ? (int)(-prmin_r+1) : 1;
	int emax_r = prmax_r>0 ? (int)(prmax_r+1) : 1;

	float prmin = prmin_r+prmin_l+semin[0]+semin[TotalStrands/2];
	float prmax = prmax_r+prmax_l+semax[0]+semax[TotalStrands/2];
	/*
	float prmin = 0;
	float prmax = 0;
	for (int i=0; i<TotalStrands; i++) {
		prmin += semin[i];
		prmax += semax[i];
	}
	*/
	int emin = prmin<0 ? (int)(-prmin+1) : 1;
	int emax = prmax>0 ? (int)(prmax+1) : 1;

	vector<vector<int> > states_pos(emax, vector<int>(PRECISION,0));
	vector<vector<int> > states_neg(emin, vector<int>(PRECISION,0));
	vector<vector<int> > states_l_pos;
	vector<vector<int> > states_l_neg;
	vector<vector<int> > states_r_pos;
	vector<vector<int> > states_r_neg;

	int first = 0,second = 0;
	int *var = new int[TotalStrands];

	int left = 0, right = 0;
	for (int ori=lb;ori<=hb;ori++) {
		for (int mid=lb;mid<=hb;mid++) {

			states_l_pos = vector< vector<int> >(emax_l, vector<int>(PRECISION,0));
			states_l_neg = vector< vector<int> >(emin_l, vector<int>(PRECISION,0));
			states_r_pos = vector< vector<int> >(emax_r, vector<int>(PRECISION,0));
			states_r_neg = vector< vector<int> >(emin_r, vector<int>(PRECISION,0));

			for(int i=0; i<TotalStrands; i++) {
				var[i] = lb;
			}
			var[0]=ori;
			var[TotalStrands/2]=mid;

			while(var[TotalStrands/2-1]<hb+1) {
				float pk=0;
				for(int i=1; i<(TotalStrands/2); i++) {
					left = var[i-1];//+((-1)*$var[$i]);
					right = var[i+1];//+((-1)*$var[$i]);
					if (i==1) {pk += sel[i][var[i]+hb][((left+hb)*window)+right+hb];}
					else if (i==TotalStrands/2-1) {pk += ser[i][var[i]+hb][((left+hb)*window)+right+hb];}
					else {pk += se[i][var[i]+hb][((left+hb)*window)+right+hb];}
				}
				pk = round(pk*PRECISION)/PRECISION;
				if(pk >= 0) {
					first = (int)pk;
					second = (int)((pk-(int)pk)*PRECISION);
					states_l_pos[first][second]++;
				}
				else {
					pk = abs(pk);
					first = int(pk);
					second = (int)((pk-(int)pk)*PRECISION);
					states_l_neg[first][second]++;
				}
				int j=1;
				var[j]++;
				while(var[j]>hb) {
					if(j == (TotalStrands/2)-1) {
						j=1;
						break;
					}
					var[j] = lb;
					j++;
					var[j]++;
				}
			}  // permutation within the first mid of barrel
			while(var[TotalStrands-1]<hb+1) {
				float pk=0;
				for(int i=TotalStrands/2+1; i<TotalStrands; i++) {
					left = var[i-1];
					if (i == TotalStrands-1) {right=var[0];}
					else {right = var[i+1];}
					if (i==TotalStrands/2+1) {pk += sel[i][var[i]+hb][((left+hb)*window)+right+hb];}
					else if (i==(TotalStrands-1)) {pk += ser[i][var[i]+hb][((left+hb)*window)+right+hb];}
					else {pk += se[i][var[i]+hb][((left+hb)*window)+right+hb];}
				}
				pk = round(pk*PRECISION)/PRECISION;
				if(pk >= 0) {
					first = (int)pk;
					second = (int)((pk-(int)pk)*PRECISION);
					states_r_pos[first][second]++;
				}
				else {
					pk = abs(pk);
					first = int(pk);
					second = (int)((pk-(int)pk)*PRECISION);
					states_r_neg[first][second]++;
				}
				int j=((TotalStrands/2)+1);
				var[j]++;
				while(var[j]>hb) {
					if(j == TotalStrands-1) {
						j=((TotalStrands/2)+1);
						break;
					}
					var[j] = lb;
					j++;
					var[j]++;
				}
			}  // permutation within the second mid of barrel

			///////combine
			float val_l, val_r, pk;
			for(int i=0; i<emax_l; i++){//states_l_pos
				for(int j=0; j<PRECISION; j++){
					if(states_l_pos[i][j]>0){
						val_l = i+(float)j/PRECISION;
						for(int k=0; k<emax_r; k++){//states_r_pos
							for(int m=0; m<PRECISION; m++){
								if(states_r_pos[k][m]>0){
									val_r = k+(float)m/PRECISION;
									pk = val_l + val_r + sb[0][ori+hb] + sb[TotalStrands/2][mid+hb];
									pk = round(pk*PRECISION)/PRECISION;
									if(pk>0){
										first = (int)pk;
										second = (int)((pk-first)*PRECISION);
										states_pos[first][second]+=states_l_pos[i][j]*states_r_pos[k][m];
									}
									else{
										pk = abs(pk);
										first = (int)pk;
										second = (int)((pk-first)*PRECISION);
										states_neg[first][second]+=states_l_pos[i][j]*states_r_pos[k][m];
									}
								}
							}
						}//states_r_pos
						for(int k=0; k<emin_r; k++){//states_r_neg
							for(int m=0; m<PRECISION; m++){
								if(states_r_neg[k][m]>0){
									val_r = -k-(float)m/PRECISION;
									pk = val_l + val_r + sb[0][ori+hb] + sb[TotalStrands/2][mid+hb];
									pk = round(pk*PRECISION)/PRECISION;
									if(pk>0){
										first = (int)pk;
										second = (int)((pk-first)*PRECISION);
										states_pos[first][second]+=states_l_pos[i][j]*states_r_neg[k][m];
									}
									else{
										pk = abs(pk);
										first = (int)pk;
										second = (int)((pk-first)*PRECISION);
										states_neg[first][second]+=states_l_pos[i][j]*states_r_neg[k][m];
									}
								}
							}
						}//states_r_neg
					}
				}
			}//states_l_pos
			for(int i=0; i<emin_l; i++){//states_l_neg
				for(int j=0; j<PRECISION; j++){
					if(states_l_neg[i][j]>0){
						val_l = -i-(float)j/PRECISION;
						for(int k=0; k<emax_r; k++){//states_r_pos
							for(int m=0; m<PRECISION; m++){
								if(states_r_pos[k][m]>0){
									val_r = k+(float)m/PRECISION;
									pk = val_l + val_r + sb[0][ori+hb] + sb[TotalStrands/2][mid+hb];
									pk = round(pk*PRECISION)/PRECISION;
									if(pk>0){
										first = (int)pk;
										second = (int)((pk-first)*PRECISION);
										states_pos[first][second]+=states_l_neg[i][j]*states_r_pos[k][m];
									}
									else{
										pk = abs(pk);
										first = (int)pk;
										second = (int)((pk-first)*PRECISION);
										states_neg[first][second]+=states_l_neg[i][j]*states_r_pos[k][m];
									}
								}
							}
						}//states_r_pos
						for(int k=0; k<emin_r; k++){//states_r_neg
							for(int m=0; m<PRECISION; m++){
								if(states_r_neg[k][m]>0){
									val_r = -k-(float)m/PRECISION;
									pk = val_l + val_r + sb[0][ori+hb] + sb[TotalStrands/2][mid+hb];
									pk = round(pk*PRECISION)/PRECISION;
									if(pk>0){
										first = (int)pk;
										second = (int)((pk-first)*PRECISION);
										states_pos[first][second]+=states_l_neg[i][j]*states_r_neg[k][m];
									}
									else{
										pk = abs(pk);
										first = (int)pk;
										second = (int)((pk-first)*PRECISION);
										states_neg[first][second]+=states_l_neg[i][j]*states_r_neg[k][m];
									}
								}
							}
						}//states_r_neg
					}
				}
			}//states_l_neg

		} // end loop for middle strand
	} //end loop for first strand


	int gcount = 0;
	double factor =0;
	for(int i=0; i<emax; i++) {
		for(int j=0; j<PRECISION; j++) {
			if(states_pos[i][j]>0) {
				gcount+=states_pos[i][j];
				factor += states_pos[i][j]*exp( -((float)i+(float)j/PRECISION)/Kb/T );
			}
		}
	}
	for(int i=0; i<emin; i++) {
		for(int j=0; j<PRECISION; j++) {
			if(states_neg[i][j]>0) {
				gcount+=states_neg[i][j];
				factor += states_neg[i][j]*exp( ((float)i+(float)j/PRECISION)/Kb/T );
			}
		}
	}
	delete[] var;
	statenum = gcount;
	return (float)(-Kb*T*log(factor));
}
