
all:
	#g++ code/freeenergy.cpp -o freeenergy.out -O2
	#g++ code/hy.cpp -o hy.out -O2
	#g++ code/freeenergy_lowprecision.cpp -o freeenergy_lp.out -O2
	#g++ code/hy_lowprecision.cpp -o hy_lp.out -O2
	#g++ code/corrected_approx_hyall.cpp -o hyall_p3.out -O2 -D P3
	#g++ code/corrected_approx_hyall.cpp -o hyall_p2.out -O2 -D P2
	#g++ code/corrected_approx_hyall.cpp -o hyall_p1.out -O2 -D P1
	#g++ code/corrected_approx_hyall.cpp -o hyall_p0.out -O2 -D P0

	#g++ code/corrected_approx_hyall.cpp -o hyall_p3_aa_ir.out -O2 -D P3 -D ALLAPP -D INTERFACE_ROTATION
	#g++ code/corrected_approx_hyall.cpp -o hyall_p3_aa.out -O2 -D P3 -D ALLAPP
	g++ code/corrected_approx_hyall.cpp -o hyall_p4.out -O2 -D P4
	g++ code/corrected_approx_hyall.cpp -o hyall_p3.out -O2 -D P3
	#g++ code/corrected_approx_hyall.cpp -o hyall_p2.out -O2 -D P2
	#g++ code/corrected_approx_hyall.cpp -o hyall_p1.out -O2 -D P1
	#g++ code/corrected_approx_hyall.cpp -o hyall_p0.out -O2 -D P0

	#g++ code/corrected_approx_hyall.cpp -o hyall_p2_aa.out -O2 -D P2 -D ALLAPP
	#g++ code/corrected_approx_hyall.cpp -o hyall_p3_ir.out -O2 -D P3 -D INTERFACE_ROTATION
	g++ code/corrected_approx_hyall.cpp -o hyall_p2_ir.out -O2 -D P2 -D INTERFACE_ROTATION
	#g++ code/corrected_approx_hyall.cpp -o hyall_p1_ir.out -O2 -D P1 -D INTERFACE_ROTATION
