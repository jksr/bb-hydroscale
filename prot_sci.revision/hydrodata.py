## process the TFE data and store them into HD files

import sys
sys.dont_write_bytecode = True

import os
import Bio
import Bio.PDB
import warnings
warnings.simplefilter('ignore', Bio.BiopythonWarning)
import numpy as np
import pickle

sys.path.append('../../../pylib')
import twbio.AminoAcid


pathprefix = '../'

pdb_info_dict = {}

class Hydrodata:
	# Hydrodata.records[seqid][keys,...]
	keys = sorted(['zcoord','depidx','resname','profile'])

	def __init__(self, code):
		self.code = code
		self.chain_id = pdb_info_dict[code][0]
		self.subunit_num = int(pdb_info_dict[code][1])
		self.barrel_num = int(pdb_info_dict[code][2])
		self.subunit_strand_num = int(pdb_info_dict[code][3])
		self.is_toxin = int(pdb_info_dict[code][4])
		self.strand_num = self.subunit_num * self.subunit_strand_num / self.barrel_num
		self.membrane_thickness = 0
		self.outfacing_list = []
		self.records = {}
		self.depth_index_dict = {}
		self.strands = []



		# membrane thickness
		with open(pathprefix+'../inputs/'+code+'/'+code+'.pdb') as pdbf:
			self.membrane_thickness = 2*abs( float(pdbf.readlines()[-1].split()[-1]) )

		# list of outfacing residues

		with open(pathprefix+code+'/'+code+'.out.list') as outlistf:
			self.outfacing_list = outlistf.readlines()
		for i in range(len(self.outfacing_list)):
			self.outfacing_list[i] = int(self.outfacing_list[i])
			self.records[self.outfacing_list[i]] = {}

		# get depthindex dict {idx:[res1,res2,...]}
		for i in range(-4,5):
			self.depth_index_dict[i]=[]
		with open(pathprefix+'../inputs/'+code+'/'+code+'.cen.topo') as centopof:
				centopolines = centopof.readlines()
		p2e = 1 # direction from periplasm to extracellular
		for line in centopolines:
			splt = line.split()
			mid = int(splt[0])
			if splt[1] == '2':
				tmpstrd = []
				for d in [4,2,0,-2,-4]:
					if self.code == '2wjr' and mid +d* p2e == 1:
						continue
					self.depth_index_dict[d].append(mid +d* p2e)
					tmpstrd.insert(0, mid +d* p2e)
				self.strands.append(tmpstrd)
			else:
				tmpstrd = []
				for d in [3,1,-1,-3]:
					if self.code == '2wjr' and mid +d* p2e == 1:
						continue
					self.depth_index_dict[d].append(mid +d* p2e)
					tmpstrd.insert(0, mid +d* p2e)
				self.strands.append(tmpstrd)
			p2e = -p2e

		## fill the records

		# get dict of res:coord
		pdb_chain = Bio.PDB.PDBParser().get_structure('MyStruct', pathprefix+'../inputs/'+code+'/'+code+'.pdb')[0][self.chain_id]
		pdb_res_dict = { res.get_id()[1] : res for res in pdb_chain if res.child_dict.has_key('CA') }
		for i in range(len(self.outfacing_list)):
			if self.code == '2wjr' and self.outfacing_list[i] == 1:
				continue
			if self.code == '4c4v' and self.outfacing_list[i] == 810:
				continue
			# get outfacing zcoord
			self.records[self.outfacing_list[i]]['zcoord'] = pdb_res_dict[self.outfacing_list[i]] ['CA'].get_coord() [2]
			# get outfacing resname
			self.records[self.outfacing_list[i]]['resname'] = twbio.AminoAcid.three_to_one( pdb_res_dict[self.outfacing_list[i]].get_resname() )

		for depidx in range(-4,5):
			for seqid in self.depth_index_dict[depidx]:
				if self.code == '2wjr' and seqid == 1:
					continue
				self.records[seqid]['depidx'] = depidx

		for i in self.outfacing_list:
			self.records[i]['profile'] = np.loadtxt(pathprefix+code+'/'+code+'_'+str(i)+'_1.0.6.0.6.0.4.0.8.energy')[:,2]


		# pickle the data
		pickled = 'HD/'+code+'_hd.pickle'
		print pickled
		if not os.path.exists(pickled):
			print 'dumppppppppppppppppppppppppppppppppppppppp'
			pickle.dump(self, open(pickled,'wb'))



if __name__ == '__main__':
	if len(sys.argv) == 1:
		with open(pathprefix+'finished.list') as f:
			pdbs = f.readlines()
	elif len(sys.argv) == 2 and os.path.isfile(sys.argv[1]):
			with open(sys.argv[1]) as f:
				pdbs = f.readlines()
	else:
		pdbs = []
		for pdb in sys.argv[1:]:
			pdbs.append(pdb)
	


	with open(pathprefix+'../pdb_info.list') as infof:
		infolist = infof.readlines()
	for line in infolist[1:]:
		splt = line.split()
		pdb_info_dict[splt[0]]=splt[1:]
	

	for pdb in pdbs:
		print pdb.strip()
		data = Hydrodata(pdb.strip())
		#for i in data.outfacing_list:
		#	for key in data.keys:
		#		print key,data.records[i][key]
		#	print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

