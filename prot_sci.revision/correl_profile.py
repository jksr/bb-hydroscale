import numpy as np
import matplotlib.pyplot as plt
import math

ori = np.loadtxt('1.0.6.0.6.0.4.0.8.mean').flatten()
ori = np.loadtxt('../summary/analysis/HD/gram_negative_mean_profile.txt').flatten()
buried = np.loadtxt('1.0.0.0.0.0.0.0.0.mean').flatten()
buried_intra = np.loadtxt('1.0.0.0.0.0.0.0.8.mean').flatten()

ori = np.loadtxt('1.0.6.0.6.0.4.0.8.mean')[:,4]
ori = np.loadtxt('../summary/analysis/HD/gram_negative_mean_profile.txt')[:,4]
buried = np.loadtxt('1.0.0.0.0.0.0.0.0.mean')[:,4]
buried_intra = np.loadtxt('1.0.0.0.0.0.0.0.8.mean')[:,4]



print( math.sqrt(np.sum((ori-buried)**2)/len(ori)) )
plt.scatter(ori, buried)
plt.show()
print( math.sqrt(np.sum((ori-buried_intra)**2)/len(ori)) )
plt.scatter(ori, buried_intra)
plt.show()
