import numpy as np
import matplotlib.pyplot as plt
import sys

means = np.loadtxt(sys.argv[1])
stds = np.loadtxt(sys.argv[2])


fig = plt.figure()
fig.set_size_inches(15,12)
mainax = fig.add_subplot(111) # the big subplot
mainax.set_xlabel('\n'+r'Position index ($i$)', fontsize=18)
mainax.set_ylabel(r'$\mathbf{\Delta\Delta G_{aa(i)}}$(kcal/mol)'+'\n', fontsize=18)
# Turn off axis lines and ticks of the big subplot
mainax.patch.set_visible(False)
mainax.spines['top'].set_visible(False)
mainax.spines['bottom'].set_visible(False)
mainax.spines['left'].set_visible(False)
mainax.spines['right'].set_visible(False)
mainax.set_xticklabels([])
mainax.set_yticklabels([])
 

plotorder = [ 0, 10, 1, 11, 8, 9, 19, 3, 2, 15, 12, 13, 6, 5, 16, 18, 17, 7, 14, 4 ]
for iprox in range(20):
    i = plotorder[iprox]
    resmean = means[:,i]
    resstd = stds[:,i]
    curax = fig.add_subplot(4,5,iprox+1)
    curax.errorbar(range(-4,5), resmean, yerr=resstd, fmt='o', color='#2171b5', ecolor='#2171b5')
    #if err:
    #    curax.errorbar(range(-4,5), resmean, yerr=resstd, fmt='o', color='#2171b5', ecolor='#2171b5')
    #else:
    #    curax.plot(range(-4,5), resmean, 'o')

    # plot outliers
    #for j in range(-4,5):
    #    for k in outdicts[i][j]:
    #        curax.plot([j],[k],'+')
    curax.axhline(0, linestyle='-.', color = '#808080')
    #curax.set_title(twbio.AminoAcid.index_to_three(i).title())
    curax.set_ylim(-3.5,7)
    curax.set_xlim(-4.5,4.5)

    fit_coefs = np.polyfit(range(-4,5), resmean,deg=3)
    fitted = np.polyval(fit_coefs, np.linspace(-4,4,50))
    #if not fitted == None:
    #    curax.plot(range(-4,5),fitted[i],linewidth = 3, color = fit_color)
    curax.plot(np.linspace(-4,4,50),fitted,linewidth = 3, color = '#de2d26')


plt.tight_layout(pad=0.2,w_pad = 0.4, h_pad = 0.4)
#plt.savefig(figname)
#plt.clf()
plt.show()

