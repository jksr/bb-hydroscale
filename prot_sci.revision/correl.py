import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
import seaborn as sb


def get_decoy(fn, intraw = 0.8):
	intraw = str(int(10*intraw))
	decoyfn = fn.replace('summary/', 'summary_buried_only/').replace('1.0.6.0.6.0.4.0.8', '1.0.0.0.0.0.0.0.'+intraw)
	if not os.path.exists( decoyfn ):
		tmp = decoyfn.split('/')
		tmp = tmp[:2]+[tmp[3]]
		decoyfn = '/'.join(tmp)
		if not os.path.exists( decoyfn ):
			return None
	data = np.loadtxt(decoyfn)
	if len(data) != 20:
		return None

	return data[:,-1]


decoys = []
reals = []
for fn in glob.glob('../summary/????/*.energy')[1:]:
	#decoy = get_decoy(fn)
	decoy = get_decoy(fn, 0)
	if decoy is not None:
		real = np.loadtxt(fn)[:,-1]
		decoys += decoy[1:].tolist()
		reals += real[1:].tolist()

sb.scatterplot(reals, decoys)
plt.xlim(-4,10)
plt.ylim(-4,10)
#plt.savefig('buried_and_intra.png')
plt.savefig('buried_only.png')
