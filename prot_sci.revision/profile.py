import glob
import numpy as np
import os

summary = '../summary_buried_only'
summary = '../summary'
weights = '1.0.0.0.0.0.0.0.0'
weights = '1.0.0.0.0.0.0.0.8'
weights = '1.0.6.0.6.0.4.0.8'

todolist = ['1a0s', '1bxw', '1e54', '1ek9', '1fep', '1i78', '1k24', '1kmo', '1nqe', '1p4t', '1prn', '1qd6', '1qj8', '1t16', '1thq', '1tly', '1uyn', '1xkw', '1yc9', '2erv', '2f1c', '2f1t', '2fcp', '2gr8', '2lhf', '2lme', '2mlh', '2mpr', '2o4v', '2omf', '2por', '2qdz', '2vqi', '2wjr', '2ynk', '3aeh', '3bs0', '3csl', '3dwo', '3dzm', '3fid', '3kvn', '3pik', '3rbh', '3rfz', '3syb', '3szv', '3v8x', '3vzt', '4c00', '4e1s', '4gey', '4k3c', '4pr7', '4q35', '7ahl', '3b07', '3o44']


records = dict()
for dep in range(-4,5):
	records[dep] = {}
	for aa in range(20):
		records[dep][aa] = []

for pdb in todolist:
	cens = np.loadtxt('../inputs/{pdb}/{pdb}.cen.topo'.format(pdb=pdb), dtype=int)
	p2e = 1
	for cen, facing in cens:
		if facing == 1:  #in
			dis = [3,1,-1,-3]
		else: #out
			dis = [4,2,0,-2,-4]
		for d in dis:
			seqid = cen+d*p2e
			energyfn = '{summary}/{pdb}_{seqid}_{weights}.energy'.format(summary=summary, pdb=pdb, seqid = str(seqid), weights=weights)
			if not os.path.exists(energyfn):
				energyfn = '{summary}/{pdb}/{pdb}_{seqid}_{weights}.energy'.format(summary=summary, pdb=pdb, seqid = str(seqid), weights=weights)
				if not os.path.exists(energyfn):
					continue
				else:
					tfes = np.loadtxt(energyfn)
			else:
				tfes = np.loadtxt(energyfn)
			if len(tfes)!=20:
				continue
			tfes = tfes[:,-1]
			#print tfes
			for aa in range(20):
				records[d][aa].append(tfes[aa])

means = {}
stds = {}
for dep in range(-4,5):
	means[dep] = zip(range(20), np.zeros(20))
	stds[dep] = zip(range(20), np.zeros(20))

for dep in range(-4,5):
	for aa in range(20):
		means[dep][aa] = np.mean(records[dep][aa])
		#print dep, aa, means[dep][aa]
		stds[dep][aa] = np.std(records[dep][aa])

#for dep in range(-4,5):
#	print dep, means[dep]
means = np.array([ means[dep] for dep in range(-4,5) ])
stds = np.array([ stds[dep] for dep in range(-4,5) ])

np.savetxt(weights+'.mean', means)
np.savetxt(weights+'.std', stds)


#for dep in range(-4,5):
#	print dep, stds[dep]


