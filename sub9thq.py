#!/usr/bin/python
import glob
import os
import sys
import time
import socket

if socket.gethostname() == 'ubeda.bioe.uic.edu':
	subprog = 'ubedarun bash'
elif not socket.gethostname()[-3:] == 'edu':
	subprog = 'bash'
else:
	subprog = 'qsub'


todolist = ['1qd6']
todolist = ['2f1c'] #n=14
todolist = ['2mpr'] #n=18
todolist = ['2o4v'] #n=16
todolist = ['1bxw', '1p4t', '1qj8', '1thq', '2erv', '2f1t', '2lhf', '2mlh', '3dzm', '1i78', '1k24', '1qd6', '1tly', '1uyn', '2wjr', '3aeh', '3fid', '3kvn', '4pr7'] #, '4e1ss'

todolist = ['1thq','9thq']

for todo in todolist:
	todo = todo.strip()
	#resf = open('inputs/'+todo+'/'+todo+'.res')
	#resnum = len(resf.readlines())
	#resf.close()
	#print todo, resnum
	if not os.path.exists('inputs/'+todo):
		continue
	if not os.path.exists('inputs/'+todo+'/'+todo+'.cen.topo'):
		continue

	centopof = open('inputs/'+todo+'/'+todo+'.cen.topo')
	lines = centopof.readlines()
	centopof.close() 
	strandnum = len(open('inputs/'+todo+'/'+todo+'.triplet.con').readlines())
	#if strandnum > 8:
	#if strandnum <= 8 or strandnum > 12:
	#if strandnum <= 12:
	#	print todo
	#	continue

	if strandnum > 12:
		exeprog = './hyall_p2_ir.out'
	else:
		exeprog = './hyall_p3_aa_ir.out'
		exeprog = './hyall_p3_aa.out'
		exeprog = './hyall_p4.out'
		exeprog = './hyall_p3.out'
		exeprog = './hyall_p2.out'
		exeprog = './hyall_p1.out'
		exeprog = './hyall_p0.out'

	exeprog = './hyall_p3_ir.out'
	restotest = []
	for line in lines:
		splt = line.split()
		seq,top = int(splt[0]),int(splt[1])
		if top == 2:
			restotest += [seq-4, seq-2, seq, seq+2, seq+4]
		else:
			restotest += [seq-3, seq-1, seq+1, seq+3]

	if not os.path.exists('summary/'+todo):
		os.mkdir('summary/'+todo)

	reslist = open('summary/'+todo+'/'+todo+'.out.list','w')
	for i in restotest:
		reslist.write(str(i)+'\n')
	reslist.close()

	for i in restotest:
		if os.path.exists('summary/'+todo+'_'+str(i)+'_1.0.6.0.6.0.4.0.8.energy'):
			continue
		if os.path.exists('summary/'+todo+'/'+todo+'_'+str(i)+'_1.0.6.0.6.0.4.0.8.energy'):
			continue

		tmpf = open('subtmp', 'w')
		tmpf.write('#$ -S /bin/bash\n')
		tmpf.write('#$ -j y\n')
		tmpf.write('#$ -m be\n')
		tmpf.write('#$ -cwd\n')
		tmpf.write('#$ -N p'+todo+'_'+str(i)+'\n')
		tmpf.write(exeprog+' '+todo+' odds_asym_old 1 0.6 0.6 0.4 0.8 '+str(i)+' >> summary/last.log \n')
		tmpf.close()
		os.system(subprog+' subtmp')
		print exeprog+' '+todo+' odds_asym_old 1 0.6 0.6 0.4 0.8 '+str(i)+' >> summary/last.log\n'
		time.sleep(0.5)
