import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as ss
import sys

lin = np.loadtxt('lin.hydro')
fleming = np.loadtxt('fleming.hydro')
hessa = np.loadtxt('hessa.hydro')
white = np.loadtxt('white.hydro')
getfepmid = np.loadtxt('getfepmid.hydro')
getfepperi = np.loadtxt('getfepperi.hydro')

linstd = np.loadtxt('lin.hydro.std')
flemingstd = np.loadtxt('fleming.hydro.std')
hessastd = np.loadtxt('hessa.hydro.std')
whitestd = np.loadtxt('white.hydro.std')
getfepmidstd = np.loadtxt('getfepmid.hydro.std')


#fig = plt.figure(figsize =(10,10),dpi=150 )
#mainax = fig.add_subplot(1,1,1)
#mainax.patch.set_visible(False)
#mainax.spines['top'].set_visible(False)
#mainax.spines['bottom'].set_visible(False)
#mainax.spines['left'].set_visible(False)
#mainax.spines['right'].set_visible(False)
#mainax.set_xticklabels([])
#mainax.set_yticklabels([])

theirss = [white, hessa, fleming, lin]
theirstds = [whitestd, hessastd, flemingstd, linstd]
theirnames = ['WW-scale', 'H-scale', 'MF-scale', 'Comp-OmpLa']

ours = getfepmid
ourstd = getfepmidstd


aa = ["A", "R", "N", "D", "C", "Q", "E", "G", "H", "I", "L", "K", "M", "F", "P", "S", "T", "W", "Y", "V", "X"]


for i in range(4):
	theirs = theirss[i]
	theirname = theirnames[i]
	theirstd = theirstds[i]

	plt.close('all')
	fig = plt.figure(figsize =(10,10),dpi=150 )
	ax1 = fig.add_subplot(1,1,1)
	
	tmptheirs = []
	tmpours = []
	tmpaa = []
	tmpourstd = []
	tmptheirstd = []
	
	for triplet in sorted(zip(ours,theirs,aa,ourstd,theirstd)):
		tmpours.append(triplet[0])
		tmptheirs.append(triplet[1])
		tmpaa.append(triplet[2])
		tmpourstd.append(triplet[3])
		tmptheirstd.append(triplet[4])

	tmpours = np.array(tmpours)
	tmptheirs = np.array(tmptheirs)
	



	x = np.arange(20)
	x = x + 0.3
	width = 0.4
	tmpourstdneg = np.array(tmpourstd)
	tmpourstdneg[tmpours>0]=0
	tmpourstdpos = np.array(tmpourstd)
	tmpourstdpos[tmpours<0]=0

	tmptheirstdneg = np.array(tmptheirstd)
	tmptheirstdneg[tmptheirs>0]=0
	tmptheirstdpos = np.array(tmptheirstd)
	tmptheirstdpos[tmptheirs<0]=0

	legours = ax1.bar(x,tmpours,width=width,color='#2b8cbe',yerr=(tmpourstdneg,tmpourstdpos),ecolor='#2b8cbe')
	legtheirs = ax1.bar(x+width,tmptheirs,width=width,color='#e34a33',yerr=(tmptheirstdneg,tmptheirstdpos),ecolor='#e34a33')
	ax1.set_xticks(x+width)
	ax1.set_xticklabels(tmpaa, fontsize = 20)
	ax1.set_ylabel(r"$\Delta\Delta G^{210}$(kcal/mol)", fontsize = 20)
	ax1.set_xlim([0,19+2*0.3+2*width])
	ax1.legend( (legours[0],legtheirs[0]), ('GetFEP-mid', theirname), loc=0, fontsize = 20)
	plt.savefig('bar_getfepmid_'+theirname+'.svg',bbox_inches='tight')







	#### plot correlation plot

	plt.close('all')
	fig = plt.figure(figsize =(10,10),dpi=150 )
	ax2 = fig.add_subplot(1,1,1)
	slope, intercept, rv, pv, std = ss.linregress(theirs, ours)
	x = np.linspace(-3,6,50)
	y = slope*x+intercept
	ax2.plot(x,y,color='#636363',linewidth=3)
	ax2.scatter(theirs,ours,s=120, color='#2b8cbe')
	ax2.set_xlim([-3,6])
	ax2.set_ylim([-3,6])
	ax2.set_xlabel(theirname +r" $\Delta\Delta G^{210}$(kcal/mol)", fontsize = 20)
	ax2.set_ylabel(r"GetFEP-mid $\Delta\Delta G^{210}$(kcal/mol)", fontsize = 20)

	ax2.text(1,-2,r'$r=%.2f$'%(rv),fontsize = 20)
	#rsqed = rv**2
	#ax2.text(1,-2,r'$R^2=%.2f$'%(rsqed),fontsize = 20)

	plt.savefig('reg_getfepmid_'+theirname+'.svg',bbox_inches='tight')




