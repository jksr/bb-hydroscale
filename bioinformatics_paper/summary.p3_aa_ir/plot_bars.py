import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as ss
import sys

getfepmid = [0.0000 ,3.2081 ,3.1320 ,3.7979 ,0.3667 ,2.0105 ,2.6250 ,1.1580 ,2.8931 ,-1.4935 ,-2.2880 ,3.7209 ,-0.6919 ,-1.8751 ,1.4134 ,1.6822 ,1.0432 ,-0.3090 ,-0.5941 ,-1.4311]
fleming = [0 ,3.71 ,3.47 ,2.95 ,0.49 ,3.01 ,1.64 ,1.72 ,4.76 ,-1.56 ,-1.81 ,5.39 ,-0.76 ,-2.2 ,-1.52 ,1.83 ,1.78 ,-0.38 ,-1.09 ,-0.78]

aa = ["A", "R", "N", "D", "C", "Q", "E", "G", "H", "I", "L", "K", "M", "F", "P", "S", "T", "W", "Y", "V", "X"]

fig = plt.figure(figsize =(10,10),dpi=150 )
ax1 = fig.add_subplot(1,1,1)

tmpfleming = []
tmpgetfepmid = []
tmpaa = []

for quadruplet in sorted(zip(getfepmid,fleming,aa)):
	tmpgetfepmid.append(quadruplet[0])
	tmpfleming.append(quadruplet[1])
	tmpaa.append(quadruplet[2])

x = np.arange(20)
x = x + 0.3
width = 0.4
leggetfepmid = ax1.bar(x,tmpgetfepmid,width=width,color='#2b8cbe')
legfleming = ax1.bar(x+width,tmpfleming,width=width,color='#e34a33',hatch='//')

ax1.set_xticks(x+width)
ax1.set_xticklabels(tmpaa, fontsize = 20)
ax1.set_ylabel(r"$\Delta\Delta G^{210}$(kcal/mol)", fontsize = 20)
ax1.set_xlim([0,19+2*0.3+2*width])
ax1.set_ylim([-2.5,6.5])
ax1.legend( (leggetfepmid[0],legfleming[0]), ('Ours', 'Moon-Fleming'), loc=4, fontsize = 18)


ax2 = plt.axes([.18, .53, .35, .35])
ax2.tick_params(labelsize=10)
slope, intercept, rv, pv, std = ss.linregress(getfepmid, fleming)
x = np.linspace(-3,6,50)
y = slope*x+intercept
ax2.plot(x,y,color='#636363',linewidth=3)
ax2.scatter(getfepmid, fleming, s=20, color='#2b8cbe')
ax2.set_xlim([-3,6])
ax2.set_ylim([-3,6])
ax2.set_xlabel(r"$\Delta\Delta G^{210}$(kcal/mol)", fontsize = 10)
ax2.set_ylabel(r"$\Delta\Delta G^{210}$(kcal/mol)", fontsize = 10)
ax2.text(2,-2,r'$R^2=%.2f$'%(rv**2),fontsize = 18)




plt.savefig('bar.png',bbox_inches='tight')
plt.savefig('bar.eps',bbox_inches='tight')


