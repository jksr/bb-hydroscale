import glob
import sys
import math
import numpy as np

def rmsd(v1,v2):
	return math.sqrt(np.sum(np.square(v1-v2))/len(v1))

list8 = ['1bxw', '1p4t', '1qj8', '1thq', '2f1t', '2lhf', '2mlh', '2erv', '3dzm']
list10 = ['1i78', '1k24']
list12 = ['1qd6', '1tly', '1uyn', '2wjr', '3aeh', '3fid', '3kvn', '4pr7']

print 'p3_aa_ir vs p4'
for alist in [list8,list10,list12]:
	rmsds = []
	for pdb in alist:
		for fn in glob.glob(pdb+'/*.energy'):
			fn = fn[fn.rindex('/')+1:]
			appvec = np.loadtxt(pdb+'/'+fn, usecols=[2])
			exvec = np.loadtxt('../summary.p4/'+pdb+'/'+fn, usecols=[2])
			rmsds.append( rmsd(appvec,exvec) )
	print np.mean(rmsds)
