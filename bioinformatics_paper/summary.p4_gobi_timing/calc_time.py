import glob
import numpy as np


list8 = ['1bxw', '1p4t', '1qj8', '1thq', '2f1t', '2lhf', '2mlh', '2erv', '3dzm']
list10 = ['1i78', '1k24']
list12 = ['1qd6', '1tly', '1uyn', '2wjr', '3aeh', '3fid', '3kvn', '4pr7']


for alist in [list8, list10, list12]:
	data = []
	for pdb in alist:
		for fn in glob.glob(pdb+'*.energy'):
			f = open(fn)
			f.readline()
			f.readline()
			f.readline()
			data.append( float(f.readline().split()[-1]) )
	print np.mean(data)

		
