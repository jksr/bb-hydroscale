import glob
import sys
import math
import numpy as np

def rmsd(v1,v2):
	return math.sqrt(np.sum(np.square(v1-v2))/len(v1))

list8 = ['1bxw', '1p4t', '1qj8', '1thq', '2f1t', '2lhf', '2mlh', '2erv', '3dzm']
list10 = ['1i78', '1k24']
list12 = ['1qd6', '1tly', '1uyn', '2wjr', '3aeh', '3fid', '3kvn', '4pr7']

print 'p3_aa overall vs p4'
for alist in [list8,list10,list12]:
	rmsds = []
	for pdb in alist:
		for fn in glob.glob(pdb+'/*.energy'):
			fn = fn[fn.rindex('/')+1:]
			appvec = np.loadtxt(pdb+'/'+fn, usecols=[2])
			exvec = np.loadtxt('../summary.p4/'+pdb+'/'+fn, usecols=[2])
			rmsds.append( rmsd(appvec,exvec) )
	print len(rmsds), np.mean(rmsds)

print 'p3_aa details vs p4'
for alist in [list8,list10,list12]:
	bdrmsds = []
	nonbdrmsds = []
	for pdb in alist:
		centopo = np.loadtxt('../inputs/'+pdb+'/'+pdb+'.cen.topo').astype(int)
		cens = centopo[:,0]
		topos = centopo[:,1]
		n = len(cens)/2
		bd = []
		nonbd = []
		for i in range(len(cens)):
			if topos[i]==2:
				outfacingres = [cens[i]-4, cens[i]-2, cens[i], cens[i]+2, cens[i]+4]
			else:
				outfacingres = [cens[i]-3, cens[i]-1, cens[i]+1, cens[i]+3]
			if i in [-1,0,1,n,n-1,n+1]:
				bd += outfacingres
			else:
				nonbd += outfacingres

		for res in bd:
			for fn in glob.glob(pdb+'/'+pdb+'_'+str(res)+'_*.energy'):
				fn = fn[fn.rindex('/')+1:]
				appvec = np.loadtxt(pdb+'/'+fn, usecols=[2])
				exvec = np.loadtxt('../summary.p4/'+pdb+'/'+fn, usecols=[2])
				bdrmsds.append( rmsd(appvec,exvec) )

		for res in nonbd:
			for fn in glob.glob(pdb+'/'+pdb+'_'+str(res)+'_*.energy'):
				fn = fn[fn.rindex('/')+1:]
				appvec = np.loadtxt(pdb+'/'+fn, usecols=[2])
				exvec = np.loadtxt('../summary.p4/'+pdb+'/'+fn, usecols=[2])
				nonbdrmsds.append( rmsd(appvec,exvec) )
		#print pdb, bd
		#print pdb, nonbd
		#print pdb, len(bd), len(bdrmsds)
		#print pdb, len(nonbd), len(nonbdrmsds)
	print np.mean(bdrmsds), np.mean(nonbdrmsds), np.mean(bdrmsds+nonbdrmsds)
	#print len(bdrmsds), len(nonbdrmsds), len(bdrmsds+nonbdrmsds)

