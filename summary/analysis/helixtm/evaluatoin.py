import glob

def get_true_segs(subject_id):
	fn = glob.glob('data/tmsegs/'+subject_id+'*')[0]
	with open(fn) as f:
		line = f.readline().strip()
	segs = line.split('||')
	rtn = []
	for seg in segs:
		start, end = seg.split('-')
		start = int(start)
		end = int(end)
		rtn.append( (start,end) )
	return rtn

def get_pred_segs(pred, subject_id):
	fn = glob.glob('mpex.out/'+pred+'/'+subject_id+'*')[0]
	rtn = []
	with open(fn) as f:
		while True:
			line = f.readline()
			if not line:
				break
			if line.startswith('Number of hydropathy predicted segments:'):
				seg_num = int(line[ line.find(':')+1 : ])
				for i in range(seg_num):
					line = f.readline()
					line = f.readline()
					start, end = line.split(' ')[0].split('-')
					start = int(start)
					end = int(end)
					rtn.append( (start,end) )
					line = f.readline()
					line = f.readline()
					line = f.readline()
				break
	return rtn


def construct_tm_resi_set(segs):
	rtn = []
	for seg in segs:
		rtn += range(seg[0],seg[1]+1)
	return set(rtn)


def check_tmseg_overlapping(segs1,segs2):
	len1 = len(segs1)
	len2 = len(segs2)
	if len1==len2:
		for i in range(len1):
			if segs1[i][0] >= segs2[i][1] or segs2[i][0] >= segs1[i][1]:
				return False
		return True
	return False



pred = 'tian_minus'
#pred = 'fleming'
pred = 'fleming_plus'
#pred = 'octanol'
#pred = 'ompla'
pred = 'asa'
pred = 'tian'
pred = 'hessa'
pred = 'degrado'
pred = 'tian_no_interstrand'

subject_num = len(glob.glob('data/tmsegs/*.tmsegs'))

true_tot_tmseg_resi_num = 0
pred_tot_tmseg_resi_num = 0
crct_tot_tmseg_resi_num = 0
crct_tmseg_num = 0

for i in range(10000,10000+subject_num):
	subject_id = 'id'+str(i)

	true_tmsegs = get_true_segs(subject_id)
	pred_tmsegs = get_pred_segs(pred,subject_id)

	true_tmseg_resi = construct_tm_resi_set( true_tmsegs )
	pred_tmseg_resi = construct_tm_resi_set( pred_tmsegs )
	crct_tmseg_resi = true_tmseg_resi & pred_tmseg_resi
	true_num = len(true_tmseg_resi)
	pred_num = len(pred_tmseg_resi)
	crct_num = len(crct_tmseg_resi)
	true_tot_tmseg_resi_num += true_num
	pred_tot_tmseg_resi_num += pred_num
	crct_tot_tmseg_resi_num += crct_num
	
	if check_tmseg_overlapping(true_tmsegs,pred_tmsegs):
		crct_tmseg_num += 1
	else:
		print '\''+str(subject_id)+'\',', true_tmsegs,',', pred_tmsegs,','


print pred
print 'true:', true_tot_tmseg_resi_num
print 'pred:', pred_tot_tmseg_resi_num
print 'crct:', crct_tot_tmseg_resi_num
print 'precision:', float(crct_tot_tmseg_resi_num)/float(pred_tot_tmseg_resi_num)
print 'recall:',    float(crct_tot_tmseg_resi_num)/float(true_tot_tmseg_resi_num)
print 'crct tmseg num:', crct_tmseg_num
