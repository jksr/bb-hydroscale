Results summary for protein: id10084_cyto._C_oxidase._Subunit_III
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 2 Neutral
    His 6 Neutral
    His 78 Neutral
    His 139 Neutral
    His 159 Neutral
    His 160 Neutral
    His 195 Neutral
    His 216 Neutral
    His 219 Neutral
    His 244 Neutral
    His 255 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 8

10-43 (34) : ILPPSIWPFFGAIGAFVMLTGAVAWMKGITFFGL
    DG = -11.69 for segment sequence
    DG = -16.11 for most favorable 19AA centered at #19F
    (ILPPSIWPFFGAIGAFVML)

48-66 (19) : PWMFLIGLVGVLYVMFGWW
    DG = -13.84 for segment sequence
    DG = -13.84 for most favorable 19AA centered at #57G
    (PWMFLIGLVGVLYVMFGWW)

90-118 (29) : FILFIMSEVMFFVAWFWAFIKNALYPMGP
    DG = -16.18 for segment sequence
    DG = -18.5 for most favorable 19AA centered at #99M
    (FILFIMSEVMFFVAWFWAF)

132-164 (33) : IVTFDPWhLPLINTLILLLSGVAVTWAhhAFVL
    DG = 0.92 for segment sequence
    DG = -7.2 for most favorable 19AA centered at #141P
    (IVTFDPWhLPLINTLILLL)

175-193 (19) : LIVAVILGVCFTGLQAYEY
    DG = -4.91 for segment sequence
    DG = -4.91 for most favorable 19AA centered at #184C
    (LIVAVILGVCFTGLQAYEY)

197-215 (19) : AFGLADTVYAGAFYMATGF
    DG = -0.46 for segment sequence
    DG = -0.46 for most favorable 19AA centered at #206A
    (AFGLADTVYAGAFYMATGF)

217-235 (19) : GAhVIIGTIFLFVCLIRLL
    DG = -5.26 for segment sequence
    DG = -5.26 for most favorable 19AA centered at #226F
    (GAhVIIGTIFLFVCLIRLL)

253-271 (19) : YWhFVDVVWLFLFVVIYIW
    DG = -12.85 for segment sequence
    DG = -12.85 for most favorable 19AA centered at #262L
    (YWhFVDVVWLFLFVVIYIW)


Number of known transmembrane regions: 0


Working sequence (length = 273):

AhVKNhDYQILPPSIWPFFGAIGAFVMLTGAVAWMKGITFFGLPVEGPWMFLIGLVGVLY
VMFGWWADVVNEGETGEhTPVVRIGLQYGFILFIMSEVMFFVAWFWAFIKNALYPMGPDS
PIKDGVWPPEGIVTFDPWhLPLINTLILLLSGVAVTWAhhAFVLEGDRKTTINGLIVAVI
LGVCFTGLQAYEYShAAFGLADTVYAGAFYMATGFhGAhVIIGTIFLFVCLIRLLKGQMT
QKQhVGFEAAAWYWhFVDVVWLFLFVVIYIWGR

