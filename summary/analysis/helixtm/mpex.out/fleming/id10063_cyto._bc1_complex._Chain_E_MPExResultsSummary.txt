Results summary for protein: id10063_cyto._bc1_complex._Chain_E
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 2 Neutral
    His 100 Neutral
    His 122 Neutral
    His 141 Neutral
    His 161 Neutral
    His 164 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

130-148 (19) : PEWVILIGVCThLGCVPIA
    DG = -1.46 for segment sequence
    DG = -1.46 for most favorable 19AA centered at #139C
    (PEWVILIGVCThLGCVPIA)


Number of known transmembrane regions: 0


Working sequence (length = 196):

ShTDIKVPDFSDYRRPEVLDSTKSSKESSEARKGFSYLVTATTTVGVAYAAKNVVSQFVS
SMSASADVLAMSKIEIKLSDIPEGKNMAFKWRGKPLFVRhRTKKEIDQEAAVEVSQLRDP
QhDLERVKKPEWVILIGVCThLGCVPIANAGDFGGYYCPChGShYDASGRIRKGPAPLNL
EVPSYEFTSDDMVIVG

