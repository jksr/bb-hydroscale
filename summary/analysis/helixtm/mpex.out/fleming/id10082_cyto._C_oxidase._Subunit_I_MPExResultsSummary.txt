Results summary for protein: id10082_cyto._C_oxidase._Subunit_I
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 7 Neutral
    His 9 Neutral
    His 12 Neutral
    His 13 Neutral
    His 28 Neutral
    His 59 Neutral
    His 85 Neutral
    His 94 Neutral
    His 119 Neutral
    His 187 Neutral
    His 269 Neutral
    His 276 Neutral
    His 292 Neutral
    His 325 Neutral
    His 326 Neutral
    His 403 Neutral
    His 411 Neutral
    His 413 Neutral
    His 448 Neutral
    His 464 Neutral
    His 526 Neutral
    His 541 Neutral
    His 556 Neutral
    His 558 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 12

31-49 (19) : IGILYLFTAGIVGLISVCF
    DG = -9.46 for segment sequence
    DG = -9.46 for most favorable 19AA centered at #40G
    (IGILYLFTAGIVGLISVCF)

100-128 (29) : FFVVIPALFGGFGNYFMPLhIGAPDMAFP
    DG = -12.13 for segment sequence
    DG = -14 for most favorable 19AA centered at #109G
    (FFVVIPALFGGFGNYFMPL)

133-151 (19) : LSYWMYVCGVALGVASLLA
    DG = -5.31 for segment sequence
    DG = -5.31 for most favorable 19AA centered at #142V
    (LSYWMYVCGVALGVASLLA)

166-184 (19) : LYPPLSTTEAGYSMDLAIF
    DG = -1.64 for segment sequence
    DG = -1.64 for most favorable 19AA centered at #175A
    (LYPPLSTTEAGYSMDLAIF)

218-247 (30) : PLFAWSVFITAWLILLSLPVLAGAITMLLM
    DG = -21.5 for segment sequence
    DG = -15.71 for most favorable 19AA centered at #227T
    (PLFAWSVFITAWLILLSLP)

271-289 (19) : LWFFGhPEVYIIILPGFGI
    DG = -10.19 for segment sequence
    DG = -10.19 for most favorable 19AA centered at #280Y
    (LWFFGhPEVYIIILPGFGI)

302-320 (19) : IFGYLPMVLAMAAIGILGF
    DG = -14.26 for segment sequence
    DG = -14.26 for most favorable 19AA centered at #311A
    (IFGYLPMVLAMAAIGILGF)

338-356 (19) : AYFMLATMTIAVPTGIKVF
    DG = -2.57 for segment sequence
    DG = -2.57 for most favorable 19AA centered at #347I
    (AYFMLATMTIAVPTGIKVF)

367-397 (31) : IEFKTPMLWAFGFLFLFTVGGVTGVVLSQAP
    DG = -3.01 for segment sequence
    DG = -9.12 for most favorable 19AA centered at #376A
    (IEFKTPMLWAFGFLFLFTV)

414-432 (19) : YVMSLGAVFGIFAGVYYWI
    DG = -9.09 for segment sequence
    DG = -9.09 for most favorable 19AA centered at #423G
    (YVMSLGAVFGIFAGVYYWI)

449-467 (19) : FWMMFIGSNLIFFPQhFLG
    DG = -4.65 for segment sequence
    DG = -4.65 for most favorable 19AA centered at #458L
    (FWMMFIGSNLIFFPQhFLG)

494-512 (19) : YISFASFLFFIGIVFYTLF
    DG = -17.3 for segment sequence
    DG = -17.3 for most favorable 19AA centered at #503F
    (YISFASFLFFIGIVFYTLF)


Number of known transmembrane regions: 0


Working sequence (length = 558):

MADAAVhGhGDhhDTRGFFTRWFMSTNhKDIGILYLFTAGIVGLISVCFTVYMRMELQhP
GVQYMCLEGARLIADASAECTPNGhLWNVMITYhGVLMMFFVVIPALFGGFGNYFMPLhI
GAPDMAFPRLNNLSYWMYVCGVALGVASLLAPGGNDQMGSGVGWVLYPPLSTTEAGYSMD
LAIFAVhVSGASSILGAINIITTFLNMRAPGMTLFKVPLFAWSVFITAWLILLSLPVLAG
AITMLLMDRNFGTQFFDPAGGGDPVLYQhILWFFGhPEVYIIILPGFGIIShVISTFAKK
PIFGYLPMVLAMAAIGILGFVVWAhhMYTAGMSLTQQAYFMLATMTIAVPTGIKVFSWIA
TMWGGSIEFKTPMLWAFGFLFLFTVGGVTGVVLSQAPLDRVYhDTYYVVAhFhYVMSLGA
VFGIFAGVYYWIGKMSGRQYPEWAGQLhFWMMFIGSNLIFFPQhFLGRQGMPRRYIDYPV
EFAYWNNISSIGAYISFASFLFFIGIVFYTLFAGKRVNVPNYWNEhADTLEWTLPSPPPE
hTFETLPKREDWDRAhAh

