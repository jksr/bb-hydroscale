Results summary for protein: id10009_KvAP_potassium_channel
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 24 Neutral
    His 109 Neutral
    His 152 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

25-71 (47) : PLVELGVSYAALLSVIVVVVEYTMQLSGEYLVRLYLVDLILVIILWA
    DG = -15.67 for segment sequence
    DG = -12.14 for most favorable 19AA centered at #62D
    (EYLVRLYLVDLILVIILWA)

90-131 (42) : TLYEIPALVPAGLLALIEGhLAGLGLFRLVRLLRFLRILLII
    DG = -11.69 for segment sequence
    DG = -8.58 for most favorable 19AA centered at #99P
    (TLYEIPALVPAGLLALIEG)

153-171 (19) : LFGAVMLTVLYGAFAIYIV
    DG = -13.01 for segment sequence
    DG = -13.01 for most favorable 19AA centered at #162L
    (LFGAVMLTVLYGAFAIYIV)

211-229 (19) : VIGIAVMLTGISALTLLIG
    DG = -5.25 for segment sequence
    DG = -5.25 for most favorable 19AA centered at #220G
    (VIGIAVMLTGISALTLLIG)


Number of known transmembrane regions: 0


Working sequence (length = 282):

MARFRRGLSDLGGRVRNIGDVMEhPLVELGVSYAALLSVIVVVVEYTMQLSGEYLVRLYL
VDLILVIILWADYAYRAYKSGDPAGYVKKTLYEIPALVPAGLLALIEGhLAGLGLFRLVR
LLRFLRILLIISRGSKFLSAIADAADKIRFYhLFGAVMLTVLYGAFAIYIVEYPDPNSSI
KSVFDALWWAVVTATTVGYGDVVPATPIGKVIGIAVMLTGISALTLLIGTVSNMFQKILV
GEPEPSCSPAKLAEMVSSMSEEEFEEFVRTLKNLRRLENSMK

