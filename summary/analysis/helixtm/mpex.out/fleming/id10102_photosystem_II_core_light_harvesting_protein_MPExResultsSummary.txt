Results summary for protein: id10102_photosystem_II_core_light_harvesting_protein
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 9 Neutral
    His 23 Neutral
    His 26 Neutral
    His 100 Neutral
    His 114 Neutral
    His 142 Neutral
    His 157 Neutral
    His 201 Neutral
    His 202 Neutral
    His 216 Neutral
    His 343 Neutral
    His 455 Neutral
    His 466 Neutral
    His 469 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

37-67 (31) : MALYELATFDPSDPVLNPMWRQGMFVLPFMA
    DG = -2.93 for segment sequence
    DG = -1.94 for most favorable 19AA centered at #58Q
    (DPVLNPMWRQGMFVLPFMA)

93-123 (31) : FEGVALAhIVLSGLLFLAACWhWVYWDLELF
    DG = -3.89 for segment sequence
    DG = -4.9 for most favorable 19AA centered at #102V
    (FEGVALAhIVLSGLLFLAA)

138-166 (29) : MFGIhLFLAGLLCFGFGAFhLTGLFGPGM
    DG = -4.83 for segment sequence
    DG = -8.43 for most favorable 19AA centered at #147G
    (MFGIhLFLAGLLCFGFGAF)

203-221 (19) : IAAGIVGIIAGLFhILVRP
    DG = -3.07 for segment sequence
    DG = -3.07 for most favorable 19AA centered at #212A
    (IAAGIVGIIAGLFhILVRP)

234-264 (31) : IETVLSSSIAAVFFAAFVVAGTMWYGSATTP
    DG = 1.12 for segment sequence
    DG = -5.74 for most favorable 19AA centered at #243A
    (IETVLSSSIAAVFFAAFVV)

352-370 (19) : EELFVRRMPAFFESFPVIL
    DG = -5.17 for segment sequence
    DG = -5.17 for most favorable 19AA centered at #361A
    (EELFVRRMPAFFESFPVIL)

450-468 (19) : WFTFAhAVFALLFFFGhIW
    DG = -6.9 for segment sequence
    DG = -6.9 for most favorable 19AA centered at #459A
    (WFTFAhAVFALLFFFGhIW)


Number of known transmembrane regions: 0


Working sequence (length = 510):

MGLPWYRVhTVLINDPGRLIAAhLMhTALVAGWAGSMALYELATFDPSDPVLNPMWRQGM
FVLPFMARLGVTGSWSGWSITGETGIDPGFWSFEGVALAhIVLSGLLFLAACWhWVYWDL
ELFRDPRTGEPALDLPKMFGIhLFLAGLLCFGFGAFhLTGLFGPGMWVSDPYGLTGSVQP
VAPEWGPDGFNPYNPGGVVAhhIAAGIVGIIAGLFhILVRPPQRLYKALRMGNIETVLSS
SIAAVFFAAFVVAGTMWYGSATTPIELFGPTRYQWDSSYFQQEINRRVQASLASGATLEE
AWSAIPEKLAFYDYIGNNPAKGGLFRTGPMNKGDGIAQAWKGhAVFRNKEGEELFVRRMP
AFFESFPVILTDKNGVVKADIPFRRAESKYSFEQQGVTVSFYGGELNGQTFTDPPTVKSY
ARKAIFGEIFEFDTETLNSDGIFRTSPRGWFTFAhAVFALLFFFGhIWhGARTLFRDVFS
GIDPELSPEQVEWGFYQKVGDVTTRRKEAV

