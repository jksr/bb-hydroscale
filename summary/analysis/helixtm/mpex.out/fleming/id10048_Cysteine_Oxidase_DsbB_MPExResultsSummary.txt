Results summary for protein: id10048_Cysteine_Oxidase_DsbB
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 34 Neutral
    His 91 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

14-60 (47) : AWLLMAFTALALELTALWFQhVMLLKPCVLCIYERCALFGVLGAALI
    DG = -10.05 for segment sequence
    DG = -11.58 for most favorable 19AA centered at #23L
    (AWLLMAFTALALELTALWF)

64-82 (19) : APKTPLRYVAMVIWLYSAF
    DG = -2.59 for segment sequence
    DG = -2.59 for most favorable 19AA centered at #73A
    (APKTPLRYVAMVIWLYSAF)

98-116 (19) : PSPFATCDFMVRFPEWLPL
    DG = -5.82 for segment sequence
    DG = -5.82 for most favorable 19AA centered at #107M
    (PSPFATCDFMVRFPEWLPL)

145-163 (19) : WLLGIFIAYLIVAVLVVIS
    DG = -16.72 for segment sequence
    DG = -16.72 for most favorable 19AA centered at #154L
    (WLLGIFIAYLIVAVLVVIS)


Number of known transmembrane regions: 0


Working sequence (length = 176):

MLRFLNQCSQGRGAWLLMAFTALALELTALWFQhVMLLKPCVLCIYERCALFGVLGAALI
GAIAPKTPLRYVAMVIWLYSAFRGVQLTYEhTMLQLYPSPFATCDFMVRFPEWLPLDKWV
PQVFVASGDCAERQWDFLGLEMPQWLLGIFIAYLIVAVLVVISQPFKAKKRDLFGR

