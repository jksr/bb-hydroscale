Results summary for protein: id10005_rhodopsin
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 65 Neutral
    His 100 Neutral
    His 152 Neutral
    His 195 Neutral
    His 211 Neutral
    His 278 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

39-57 (19) : MLAAYMFLLIMLGFPINFL
    DG = -18.47 for segment sequence
    DG = -18.47 for most favorable 19AA centered at #48I
    (MLAAYMFLLIMLGFPINFL)

71-99 (29) : PLNYILLNLAVADLFMVFGGFTTTLYTSL
    DG = -4.57 for segment sequence
    DG = -8.33 for most favorable 19AA centered at #80A
    (PLNYILLNLAVADLFMVFG)

115-143 (29) : FFATLGGEIALWSLVVLAIERYVVVCKPM
    DG = -2.49 for segment sequence
    DG = -8.01 for most favorable 19AA centered at #124A
    (FFATLGGEIALWSLVVLAI)

154-183 (30) : IMGVAFTWVMALACAAPPLVGWSRYIPEGM
    DG = -5.36 for segment sequence
    DG = -9.89 for most favorable 19AA centered at #163M
    (IMGVAFTWVMALACAAPPL)

203-221 (19) : FVIYMFVVhFIIPLIVIFF
    DG = -22.34 for segment sequence
    DG = -22.34 for most favorable 19AA centered at #212F
    (FVIYMFVVhFIIPLIVIFF)

258-276 (19) : VIAFLICWLPYAGVAFYIF
    DG = -18.33 for segment sequence
    DG = -18.33 for most favorable 19AA centered at #267P
    (VIAFLICWLPYAGVAFYIF)

283-301 (19) : FGPIFMTIPAFFAKTSAVY
    DG = -5.09 for segment sequence
    DG = -5.09 for most favorable 19AA centered at #292A
    (FGPIFMTIPAFFAKTSAVY)


Number of known transmembrane regions: 0


Working sequence (length = 348):

MNGTEGPNFYVPFSNKTGVVRSPFEAPQYYLAEPWQFSMLAAYMFLLIMLGFPINFLTLY
VTVQhKKLRTPLNYILLNLAVADLFMVFGGFTTTLYTSLhGYFVFGPTGCNLEGFFATLG
GEIALWSLVVLAIERYVVVCKPMSNFRFGENhAIMGVAFTWVMALACAAPPLVGWSRYIP
EGMQCSCGIDYYTPhEETNNESFVIYMFVVhFIIPLIVIFFCYGQLVFTVKEAAAQQQES
ATTQKAEKEVTRMVIIMVIAFLICWLPYAGVAFYIFThQGSDFGPIFMTIPAFFAKTSAV
YNPVIYIMMNKQFRNCMVTTLCCGKNPLGDDEASTTVSKTETSQVAPA

