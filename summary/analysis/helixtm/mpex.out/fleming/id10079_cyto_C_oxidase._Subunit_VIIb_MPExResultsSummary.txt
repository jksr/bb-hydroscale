Results summary for protein: id10079_cyto_C_oxidase._Subunit_VIIb
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 2 Neutral
    His 10 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

16-34 (19) : AVLASGATFCVAVWVYMAT
    DG = -1.76 for segment sequence
    DG = -1.76 for most favorable 19AA centered at #25C
    (AVLASGATFCVAVWVYMAT)


Number of known transmembrane regions: 0


Working sequence (length = 56):

IhQKRAPDFhDKYGNAVLASGATFCVAVWVYMATQIGIEWNPSPVGRVTPKEWREQ

