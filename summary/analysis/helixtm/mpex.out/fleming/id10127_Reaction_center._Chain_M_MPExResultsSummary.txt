Results summary for protein: id10127_Reaction_center._Chain_M
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 145 Neutral
    His 182 Neutral
    His 193 Neutral
    His 202 Neutral
    His 219 Neutral
    His 266 Neutral
    His 301 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

34-52 (19) : PFSTLLGWFGNAQLGPIYL
    DG = -2.46 for segment sequence
    DG = -2.46 for most favorable 19AA centered at #43G
    (PFSTLLGWFGNAQLGPIYL)

58-76 (19) : LSLFSGLMWFFTIGIWFWY
    DG = -11.46 for segment sequence
    DG = -11.46 for most favorable 19AA centered at #67F
    (LSLFSGLMWFFTIGIWFWY)

89-107 (19) : LFFFSLEPPAPEYGLSFAA
    DG = -11.22 for segment sequence
    DG = -11.22 for most favorable 19AA centered at #98A
    (LFFFSLEPPAPEYGLSFAA)

112-130 (19) : GGLWLIASFFMFVAVWSWW
    DG = -8.52 for segment sequence
    DG = -8.52 for most favorable 19AA centered at #121F
    (GGLWLIASFFMFVAVWSWW)

150-180 (31) : FLSAIWLWMVLGFIRPILMGSWSEAVPYGIF
    DG = -12.43 for segment sequence
    DG = -13.64 for most favorable 19AA centered at #159V
    (FLSAIWLWMVLGFIRPILM)

200-218 (19) : PFhGLSIAFLYGSALLFAM
    DG = -6.91 for segment sequence
    DG = -6.91 for most favorable 19AA centered at #209L
    (PFhGLSIAFLYGSALLFAM)

268-286 (19) : WAIWMAVLVTLTGGIGILL
    DG = -6.28 for segment sequence
    DG = -6.28 for most favorable 19AA centered at #277T
    (WAIWMAVLVTLTGGIGILL)


Number of known transmembrane regions: 0


Working sequence (length = 307):

AEYQNIFSQVQVRGPADLGMTEDVNLANRSGVGPFSTLLGWFGNAQLGPIYLGSLGVLSL
FSGLMWFFTIGIWFWYQAGWNPAVFLRDLFFFSLEPPAPEYGLSFAAPLKEGGLWLIASF
FMFVAVWSWWGRTYLRAQALGMGKhTAWAFLSAIWLWMVLGFIRPILMGSWSEAVPYGIF
ShLDWTNNFSLVhGNLFYNPFhGLSIAFLYGSALLFAMhGATILAVSRFGGERELEQIAD
RGTAAERAALFWRWTMGFNATMEGIhRWAIWMAVLVTLTGGIGILLSGTVVDNWYVWGQN
hGMAPLN

