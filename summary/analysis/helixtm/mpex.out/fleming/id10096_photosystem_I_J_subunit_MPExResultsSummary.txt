Results summary for protein: id10096_photosystem_I_J_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 3 Neutral
    His 39 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

11-41 (31) : APVLAAIWMTITAGILIEFNRFYPDLLFhPL
    DG = -7.65 for segment sequence
    DG = -8.58 for most favorable 19AA centered at #20T
    (APVLAAIWMTITAGILIEF)


Number of known transmembrane regions: 0


Working sequence (length = 41):

MKhFLTYLSTAPVLAAIWMTITAGILIEFNRFYPDLLFhPL

