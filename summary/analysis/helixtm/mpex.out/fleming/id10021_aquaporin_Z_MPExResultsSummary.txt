Results summary for protein: id10021_aquaporin_Z
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 56 Neutral
    His 61 Neutral
    His 124 Neutral
    His 150 Neutral
    His 174 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

13-54 (42) : FWLVFGGCGSAVLAAGFPELGIGFAGVALAFGLTVLTMAFAV
    DG = -10.9 for segment sequence
    DG = -8.13 for most favorable 19AA centered at #45L
    (FAGVALAFGLTVLTMAFAV)

84-102 (19) : YVIAQVVGGIVAAALLYLI
    DG = -8.96 for segment sequence
    DG = -8.96 for most favorable 19AA centered at #93I
    (YVIAQVVGGIVAAALLYLI)

131-149 (19) : MLSALVVELVLSAGFLLVI
    DG = -11.48 for segment sequence
    DG = -11.48 for most favorable 19AA centered at #140V
    (MLSALVVELVLSAGFLLVI)

161-179 (19) : FAPIAIGLALTLIhLISIP
    DG = -10.19 for segment sequence
    DG = -10.19 for most favorable 19AA centered at #170L
    (FAPIAIGLALTLIhLISIP)

205-223 (19) : LWFFWVVPIVGGIIGGLIY
    DG = -13.09 for segment sequence
    DG = -13.09 for most favorable 19AA centered at #214V
    (LWFFWVVPIVGGIIGGLIY)


Number of known transmembrane regions: 0


Working sequence (length = 231):

MFRKLAAECFGTFWLVFGGCGSAVLAAGFPELGIGFAGVALAFGLTVLTMAFAVGhISGG
hFNPAVTIGLWAGGRFPAKEVVGYVIAQVVGGIVAAALLYLIASGKTGFDAAASGFASNG
YGEhSPGGYSMLSALVVELVLSAGFLLVIhGATDKFAPAGFAPIAIGLALTLIhLISIPV
TNTSVNPARSTAVAIFQGGWALEQLWFFWVVPIVGGIIGGLIYRTLLEKRD

