Results summary for protein: id10053_formate_dehydrogenase-N,_gamma_chain
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 18 Neutral
    His 57 Neutral
    His 77 Neutral
    His 78 Neutral
    His 101 Neutral
    His 155 Neutral
    His 164 Neutral
    His 169 Neutral
    His 196 Neutral
    His 197 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

22-40 (19) : VICFFLVALSGISFFFPTL
    DG = -14.98 for segment sequence
    DG = -14.98 for most favorable 19AA centered at #31S
    (VICFFLVALSGISFFFPTL)

58-76 (19) : PFFGIAIFVALMFMFVRFV
    DG = -18.08 for segment sequence
    DG = -18.08 for most favorable 19AA centered at #67A
    (PFFGIAIFVALMFMFVRFV)

116-150 (35) : MFWSIMSMIFVLLVTGVIIWRPYFAQYFPMQVVRY
    DG = -12.07 for segment sequence
    DG = -12.1 for most favorable 19AA centered at #125F
    (MFWSIMSMIFVLLVTGVII)

158-176 (19) : AGIILIhAILIhMYMAFWV
    DG = -6.15 for segment sequence
    DG = -6.15 for most favorable 19AA centered at #167L
    (AGIILIhAILIhMYMAFWV)


Number of known transmembrane regions: 0


Working sequence (length = 217):

MSKSKMIVRTKFIDRAChWTVVICFFLVALSGISFFFPTLQWLTQTFGTPQMGRILhPFF
GIAIFVALMFMFVRFVhhNIPDKKDIPWLLNIVEVLKGNEhKVADVGKYNAGQKMMFWSI
MSMIFVLLVTGVIIWRPYFAQYFPMQVVRYSLLIhAAAGIILIhAILIhMYMAFWVKGSI
KGMIEGKVSRRWAKKhhPRWYREIEKAEAKKESEEGI

