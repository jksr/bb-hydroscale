Results summary for protein: id10069_cyt._b6f_complex,_PetM
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

9-27 (19) : ALLSFGLIFVGWGLGVLLL
    DG = -11.86 for segment sequence
    DG = -11.86 for most favorable 19AA centered at #18V
    (ALLSFGLIFVGWGLGVLLL)


Number of known transmembrane regions: 0


Working sequence (length = 35):

MTEEMLYAALLSFGLIFVGWGLGVLLLKIQGAEKE

