Results summary for protein: id10080_cyto._C_oxidase._Subunit_VIIc
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 2 Neutral
    His 42 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

21-39 (19) : LLAMMTLFFGSGFAAPFFI
    DG = -13.98 for segment sequence
    DG = -13.98 for most favorable 19AA centered at #30G
    (LLAMMTLFFGSGFAAPFFI)


Number of known transmembrane regions: 0


Working sequence (length = 47):

ShYEEGPGKNIPFSVENKWRLLAMMTLFFGSGFAAPFFIVRhQLLKK

