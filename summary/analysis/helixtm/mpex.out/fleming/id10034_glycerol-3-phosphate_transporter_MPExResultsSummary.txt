Results summary for protein: id10034_glycerol-3-phosphate_transporter
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 10 Neutral
    His 147 Neutral
    His 165 Neutral
    His 187 Neutral
    His 285 Neutral
    His 371 Neutral
    His 442 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 9

26-56 (31) : LRWQIFLGIFFGYAAYYLVRKNFALAMPYLV
    DG = -6.82 for segment sequence
    DG = -9.42 for most favorable 19AA centered at #35F
    (LRWQIFLGIFFGYAAYYLV)

65-83 (19) : LGFALSGISIAYGFSKFIM
    DG = -0.71 for segment sequence
    DG = -0.71 for most favorable 19AA centered at #74I
    (LGFALSGISIAYGFSKFIM)

94-140 (47) : VFLPAGLILAAAVMLFMGFVPWATSSIAVMFVLLFLCGWFQGMGWPP
    DG = -25.61 for segment sequence
    DG = -16.56 for most favorable 19AA centered at #103A
    (VFLPAGLILAAAVMLFMGF)

172-220 (49) : PPLLFLLGMAWFNDWhAALYMPAFCAILVALFAFAMMRDTPQSCGLPPI
    DG = -14.51 for segment sequence
    DG = -18.77 for most favorable 19AA centered at #199L
    (LYMPAFCAILVALFAFAMM)

255-273 (19) : LWYIAIANVFVYLLRYGIL
    DG = -10.43 for segment sequence
    DG = -10.43 for most favorable 19AA centered at #264F
    (LWYIAIANVFVYLLRYGIL)

294-312 (19) : AYFLYEYAGIPGTLLCGWM
    DG = -6.05 for segment sequence
    DG = -6.05 for most favorable 19AA centered at #303I
    (AYFLYEYAGIPGTLLCGWM)

326-344 (19) : VFFMTLVTIATIVYWMNPA
    DG = -7.37 for segment sequence
    DG = -7.37 for most favorable 19AA centered at #335A
    (VFFMTLVTIATIVYWMNPA)

352-370 (19) : ICMIVIGFLIYGPVMLIGL
    DG = -15.47 for segment sequence
    DG = -15.47 for most favorable 19AA centered at #361I
    (ICMIVIGFLIYGPVMLIGL)

417-435 (19) : FMVMIGGSILAVILLIVVM
    DG = -14 for segment sequence
    DG = -14 for most favorable 19AA centered at #426L
    (FMVMIGGSILAVILLIVVM)


Number of known transmembrane regions: 0


Working sequence (length = 452):

MLSIFKPAPhKARLPAAEIDPTYRRLRWQIFLGIFFGYAAYYLVRKNFALAMPYLVEQGF
SRGDLGFALSGISIAYGFSKFIMGSVSDRSNPRVFLPAGLILAAAVMLFMGFVPWATSSI
AVMFVLLFLCGWFQGMGWPPCGRTMVhWWSQKERGGIVSVWNCAhNVGGGIPPLLFLLGM
AWFNDWhAALYMPAFCAILVALFAFAMMRDTPQSCGLPPIEEYKNDYPDDYNEKAEQELT
AKQIFMQYVLPNKLLWYIAIANVFVYLLRYGILDWSPTYLKEVKhFALDKSSWAYFLYEY
AGIPGTLLCGWMSDKVFRGNRGATGVFFMTLVTIATIVYWMNPAGNPTVDMICMIVIGFL
IYGPVMLIGLhALELAPKKAAGTAAGFTGLFGYLGGSVAASAIVGYTVDFFGWDGGFMVM
IGGSILAVILLIVVMIGEKRRhEQLLQERNGG

