Results summary for protein: id10075_cyto._C_oxidase._Subunit_IV
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 2 Neutral
    His 29 Neutral
    His 101 Neutral
    His 109 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

81-99 (19) : VVGAAMFFIGFTALLLIWE
    DG = -10.99 for segment sequence
    DG = -10.99 for most favorable 19AA centered at #90G
    (VVGAAMFFIGFTALLLIWE)


Number of known transmembrane regions: 0


Working sequence (length = 147):

AhGSVVKSEDYALPSYVDRRDYPLPDVAhVKNLSASQKALKEKEKASWSSLSIDEKVELY
RLKFKESFAEMNRSTNEWKTVVGAAMFFIGFTALLLIWEKhYVYGPIPhTFEEEWVAKQT
KRMLDMKVAPIQGFSAKWDYDKNEWKK

