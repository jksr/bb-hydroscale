Results summary for protein: id10055_succinate_dehydrogenase_D_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 14 Neutral
    His 71 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

16-46 (31) : FILVRATAIVLTLYIIYMVGFFATSGELTYE
    DG = -7.07 for segment sequence
    DG = -11.88 for most favorable 19AA centered at #25V
    (FILVRATAIVLTLYIIYMV)

59-77 (19) : VFTLLALFSILIhAWIGMW
    DG = -8.53 for segment sequence
    DG = -8.53 for most favorable 19AA centered at #68I
    (VFTLLALFSILIhAWIGMW)

95-113 (19) : LVIVVALVVYVIYGFVVVW
    DG = -16.8 for segment sequence
    DG = -16.8 for most favorable 19AA centered at #104Y
    (LVIVVALVVYVIYGFVVVW)


Number of known transmembrane regions: 0


Working sequence (length = 115):

MVSNASALGRNGVhDFILVRATAIVLTLYIIYMVGFFATSGELTYEVWIGFFASAFTKVF
TLLALFSILIhAWIGMWQVLTDYVKPLALRLMLQLVIVVALVVYVIYGFVVVWGV

