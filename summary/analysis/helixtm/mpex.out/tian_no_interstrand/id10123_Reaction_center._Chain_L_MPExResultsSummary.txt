Results summary for protein: id10123_Reaction_center._Chain_L
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 116 Neutral
    His 144 Neutral
    His 153 Neutral
    His 168 Neutral
    His 173 Neutral
    His 190 Neutral
    His 211 Neutral
    His 230 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

31-49 (19) : VGFFGVSAIFFIFLGVSLI
    DG = -16.4 for segment sequence
    DG = -16.4 for most favorable 19AA centered at #40F
    (VGFFGVSAIFFIFLGVSLI)

84-102 (19) : GFWQAITVCALGAFISWML
    DG = -8.17 for segment sequence
    DG = -8.17 for most favorable 19AA centered at #93A
    (GFWQAITVCALGAFISWML)

121-151 (31) : FCVPIFMFCVLQVFRPLLLGSWGhAFPYGIL
    DG = -15.8 for segment sequence
    DG = -15.86 for most favorable 19AA centered at #130V
    (FCVPIFMFCVLQVFRPLLL)

177-195 (19) : VSFLFVNAMALGLhGGLIL
    DG = -11.96 for segment sequence
    DG = -11.96 for most favorable 19AA centered at #186A
    (VSFLFVNAMALGLhGGLIL)

232-250 (19) : LGLFLASNIFLTGAFGTIA
    DG = -9.02 for segment sequence
    DG = -9.02 for most favorable 19AA centered at #241F
    (LGLFLASNIFLTGAFGTIA)


Number of known transmembrane regions: 0


Working sequence (length = 273):

ALLSFERKYRVRGGTLIGGDLFDFWVGPYFVGFFGVSAIFFIFLGVSLIGYAASQGPTWD
PFAISINPPDLKYGLGAAPLLEGGFWQAITVCALGAFISWMLREVEISRKLGIGWhVPLA
FCVPIFMFCVLQVFRPLLLGSWGhAFPYGILShLDWVNNFGYQYLNWhYNPGhMSSVSFL
FVNAMALGLhGGLILSVANPGDGDKVKTAEhENQYFRDVVGYSIGALSIhRLGLFLASNI
FLTGAFGTIASGPFWTRGWPEWWGWWLDIPFWS

