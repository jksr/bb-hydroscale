Results summary for protein: id10059_cyto._bc1_complex._Chain_D
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 6 Neutral
    His 14 Neutral
    His 23 Neutral
    His 41 Neutral
    His 50 Neutral
    His 121 Neutral
    His 198 Neutral
    His 200 Neutral
    His 225 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

147-165 (19) : LYFNPYFPGQAIGMAPPIY
    DG = -1.83 for segment sequence
    DG = -1.83 for most favorable 19AA centered at #156Q
    (LYFNPYFPGQAIGMAPPIY)

204-222 (19) : MGLKMLLMMGLLLPLVYAM
    DG = -16.89 for segment sequence
    DG = -16.89 for most favorable 19AA centered at #213G
    (MGLKMLLMMGLLLPLVYAM)


Number of known transmembrane regions: 0


Working sequence (length = 241):

SDLELhPPSYPWShRGLLSSLDhTSIRRGFQVYKQVCSSChSMDYVAYRhLVGVCYTEDE
AKALAEEVEVQDGPNEDGEMFMRPGKLSDYFPKPYPNPEAARAANNGALPPDLSYIVRAR
hGGEDYVFSLLTGYCEPPTGVSLREGLYFNPYFPGQAIGMAPPIYNEVLEFDDGTPATMS
QVAKDVCTFLRWAAEPEhDhRKRMGLKMLLMMGLLLPLVYAMKRhKWSVLKSRKLAYRPP
K

