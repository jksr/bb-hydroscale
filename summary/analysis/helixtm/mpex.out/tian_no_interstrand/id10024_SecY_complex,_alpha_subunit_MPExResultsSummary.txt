Results summary for protein: id10024_SecY_complex,_alpha_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 236 Neutral
    His 285 Neutral
    His 314 Neutral
    His 367 Neutral
    His 426 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 8

31-49 (19) : IVLVLYFIMGCIDVYTAGA
    DG = -11.55 for segment sequence
    DG = -11.55 for most favorable 19AA centered at #40G
    (IVLVLYFIMGCIDVYTAGA)

69-100 (32) : LITLGIGPIVTAGIIMQLLVGSGIIQMDLSIP
    DG = -12.05 for segment sequence
    DG = -12.62 for most favorable 19AA centered at #78V
    (LITLGIGPIVTAGIIMQLL)

137-200 (64) : LLAFLVIIQIAFGSIILIYLDEIVSKYGIGSGIGLFIAAGVSQTIFVGALGPEGYLWKFLNSLI
    DG = -22.35 for segment sequence
    DG = -20.07 for most favorable 19AA centered at #146I
    (LLAFLVIIQIAFGSIILIY)

206-234 (29) : IEYIAPIIGTIIVFLMVVYAECMRVEIPL
    DG = -14.95 for segment sequence
    DG = -15.83 for most favorable 19AA centered at #215T
    (IEYIAPIIGTIIVFLMVVY)

258-286 (29) : VILAAALFANIQLWGLALYRMGIPILGhY
    DG = -14.3 for segment sequence
    DG = -13.37 for most favorable 19AA centered at #267N
    (VILAAALFANIQLWGLALY)

291-309 (19) : AVDGIAYYLSTPYGLSSVI
    DG = -0.57 for segment sequence
    DG = -0.57 for most favorable 19AA centered at #300S
    (AVDGIAYYLSTPYGLSSVI)

316-334 (19) : IVYMIAMIITCVMFGIFWV
    DG = -18.37 for segment sequence
    DG = -18.37 for most favorable 19AA centered at #325T
    (IVYMIAMIITCVMFGIFWV)

376-422 (47) : LTVMSSAFVGFLATIANFIGALGGGTGVLLTVSIVYRMYEQLLREKV
    DG = -7.62 for segment sequence
    DG = -8.68 for most favorable 19AA centered at #385G
    (LTVMSSAFVGFLATIANFI)


Number of known transmembrane regions: 0


Working sequence (length = 435):

KKLIPILEKIPEVELPVKEITFKEKLKWTGIVLVLYFIMGCIDVYTAGAQIPAIFEFWQT
ITASRIGTLITLGIGPIVTAGIIMQLLVGSGIIQMDLSIPENRALFQGCQKLLSIIMCFV
EAVLFVGAGAFGILTPLLAFLVIIQIAFGSIILIYLDEIVSKYGIGSGIGLFIAAGVSQT
IFVGALGPEGYLWKFLNSLIQGVPNIEYIAPIIGTIIVFLMVVYAECMRVEIPLAhGRIK
GAVGKYPIKFVYVSNIPVILAAALFANIQLWGLALYRMGIPILGhYEGGRAVDGIAYYLS
TPYGLSSVISDPIhAIVYMIAMIITCVMFGIFWVETTGLDPKSMAKRIGSLGMAIKGFRK
SEKAIEhRLKRYIPPLTVMSSAFVGFLATIANFIGALGGGTGVLLTVSIVYRMYEQLLRE
KVSELhPAIAKLLNK

