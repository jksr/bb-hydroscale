Results summary for protein: id10066_cyt._b6f_complex,_cytochrome_f
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 26 Neutral
    His 143 Neutral
    His 150 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

68-86 (19) : VGLNVGAVLMLPEGFKIAP
    DG = -3.66 for segment sequence
    DG = -3.66 for most favorable 19AA centered at #77M
    (VGLNVGAVLMLPEGFKIAP)

114-132 (19) : LLVGPLPGEQYQEIVFPVL
    DG = -4.45 for segment sequence
    DG = -4.45 for most favorable 19AA centered at #123Q
    (LLVGPLPGEQYQEIVFPVL)

257-275 (19) : WMIAFICLVMLAQLMLILK
    DG = -17.93 for segment sequence
    DG = -17.93 for most favorable 19AA centered at #266M
    (WMIAFICLVMLAQLMLILK)


Number of known transmembrane regions: 0


Working sequence (length = 289):

YPFWAQQTYPPTPREPTGRIVCANChLAAKPAEVEVPQSVLPDTVFKAVVKIPYDTKLQQ
VAADGSKVGLNVGAVLMLPEGFKIAPEERIPEELKKEVGDVYFQPYKEGQDNVLLVGPLP
GEQYQEIVFPVLSPNPTTDKNIhFGKYAIhLGANRGRGQIYPTGEKSNNNVFTASATGTI
TKIAKEEDEYGNVKYQVSIQTDSGKTVVDTIPAGPELIVSEGQAVKAGEALTNNPNVGGF
GQDDTEIVLQDPNRVKWMIAFICLVMLAQLMLILKKKQVEKVQAAEMNF

