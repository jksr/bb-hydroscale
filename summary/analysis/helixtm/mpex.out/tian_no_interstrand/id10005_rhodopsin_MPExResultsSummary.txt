Results summary for protein: id10005_rhodopsin
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 65 Neutral
    His 100 Neutral
    His 152 Neutral
    His 195 Neutral
    His 211 Neutral
    His 278 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

39-68 (30) : MLAAYMFLLIMLGFPINFLTLYVTVQhKKL
    DG = -15.84 for segment sequence
    DG = -19.3 for most favorable 19AA centered at #48I
    (MLAAYMFLLIMLGFPINFL)

74-143 (70) : YILLNLAVADLFMVFGGFTTTLYTSLhGYFVFGPTGCNLEGFFATLGGEIALWSLVVLAIERYVVVCKPM
    DG = -18.58 for segment sequence
    DG = -12.69 for most favorable 19AA centered at #124A
    (FFATLGGEIALWSLVVLAI)

154-172 (19) : IMGVAFTWVMALACAAPPL
    DG = -9.26 for segment sequence
    DG = -9.26 for most favorable 19AA centered at #163M
    (IMGVAFTWVMALACAAPPL)

203-234 (32) : FVIYMFVVhFIIPLIVIFFCYGQLVFTVKEAA
    DG = -22.37 for segment sequence
    DG = -24.34 for most favorable 19AA centered at #212F
    (FVIYMFVVhFIIPLIVIFF)

253-271 (19) : MVIIMVIAFLICWLPYAGV
    DG = -18.13 for segment sequence
    DG = -18.13 for most favorable 19AA centered at #262L
    (MVIIMVIAFLICWLPYAGV)

272-301 (30) : AFYIFThQGSDFGPIFMTIPAFFAKTSAVY
    DG = -0.51 for segment sequence
    DG = -4.26 for most favorable 19AA centered at #292A
    (FGPIFMTIPAFFAKTSAVY)


Number of known transmembrane regions: 0


Working sequence (length = 348):

MNGTEGPNFYVPFSNKTGVVRSPFEAPQYYLAEPWQFSMLAAYMFLLIMLGFPINFLTLY
VTVQhKKLRTPLNYILLNLAVADLFMVFGGFTTTLYTSLhGYFVFGPTGCNLEGFFATLG
GEIALWSLVVLAIERYVVVCKPMSNFRFGENhAIMGVAFTWVMALACAAPPLVGWSRYIP
EGMQCSCGIDYYTPhEETNNESFVIYMFVVhFIIPLIVIFFCYGQLVFTVKEAAAQQQES
ATTQKAEKEVTRMVIIMVIAFLICWLPYAGVAFYIFThQGSDFGPIFMTIPAFFAKTSAV
YNPVIYIMMNKQFRNCMVTTLCCGKNPLGDDEASTTVSKTETSQVAPA

