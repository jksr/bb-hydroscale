Results summary for protein: id10064_cyt._b6f_complex,_cyto_b6_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 29 Neutral
    His 86 Neutral
    His 100 Neutral
    His 187 Neutral
    His 202 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

30-62 (33) : VNIFYCLGGITLTCFLIQFATGFAMTFYYKPTV
    DG = -9.34 for segment sequence
    DG = -10.12 for most favorable 19AA centered at #39I
    (VNIFYCLGGITLTCFLIQF)

64-82 (19) : EAYASVQYIMNEVSFGWLI
    DG = -0.41 for segment sequence
    DG = -0.41 for most favorable 19AA centered at #73M
    (EAYASVQYIMNEVSFGWLI)

92-110 (19) : MMVLMMILhVFRVYLTGGF
    DG = -14.34 for segment sequence
    DG = -14.34 for most favorable 19AA centered at #101V
    (MMVLMMILhVFRVYLTGGF)

116-134 (19) : LTWISGVILAVITVSFGVT
    DG = -9.5 for segment sequence
    DG = -9.5 for most favorable 19AA centered at #125A
    (LTWISGVILAVITVSFGVT)

147-175 (29) : AVKIVSGVPEAIPVVGVLISDLLRGGSSV
    DG = 0.21 for segment sequence
    DG = -5.57 for most favorable 19AA centered at #156E
    (AVKIVSGVPEAIPVVGVLI)

188-206 (19) : TFVLPWLIAVFMLLhFLMI
    DG = -21.91 for segment sequence
    DG = -21.91 for most favorable 19AA centered at #197V
    (TFVLPWLIAVFMLLhFLMI)


Number of known transmembrane regions: 0


Working sequence (length = 215):

MANVYDWFQERLEIQALADDVTSKYVPPhVNIFYCLGGITLTCFLIQFATGFAMTFYYKP
TVTEAYASVQYIMNEVSFGWLIRSIhRWSASMMVLMMILhVFRVYLTGGFKKPRELTWIS
GVILAVITVSFGVTGYSLPWDQVGYWAVKIVSGVPEAIPVVGVLISDLLRGGSSVGQATL
TRYYSAhTFVLPWLIAVFMLLhFLMIRKQGISGPL

