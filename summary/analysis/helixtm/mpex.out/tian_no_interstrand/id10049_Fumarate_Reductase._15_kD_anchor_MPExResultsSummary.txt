Results summary for protein: id10049_Fumarate_Reductase._15_kD_anchor
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 83 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

32-61 (30) : TAVPAVWFSIELIFGLFALKNGPEAWAGFV
    DG = -6.17 for segment sequence
    DG = -12.27 for most favorable 19AA centered at #41I
    (TAVPAVWFSIELIFGLFAL)

64-92 (29) : LQNPVIVIINLITLAAALLhTKTWFELAP
    DG = -8.05 for segment sequence
    DG = -12.49 for most favorable 19AA centered at #73N
    (LQNPVIVIINLITLAAALL)

113-131 (19) : LWAVTVVATIVILFVALYW
    DG = -18.19 for segment sequence
    DG = -18.19 for most favorable 19AA centered at #122I
    (LWAVTVVATIVILFVALYW)


Number of known transmembrane regions: 0


Working sequence (length = 131):

MTTKRKPYVRPMTSTWWKKLPFYRFYMLREGTAVPAVWFSIELIFGLFALKNGPEAWAGF
VDFLQNPVIVIINLITLAAALLhTKTWFELAPKAANIIVKDEKMGPEPIIKSLWAVTVVA
TIVILFVALYW

