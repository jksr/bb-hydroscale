Results summary for protein: id10007_glycophorin_A
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 9 Neutral
    His 29 Neutral
    His 41 Neutral
    His 66 Neutral
    His 67 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

73-91 (19) : ITLIIFGVMAGVIGTILLI
    DG = -18.14 for segment sequence
    DG = -18.14 for most favorable 19AA centered at #82A
    (ITLIIFGVMAGVIGTILLI)


Number of known transmembrane regions: 0


Working sequence (length = 131):

SSTTGVAMhTSTSSSVTKSYISSQTNDThKRDTYAATPRAhEVSEISVRTVYPPEEETGE
RVQLAhhFSEPEITLIIFGVMAGVIGTILLISYGIRRLIKKSPSDVKPLPSPDTDVPLSS
VEIENPETSDQ

