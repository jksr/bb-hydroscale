Results summary for protein: id10035_multidrug_resistance_protein_EmrD
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 132 Neutral
    His 389 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 9

9-27 (19) : LLLMLVLLVAVGQMAQTIY
    DG = -15.78 for segment sequence
    DG = -15.78 for most favorable 19AA centered at #18A
    (LLLMLVLLVAVGQMAQTIY)

48-66 (19) : VMGAYLLTYGVSQLFYGPI
    DG = -6.83 for segment sequence
    DG = -6.83 for most favorable 19AA centered at #57G
    (VMGAYLLTYGVSQLFYGPI)

74-116 (43) : PVILVGMSIFMLATLVAVTTSSLTVLIAASAMQGMGTGVGGVM
    DG = -13.48 for segment sequence
    DG = -14.15 for most favorable 19AA centered at #83F
    (PVILVGMSIFMLATLVAVT)

136-154 (19) : LLNMGILVSPLLAPLIGGL
    DG = -12.84 for segment sequence
    DG = -12.84 for most favorable 19AA centered at #145P
    (LLNMGILVSPLLAPLIGGL)

165-183 (19) : YLFLLVLCAGVTFSMARWM
    DG = -11.96 for segment sequence
    DG = -11.96 for most favorable 19AA centered at #174G
    (YLFLLVLCAGVTFSMARWM)

208-263 (56) : GFNCYLLMLIGGLAGIAAFEACSGVLMGAVLGLSSMTVSILFILPIPAAFFGAWFA
    DG = -25.62 for segment sequence
    DG = -14.17 for most favorable 19AA centered at #254P
    (VSILFILPIPAAFFGAWFA)

273-291 (19) : LMWQSVICCLLAGLLMWIP
    DG = -12.85 for segment sequence
    DG = -12.85 for most favorable 19AA centered at #282L
    (LMWQSVICCLLAGLLMWIP)

302-357 (56) : LLVPAALFFFGAGMLFPLATSGAMEPFPFLAGTAGALVGGLQNIGSGVLASLSAML
    DG = -17.25 for segment sequence
    DG = -16.66 for most favorable 19AA centered at #311F
    (LLVPAALFFFGAGMLFPLA)

365-383 (19) : LGLLMTLMGLLIVLCWLPL
    DG = -21.4 for segment sequence
    DG = -21.4 for most favorable 19AA centered at #374L
    (LGLLMTLMGLLIVLCWLPL)


Number of known transmembrane regions: 0


Working sequence (length = 394):

MKRQRNVNLLLMLVLLVAVGQMAQTIYIPAIADMARDLNVREGAVQSVMGAYLLTYGVSQ
LFYGPISDRVGRRPVILVGMSIFMLATLVAVTTSSLTVLIAASAMQGMGTGVGGVMARTL
PRDLYERTQLRhANSLLNMGILVSPLLAPLIGGLLDTMWNWRACYLFLLVLCAGVTFSMA
RWMPETRPVDAPRTRLLTSYKTLFGNSGFNCYLLMLIGGLAGIAAFEACSGVLMGAVLGL
SSMTVSILFILPIPAAFFGAWFAGRPNKRFSTLMWQSVICCLLAGLLMWIPDWFGVMNVW
TLLVPAALFFFGAGMLFPLATSGAMEPFPFLAGTAGALVGGLQNIGSGVLASLSAMLPQT
GQGSLGLLMTLMGLLIVLCWLPLATRMShQGQPV

