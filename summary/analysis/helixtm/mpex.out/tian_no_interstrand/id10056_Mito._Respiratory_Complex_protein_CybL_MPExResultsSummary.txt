Results summary for protein: id10056_Mito._Respiratory_Complex_protein_CybL
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 26 Neutral
    His 42 Neutral
    His 70 Neutral
    His 98 Neutral
    His 105 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

23-41 (19) : LSPhITIYRWSLPMAMSIC
    DG = -1.21 for segment sequence
    DG = -1.21 for most favorable 19AA centered at #32W
    (LSPhITIYRWSLPMAMSIC)

46-107 (62) : GIALSAGVSLFGLSALLLPGNFEShLELVKSLCLGPTLIYTAKFGIVFPLMYhTWNGIRhLI
    DG = -9.81 for segment sequence
    DG = -9.38 for most favorable 19AA centered at #55L
    (GIALSAGVSLFGLSALLLP)

122-140 (19) : SGVVVLILTVLSSVGLAAM
    DG = -10.7 for segment sequence
    DG = -10.7 for most favorable 19AA centered at #131V
    (SGVVVLILTVLSSVGLAAM)


Number of known transmembrane regions: 0


Working sequence (length = 140):

LGTTAKEEMERFWNKNLGSNRPLSPhITIYRWSLPMAMSIChRGTGIALSAGVSLFGLSA
LLLPGNFEShLELVKSLCLGPTLIYTAKFGIVFPLMYhTWNGIRhLIWDLGKGLTIPQLT
QSGVVVLILTVLSSVGLAAM

