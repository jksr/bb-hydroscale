Results summary for protein: id10083_cyto._C_oxidase._Subunit_II
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 29 Neutral
    His 36 Neutral
    His 73 Neutral
    His 119 Neutral
    His 181 Neutral
    His 224 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

27-69 (43) : LAhDQQWLDhFVLYIITAVTIFVCLLLLICIVRFNRRANPVPA
    DG = -9.78 for segment sequence
    DG = -23.63 for most favorable 19AA centered at #46T
    (FVLYIITAVTIFVCLLLLI)

83-101 (19) : LVPVLILVAIGAFSLPILF
    DG = -19.39 for segment sequence
    DG = -19.39 for most favorable 19AA centered at #92I
    (LVPVLILVAIGAFSLPILF)

170-188 (19) : VLVQVTATDVIhAWTIPAF
    DG = -3.85 for segment sequence
    DG = -3.85 for most favorable 19AA centered at #179V
    (VLVQVTATDVIhAWTIPAF)


Number of known transmembrane regions: 0


Working sequence (length = 269):

QDVLGDLPVIGKPVNGGMNFQPASSPLAhDQQWLDhFVLYIITAVTIFVCLLLLICIVRF
NRRANPVPARFThNTPIEVIWTLVPVLILVAIGAFSLPILFRSQEMPNDPDLVIKAIGhQ
WYWSYEYPNDGVAFDALMLEKEALADAGYSEDEYLLATDNPVVVPVGKKVLVQVTATDVI
hAWTIPAFAVKQDAVPGRIAQLWFSVDQEGVYFGQCSELCGINhAYMPIVVKAVSQEKYE
AWLAGAKEEFAADASDYLPASPVKLASAE

