Results summary for protein: id10090_ubiquinol_oxidase_subunit_II
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 84 Neutral
    His 112 Neutral
    His 122 Neutral
    His 194 Neutral
    His 284 Neutral
    His 297 Neutral
    His 300 Neutral
    His 310 Neutral
    His 315 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

12-30 (19) : WLSLFAGTVLLSGCNSALL
    DG = -6.39 for segment sequence
    DG = -6.39 for most favorable 19AA centered at #21L
    (WLSLFAGTVLLSGCNSALL)

43-61 (19) : LILTAFGLMLIVVIPAILM
    DG = -21.9 for segment sequence
    DG = -21.9 for most favorable 19AA centered at #52L
    (LILTAFGLMLIVVIPAILM)

90-108 (19) : AVVWTVPILIIIFLAVLTW
    DG = -19.12 for segment sequence
    DG = -19.12 for most favorable 19AA centered at #99I
    (AVVWTVPILIIIFLAVLTW)

126-156 (31) : PITIEVVSMDWKWFFIYPEQGIATVNEIAFP
    DG = 1.72 for segment sequence
    DG = -1.2 for most favorable 19AA centered at #147I
    (WFFIYPEQGIATVNEIAFP)

179-197 (19) : LGSQIYAMAGMQTRLhLIA
    DG = -0.87 for segment sequence
    DG = -0.87 for most favorable 19AA centered at #188G
    (LGSQIYAMAGMQTRLhLIA)


Number of known transmembrane regions: 0


Working sequence (length = 315):

MRLRKYNKSLGWLSLFAGTVLLSGCNSALLDPKGQIGLEQRSLILTAFGLMLIVVIPAIL
MAVGFAWKYRASNKDAKYSPNWShSNKVEAVVWTVPILIIIFLAVLTWKTThALEPSKPL
AhDEKPITIEVVSMDWKWFFIYPEQGIATVNEIAFPANTPVYFKVTSNSVMNSFFIPRLG
SQIYAMAGMQTRLhLIANEPGTYDGISASYSGPGFSGMKFKAIATPDRAAFDQWVAKAKQ
SPNTMSDMAAFEKLAAPSEYNQVEYFSNVKPDLFADVINKFMAhGKSMDMTQPEGEhSAh
EGMEGMDMShAESAh

