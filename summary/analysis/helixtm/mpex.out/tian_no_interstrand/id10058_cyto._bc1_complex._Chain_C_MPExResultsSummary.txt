Results summary for protein: id10058_cyto._bc1_complex._Chain_C
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 8 Neutral
    His 54 Neutral
    His 68 Neutral
    His 83 Neutral
    His 97 Neutral
    His 182 Neutral
    His 196 Neutral
    His 201 Neutral
    His 221 Neutral
    His 267 Neutral
    His 308 Neutral
    His 345 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

36-54 (19) : LLGICLILQILTGLFLAMh
    DG = -16.92 for segment sequence
    DG = -16.92 for most favorable 19AA centered at #45I
    (LLGICLILQILTGLFLAMh)

78-107 (30) : IIRYMhANGASMFFICLYMhVGRGLYYGSY
    DG = -3.02 for segment sequence
    DG = -6.31 for most favorable 19AA centered at #98V
    (MFFICLYMhVGRGLYYGSY)

115-167 (53) : IGVILLLTVMATAFMGYVLPWGQMSFWGATVITNLLSAIPYIGTNLVEWIWGG
    DG = -17.98 for segment sequence
    DG = -16.97 for most favorable 19AA centered at #124M
    (IGVILLLTVMATAFMGYVL)

182-200 (19) : hFILPFIIMAIAMVhLLFL
    DG = -20.93 for segment sequence
    DG = -20.93 for most favorable 19AA centered at #191A
    (hFILPFIIMAIAMVhLLFL)

229-247 (19) : ILGALLLILALMLLVLFAP
    DG = -25.53 for segment sequence
    DG = -25.53 for most favorable 19AA centered at #238A
    (ILGALLLILALMLLVLFAP)

274-316 (43) : FLFAYAILRSIPNKLGGVLALAFSILILALIPLLhTSKQRSMM
    DG = -13.1 for segment sequence
    DG = -19.29 for most favorable 19AA centered at #297S
    (LGGVLALAFSILILALIPL)

320-377 (58) : LSQCLFWALVADLLTLTWIGGQPVEhPYITIGQLASVLYFLLILVLMPTAGTIENKLL
    DG = -17.85 for segment sequence
    DG = -18.91 for most favorable 19AA centered at #357L
    (ITIGQLASVLYFLLILVLM)


Number of known transmembrane regions: 0


Working sequence (length = 379):

MTNIRKShPLMKIVNNAFIDLPAPSNISSWWNFGSLLGICLILQILTGLFLAMhYTSDTT
TAFSSVThICRDVNYGWIIRYMhANGASMFFICLYMhVGRGLYYGSYTFLETWNIGVILL
LTVMATAFMGYVLPWGQMSFWGATVITNLLSAIPYIGTNLVEWIWGGFSVDKATLTRFFA
FhFILPFIIMAIAMVhLLFLhETGSNNPTGISSDVDKIPFhPYYTIKDILGALLLILALM
LLVLFAPDLLGDPDNYTPANPLNTPPhIKPEWYFLFAYAILRSIPNKLGGVLALAFSILI
LALIPLLhTSKQRSMMFRPLSQCLFWALVADLLTLTWIGGQPVEhPYITIGQLASVLYFL
LILVLMPTAGTIENKLLKW

