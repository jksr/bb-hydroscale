Results summary for protein: id10036_Na+_H+_antiporter
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 3 Neutral
    His 5 Neutral
    His 39 Neutral
    His 225 Neutral
    His 243 Neutral
    His 253 Neutral
    His 256 Neutral
    His 318 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 9

12-30 (19) : ASGGIILIIAAILAMIMAN
    DG = -11.52 for segment sequence
    DG = -11.52 for most favorable 19AA centered at #21A
    (ASGGIILIIAAILAMIMAN)

59-88 (30) : MLLWINDALMAVFFLLVGLEVKRELMQGSL
    DG = -11.54 for segment sequence
    DG = -18.04 for most favorable 19AA centered at #68M
    (MLLWINDALMAVFFLLVGL)

95-128 (34) : AFPVIAAIGGMIVPALLYLAFNYADPITREGWAI
    DG = -8.72 for segment sequence
    DG = -14.14 for most favorable 19AA centered at #104G
    (AFPVIAAIGGMIVPALLYL)

134-152 (19) : IAFALGVLALLGSRVPLAL
    DG = -12.75 for segment sequence
    DG = -12.75 for most favorable 19AA centered at #143L
    (IAFALGVLALLGSRVPLAL)

155-197 (43) : FLMALAIIDDLGAIIIIALFYTNDLSMASLGVAAVAIAVLAVL
    DG = -25.29 for segment sequence
    DG = -16.1 for most favorable 19AA centered at #164D
    (FLMALAIIDDLGAIIIIAL)

206-237 (32) : GVYILVGVVLWTAVLKSGVhATLAGVIVGFFI
    DG = -16.93 for segment sequence
    DG = -9.16 for most favorable 19AA centered at #215L
    (GVYILVGVVLWTAVLKSGV)

251-279 (29) : LEhVLhPWVAYLILPLFAFANAGVSLQGV
    DG = -10.56 for segment sequence
    DG = -13.29 for most favorable 19AA centered at #260A
    (LEhVLhPWVAYLILPLFAF)

281-322 (42) : LDGLTSILPLGIIAGLLIGKPLGISLFCWLALRLKLAhLPEG
    DG = -12.87 for segment sequence
    DG = -11.68 for most favorable 19AA centered at #301P
    (IIAGLLIGKPLGISLFCWL)

328-375 (48) : IMVVGILCGIGFTMSIFIASLAFGSVDPELINWAKLGILVGSISSAVI
    DG = -14.45 for segment sequence
    DG = -13.71 for most favorable 19AA centered at #337I
    (IMVVGILCGIGFTMSIFIA)


Number of known transmembrane regions: 0


Working sequence (length = 388):

MKhLhRFFSSDASGGIILIIAAILAMIMANSGATSGWYhDFLETPVQLRVGSLEINKNML
LWINDALMAVFFLLVGLEVKRELMQGSLASLRQAAFPVIAAIGGMIVPALLYLAFNYADP
ITREGWAIPAATDIAFALGVLALLGSRVPLALKIFLMALAIIDDLGAIIIIALFYTNDLS
MASLGVAAVAIAVLAVLNLCGARRTGVYILVGVVLWTAVLKSGVhATLAGVIVGFFIPLK
EKhGRSPAKRLEhVLhPWVAYLILPLFAFANAGVSLQGVTLDGLTSILPLGIIAGLLIGK
PLGISLFCWLALRLKLAhLPEGTTYQQIMVVGILCGIGFTMSIFIASLAFGSVDPELINW
AKLGILVGSISSAVIGYSWLRVRLRPSV

