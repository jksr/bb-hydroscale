Results summary for protein: id10111_Photosystem_II_PsbL_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

17-35 (19) : LYLGLLLILVLALLFSSYF
    DG = -22.76 for segment sequence
    DG = -22.76 for most favorable 19AA centered at #26V
    (LYLGLLLILVLALLFSSYF)


Number of known transmembrane regions: 0


Working sequence (length = 37):

MEPNPNRQPVELNRTSLYLGLLLILVLALLFSSYFFN

