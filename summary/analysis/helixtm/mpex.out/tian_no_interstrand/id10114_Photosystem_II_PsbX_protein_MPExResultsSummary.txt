Results summary for protein: id10114_Photosystem_II_PsbX_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

20-50 (31) : FIGLLSGAVVLGLTFAVLIAISQIDKVQRSL
    DG = -8.99 for segment sequence
    DG = -16.67 for most favorable 19AA centered at #29V
    (FIGLLSGAVVLGLTFAVLI)


Number of known transmembrane regions: 0


Working sequence (length = 50):

MIQSASSLLLTITPSLKGFFIGLLSGAVVLGLTFAVLIAISQIDKVQRSL

