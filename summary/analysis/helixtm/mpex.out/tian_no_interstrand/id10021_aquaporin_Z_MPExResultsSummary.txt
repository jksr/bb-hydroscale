Results summary for protein: id10021_aquaporin_Z
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 56 Neutral
    His 61 Neutral
    His 124 Neutral
    His 150 Neutral
    His 174 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

9-27 (19) : CFGTFWLVFGGCGSAVLAA
    DG = -5.75 for segment sequence
    DG = -5.75 for most favorable 19AA centered at #18G
    (CFGTFWLVFGGCGSAVLAA)

36-66 (31) : FAGVALAFGLTVLTMAFAVGhISGGhFNPAV
    DG = -7.84 for segment sequence
    DG = -13.24 for most favorable 19AA centered at #45L
    (FAGVALAFGLTVLTMAFAV)

84-102 (19) : YVIAQVVGGIVAAALLYLI
    DG = -14.24 for segment sequence
    DG = -14.24 for most favorable 19AA centered at #93I
    (YVIAQVVGGIVAAALLYLI)

131-149 (19) : MLSALVVELVLSAGFLLVI
    DG = -17.01 for segment sequence
    DG = -17.01 for most favorable 19AA centered at #140V
    (MLSALVVELVLSAGFLLVI)

161-179 (19) : FAPIAIGLALTLIhLISIP
    DG = -12.67 for segment sequence
    DG = -12.67 for most favorable 19AA centered at #170L
    (FAPIAIGLALTLIhLISIP)

205-223 (19) : LWFFWVVPIVGGIIGGLIY
    DG = -15.87 for segment sequence
    DG = -15.87 for most favorable 19AA centered at #214V
    (LWFFWVVPIVGGIIGGLIY)


Number of known transmembrane regions: 0


Working sequence (length = 231):

MFRKLAAECFGTFWLVFGGCGSAVLAAGFPELGIGFAGVALAFGLTVLTMAFAVGhISGG
hFNPAVTIGLWAGGRFPAKEVVGYVIAQVVGGIVAAALLYLIASGKTGFDAAASGFASNG
YGEhSPGGYSMLSALVVELVLSAGFLLVIhGATDKFAPAGFAPIAIGLALTLIhLISIPV
TNTSVNPARSTAVAIFQGGWALEQLWFFWVVPIVGGIIGGLIYRTLLEKRD

