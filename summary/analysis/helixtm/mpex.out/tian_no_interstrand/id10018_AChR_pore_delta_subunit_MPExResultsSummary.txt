Results summary for protein: id10018_AChR_pore_delta_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 20 Neutral
    His 26 Neutral
    His 60 Neutral
    His 65 Neutral
    His 194 Neutral
    His 313 Neutral
    His 320 Neutral
    His 339 Neutral
    His 390 Neutral
    His 416 Neutral
    His 497 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

30-48 (19) : VVNIALSLTLSNLISLKET
    DG = -1.18 for segment sequence
    DG = -1.18 for most favorable 19AA centered at #39L
    (VVNIALSLTLSNLISLKET)

77-95 (19) : ISILRLRPELIWIPDIVLQ
    DG = -5.29 for segment sequence
    DG = -5.29 for most favorable 19AA centered at #86L
    (ISILRLRPELIWIPDIVLQ)

104-139 (36) : VAYFCNVLVRPNGYVTWLPPAIFRSSCPINVLYFPF
    DG = -4 for segment sequence
    DG = -3.16 for most favorable 19AA centered at #130C
    (LPPAIFRSSCPINVLYFPF)

215-250 (36) : VTFYLIIRRKPLFYVINFITPCVLISFLAALAFYLP
    DG = -19.78 for segment sequence
    DG = -14.42 for most favorable 19AA centered at #241F
    (FITPCVLISFLAALAFYLP)

257-275 (19) : MSTAICVLLAQAVFLLLTS
    DG = -10.75 for segment sequence
    DG = -10.75 for most favorable 19AA centered at #266A
    (MSTAICVLLAQAVFLLLTS)

282-310 (29) : ALAVPLIGKYLMFIMSLVTGVVVNCGIVL
    DG = -16.93 for segment sequence
    DG = -13.76 for most favorable 19AA centered at #301G
    (LMFIMSLVTGVVVNCGIVL)

456-474 (19) : LSMFIITPVMVLGTIFIFV
    DG = -17.64 for segment sequence
    DG = -17.64 for most favorable 19AA centered at #465M
    (LSMFIITPVMVLGTIFIFV)


Number of known transmembrane regions: 0


Working sequence (length = 501):

VNEEERLINDLLIVNKYNKhVRPVKhNNEVVNIALSLTLSNLISLKETDETLTTNVWMDh
AWYDhRLTWNASEYSDISILRLRPELIWIPDIVLQNNNDGQYNVAYFCNVLVRPNGYVTW
LPPAIFRSSCPINVLYFPFDWQNCSLKFTALNYNANEISMDLMTDTIDGKDYPIEWIIID
PEAFTENGEWEIIhKPAKKNIYGDKFPNGTNYQDVTFYLIIRRKPLFYVINFITPCVLIS
FLAALAFYLPAESGEKMSTAICVLLAQAVFLLLTSQRLPETALAVPLIGKYLMFIMSLVT
GVVVNCGIVLNFhFRTPSThVLSTRVKQIFLEKLPRILhMSRVDEIEQPDWQNDLKLRRS
SSVGYISKAQEYFNIKSRSELMFEKQSERhGLVPRVTPRIGFGNNNENIAASDQLhDEIK
SGIDSTNYIVKQIKEKNAYDEEVGNWNLVGQTIDRLSMFIITPVMVLGTIFIFVMGNFNR
PPAKPFEGDPFDYSSDhPRCA

