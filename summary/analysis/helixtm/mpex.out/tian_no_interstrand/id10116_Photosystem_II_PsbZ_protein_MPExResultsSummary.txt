Results summary for protein: id10116_Photosystem_II_PsbZ_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

3-21 (19) : ILFQLALAALVILSFVMVI
    DG = -21.28 for segment sequence
    DG = -21.28 for most favorable 19AA centered at #12L
    (ILFQLALAALVILSFVMVI)

39-57 (19) : LIFLGSGLWIALVLVVGVL
    DG = -19.92 for segment sequence
    DG = -19.92 for most favorable 19AA centered at #48I
    (LIFLGSGLWIALVLVVGVL)


Number of known transmembrane regions: 0


Working sequence (length = 62):

MTILFQLALAALVILSFVMVIGVPVAYASPQDWDRSKQLIFLGSGLWIALVLVVGVLNFF
VV

