Results summary for protein: id10045_Metalloenzyme_pMMO_subunit_pmoC
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 2 Neutral
    His 21 Neutral
    His 134 Neutral
    His 160 Neutral
    His 173 Neutral
    His 231 Neutral
    His 245 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

50-78 (29) : WLTFALAIYTVFYLWVRWYEGVYGWSAGL
    DG = -5.13 for segment sequence
    DG = -8.39 for most favorable 19AA centered at #59T
    (WLTFALAIYTVFYLWVRWY)

93-111 (19) : LYTEIVLEIVTASILWGYL
    DG = -5.1 for segment sequence
    DG = -5.1 for most favorable 19AA centered at #102V
    (LYTEIVLEIVTASILWGYL)

135-153 (19) : LVWLVAYAWAIYWGASYFT
    DG = -5.95 for segment sequence
    DG = -5.95 for most favorable 19AA centered at #144A
    (LVWLVAYAWAIYWGASYFT)

177-195 (19) : FYLSYPIYIITGFAAFIYA
    DG = -9.93 for segment sequence
    DG = -9.93 for most favorable 19AA centered at #186I
    (FYLSYPIYIITGFAAFIYA)

208-226 (19) : LPYLVLVVGPFMILPNVGL
    DG = -17.21 for segment sequence
    DG = -17.21 for most favorable 19AA centered at #217P
    (LPYLVLVVGPFMILPNVGL)

239-257 (19) : LFVAPLhYGFVIFGWLALA
    DG = -13.43 for segment sequence
    DG = -13.43 for most favorable 19AA centered at #248F
    (LFVAPLhYGFVIFGWLALA)


Number of known transmembrane regions: 0


Working sequence (length = 289):

MhETKQGGEKRFTGAICRCShRYNSMEVKMAATTIGGAAAAEAPLLDKKWLTFALAIYTV
FYLWVRWYEGVYGWSAGLDSFAPEFETYWMNFLYTEIVLEIVTASILWGYLWKTRDRNLA
ALTPREELRRNFThLVWLVAYAWAIYWGASYFTEQDGTWhQTIVRDTDFTPShIIEFYLS
YPIYIITGFAAFIYAKTRLPFFAKGISLPYLVLVVGPFMILPNVGLNEWGhTFWFMEELF
VAPLhYGFVIFGWLALAVMGTLTQTFYSFAQGGLGQSLCEAVDEGLIAK

