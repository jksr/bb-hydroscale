Results summary for protein: id10029_ClC_chloride_channel
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 74 Neutral
    His 175 Neutral
    His 284 Neutral
    His 383 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 9

32-50 (19) : PLAILFMAAVVGTLTGLVG
    DG = -10.83 for segment sequence
    DG = -10.83 for most favorable 19AA centered at #41V
    (PLAILFMAAVVGTLTGLVG)

77-105 (29) : LLWPLAFILSALLAMVGYFLVRKFAPEAG
    DG = -13.28 for segment sequence
    DG = -16.1 for most favorable 19AA centered at #86S
    (LLWPLAFILSALLAMVGYF)

127-145 (19) : VLPVKFIGGMGTLGAGMVL
    DG = -5.9 for segment sequence
    DG = -5.9 for most favorable 19AA centered at #136M
    (VLPVKFIGGMGTLGAGMVL)

183-212 (30) : AAGLSAAFNAPLAGILFIIEEMRPQFRYNL
    DG = 0.37 for segment sequence
    DG = -7.72 for most favorable 19AA centered at #192A
    (AAGLSAAFNAPLAGILFII)

221-239 (19) : GVIMSSIVFRIFNGEAPII
    DG = -1.17 for segment sequence
    DG = -1.17 for most favorable 19AA centered at #230R
    (GVIMSSIVFRIFNGEAPII)

251-269 (19) : TLWLYLILGIIFGCVGPVF
    DG = -14.06 for segment sequence
    DG = -14.06 for most favorable 19AA centered at #260I
    (TLWLYLILGIIFGCVGPVF)

292-348 (57) : VLMGGAIGGLCGILGLIEPAAAGGGFNLIPIAAAGNFSVGLLLFIFITRVVTTLLCF
    DG = -17.71 for segment sequence
    DG = -12.28 for most favorable 19AA centered at #339T
    (VGLLLFIFITRVVTTLLCF)

356-394 (39) : IFAPMLALGTLLGTAFGMAAAVLFPQYhLEAGTFAIAGM
    DG = -12.65 for segment sequence
    DG = -9.88 for most favorable 19AA centered at #365T
    (IFAPMLALGTLLGTAFGMA)

395-450 (56) : GALMAASVRAPLTGIVLVLEMTDNYQLILPMIITCLGATLLAQFLGGKPLYSTILA
    DG = -9.89 for segment sequence
    DG = -13.44 for most favorable 19AA centered at #430L
    (LILPMIITCLGATLLAQFL)


Number of known transmembrane regions: 0


Working sequence (length = 473):

MKTDTSTFLAQQIVRLRRRDQIRRLMQRDKTPLAILFMAAVVGTLTGLVGVAFEKAVSWV
QNMRIGALVQVADhAFLLWPLAFILSALLAMVGYFLVRKFAPEAGGSGIPEIEGALEELR
PVRWWRVLPVKFIGGMGTLGAGMVLGREGPTVQIGGNLGRMVLDVFRMRSAEARhTLLAT
GAAAGLSAAFNAPLAGILFIIEEMRPQFRYNLISIKAVFTGVIMSSIVFRIFNGEAPIIE
VGKLSDAPVNTLWLYLILGIIFGCVGPVFNSLVLRTQDMFQRFhGGEIKKWVLMGGAIGG
LCGILGLIEPAAAGGGFNLIPIAAAGNFSVGLLLFIFITRVVTTLLCFSSGAPGGIFAPM
LALGTLLGTAFGMAAAVLFPQYhLEAGTFAIAGMGALMAASVRAPLTGIVLVLEMTDNYQ
LILPMIITCLGATLLAQFLGGKPLYSTILARTLAKQDAEQAEKNQNAPADENT

