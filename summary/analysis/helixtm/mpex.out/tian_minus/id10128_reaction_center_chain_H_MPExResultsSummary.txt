Results summary for protein: id10128_reaction_center_chain_H
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 7 Neutral
    His 71 Neutral
    His 122 Neutral
    His 218 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

12-30 (19) : AQITIWAFWLFFFGLIIYL
    DG = -13.06 for segment sequence
    DG = -13.06 for most favorable 19AA centered at #21L
    (AQITIWAFWLFFFGLIIYL)

84-117 (34) : PVAVNATPFSPAPGSPLVPNGDPMLSGFGPAASP
    DG = -4.03 for segment sequence
    DG = -7.31 for most favorable 19AA centered at #93S
    (PVAVNATPFSPAPGSPLVP)


Number of known transmembrane regions: 0


Working sequence (length = 259):

MPAGIThYIDAAQITIWAFWLFFFGLIIYLRREDKREGYPLDSNRTERSGGRYKVVGFPD
LPDPKTFVLPhNGGTVVAPRVEAPVAVNATPFSPAPGSPLVPNGDPMLSGFGPAASPDRP
KhCDLTFEGLPKIVPMRVAKEFSIAEGDPDPRGMTVVGLDGEVAGTVSDVWVDRSEPQIR
YLEVEVAANKKKVLLPIGFSRFDKKARKVKVDAIKAAhFANVPTLSNPDQVTLYEEDKVC
AYYAGGKLYATAERAGPLL

