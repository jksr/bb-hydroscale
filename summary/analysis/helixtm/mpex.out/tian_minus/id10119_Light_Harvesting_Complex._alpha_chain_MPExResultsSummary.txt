Results summary for protein: id10119_Light_Harvesting_Complex._alpha_chain
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 34 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

19-48 (30) : LPVIWIVATVVAIAVhAAVLAAPGSNWIAL
    DG = -12.62 for segment sequence
    DG = -11.07 for most favorable 19AA centered at #28V
    (LPVIWIVATVVAIAVhAAV)


Number of known transmembrane regions: 0


Working sequence (length = 56):

SNPKDDYKIWLVINPSTWLPVIWIVATVVAIAVhAAVLAAPGSNWIALGAAKSAAK

