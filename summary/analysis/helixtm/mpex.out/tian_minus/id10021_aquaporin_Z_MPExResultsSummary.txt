Results summary for protein: id10021_aquaporin_Z
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 56 Neutral
    His 61 Neutral
    His 124 Neutral
    His 150 Neutral
    His 174 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

12-30 (19) : TFWLVFGGCGSAVLAAGFP
    DG = -4.8 for segment sequence
    DG = -4.8 for most favorable 19AA centered at #21G
    (TFWLVFGGCGSAVLAAGFP)

36-54 (19) : FAGVALAFGLTVLTMAFAV
    DG = -9.65 for segment sequence
    DG = -9.65 for most favorable 19AA centered at #45L
    (FAGVALAFGLTVLTMAFAV)

84-102 (19) : YVIAQVVGGIVAAALLYLI
    DG = -10.31 for segment sequence
    DG = -10.31 for most favorable 19AA centered at #93I
    (YVIAQVVGGIVAAALLYLI)

131-149 (19) : MLSALVVELVLSAGFLLVI
    DG = -11.87 for segment sequence
    DG = -11.87 for most favorable 19AA centered at #140V
    (MLSALVVELVLSAGFLLVI)

161-179 (19) : FAPIAIGLALTLIhLISIP
    DG = -13.26 for segment sequence
    DG = -13.26 for most favorable 19AA centered at #170L
    (FAPIAIGLALTLIhLISIP)

205-223 (19) : LWFFWVVPIVGGIIGGLIY
    DG = -13.5 for segment sequence
    DG = -13.5 for most favorable 19AA centered at #214V
    (LWFFWVVPIVGGIIGGLIY)


Number of known transmembrane regions: 0


Working sequence (length = 231):

MFRKLAAECFGTFWLVFGGCGSAVLAAGFPELGIGFAGVALAFGLTVLTMAFAVGhISGG
hFNPAVTIGLWAGGRFPAKEVVGYVIAQVVGGIVAAALLYLIASGKTGFDAAASGFASNG
YGEhSPGGYSMLSALVVELVLSAGFLLVIhGATDKFAPAGFAPIAIGLALTLIhLISIPV
TNTSVNPARSTAVAIFQGGWALEQLWFFWVVPIVGGIIGGLIYRTLLEKRD

