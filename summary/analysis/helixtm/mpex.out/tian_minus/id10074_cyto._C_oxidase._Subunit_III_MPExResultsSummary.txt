Results summary for protein: id10074_cyto._C_oxidase._Subunit_III
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 3 Neutral
    His 6 Neutral
    His 9 Neutral
    His 36 Neutral
    His 70 Neutral
    His 71 Neutral
    His 103 Neutral
    His 122 Neutral
    His 148 Neutral
    His 149 Neutral
    His 158 Neutral
    His 204 Neutral
    His 207 Neutral
    His 226 Neutral
    His 231 Neutral
    His 232 Neutral
    His 243 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

15-58 (44) : PWPLTGALSALLMTSGLTMWFhFNSMTLLMIGLTTNMLTMYQWW
    DG = -5.5 for segment sequence
    DG = -5.64 for most favorable 19AA centered at #24A
    (PWPLTGALSALLMTSGLTM)

83-112 (30) : MILFIISEVLFFTGFFWAFYhSSLAPTPEL
    DG = -10.47 for segment sequence
    DG = -11.89 for most favorable 19AA centered at #92L
    (MILFIISEVLFFTGFFWAF)

114-144 (31) : GCWPPTGIhPLNPLEVPLLNTSVLLASGVSI
    DG = -5.36 for segment sequence
    DG = -8.14 for most favorable 19AA centered at #123P
    (GCWPPTGIhPLNPLEVPLL)

158-176 (19) : hMLQALFITITLGVYFTLL
    DG = -9.18 for segment sequence
    DG = -9.18 for most favorable 19AA centered at #167I
    (hMLQALFITITLGVYFTLL)

202-220 (19) : GFhGLhVIIGSTFLIVCFF
    DG = -6.15 for segment sequence
    DG = -6.15 for most favorable 19AA centered at #211G
    (GFhGLhVIIGSTFLIVCFF)

240-258 (19) : WYWhFVDVVWLFLYVSIYW
    DG = -6.73 for segment sequence
    DG = -6.73 for most favorable 19AA centered at #249W
    (WYWhFVDVVWLFLYVSIYW)


Number of known transmembrane regions: 0


Working sequence (length = 261):

MThQThAYhMVNPSPWPLTGALSALLMTSGLTMWFhFNSMTLLMIGLTTNMLTMYQWWRD
VIRESTFQGhhTPAVQKGLRYGMILFIISEVLFFTGFFWAFYhSSLAPTPELGGCWPPTG
IhPLNPLEVPLLNTSVLLASGVSITWAhhSLMEGDRKhMLQALFITITLGVYFTLLQASE
YYEAPFTISDGVYGSTFFVATGFhGLhVIIGSTFLIVCFFRQLKFhFTSNhhFGFEAGAW
YWhFVDVVWLFLYVSIYWWGS

