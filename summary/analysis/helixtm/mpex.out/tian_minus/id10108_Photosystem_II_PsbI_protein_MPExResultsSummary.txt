Results summary for protein: id10108_Photosystem_II_PsbI_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

6-24 (19) : ITVYIVVTFFVLLFVFGFL
    DG = -17.36 for segment sequence
    DG = -17.36 for most favorable 19AA centered at #15F
    (ITVYIVVTFFVLLFVFGFL)


Number of known transmembrane regions: 0


Working sequence (length = 38):

METLKITVYIVVTFFVLLFVFGFLSGDPARNPKRKDLE

