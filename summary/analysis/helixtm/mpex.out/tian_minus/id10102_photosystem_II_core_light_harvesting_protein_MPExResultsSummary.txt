Results summary for protein: id10102_photosystem_II_core_light_harvesting_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 9 Neutral
    His 23 Neutral
    His 26 Neutral
    His 100 Neutral
    His 114 Neutral
    His 142 Neutral
    His 157 Neutral
    His 201 Neutral
    His 202 Neutral
    His 216 Neutral
    His 343 Neutral
    His 455 Neutral
    His 466 Neutral
    His 469 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

16-69 (54) : PGRLIAAhLMhTALVAGWAGSMALYELATFDPSDPVLNPMWRQGMFVLPFMARL
    DG = -2.76 for segment sequence
    DG = -5.38 for most favorable 19AA centered at #60M
    (VLNPMWRQGMFVLPFMARL)

95-123 (29) : GVALAhIVLSGLLFLAACWhWVYWDLELF
    DG = -8.48 for segment sequence
    DG = -8.59 for most favorable 19AA centered at #104S
    (GVALAhIVLSGLLFLAACW)

138-176 (39) : MFGIhLFLAGLLCFGFGAFhLTGLFGPGMWVSDPYGLTG
    DG = -10.18 for segment sequence
    DG = -10.75 for most favorable 19AA centered at #147G
    (MFGIhLFLAGLLCFGFGAF)

204-232 (29) : AAGIVGIIAGLFhILVRPPQRLYKALRMG
    DG = -2.71 for segment sequence
    DG = -8.9 for most favorable 19AA centered at #213G
    (AAGIVGIIAGLFhILVRPP)

241-270 (30) : SIAAVFFAAFVVAGTMWYGSATTPIELFGP
    DG = -5.79 for segment sequence
    DG = -5.01 for most favorable 19AA centered at #250F
    (SIAAVFFAAFVVAGTMWYG)

353-371 (19) : ELFVRRMPAFFESFPVILT
    DG = -4.18 for segment sequence
    DG = -4.18 for most favorable 19AA centered at #362F
    (ELFVRRMPAFFESFPVILT)

450-468 (19) : WFTFAhAVFALLFFFGhIW
    DG = -9.52 for segment sequence
    DG = -9.52 for most favorable 19AA centered at #459A
    (WFTFAhAVFALLFFFGhIW)


Number of known transmembrane regions: 0


Working sequence (length = 510):

MGLPWYRVhTVLINDPGRLIAAhLMhTALVAGWAGSMALYELATFDPSDPVLNPMWRQGM
FVLPFMARLGVTGSWSGWSITGETGIDPGFWSFEGVALAhIVLSGLLFLAACWhWVYWDL
ELFRDPRTGEPALDLPKMFGIhLFLAGLLCFGFGAFhLTGLFGPGMWVSDPYGLTGSVQP
VAPEWGPDGFNPYNPGGVVAhhIAAGIVGIIAGLFhILVRPPQRLYKALRMGNIETVLSS
SIAAVFFAAFVVAGTMWYGSATTPIELFGPTRYQWDSSYFQQEINRRVQASLASGATLEE
AWSAIPEKLAFYDYIGNNPAKGGLFRTGPMNKGDGIAQAWKGhAVFRNKEGEELFVRRMP
AFFESFPVILTDKNGVVKADIPFRRAESKYSFEQQGVTVSFYGGELNGQTFTDPPTVKSY
ARKAIFGEIFEFDTETLNSDGIFRTSPRGWFTFAhAVFALLFFFGhIWhGARTLFRDVFS
GIDPELSPEQVEWGFYQKVGDVTTRRKEAV

