Results summary for protein: id10040_F-type_Na+-ATPase_Subunit_C
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

10-28 (19) : VLAASAVGAGTAMIAGIGP
    DG = -2.59 for segment sequence
    DG = -2.59 for most favorable 19AA centered at #19G
    (VLAASAVGAGTAMIAGIGP)

69-87 (19) : YSLVIALILLYANPFVGLL
    DG = -13.86 for segment sequence
    DG = -13.86 for most favorable 19AA centered at #78L
    (YSLVIALILLYANPFVGLL)


Number of known transmembrane regions: 0


Working sequence (length = 88):

MDMLFAKTVVLAASAVGAGTAMIAGIGPGVGQGYAAGKAVESVARQPEAKGDIISTMVLG
AVAESTGIYSLVIALILLYANPFVGLLG

