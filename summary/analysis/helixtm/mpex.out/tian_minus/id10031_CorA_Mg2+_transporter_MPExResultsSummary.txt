Results summary for protein: id10031_CorA_Mg2+_transporter
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 68 Neutral
    His 83 Neutral
    His 94 Neutral
    His 120 Neutral
    His 212 Neutral
    His 257 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

76-94 (19) : VGEFFGIhPLVLEDILNVh
    DG = -0.73 for segment sequence
    DG = -0.73 for most favorable 19AA centered at #85L
    (VGEFFGIhPLVLEDILNVh)

97-115 (19) : PKVEFFENYVFIVLKMFTY
    DG = -0.21 for segment sequence
    DG = -0.21 for most favorable 19AA centered at #106V
    (PKVEFFENYVFIVLKMFTY)

168-186 (19) : YLLYSLIDALVDDYFVLLE
    DG = -3.34 for segment sequence
    DG = -3.34 for most favorable 19AA centered at #177L
    (YLLYSLIDALVDDYFVLLE)

225-243 (19) : IWPLREVLSSLYRDVPPLI
    DG = -3.34 for segment sequence
    DG = -3.34 for most favorable 19AA centered at #234S
    (IWPLREVLSSLYRDVPPLI)

293-321 (29) : VLTIIATIFMPLTFIAGIYGMNFEYMPEL
    DG = -11.41 for segment sequence
    DG = -12.75 for most favorable 19AA centered at #302M
    (VLTIIATIFMPLTFIAGIY)

327-345 (19) : YPVVLAVMGVIAVIMVVYF
    DG = -15.44 for segment sequence
    DG = -15.44 for most favorable 19AA centered at #336V
    (YPVVLAVMGVIAVIMVVYF)


Number of known transmembrane regions: 0


Working sequence (length = 351):

MEEKRLSAKKGLPPGTLVYTGKYREDFEIEVMNYSIEEFREFKTTDVESVLPFRDSSTPT
WINITGIhRTDVVQRVGEFFGIhPLVLEDILNVhQRPKVEFFENYVFIVLKMFTYDKNLh
ELESEQVSLILTKNCVLMFQEKIGDVFDPVRERIRYNRGIIRKKRADYLLYSLIDALVDD
YFVLLEKIDDEIDVLEEEVLERPEKETVQRThQLKRNLVELRKTIWPLREVLSSLYRDVP
PLIEKETVPYFRDVYDhTIQIADTVETFRDIVSGLLDVYLSSVSNKTNEVMKVLTIIATI
FMPLTFIAGIYGMNFEYMPELRWKWGYPVVLAVMGVIAVIMVVYFKKKKWL

