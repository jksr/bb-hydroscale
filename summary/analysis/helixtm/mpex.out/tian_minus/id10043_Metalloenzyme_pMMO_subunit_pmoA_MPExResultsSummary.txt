Results summary for protein: id10043_Metalloenzyme_pMMO_subunit_pmoA
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 11 Neutral
    His 38 Neutral
    His 40 Neutral
    His 168 Neutral
    His 232 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

25-43 (19) : ALFVVFFVIVGSYhIhAML
    DG = -9.53 for segment sequence
    DG = -9.53 for most favorable 19AA centered at #34V
    (ALFVVFFVIVGSYhIhAML)

61-79 (19) : VTVTPIVLVTFPAAVQSYL
    DG = -7.88 for segment sequence
    DG = -7.88 for most favorable 19AA centered at #70T
    (VTVTPIVLVTFPAAVQSYL)

84-102 (19) : RLPWGATVCVLGLLLGEWI
    DG = -5.94 for segment sequence
    DG = -5.94 for most favorable 19AA centered at #93V
    (RLPWGATVCVLGLLLGEWI)

113-142 (30) : YFPINFVFPASLVPGAIILDTVLMLSGSYL
    DG = -12.29 for segment sequence
    DG = -13.32 for most favorable 19AA centered at #122A
    (YFPINFVFPASLVPGAIIL)

153-183 (31) : GLIFYPGNWPIIAPLhVPVEYNGMLMSIADI
    DG = -7.54 for segment sequence
    DG = -11.76 for most favorable 19AA centered at #162P
    (GLIFYPGNWPIIAPLhVPV)

212-230 (19) : VAPVSAFFSAFMSILIYFM
    DG = -9.6 for segment sequence
    DG = -9.6 for most favorable 19AA centered at #221A
    (VAPVSAFFSAFMSILIYFM)


Number of known transmembrane regions: 0


Working sequence (length = 247):

MSAAQSAVRShAEAVQVSRTIDWMALFVVFFVIVGSYhIhAMLTMGDWDFWSDWKDRRLW
VTVTPIVLVTFPAAVQSYLWERYRLPWGATVCVLGLLLGEWINRYFNFWGWTYFPINFVF
PASLVPGAIILDTVLMLSGSYLFTAIVGAMGWGLIFYPGNWPIIAPLhVPVEYNGMLMSI
ADIQGYNYVRTGTPEYIRMVEKGTLRTFGKDVAPVSAFFSAFMSILIYFMWhFIGRWFSN
ERFLQST

