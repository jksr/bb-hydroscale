Results summary for protein: id10049_Fumarate_Reductase._15_kD_anchor
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 83 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

20-61 (42) : LPFYRFYMLREGTAVPAVWFSIELIFGLFALKNGPEAWAGFV
    DG = -5.92 for segment sequence
    DG = -10.18 for most favorable 19AA centered at #41I
    (TAVPAVWFSIELIFGLFAL)

64-92 (29) : LQNPVIVIINLITLAAALLhTKTWFELAP
    DG = -6.21 for segment sequence
    DG = -10.09 for most favorable 19AA centered at #73N
    (LQNPVIVIINLITLAAALL)

113-131 (19) : LWAVTVVATIVILFVALYW
    DG = -13.24 for segment sequence
    DG = -13.24 for most favorable 19AA centered at #122I
    (LWAVTVVATIVILFVALYW)


Number of known transmembrane regions: 0


Working sequence (length = 131):

MTTKRKPYVRPMTSTWWKKLPFYRFYMLREGTAVPAVWFSIELIFGLFALKNGPEAWAGF
VDFLQNPVIVIINLITLAAALLhTKTWFELAPKAANIIVKDEKMGPEPIIKSLWAVTVVA
TIVILFVALYW

