Results summary for protein: id10060_cyto._bc1_complex._Chain_G
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 6 Neutral
    His 12 Neutral
    His 28 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

41-59 (19) : LRACILRVAPPFLAFYLLY
    DG = -12.39 for segment sequence
    DG = -12.39 for most favorable 19AA centered at #50P
    (LRACILRVAPPFLAFYLLY)


Number of known transmembrane regions: 0


Working sequence (length = 81):

GRQFGhLTRVRhLITYSLSPFEQRPFPhYFSKGVPNVWRRLRACILRVAPPFLAFYLLYT
WGTQEFEKSKRKNPAAYVNDR

