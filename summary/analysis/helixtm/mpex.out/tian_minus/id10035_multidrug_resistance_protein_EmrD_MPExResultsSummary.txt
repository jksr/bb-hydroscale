Results summary for protein: id10035_multidrug_resistance_protein_EmrD
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 132 Neutral
    His 389 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 10

9-27 (19) : LLLMLVLLVAVGQMAQTIY
    DG = -11.27 for segment sequence
    DG = -11.27 for most favorable 19AA centered at #18A
    (LLLMLVLLVAVGQMAQTIY)

48-66 (19) : VMGAYLLTYGVSQLFYGPI
    DG = -5.75 for segment sequence
    DG = -5.75 for most favorable 19AA centered at #57G
    (VMGAYLLTYGVSQLFYGPI)

74-105 (32) : PVILVGMSIFMLATLVAVTTSSLTVLIAASAM
    DG = -11.25 for segment sequence
    DG = -11.92 for most favorable 19AA centered at #83F
    (PVILVGMSIFMLATLVAVT)

137-155 (19) : LNMGILVSPLLAPLIGGLL
    DG = -13.5 for segment sequence
    DG = -13.5 for most favorable 19AA centered at #146L
    (LNMGILVSPLLAPLIGGLL)

166-184 (19) : LFLLVLCAGVTFSMARWMP
    DG = -8.7 for segment sequence
    DG = -8.7 for most favorable 19AA centered at #175V
    (LFLLVLCAGVTFSMARWMP)

208-238 (31) : GFNCYLLMLIGGLAGIAAFEACSGVLMGAVL
    DG = -7.61 for segment sequence
    DG = -7.31 for most favorable 19AA centered at #217I
    (GFNCYLLMLIGGLAGIAAF)

245-263 (19) : VSILFILPIPAAFFGAWFA
    DG = -14.66 for segment sequence
    DG = -14.66 for most favorable 19AA centered at #254P
    (VSILFILPIPAAFFGAWFA)

273-291 (19) : LMWQSVICCLLAGLLMWIP
    DG = -10.71 for segment sequence
    DG = -10.71 for most favorable 19AA centered at #282L
    (LMWQSVICCLLAGLLMWIP)

302-342 (41) : LLVPAALFFFGAGMLFPLATSGAMEPFPFLAGTAGALVGGL
    DG = -20.51 for segment sequence
    DG = -17.69 for most favorable 19AA centered at #311F
    (LLVPAALFFFGAGMLFPLA)

365-383 (19) : LGLLMTLMGLLIVLCWLPL
    DG = -19.02 for segment sequence
    DG = -19.02 for most favorable 19AA centered at #374L
    (LGLLMTLMGLLIVLCWLPL)


Number of known transmembrane regions: 0


Working sequence (length = 394):

MKRQRNVNLLLMLVLLVAVGQMAQTIYIPAIADMARDLNVREGAVQSVMGAYLLTYGVSQ
LFYGPISDRVGRRPVILVGMSIFMLATLVAVTTSSLTVLIAASAMQGMGTGVGGVMARTL
PRDLYERTQLRhANSLLNMGILVSPLLAPLIGGLLDTMWNWRACYLFLLVLCAGVTFSMA
RWMPETRPVDAPRTRLLTSYKTLFGNSGFNCYLLMLIGGLAGIAAFEACSGVLMGAVLGL
SSMTVSILFILPIPAAFFGAWFAGRPNKRFSTLMWQSVICCLLAGLLMWIPDWFGVMNVW
TLLVPAALFFFGAGMLFPLATSGAMEPFPFLAGTAGALVGGLQNIGSGVLASLSAMLPQT
GQGSLGLLMTLMGLLIVLCWLPLATRMShQGQPV

