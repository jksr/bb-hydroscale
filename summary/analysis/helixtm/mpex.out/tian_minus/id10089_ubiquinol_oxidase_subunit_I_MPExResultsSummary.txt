Results summary for protein: id10089_ubiquinol_oxidase_subunit_I
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 13 Neutral
    His 54 Neutral
    His 97 Neutral
    His 98 Neutral
    His 106 Neutral
    His 262 Neutral
    His 284 Neutral
    His 333 Neutral
    His 334 Neutral
    His 378 Neutral
    His 411 Neutral
    His 419 Neutral
    His 421 Neutral
    His 492 Neutral
    His 557 Neutral
    His 559 Neutral
    His 579 Neutral
    His 584 Neutral
    His 609 Neutral
    His 650 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 15

17-49 (33) : VMVTIAGIILGGLALVGLITYFGKWTYLWKEWL
    DG = -7.03 for segment sequence
    DG = -12.13 for most favorable 19AA centered at #26L
    (VMVTIAGIILGGLALVGLI)

56-74 (19) : RLGIMYIIVAIVMLLRGFA
    DG = -9.34 for segment sequence
    DG = -9.34 for most favorable 19AA centered at #65A
    (RLGIMYIIVAIVMLLRGFA)

111-156 (46) : IFFVAMPFVIGLMNLVVPLQIGARDVAFPFLNNLSFWFTVVGVILV
    DG = -20.68 for segment sequence
    DG = -17.38 for most favorable 19AA centered at #120I
    (IFFVAMPFVIGLMNLVVPL)

158-187 (30) : VSLGVGEFAQTGWLAYPPLSGIEYSPGVGV
    DG = 0.42 for segment sequence
    DG = -3.03 for most favorable 19AA centered at #178G
    (GWLAYPPLSGIEYSPGVGV)

194-212 (19) : LQLSGIGTTLTGINFFVTI
    DG = -0.43 for segment sequence
    DG = -0.43 for most favorable 19AA centered at #203L
    (LQLSGIGTTLTGINFFVTI)

237-255 (19) : VLIIASFPILTVTVALLTL
    DG = -14.38 for segment sequence
    DG = -14.38 for most favorable 19AA centered at #246L
    (VLIIASFPILTVTVALLTL)

278-296 (19) : LIWAWGhPEVYILILPVFG
    DG = -11.15 for segment sequence
    DG = -11.15 for most favorable 19AA centered at #287V
    (LIWAWGhPEVYILILPVFG)

314-332 (19) : TSLVWATVCITVLSFIVWL
    DG = -6.94 for segment sequence
    DG = -6.94 for most favorable 19AA centered at #323I
    (TSLVWATVCITVLSFIVWL)

346-364 (19) : AFFGITTMIIAIPTGVKIF
    DG = -6.25 for segment sequence
    DG = -6.25 for most favorable 19AA centered at #355I
    (AFFGITTMIIAIPTGVKIF)

375-417 (43) : IVFhSAMLWTIGFIVTFSVGGMTGVLLAVPGADFVLhNSLFLI
    DG = -10.99 for segment sequence
    DG = -8 for most favorable 19AA centered at #408F
    (VLLAVPGADFVLhNSLFLI)

423-441 (19) : VIIGGVVFGCFAGMTYWWP
    DG = -6.4 for segment sequence
    DG = -6.4 for most favorable 19AA centered at #432C
    (VIIGGVVFGCFAGMTYWWP)

456-474 (19) : AFWFWIIGFFVAFMPLYAL
    DG = -16.09 for segment sequence
    DG = -16.09 for most favorable 19AA centered at #465F
    (AFWFWIIGFFVAFMPLYAL)

495-513 (19) : LMIAASGAVLIALGILCLV
    DG = -12.03 for segment sequence
    DG = -12.03 for most favorable 19AA centered at #504L
    (LMIAASGAVLIALGILCLV)

538-556 (19) : TLEWATSSPPPFYNFAVVP
    DG = -2.72 for segment sequence
    DG = -2.72 for most favorable 19AA centered at #547P
    (TLEWATSSPPPFYNFAVVP)

601-630 (30) : IFGFAMIWhIWWLAIVGFAGMIITWIVKSF
    DG = -10.84 for segment sequence
    DG = -10.3 for most favorable 19AA centered at #610I
    (IFGFAMIWhIWWLAIVGFA)


Number of known transmembrane regions: 0


Working sequence (length = 663):

MFGKLSLDAVPFhEPIVMVTIAGIILGGLALVGLITYFGKWTYLWKEWLTSVDhKRLGIM
YIIVAIVMLLRGFADAIMMRSQQALASAGEAGFLPPhhYDQIFTAhGVIMIFFVAMPFVI
GLMNLVVPLQIGARDVAFPFLNNLSFWFTVVGVILVNVSLGVGEFAQTGWLAYPPLSGIE
YSPGVGVDYWIWSLQLSGIGTTLTGINFFVTILKMRAPGMTMFKMPVFTWASLCANVLII
ASFPILTVTVALLTLDRYLGThFFTNDMGGNMMMYINLIWAWGhPEVYILILPVFGVFSE
IAATFSRKRLFGYTSLVWATVCITVLSFIVWLhhFFTMGAGANVNAFFGITTMIIAIPTG
VKIFNWLFTMYQGRIVFhSAMLWTIGFIVTFSVGGMTGVLLAVPGADFVLhNSLFLIAhF
hNVIIGGVVFGCFAGMTYWWPKAFGFKLNETWGKRAFWFWIIGFFVAFMPLYALGFMGMT
RRLSQQIDPQFhTMLMIAASGAVLIALGILCLVIQMYVSIRDRDQNRDLTGDPWGGRTLE
WATSSPPPFYNFAVVPhVhERDAFWEMKEKGEAYKKPDhYEEIhMPKNSGAGIVIAAFST
IFGFAMIWhIWWLAIVGFAGMIITWIVKSFDEDVDYYVPVAEIEKLENQhFDEITKAGLK
NGN

