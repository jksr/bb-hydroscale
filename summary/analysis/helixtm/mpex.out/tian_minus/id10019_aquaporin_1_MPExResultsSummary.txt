Results summary for protein: id10019_aquaporin_1
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 69 Neutral
    His 74 Neutral
    His 180 Neutral
    His 204 Neutral
    His 209 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

9-40 (32) : LFWRAVVAEFLATTLFVFISIGSALGFKYPVG
    DG = -6.89 for segment sequence
    DG = -8.91 for most favorable 19AA centered at #18F
    (LFWRAVVAEFLATTLFVFI)

81-112 (32) : LGLLLSCQISIFRALMYIIAQCVGAIVATAIL
    DG = -9.74 for segment sequence
    DG = -7.72 for most favorable 19AA centered at #103V
    (ALMYIIAQCVGAIVATAIL)

138-156 (19) : GLGIEIIGTLQLVLCVLAT
    DG = -6.17 for segment sequence
    DG = -6.17 for most favorable 19AA centered at #147L
    (GLGIEIIGTLQLVLCVLAT)

166-184 (19) : GSAPLAIGLSVALGhLLAI
    DG = -6.69 for segment sequence
    DG = -6.69 for most favorable 19AA centered at #175S
    (GSAPLAIGLSVALGhLLAI)

215-233 (19) : GPFIGGALAVLIYDFILAP
    DG = -10.94 for segment sequence
    DG = -10.94 for most favorable 19AA centered at #224V
    (GPFIGGALAVLIYDFILAP)


Number of known transmembrane regions: 0


Working sequence (length = 269):

MASEFKKKLFWRAVVAEFLATTLFVFISIGSALGFKYPVGNNQTAVQDNVKVSLAFGLSI
ATLAQSVGhISGAhLNPAVTLGLLLSCQISIFRALMYIIAQCVGAIVATAILSGITSSLT
GNSLGRNDLADGVNSGQGLGIEIIGTLQLVLCVLATTDRRRRDLGGSAPLAIGLSVALGh
LLAIDYTGCGINPARSFGSAVIThNFSNhWIFWVGPFIGGALAVLIYDFILAPRSSDLTD
RVKVWTSGQVEEYDLDADDINSRVEMKPK

