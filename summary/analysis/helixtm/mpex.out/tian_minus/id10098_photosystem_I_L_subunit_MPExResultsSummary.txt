Results summary for protein: id10098_photosystem_I_L_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 16 Neutral
    His 54 Neutral
Scale: Tian_minus
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

30-48 (19) : FIGNLPAYRQGLSPILRGL
    DG = -0.89 for segment sequence
    DG = -0.89 for most favorable 19AA centered at #39Q
    (FIGNLPAYRQGLSPILRGL)

50-68 (19) : VGMAhGYFLIGPWVKLGPL
    DG = -6.35 for segment sequence
    DG = -6.35 for most favorable 19AA centered at #59I
    (VGMAhGYFLIGPWVKLGPL)

83-101 (19) : IALILVATACLAAYGLVSF
    DG = -9.53 for segment sequence
    DG = -9.53 for most favorable 19AA centered at #92C
    (IALILVATACLAAYGLVSF)

121-150 (30) : TAGFFVGAMGSAFVAFFLLENFLVVDGIMT
    DG = -5.51 for segment sequence
    DG = -7.79 for most favorable 19AA centered at #130G
    (TAGFFVGAMGSAFVAFFLL)


Number of known transmembrane regions: 0


Working sequence (length = 154):

AEELVKPYNGDPFVGhLSTPISDSGLVKTFIGNLPAYRQGLSPILRGLEVGMAhGYFLIG
PWVKLGPLRDSDVANLGGLISGIALILVATACLAAYGLVSFQKGGSSSDPLKTSEGWSQF
TAGFFVGAMGSAFVAFFLLENFLVVDGIMTGLFN

