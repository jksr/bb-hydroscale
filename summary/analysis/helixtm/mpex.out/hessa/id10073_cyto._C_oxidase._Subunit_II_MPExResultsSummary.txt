Results summary for protein: id10073_cyto._C_oxidase._Subunit_II
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 22 Neutral
    His 24 Neutral
    His 26 Neutral
    His 52 Neutral
    His 102 Neutral
    His 161 Neutral
    His 204 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

28-46 (19) : LMIVFLISSLVLYIISLML
    DG = -3.64 for segment sequence
    DG = -3.64 for most favorable 19AA centered at #37L
    (LMIVFLISSLVLYIISLML)


Number of known transmembrane regions: 0


Working sequence (length = 227):

MAYPMQLGFQDATSPIMEELLhFhDhTLMIVFLISSLVLYIISLMLTTKLThTSTMDAQE
VETIWTILPAIILILIALPSLRILYMMDEINNPSLTVKTMGhQWYWSYEYTDYEDLSFDS
YMIPTSELKPGELRLLEVDNRVVLPMEMTIRMLVSSEDVLhSWAVPSLGLKTDAIPGRLN
QTTLMSSRPGLYYGQCSEICGSNhSFMPIVLELVPLKYFEKWSASML

