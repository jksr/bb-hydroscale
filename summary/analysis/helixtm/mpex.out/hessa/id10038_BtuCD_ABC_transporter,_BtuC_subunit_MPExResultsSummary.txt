Results summary for protein: id10038_BtuCD_ABC_transporter,_BtuC_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 139 Neutral
    His 262 Neutral
    His 272 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

15-33 (19) : WLLCLSVLMLLALLLSLCA
    DG = -3.97 for segment sequence
    DG = -3.97 for most favorable 19AA centered at #24L
    (WLLCLSVLMLLALLLSLCA)

115-133 (19) : WALGLCAIAGALIITLILL
    DG = -3.09 for segment sequence
    DG = -3.09 for most favorable 19AA centered at #124G
    (WALGLCAIAGALIITLILL)

146-164 (19) : LLAGVALGIICSALMTWAI
    DG = -0.96 for segment sequence
    DG = -0.96 for most favorable 19AA centered at #155I
    (LLAGVALGIICSALMTWAI)

242-260 (19) : MVGVSVALAGAIGFIGLVI
    DG = -0.43 for segment sequence
    DG = -0.43 for most favorable 19AA centered at #251G
    (MVGVSVALAGAIGFIGLVI)


Number of known transmembrane regions: 0


Working sequence (length = 326):

MLTLARQQQRQNIRWLLCLSVLMLLALLLSLCAGEQWISPGDWFSPRGELFVWQIRLPRT
LAVLLVGAALAISGAVMQALFENPLAEPGLLGVSNGAGVGLIAAVLLGQGQLPNWALGLC
AIAGALIITLILLRFARRhLSTSRLLLAGVALGIICSALMTWAIYFSTSVDLRQLMYWMM
GGFGGVDWRQSWLMLALIPMLLWICCQSRPMNMLALGEISARQLGLPLWFWRNVLVAATG
WMVGVSVALAGAIGFIGLVIPhILRLCGLTDhRALLPGCALAGASALLLADIVARLALAA
AELPIGVVTATLGAPVFIWLLLKAGR

