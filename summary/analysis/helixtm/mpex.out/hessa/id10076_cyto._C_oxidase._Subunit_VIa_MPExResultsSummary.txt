Results summary for protein: id10076_cyto._C_oxidase._Subunit_VIa
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 8 Neutral
    His 38 Neutral
    His 41 Neutral
    His 51 Neutral
    His 52 Neutral
    His 67 Neutral
    His 71 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 0


Number of known transmembrane regions: 0


Working sequence (length = 85):

ASAAKGDhGGTGARTWRFLTFGLALPSVALCTLNSWLhSGhRERPAFIPYhhLRIRTKPF
SWGDGNhTFFhNPRVNPLPTGYEKP

