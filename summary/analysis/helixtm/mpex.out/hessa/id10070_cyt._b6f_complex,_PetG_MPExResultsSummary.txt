Results summary for protein: id10070_cyt._b6f_complex,_PetG
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 0


Number of known transmembrane regions: 0


Working sequence (length = 37):

MVEPLLDGLVLGLVFATLGGLFYAAYQQYKRPNELGG

