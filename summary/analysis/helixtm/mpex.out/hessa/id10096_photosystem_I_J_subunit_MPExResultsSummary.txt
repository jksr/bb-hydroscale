Results summary for protein: id10096_photosystem_I_J_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 3 Neutral
    His 39 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 0


Number of known transmembrane regions: 0


Working sequence (length = 41):

MKhFLTYLSTAPVLAAIWMTITAGILIEFNRFYPDLLFhPL

