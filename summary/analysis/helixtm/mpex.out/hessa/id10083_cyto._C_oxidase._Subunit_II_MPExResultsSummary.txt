Results summary for protein: id10083_cyto._C_oxidase._Subunit_II
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 29 Neutral
    His 36 Neutral
    His 73 Neutral
    His 119 Neutral
    His 181 Neutral
    His 224 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

39-57 (19) : LYIITAVTIFVCLLLLICI
    DG = -5.12 for segment sequence
    DG = -5.12 for most favorable 19AA centered at #48F
    (LYIITAVTIFVCLLLLICI)

79-97 (19) : VIWTLVPVLILVAIGAFSL
    DG = -0.71 for segment sequence
    DG = -0.71 for most favorable 19AA centered at #88I
    (VIWTLVPVLILVAIGAFSL)


Number of known transmembrane regions: 0


Working sequence (length = 269):

QDVLGDLPVIGKPVNGGMNFQPASSPLAhDQQWLDhFVLYIITAVTIFVCLLLLICIVRF
NRRANPVPARFThNTPIEVIWTLVPVLILVAIGAFSLPILFRSQEMPNDPDLVIKAIGhQ
WYWSYEYPNDGVAFDALMLEKEALADAGYSEDEYLLATDNPVVVPVGKKVLVQVTATDVI
hAWTIPAFAVKQDAVPGRIAQLWFSVDQEGVYFGQCSELCGINhAYMPIVVKAVSQEKYE
AWLAGAKEEFAADASDYLPASPVKLASAE

