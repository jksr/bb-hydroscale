Results summary for protein: id10030_ClC_chloride_channel
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 70 Neutral
    His 175 Neutral
    His 234 Neutral
    His 281 Neutral
    His 284 Neutral
    His 383 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

33-51 (19) : LAILFMAAVVGTLVGLAAV
    DG = -1.91 for segment sequence
    DG = -1.91 for most favorable 19AA centered at #42V
    (LAILFMAAVVGTLVGLAAV)

77-95 (19) : LLLTVAFLCSAVLAMFGYF
    DG = -1.45 for segment sequence
    DG = -1.45 for most favorable 19AA centered at #86S
    (LLLTVAFLCSAVLAMFGYF)

251-269 (19) : TLWLYLILGIIFGIFGPIF
    DG = -0.21 for segment sequence
    DG = -0.21 for most favorable 19AA centered at #260I
    (TLWLYLILGIIFGIFGPIF)

330-348 (19) : MGMLVFIFVARVITTLLCF
    DG = -0.6 for segment sequence
    DG = -0.6 for most favorable 19AA centered at #339A
    (MGMLVFIFVARVITTLLCF)


Number of known transmembrane regions: 0


Working sequence (length = 473):

MKTDTPSLETPQAARLRRRQLIRQLLERDKTPLAILFMAAVVGTLVGLAAVAFDKGVAWL
QNQRMGALVhTADNYPLLLTVAFLCSAVLAMFGYFLVRKYAPEAGGSGIPEIEGALEDQR
PVRWWRVLPVKFFGGLGTLGGGMVLGREGPTVQIGGNIGRMVLDIFRLKGDEARhTLLAT
GAAAGLAAAFNAPLAGILFIIEEMRPQFRYTLISIKAVFIGVIMSTIMYRIFNhEVALID
VGKLSDAPLNTLWLYLILGIIFGIFGPIFNKWVLGMQDLLhRVhGGNITKWVLMGGAIGG
LCGLLGFVAPATSGGGFNLIPIATAGNFSMGMLVFIFVARVITTLLCFSSGAPGGIFAPM
LALGTVLGTAFGMVAVELFPQYhLEAGTFAIAGMGALLAASIRAPLTGIILVLEMTDNYQ
LILPMIITGLGATLLAQFTGGKPLYSAILARTLAKQEAEQLARSKAASASENT

