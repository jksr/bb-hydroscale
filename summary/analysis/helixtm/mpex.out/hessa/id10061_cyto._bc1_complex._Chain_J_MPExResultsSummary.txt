Results summary for protein: id10061_cyto._bc1_complex._Chain_J
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 45 Neutral
    His 54 Neutral
    His 57 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 0


Number of known transmembrane regions: 0


Working sequence (length = 62):

VAPTLTARLYSLLFRRTSTFALTIVVGALLFERAFDQGADAIYEhINEGKLWKhIKhKYE
NK

