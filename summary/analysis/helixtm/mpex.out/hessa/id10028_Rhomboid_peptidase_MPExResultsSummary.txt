Results summary for protein: id10028_Rhomboid_peptidase
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 39 Neutral
    His 56 Neutral
    His 60 Neutral
    His 65 Neutral
    His 134 Neutral
    His 135 Neutral
    His 169 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 0


Number of known transmembrane regions: 0


Working sequence (length = 192):

MKNFLAQQGKITLILTALCVLIYLAQQLGFEDDIMYLMhYPAYEEQDSEVWRYIShTLVh
LSNLhILFNLSWFFIFGGMIERTFGSVKLLMLYVVASAITGYVQNYVSGPAFFGLSGVVY
AVLGYVFIRDKLNhhLFDLPEGFFTMLLVGIALGFISPLFGVEMGNAAhISGLIVGLIWG
FIDSKLRKNSLE

