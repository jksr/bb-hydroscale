Results summary for protein: id10112_Photosystem_II_PsbM_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

8-26 (19) : LIATALFVLVPSVFLIILY
    DG = -1.63 for segment sequence
    DG = -1.63 for most favorable 19AA centered at #17V
    (LIATALFVLVPSVFLIILY)


Number of known transmembrane regions: 0


Working sequence (length = 36):

MEVNQLGLIATALFVLVPSVFLIILYVQTESQQKSS

