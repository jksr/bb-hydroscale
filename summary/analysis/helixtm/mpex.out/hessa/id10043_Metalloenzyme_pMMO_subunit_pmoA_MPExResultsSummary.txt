Results summary for protein: id10043_Metalloenzyme_pMMO_subunit_pmoA
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 11 Neutral
    His 38 Neutral
    His 40 Neutral
    His 168 Neutral
    His 232 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 0


Number of known transmembrane regions: 0


Working sequence (length = 247):

MSAAQSAVRShAEAVQVSRTIDWMALFVVFFVIVGSYhIhAMLTMGDWDFWSDWKDRRLW
VTVTPIVLVTFPAAVQSYLWERYRLPWGATVCVLGLLLGEWINRYFNFWGWTYFPINFVF
PASLVPGAIILDTVLMLSGSYLFTAIVGAMGWGLIFYPGNWPIIAPLhVPVEYNGMLMSI
ADIQGYNYVRTGTPEYIRMVEKGTLRTFGKDVAPVSAFFSAFMSILIYFMWhFIGRWFSN
ERFLQST

