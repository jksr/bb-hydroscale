Results summary for protein: id10127_Reaction_center._Chain_M
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 145 Neutral
    His 182 Neutral
    His 193 Neutral
    His 202 Neutral
    His 219 Neutral
    His 266 Neutral
    His 301 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

268-286 (19) : WAIWMAVLVTLTGGIGILL
    DG = -0.64 for segment sequence
    DG = -0.64 for most favorable 19AA centered at #277T
    (WAIWMAVLVTLTGGIGILL)


Number of known transmembrane regions: 0


Working sequence (length = 307):

AEYQNIFSQVQVRGPADLGMTEDVNLANRSGVGPFSTLLGWFGNAQLGPIYLGSLGVLSL
FSGLMWFFTIGIWFWYQAGWNPAVFLRDLFFFSLEPPAPEYGLSFAAPLKEGGLWLIASF
FMFVAVWSWWGRTYLRAQALGMGKhTAWAFLSAIWLWMVLGFIRPILMGSWSEAVPYGIF
ShLDWTNNFSLVhGNLFYNPFhGLSIAFLYGSALLFAMhGATILAVSRFGGERELEQIAD
RGTAAERAALFWRWTMGFNATMEGIhRWAIWMAVLVTLTGGIGILLSGTVVDNWYVWGQN
hGMAPLN

