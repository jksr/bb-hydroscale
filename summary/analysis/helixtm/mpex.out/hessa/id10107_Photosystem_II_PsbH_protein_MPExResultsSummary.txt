Results summary for protein: id10107_Photosystem_II_PsbH_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

30-48 (19) : LMAVFMGLFLVFLLIILEI
    DG = -3.35 for segment sequence
    DG = -3.35 for most favorable 19AA centered at #39L
    (LMAVFMGLFLVFLLIILEI)


Number of known transmembrane regions: 0


Working sequence (length = 66):

MARRTWLGDILRPLNSEYGKVAPGWGTTPLMAVFMGLFLVFLLIILEIYNSTLILDGVNV
SWKALG

