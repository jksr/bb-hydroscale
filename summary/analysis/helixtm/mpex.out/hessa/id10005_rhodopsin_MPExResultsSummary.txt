Results summary for protein: id10005_rhodopsin
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 65 Neutral
    His 100 Neutral
    His 152 Neutral
    His 195 Neutral
    His 211 Neutral
    His 278 Neutral
Scale: Hessa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

203-221 (19) : FVIYMFVVhFIIPLIVIFF
    DG = -1.52 for segment sequence
    DG = -1.52 for most favorable 19AA centered at #212F
    (FVIYMFVVhFIIPLIVIFF)

253-271 (19) : MVIIMVIAFLICWLPYAGV
    DG = -0.91 for segment sequence
    DG = -0.91 for most favorable 19AA centered at #262L
    (MVIIMVIAFLICWLPYAGV)


Number of known transmembrane regions: 0


Working sequence (length = 348):

MNGTEGPNFYVPFSNKTGVVRSPFEAPQYYLAEPWQFSMLAAYMFLLIMLGFPINFLTLY
VTVQhKKLRTPLNYILLNLAVADLFMVFGGFTTTLYTSLhGYFVFGPTGCNLEGFFATLG
GEIALWSLVVLAIERYVVVCKPMSNFRFGENhAIMGVAFTWVMALACAAPPLVGWSRYIP
EGMQCSCGIDYYTPhEETNNESFVIYMFVVhFIIPLIVIFFCYGQLVFTVKEAAAQQQES
ATTQKAEKEVTRMVIIMVIAFLICWLPYAGVAFYIFThQGSDFGPIFMTIPAFFAKTSAV
YNPVIYIMMNKQFRNCMVTTLCCGKNPLGDDEASTTVSKTETSQVAPA

