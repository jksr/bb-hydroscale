Results summary for protein: id10000_bacteriorhodopsin
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

11-29 (19) : IWLALGTALMGLGTLYFLV
    DG = -14.47 for segment sequence
    DG = -14.47 for most favorable 19AA centered at #20M
    (IWLALGTALMGLGTLYFLV)

48-66 (19) : LVPAIAFTMYLSMLLGYGL
    DG = -12.15 for segment sequence
    DG = -12.15 for most favorable 19AA centered at #57Y
    (LVPAIAFTMYLSMLLGYGL)

83-127 (45) : YADWLFTTPLLLLDLALLVDADQGTILALVGADGIMIGTGLVGAL
    DG = -4.18 for segment sequence
    DG = -10.62 for most favorable 19AA centered at #92L
    (YADWLFTTPLLLLDLALLV)

135-163 (29) : FVWWAISTAAMLYILYVLFFGFTSKAESM
    DG = -8.48 for segment sequence
    DG = -16.18 for most favorable 19AA centered at #144A
    (FVWWAISTAAMLYILYVLF)

173-191 (19) : VLRNVTVVLWSAYPVVWLI
    DG = -7.22 for segment sequence
    DG = -7.22 for most favorable 19AA centered at #182W
    (VLRNVTVVLWSAYPVVWLI)

195-224 (30) : GAGIVPLNIETLLFMVLDVSAKVGFGLILL
    DG = -7.67 for segment sequence
    DG = -11.94 for most favorable 19AA centered at #215A
    (LLFMVLDVSAKVGFGLILL)


Number of known transmembrane regions: 0


Working sequence (length = 249):

QAQITGRPEWIWLALGTALMGLGTLYFLVKGMGVSDPDAKKFYAITTLVPAIAFTMYLSM
LLGYGLTMVPFGGEQNPIYWARYADWLFTTPLLLLDLALLVDADQGTILALVGADGIMIG
TGLVGALTKVYSYRFVWWAISTAAMLYILYVLFFGFTSKAESMRPEVASTFKVLRNVTVV
LWSAYPVVWLIGSEGAGIVPLNIETLLFMVLDVSAKVGFGLILLRSRAIFGEAEAPEPSA
GDGAAATSD

