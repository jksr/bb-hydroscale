Results summary for protein: id10038_BtuCD_ABC_transporter,_BtuC_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 139 Neutral
    His 262 Neutral
    His 272 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

15-33 (19) : WLLCLSVLMLLALLLSLCA
    DG = -21.17 for segment sequence
    DG = -21.17 for most favorable 19AA centered at #24L
    (WLLCLSVLMLLALLLSLCA)

50-81 (32) : LFVWQIRLPRTLAVLLVGAALAISGAVMQALF
    DG = -11.99 for segment sequence
    DG = -11.33 for most favorable 19AA centered at #72I
    (VLLVGAALAISGAVMQALF)

89-133 (45) : GLLGVSNGAGVGLIAAVLLGQGQLPNWALGLCAIAGALIITLILL
    DG = -13.46 for segment sequence
    DG = -16.17 for most favorable 19AA centered at #124G
    (WALGLCAIAGALIITLILL)

145-163 (19) : LLLAGVALGIICSALMTWA
    DG = -11.35 for segment sequence
    DG = -11.35 for most favorable 19AA centered at #154I
    (LLLAGVALGIICSALMTWA)

188-206 (19) : WRQSWLMLALIPMLLWICC
    DG = -7.3 for segment sequence
    DG = -7.3 for most favorable 19AA centered at #197L
    (WRQSWLMLALIPMLLWICC)

213-270 (58) : MLALGEISARQLGLPLWFWRNVLVAATGWMVGVSVALAGAIGFIGLVIPhILRLCGLT
    DG = -7.26 for segment sequence
    DG = -10.87 for most favorable 19AA centered at #251G
    (MVGVSVALAGAIGFIGLVI)

275-321 (47) : LLPGCALAGASALLLADIVARLALAAAELPIGVVTATLGAPVFIWLL
    DG = -16.02 for segment sequence
    DG = -10.93 for most favorable 19AA centered at #312L
    (LPIGVVTATLGAPVFIWLL)


Number of known transmembrane regions: 0


Working sequence (length = 326):

MLTLARQQQRQNIRWLLCLSVLMLLALLLSLCAGEQWISPGDWFSPRGELFVWQIRLPRT
LAVLLVGAALAISGAVMQALFENPLAEPGLLGVSNGAGVGLIAAVLLGQGQLPNWALGLC
AIAGALIITLILLRFARRhLSTSRLLLAGVALGIICSALMTWAIYFSTSVDLRQLMYWMM
GGFGGVDWRQSWLMLALIPMLLWICCQSRPMNMLALGEISARQLGLPLWFWRNVLVAATG
WMVGVSVALAGAIGFIGLVIPhILRLCGLTDhRALLPGCALAGASALLLADIVARLALAA
AELPIGVVTATLGAPVFIWLLLKAGR

