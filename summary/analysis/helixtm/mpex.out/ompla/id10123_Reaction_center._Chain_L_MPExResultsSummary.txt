Results summary for protein: id10123_Reaction_center._Chain_L
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 116 Neutral
    His 144 Neutral
    His 153 Neutral
    His 168 Neutral
    His 173 Neutral
    His 190 Neutral
    His 211 Neutral
    His 230 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

30-48 (19) : FVGFFGVSAIFFIFLGVSL
    DG = -16.16 for segment sequence
    DG = -16.16 for most favorable 19AA centered at #39I
    (FVGFFGVSAIFFIFLGVSL)

84-102 (19) : GFWQAITVCALGAFISWML
    DG = -6.45 for segment sequence
    DG = -6.45 for most favorable 19AA centered at #93A
    (GFWQAITVCALGAFISWML)

121-151 (31) : FCVPIFMFCVLQVFRPLLLGSWGhAFPYGIL
    DG = -10.04 for segment sequence
    DG = -13.84 for most favorable 19AA centered at #130V
    (FCVPIFMFCVLQVFRPLLL)

177-195 (19) : VSFLFVNAMALGLhGGLIL
    DG = -8.22 for segment sequence
    DG = -8.22 for most favorable 19AA centered at #186A
    (VSFLFVNAMALGLhGGLIL)

232-250 (19) : LGLFLASNIFLTGAFGTIA
    DG = -7.2 for segment sequence
    DG = -7.2 for most favorable 19AA centered at #241F
    (LGLFLASNIFLTGAFGTIA)


Number of known transmembrane regions: 0


Working sequence (length = 273):

ALLSFERKYRVRGGTLIGGDLFDFWVGPYFVGFFGVSAIFFIFLGVSLIGYAASQGPTWD
PFAISINPPDLKYGLGAAPLLEGGFWQAITVCALGAFISWMLREVEISRKLGIGWhVPLA
FCVPIFMFCVLQVFRPLLLGSWGhAFPYGILShLDWVNNFGYQYLNWhYNPGhMSSVSFL
FVNAMALGLhGGLILSVANPGDGDKVKTAEhENQYFRDVVGYSIGALSIhRLGLFLASNI
FLTGAFGTIASGPFWTRGWPEWWGWWLDIPFWS

