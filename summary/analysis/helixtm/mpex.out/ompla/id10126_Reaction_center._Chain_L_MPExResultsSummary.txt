Results summary for protein: id10126_Reaction_center._Chain_L
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 116 Neutral
    His 153 Neutral
    His 168 Neutral
    His 173 Neutral
    His 190 Neutral
    His 211 Neutral
    His 230 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

33-81 (49) : FFGVATFFFAALGIILIAWSAVLQGTWNPQLISVYPPALEYGLGGAPLA
    DG = -8.38 for segment sequence
    DG = -16.72 for most favorable 19AA centered at #42A
    (FFGVATFFFAALGIILIAW)

84-102 (19) : GLWQIITICATGAFVSWAL
    DG = -4.31 for segment sequence
    DG = -4.31 for most favorable 19AA centered at #93A
    (GLWQIITICATGAFVSWAL)

117-146 (30) : IPFAFAFAILAYLTLVLFRPVMMGAWGYAF
    DG = -17.4 for segment sequence
    DG = -15.74 for most favorable 19AA centered at #126L
    (IPFAFAFAILAYLTLVLFR)

177-195 (19) : ISFFFTNALALALhGALVL
    DG = -9.29 for segment sequence
    DG = -9.29 for most favorable 19AA centered at #186A
    (ISFFFTNALALALhGALVL)

232-250 (19) : LGLLLSLSAVFFSALCMII
    DG = -15.95 for segment sequence
    DG = -15.95 for most favorable 19AA centered at #241V
    (LGLLLSLSAVFFSALCMII)


Number of known transmembrane regions: 0


Working sequence (length = 281):

ALLSFERKYRVPGGTLVGGNLFDFWVGPFYVGFFGVATFFFAALGIILIAWSAVLQGTWN
PQLISVYPPALEYGLGGAPLAKGGLWQIITICATGAFVSWALREVEICRKLGIGYhIPFA
FAFAILAYLTLVLFRPVMMGAWGYAFPYGIWThLDWVSNTGYTYGNFhYNPAhMIAISFF
FTNALALALhGALVLSAANPEKGKEMRTPDhEDTFFRDLVGYSIGTLGIhRLGLLLSLSA
VFFSALCMIITGTIWFDQWVDWWQWWVKLPWWANIPGGING

