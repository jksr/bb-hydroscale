Results summary for protein: id10033_lactose_permease,_3D_structure
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 35 Neutral
    His 39 Neutral
    His 205 Neutral
    His 322 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 10

9-27 (19) : FWMFGLFFFFYFFIMGAYF
    DG = -21.22 for segment sequence
    DG = -21.22 for most favorable 19AA centered at #18F
    (FWMFGLFFFFYFFIMGAYF)

48-66 (19) : IFAAISLFSLLFQPLFGLL
    DG = -15.97 for segment sequence
    DG = -15.97 for most favorable 19AA centered at #57L
    (IFAAISLFSLLFQPLFGLL)

76-129 (54) : LLWIITGMLVMFAPFFIFIFGPLLQYNILVGSIVGGIYLGFCFNAGAPAVEAFI
    DG = -24.08 for segment sequence
    DG = -19.6 for most favorable 19AA centered at #85V
    (LLWIITGMLVMFAPFFIFI)

146-164 (19) : FGCVGWALGASIVGIMFTI
    DG = -6.5 for segment sequence
    DG = -6.5 for most favorable 19AA centered at #155A
    (FGCVGWALGASIVGIMFTI)

168-186 (19) : FVFWLGSGCALILAVLLFF
    DG = -19.15 for segment sequence
    DG = -19.15 for most favorable 19AA centered at #177A
    (FVFWLGSGCALILAVLLFF)

212-230 (19) : LALELFRQPKLWFLSLYVI
    DG = -6.09 for segment sequence
    DG = -6.09 for most favorable 19AA centered at #221K
    (LALELFRQPKLWFLSLYVI)

260-288 (29) : VFGYVTTMGELLNASIMFFAPLIINRIGG
    DG = -0.71 for segment sequence
    DG = -4.43 for most favorable 19AA centered at #269E
    (VFGYVTTMGELLNASIMFF)

291-341 (51) : ALLLAGTIMSVRIIGSSFATSALEVVILKTLhMFEVPFLLVGCFKYITSQF
    DG = -4.08 for segment sequence
    DG = -7.61 for most favorable 19AA centered at #322h
    (LEVVILKTLhMFEVPFLLV)

350-378 (29) : YLVCFCFFKQLAMIFMSVLAGNMYESIGF
    DG = -7.02 for segment sequence
    DG = -12.37 for most favorable 19AA centered at #359Q
    (YLVCFCFFKQLAMIFMSVL)

382-400 (19) : YLVLGLVALGFTLISVFTL
    DG = -17.69 for segment sequence
    DG = -17.69 for most favorable 19AA centered at #391G
    (YLVLGLVALGFTLISVFTL)


Number of known transmembrane regions: 0


Working sequence (length = 417):

MYYLKNTNFWMFGLFFFFYFFIMGAYFPFFPIWLhDINhISKSDTGIIFAAISLFSLLFQ
PLFGLLSDKLGLRKYLLWIITGMLVMFAPFFIFIFGPLLQYNILVGSIVGGIYLGFCFNA
GAPAVEAFIEKVSRRSNFEFGRARMFGCVGWALGASIVGIMFTINNQFVFWLGSGCALIL
AVLLFFAKTDAPSSATVANAVGANhSAFSLKLALELFRQPKLWFLSLYVIGVSCTYDVFD
QQFANFFTSFFATGEQGTRVFGYVTTMGELLNASIMFFAPLIINRIGGKNALLLAGTIMS
VRIIGSSFATSALEVVILKTLhMFEVPFLLVGCFKYITSQFEVRFSATIYLVCFCFFKQL
AMIFMSVLAGNMYESIGFQGAYLVLGLVALGFTLISVFTLSGPGPLSLLRRQVNEVA

