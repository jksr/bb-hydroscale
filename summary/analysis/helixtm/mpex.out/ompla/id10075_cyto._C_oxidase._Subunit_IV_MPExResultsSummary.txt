Results summary for protein: id10075_cyto._C_oxidase._Subunit_IV
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 2 Neutral
    His 29 Neutral
    His 101 Neutral
    His 109 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

80-98 (19) : TVVGAAMFFIGFTALLLIW
    DG = -14.84 for segment sequence
    DG = -14.84 for most favorable 19AA centered at #89I
    (TVVGAAMFFIGFTALLLIW)


Number of known transmembrane regions: 0


Working sequence (length = 147):

AhGSVVKSEDYALPSYVDRRDYPLPDVAhVKNLSASQKALKEKEKASWSSLSIDEKVELY
RLKFKESFAEMNRSTNEWKTVVGAAMFFIGFTALLLIWEKhYVYGPIPhTFEEEWVAKQT
KRMLDMKVAPIQGFSAKWDYDKNEWKK

