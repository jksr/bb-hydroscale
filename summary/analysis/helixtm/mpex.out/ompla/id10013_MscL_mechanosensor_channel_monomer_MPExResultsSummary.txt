Results summary for protein: id10013_MscL_mechanosensor_channel_monomer
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 132 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

14-32 (19) : IVDLAVAVVIGTAFTALVT
    DG = -8.1 for segment sequence
    DG = -8.1 for most favorable 19AA centered at #23I
    (IVDLAVAVVIGTAFTALVT)

71-89 (19) : VLLSAAINFFLIAFAVYFL
    DG = -18.15 for segment sequence
    DG = -18.15 for most favorable 19AA centered at #80F
    (VLLSAAINFFLIAFAVYFL)


Number of known transmembrane regions: 0


Working sequence (length = 151):

MLKGFKEFLARGNIVDLAVAVVIGTAFTALVTKFTDSIITPLINRIGVNAQSDVGILRIG
IGGGQTIDLNVLLSAAINFFLIAFAVYFLVVLPYNTLRKKGEVEQPGDTQVVLLTEIRDL
LAQTNGDSPGRhGGRGTPSPTDGPRASTESQ

