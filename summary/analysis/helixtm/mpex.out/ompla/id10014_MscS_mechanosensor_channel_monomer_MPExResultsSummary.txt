Results summary for protein: id10014_MscS_mechanosensor_channel_monomer
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

22-52 (31) : ALLLSYAVNIVAALAIIIVGLIIARMISNAV
    DG = -14.47 for segment sequence
    DG = -15.01 for most favorable 19AA centered at #31I
    (ALLLSYAVNIVAALAIIIV)

68-86 (19) : FLSALVRYGIIAFTLIAAL
    DG = -12.18 for segment sequence
    DG = -12.18 for most favorable 19AA centered at #77I
    (FLSALVRYGIIAFTLIAAL)

93-127 (35) : TASVIAVLGAAGLAVGLALQGSLSNLAAGVLLVMF
    DG = -12.13 for segment sequence
    DG = -8.62 for most favorable 19AA centered at #102A
    (TASVIAVLGAAGLAVGLAL)


Number of known transmembrane regions: 0


Working sequence (length = 286):

MEDLNVVDSINGAGSWLVANQALLLSYAVNIVAALAIIIVGLIIARMISNAVNRLMISRK
IDATVADFLSALVRYGIIAFTLIAALGRVGVQTASVIAVLGAAGLAVGLALQGSLSNLAA
GVLLVMFRPFRAGEYVDLGGVAGTVLSVQIFSTTMRTADGKIIVIPNGKIIAGNIINFSR
EPVRRNEFIIGVAYDSDIDQVKQILTNIIQSEDRILKDREMTVRLNELGASSINFVVRVW
SNSGDLQNVYWDVLERIKREFDAAGISFPYPQMDVNFKRVKEDKAA

