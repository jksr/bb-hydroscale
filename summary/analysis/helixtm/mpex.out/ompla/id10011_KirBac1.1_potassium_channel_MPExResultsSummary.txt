Results summary for protein: id10011_KirBac1.1_potassium_channel
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 9 Neutral
    His 117 Neutral
    His 124 Neutral
    His 161 Neutral
    His 199 Neutral
    His 210 Neutral
    His 219 Neutral
    His 230 Neutral
    His 272 Neutral
    His 276 Neutral
    His 282 Neutral
    His 283 Neutral
    His 297 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

63-81 (19) : FFASLAALFVVNNTLFALL
    DG = -12.59 for segment sequence
    DG = -12.59 for most favorable 19AA centered at #72V
    (FFASLAALFVVNNTLFALL)

96-114 (19) : GFVGAFFFSVETLATVGYG
    DG = -3.45 for segment sequence
    DG = -3.45 for most favorable 19AA centered at #105V
    (GFVGAFFFSVETLATVGYG)

129-147 (19) : LEIFVGMSGIALSTGLVFA
    DG = -6.43 for segment sequence
    DG = -6.43 for most favorable 19AA centered at #138I
    (LEIFVGMSGIALSTGLVFA)


Number of known transmembrane regions: 0


Working sequence (length = 333):

MNVDPFSPhSSDSFAQAASPARKPPRGGRRIWSGTREVIAYGMPASVWRDLYYWALKVSW
PVFFASLAALFVVNNTLFALLYQLGDAPIANQSPPGFVGAFFFSVETLATVGYGDMhPQT
VYAhAIATLEIFVGMSGIALSTGLVFARFARPRAKIMFARhAIVRPFNGRMTLMVRAANA
RQNVIAEARAKMRLMRREhSSEGYSLMKIhDLKLVRNEhPIFLLGWNMMhVIDESSPLFG
ETPESLAEGRAMLLVMIEGSDETTAQVMQARhAWEhDDIRWhhRYVDLMSDVDGMThIDY
TRFNDTEPVEPPGAAPDAQAFAAKPGEGDARPV

