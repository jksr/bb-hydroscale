Results summary for protein: id10114_Photosystem_II_PsbX_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

19-37 (19) : FFIGLLSGAVVLGLTFAVL
    DG = -16.54 for segment sequence
    DG = -16.54 for most favorable 19AA centered at #28V
    (FFIGLLSGAVVLGLTFAVL)


Number of known transmembrane regions: 0


Working sequence (length = 50):

MIQSASSLLLTITPSLKGFFIGLLSGAVVLGLTFAVLIAISQIDKVQRSL

