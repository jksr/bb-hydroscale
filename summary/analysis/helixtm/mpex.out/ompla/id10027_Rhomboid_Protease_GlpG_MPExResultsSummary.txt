Results summary for protein: id10027_Rhomboid_Protease_GlpG
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 32 Neutral
    His 73 Neutral
    His 79 Neutral
    His 141 Neutral
    His 145 Neutral
    His 150 Neutral
    His 254 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

96-114 (19) : VTWVMMIACVVVFIAMQIL
    DG = -14.59 for segment sequence
    DG = -14.59 for most favorable 19AA centered at #105V
    (VTWVMMIACVVVFIAMQIL)

143-161 (19) : LMhFSLMhILFNLLWWWYL
    DG = -9.84 for segment sequence
    DG = -9.84 for most favorable 19AA centered at #152L
    (LMhFSLMhILFNLLWWWYL)

174-192 (19) : LIVITLISALLSGYVQQKF
    DG = -5.31 for segment sequence
    DG = -5.31 for most favorable 19AA centered at #183L
    (LIVITLISALLSGYVQQKF)

195-213 (19) : PWFGGLSGVVYALMGYVWL
    DG = -7.5 for segment sequence
    DG = -7.5 for most favorable 19AA centered at #204V
    (PWFGGLSGVVYALMGYVWL)

229-247 (19) : LIIFALIWIVAGWFDLFGM
    DG = -14.66 for segment sequence
    DG = -14.66 for most favorable 19AA centered at #238V
    (LIIFALIWIVAGWFDLFGM)

249-267 (19) : MANGAhIAGLAVGLAMAFV
    DG = -1.86 for segment sequence
    DG = -1.86 for most favorable 19AA centered at #258L
    (MANGAhIAGLAVGLAMAFV)


Number of known transmembrane regions: 0


Working sequence (length = 276):

MLMITSFANPRVAQAFVDYMATQGVILTIQQhNQSDVWLADESQAERVRAELARFLENPA
DPRYLAASWQAGhTGSGLhYRRYPFFAALRERAGPVTWVMMIACVVVFIAMQILGDQEVM
LWLAWPFDPTLKFEFWRYFThALMhFSLMhILFNLLWWWYLGGAVEKRLGSGKLIVITLI
SALLSGYVQQKFSGPWFGGLSGVVYALMGYVWLRGERDPQSGIYLQRGLIIFALIWIVAG
WFDLFGMSMANGAhIAGLAVGLAMAFVDSLNARKRK

