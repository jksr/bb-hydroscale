Results summary for protein: id10022_Glycerol_Channel,_GlpF
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 66 Neutral
    His 119 Neutral
    His 120 Neutral
    His 142 Neutral
    His 258 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

14-32 (19) : EFLGTGLLIFFGVGCVAAL
    DG = -10.25 for segment sequence
    DG = -10.25 for most favorable 19AA centered at #23F
    (EFLGTGLLIFFGVGCVAAL)

44-62 (19) : ISVIWGLGVAMAIYLTAGV
    DG = -8.6 for segment sequence
    DG = -8.6 for most favorable 19AA centered at #53A
    (ISVIWGLGVAMAIYLTAGV)

63-91 (29) : SGAhLNPAVTIALWLFACFDKRKVIPFIV
    DG = 7.74 for segment sequence
    DG = -1.19 for most favorable 19AA centered at #72T
    (SGAhLNPAVTIALWLFACF)

95-113 (19) : AGAFCAAALVYGLYYNLFF
    DG = -9.77 for segment sequence
    DG = -9.77 for most favorable 19AA centered at #104V
    (AGAFCAAALVYGLYYNLFF)

148-166 (19) : AFAVEMVITAILMGLILAL
    DG = -14.75 for segment sequence
    DG = -14.75 for most favorable 19AA centered at #157A
    (AFAVEMVITAILMGLILAL)

176-194 (19) : GPLAPLLIGLLIAVIGASM
    DG = -9.61 for segment sequence
    DG = -9.61 for most favorable 19AA centered at #185L
    (GPLAPLLIGLLIAVIGASM)

232-250 (19) : YFLVPLFGPIVGAIVGAFA
    DG = -11.34 for segment sequence
    DG = -11.34 for most favorable 19AA centered at #241I
    (YFLVPLFGPIVGAIVGAFA)


Number of known transmembrane regions: 0


Working sequence (length = 281):

MSQTSTLKGQCIAEFLGTGLLIFFGVGCVAALKVAGASFGQWEISVIWGLGVAMAIYLTA
GVSGAhLNPAVTIALWLFACFDKRKVIPFIVSQVAGAFCAAALVYGLYYNLFFDFEQThh
IVRGSVESVDLAGTFSTYPNPhINFVQAFAVEMVITAILMGLILALTDDGNGVPRGPLAP
LLIGLLIAVIGASMGPLTGFAMNPARDFGPKVFAWLAGWGNVAFTGGRDIPYFLVPLFGP
IVGAIVGAFAYRKLIGRhLPCDICVVEEKETTTPSEQKASL

