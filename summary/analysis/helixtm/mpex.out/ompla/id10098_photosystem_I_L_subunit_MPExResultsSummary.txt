Results summary for protein: id10098_photosystem_I_L_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 16 Neutral
    His 54 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

41-59 (19) : LSPILRGLEVGMAhGYFLI
    DG = -0.35 for segment sequence
    DG = -0.35 for most favorable 19AA centered at #50V
    (LSPILRGLEVGMAhGYFLI)

83-101 (19) : IALILVATACLAAYGLVSF
    DG = -13.15 for segment sequence
    DG = -13.15 for most favorable 19AA centered at #92C
    (IALILVATACLAAYGLVSF)

121-150 (30) : TAGFFVGAMGSAFVAFFLLENFLVVDGIMT
    DG = -8.13 for segment sequence
    DG = -11.24 for most favorable 19AA centered at #130G
    (TAGFFVGAMGSAFVAFFLL)


Number of known transmembrane regions: 0


Working sequence (length = 154):

AEELVKPYNGDPFVGhLSTPISDSGLVKTFIGNLPAYRQGLSPILRGLEVGMAhGYFLIG
PWVKLGPLRDSDVANLGGLISGIALILVATACLAAYGLVSFQKGGSSSDPLKTSEGWSQF
TAGFFVGAMGSAFVAFFLLENFLVVDGIMTGLFN

