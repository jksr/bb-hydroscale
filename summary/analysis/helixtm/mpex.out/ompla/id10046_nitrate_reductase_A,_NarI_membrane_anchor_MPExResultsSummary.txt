Results summary for protein: id10046_nitrate_reductase_A,_NarI_membrane_anchor
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 56 Neutral
    His 66 Neutral
    His 74 Neutral
    His 150 Neutral
    His 170 Neutral
    His 176 Neutral
    His 187 Neutral
    His 205 Neutral
    His 225 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

3-21 (19) : FLNMFFFDIYPYIAGAVFL
    DG = -10.16 for segment sequence
    DG = -10.16 for most favorable 19AA centered at #12Y
    (FLNMFFFDIYPYIAGAVFL)

54-72 (19) : LFhIGILGIFVGhFFGMLT
    DG = -8.1 for segment sequence
    DG = -8.1 for most favorable 19AA centered at #63F
    (LFhIGILGIFVGhFFGMLT)

91-109 (19) : MFAGGASGVLCLIGGVLLL
    DG = -10.38 for segment sequence
    DG = -10.38 for most favorable 19AA centered at #100L
    (MFAGGASGVLCLIGGVLLL)

128-146 (19) : LILSLLVIQCALGLLTIPF
    DG = -15.8 for segment sequence
    DG = -15.8 for most favorable 19AA centered at #137C
    (LILSLLVIQCALGLLTIPF)

180-211 (32) : VAFIFRLhLVLGMTLFLLFPFSRLIhIWSVPV
    DG = -14.12 for segment sequence
    DG = -17.22 for most favorable 19AA centered at #189V
    (VAFIFRLhLVLGMTLFLLF)


Number of known transmembrane regions: 0


Working sequence (length = 225):

MQFLNMFFFDIYPYIAGAVFLIGSWLRYDYGQYTWRAASSQMLDRKGMNLASNLFhIGIL
GIFVGhFFGMLTPhWMYEAWLPIEVKQKMAMFAGGASGVLCLIGGVLLLKRRLFSPRVRA
TTTGADILILSLLVIQCALGLLTIPFSAQhMDGSEMMKLVGWAQSVVTFhGGASQhLDGV
AFIFRLhLVLGMTLFLLFPFSRLIhIWSVPVEYLTRKYQLVRARh

