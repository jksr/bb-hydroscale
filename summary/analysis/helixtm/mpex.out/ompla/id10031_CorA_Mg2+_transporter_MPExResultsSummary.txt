Results summary for protein: id10031_CorA_Mg2+_transporter
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 68 Neutral
    His 83 Neutral
    His 94 Neutral
    His 120 Neutral
    His 212 Neutral
    His 257 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

168-186 (19) : YLLYSLIDALVDDYFVLLE
    DG = -5 for segment sequence
    DG = -5 for most favorable 19AA centered at #177L
    (YLLYSLIDALVDDYFVLLE)

293-311 (19) : VLTIIATIFMPLTFIAGIY
    DG = -12.56 for segment sequence
    DG = -12.56 for most favorable 19AA centered at #302M
    (VLTIIATIFMPLTFIAGIY)

327-345 (19) : YPVVLAVMGVIAVIMVVYF
    DG = -16.91 for segment sequence
    DG = -16.91 for most favorable 19AA centered at #336V
    (YPVVLAVMGVIAVIMVVYF)


Number of known transmembrane regions: 0


Working sequence (length = 351):

MEEKRLSAKKGLPPGTLVYTGKYREDFEIEVMNYSIEEFREFKTTDVESVLPFRDSSTPT
WINITGIhRTDVVQRVGEFFGIhPLVLEDILNVhQRPKVEFFENYVFIVLKMFTYDKNLh
ELESEQVSLILTKNCVLMFQEKIGDVFDPVRERIRYNRGIIRKKRADYLLYSLIDALVDD
YFVLLEKIDDEIDVLEEEVLERPEKETVQRThQLKRNLVELRKTIWPLREVLSSLYRDVP
PLIEKETVPYFRDVYDhTIQIADTVETFRDIVSGLLDVYLSSVSNKTNEVMKVLTIIATI
FMPLTFIAGIYGMNFEYMPELRWKWGYPVVLAVMGVIAVIMVVYFKKKKWL

