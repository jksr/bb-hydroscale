Results summary for protein: id10039_F1F0_ATPsynthase_Subunit_C
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

8-36 (29) : LLYMAAAVMMGLAAIGAAIGIGILGGKFL
    DG = -12.45 for segment sequence
    DG = -11.58 for most favorable 19AA centered at #17M
    (LLYMAAAVMMGLAAIGAAI)

59-77 (19) : LVDAIPMIAVGLGLYVMFA
    DG = -9.92 for segment sequence
    DG = -9.92 for most favorable 19AA centered at #68V
    (LVDAIPMIAVGLGLYVMFA)


Number of known transmembrane regions: 0


Working sequence (length = 79):

MENLNMDLLYMAAAVMMGLAAIGAAIGIGILGGKFLEGAARQPDLIPLLRTQFFIVMGLV
DAIPMIAVGLGLYVMFAVA

