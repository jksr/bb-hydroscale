Results summary for protein: id10035_multidrug_resistance_protein_EmrD
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 132 Neutral
    His 389 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 9

9-27 (19) : LLLMLVLLVAVGQMAQTIY
    DG = -15.06 for segment sequence
    DG = -15.06 for most favorable 19AA centered at #18A
    (LLLMLVLLVAVGQMAQTIY)

48-66 (19) : VMGAYLLTYGVSQLFYGPI
    DG = -5.63 for segment sequence
    DG = -5.63 for most favorable 19AA centered at #57G
    (VMGAYLLTYGVSQLFYGPI)

75-105 (31) : VILVGMSIFMLATLVAVTTSSLTVLIAASAM
    DG = -14.84 for segment sequence
    DG = -12.78 for most favorable 19AA centered at #84M
    (VILVGMSIFMLATLVAVTT)

136-154 (19) : LLNMGILVSPLLAPLIGGL
    DG = -9.47 for segment sequence
    DG = -9.47 for most favorable 19AA centered at #145P
    (LLNMGILVSPLLAPLIGGL)

165-183 (19) : YLFLLVLCAGVTFSMARWM
    DG = -10.48 for segment sequence
    DG = -10.48 for most favorable 19AA centered at #174G
    (YLFLLVLCAGVTFSMARWM)

208-263 (56) : GFNCYLLMLIGGLAGIAAFEACSGVLMGAVLGLSSMTVSILFILPIPAAFFGAWFA
    DG = -19.42 for segment sequence
    DG = -12.22 for most favorable 19AA centered at #254P
    (VSILFILPIPAAFFGAWFA)

272-290 (19) : TLMWQSVICCLLAGLLMWI
    DG = -11.06 for segment sequence
    DG = -11.06 for most favorable 19AA centered at #281C
    (TLMWQSVICCLLAGLLMWI)

302-333 (32) : LLVPAALFFFGAGMLFPLATSGAMEPFPFLAG
    DG = -11.31 for segment sequence
    DG = -15.55 for most favorable 19AA centered at #311F
    (LLVPAALFFFGAGMLFPLA)

365-383 (19) : LGLLMTLMGLLIVLCWLPL
    DG = -19.82 for segment sequence
    DG = -19.82 for most favorable 19AA centered at #374L
    (LGLLMTLMGLLIVLCWLPL)


Number of known transmembrane regions: 0


Working sequence (length = 394):

MKRQRNVNLLLMLVLLVAVGQMAQTIYIPAIADMARDLNVREGAVQSVMGAYLLTYGVSQ
LFYGPISDRVGRRPVILVGMSIFMLATLVAVTTSSLTVLIAASAMQGMGTGVGGVMARTL
PRDLYERTQLRhANSLLNMGILVSPLLAPLIGGLLDTMWNWRACYLFLLVLCAGVTFSMA
RWMPETRPVDAPRTRLLTSYKTLFGNSGFNCYLLMLIGGLAGIAAFEACSGVLMGAVLGL
SSMTVSILFILPIPAAFFGAWFAGRPNKRFSTLMWQSVICCLLAGLLMWIPDWFGVMNVW
TLLVPAALFFFGAGMLFPLATSGAMEPFPFLAGTAGALVGGLQNIGSGVLASLSAMLPQT
GQGSLGLLMTLMGLLIVLCWLPLATRMShQGQPV

