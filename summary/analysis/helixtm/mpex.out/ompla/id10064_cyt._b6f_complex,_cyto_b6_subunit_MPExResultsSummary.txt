Results summary for protein: id10064_cyt._b6f_complex,_cyto_b6_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 29 Neutral
    His 86 Neutral
    His 100 Neutral
    His 187 Neutral
    His 202 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

32-62 (31) : IFYCLGGITLTCFLIQFATGFAMTFYYKPTV
    DG = -7.65 for segment sequence
    DG = -9.24 for most favorable 19AA centered at #41L
    (IFYCLGGITLTCFLIQFAT)

92-110 (19) : MMVLMMILhVFRVYLTGGF
    DG = -9.53 for segment sequence
    DG = -9.53 for most favorable 19AA centered at #101V
    (MMVLMMILhVFRVYLTGGF)

116-134 (19) : LTWISGVILAVITVSFGVT
    DG = -8 for segment sequence
    DG = -8 for most favorable 19AA centered at #125A
    (LTWISGVILAVITVSFGVT)

147-165 (19) : AVKIVSGVPEAIPVVGVLI
    DG = -1.49 for segment sequence
    DG = -1.49 for most favorable 19AA centered at #156E
    (AVKIVSGVPEAIPVVGVLI)

188-206 (19) : TFVLPWLIAVFMLLhFLMI
    DG = -18.4 for segment sequence
    DG = -18.4 for most favorable 19AA centered at #197V
    (TFVLPWLIAVFMLLhFLMI)


Number of known transmembrane regions: 0


Working sequence (length = 215):

MANVYDWFQERLEIQALADDVTSKYVPPhVNIFYCLGGITLTCFLIQFATGFAMTFYYKP
TVTEAYASVQYIMNEVSFGWLIRSIhRWSASMMVLMMILhVFRVYLTGGFKKPRELTWIS
GVILAVITVSFGVTGYSLPWDQVGYWAVKIVSGVPEAIPVVGVLISDLLRGGSSVGQATL
TRYYSAhTFVLPWLIAVFMLLhFLMIRKQGISGPL

