Results summary for protein: id10115_Photosystem_II_PsbN_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 41 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

7-25 (19) : LSIALAAVCIGVTGYSIYL
    DG = -8.2 for segment sequence
    DG = -8.2 for most favorable 19AA centered at #16I
    (LSIALAAVCIGVTGYSIYL)


Number of known transmembrane regions: 0


Working sequence (length = 43):

MEPATVLSIALAAVCIGVTGYSIYLSFGPPSKELADPFDDhED

