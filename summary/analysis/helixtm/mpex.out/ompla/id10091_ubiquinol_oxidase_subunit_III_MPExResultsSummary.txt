Results summary for protein: id10091_ubiquinol_oxidase_subunit_III
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 8 Neutral
    His 12 Neutral
    His 14 Neutral
    His 16 Neutral
    His 18 Neutral
    His 19 Neutral
    His 121 Neutral
    His 122 Neutral
    His 146 Neutral
    His 149 Neutral
    His 185 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

26-55 (30) : IFGFWIYLMSDCILFSILFATYAVLVNGTA
    DG = -12.1 for segment sequence
    DG = -12.87 for most favorable 19AA centered at #35S
    (IFGFWIYLMSDCILFSILF)

63-91 (29) : IFELPFVLVETFLLLFSSITYGMAAIAMY
    DG = -15.15 for segment sequence
    DG = -13.29 for most favorable 19AA centered at #72E
    (IFELPFVLVETFLLLFSSI)

97-125 (29) : QVISWLALTWLFGAGFIGMEIYEFhhLIV
    DG = -4.54 for segment sequence
    DG = -7.92 for most favorable 19AA centered at #106W
    (QVISWLALTWLFGAGFIGM)

147-165 (19) : GLhVTSGLIWMAVLMVQIA
    DG = -5.05 for segment sequence
    DG = -5.05 for most favorable 19AA centered at #156W
    (GLhVTSGLIWMAVLMVQIA)

182-200 (19) : LFWhFLDVVWICVFTVVYL
    DG = -13.22 for segment sequence
    DG = -13.22 for most favorable 19AA centered at #191W
    (LFWhFLDVVWICVFTVVYL)


Number of known transmembrane regions: 0


Working sequence (length = 204):

MATDTLThATAhAhEhGhhDAGGTKIFGFWIYLMSDCILFSILFATYAVLVNGTAGGPTG
KDIFELPFVLVETFLLLFSSITYGMAAIAMYKNNKSQVISWLALTWLFGAGFIGMEIYEF
hhLIVNGMGPDRSGFLSAFFALVGThGLhVTSGLIWMAVLMVQIARRGLTSTNRTRIMCL
SLFWhFLDVVWICVFTVVYLMGAM

