Results summary for protein: id10089_ubiquinol_oxidase_subunit_I
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 13 Neutral
    His 54 Neutral
    His 97 Neutral
    His 98 Neutral
    His 106 Neutral
    His 262 Neutral
    His 284 Neutral
    His 333 Neutral
    His 334 Neutral
    His 378 Neutral
    His 411 Neutral
    His 419 Neutral
    His 421 Neutral
    His 492 Neutral
    His 557 Neutral
    His 559 Neutral
    His 579 Neutral
    His 584 Neutral
    His 609 Neutral
    His 650 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 14

17-49 (33) : VMVTIAGIILGGLALVGLITYFGKWTYLWKEWL
    DG = -9.16 for segment sequence
    DG = -14.27 for most favorable 19AA centered at #26L
    (VMVTIAGIILGGLALVGLI)

56-74 (19) : RLGIMYIIVAIVMLLRGFA
    DG = -10.58 for segment sequence
    DG = -10.58 for most favorable 19AA centered at #65A
    (RLGIMYIIVAIVMLLRGFA)

108-136 (29) : VIMIFFVAMPFVIGLMNLVVPLQIGARDV
    DG = -10.8 for segment sequence
    DG = -16.43 for most favorable 19AA centered at #117P
    (VIMIFFVAMPFVIGLMNLV)

144-162 (19) : LSFWFTVVGVILVNVSLGV
    DG = -10.9 for segment sequence
    DG = -10.9 for most favorable 19AA centered at #153V
    (LSFWFTVVGVILVNVSLGV)

194-212 (19) : LQLSGIGTTLTGINFFVTI
    DG = -1.76 for segment sequence
    DG = -1.76 for most favorable 19AA centered at #203L
    (LQLSGIGTTLTGINFFVTI)

237-255 (19) : VLIIASFPILTVTVALLTL
    DG = -15.62 for segment sequence
    DG = -15.62 for most favorable 19AA centered at #246L
    (VLIIASFPILTVTVALLTL)

287-305 (19) : VYILILPVFGVFSEIAATF
    DG = -11.29 for segment sequence
    DG = -11.29 for most favorable 19AA centered at #296G
    (VYILILPVFGVFSEIAATF)

314-344 (31) : TSLVWATVCITVLSFIVWLhhFFTMGAGANV
    DG = -3.26 for segment sequence
    DG = -11.09 for most favorable 19AA centered at #323I
    (TSLVWATVCITVLSFIVWL)

346-364 (19) : AFFGITTMIIAIPTGVKIF
    DG = -4.27 for segment sequence
    DG = -4.27 for most favorable 19AA centered at #355I
    (AFFGITTMIIAIPTGVKIF)

375-417 (43) : IVFhSAMLWTIGFIVTFSVGGMTGVLLAVPGADFVLhNSLFLI
    DG = -9.17 for segment sequence
    DG = -9.18 for most favorable 19AA centered at #394G
    (IGFIVTFSVGGMTGVLLAV)

423-441 (19) : VIIGGVVFGCFAGMTYWWP
    DG = -5.16 for segment sequence
    DG = -5.16 for most favorable 19AA centered at #432C
    (VIIGGVVFGCFAGMTYWWP)

456-474 (19) : AFWFWIIGFFVAFMPLYAL
    DG = -17.48 for segment sequence
    DG = -17.48 for most favorable 19AA centered at #465F
    (AFWFWIIGFFVAFMPLYAL)

502-520 (19) : AVLIALGILCLVIQMYVSI
    DG = -15.31 for segment sequence
    DG = -15.31 for most favorable 19AA centered at #511C
    (AVLIALGILCLVIQMYVSI)

601-630 (30) : IFGFAMIWhIWWLAIVGFAGMIITWIVKSF
    DG = -11.94 for segment sequence
    DG = -10.95 for most favorable 19AA centered at #610I
    (IFGFAMIWhIWWLAIVGFA)


Number of known transmembrane regions: 0


Working sequence (length = 663):

MFGKLSLDAVPFhEPIVMVTIAGIILGGLALVGLITYFGKWTYLWKEWLTSVDhKRLGIM
YIIVAIVMLLRGFADAIMMRSQQALASAGEAGFLPPhhYDQIFTAhGVIMIFFVAMPFVI
GLMNLVVPLQIGARDVAFPFLNNLSFWFTVVGVILVNVSLGVGEFAQTGWLAYPPLSGIE
YSPGVGVDYWIWSLQLSGIGTTLTGINFFVTILKMRAPGMTMFKMPVFTWASLCANVLII
ASFPILTVTVALLTLDRYLGThFFTNDMGGNMMMYINLIWAWGhPEVYILILPVFGVFSE
IAATFSRKRLFGYTSLVWATVCITVLSFIVWLhhFFTMGAGANVNAFFGITTMIIAIPTG
VKIFNWLFTMYQGRIVFhSAMLWTIGFIVTFSVGGMTGVLLAVPGADFVLhNSLFLIAhF
hNVIIGGVVFGCFAGMTYWWPKAFGFKLNETWGKRAFWFWIIGFFVAFMPLYALGFMGMT
RRLSQQIDPQFhTMLMIAASGAVLIALGILCLVIQMYVSIRDRDQNRDLTGDPWGGRTLE
WATSSPPPFYNFAVVPhVhERDAFWEMKEKGEAYKKPDhYEEIhMPKNSGAGIVIAAFST
IFGFAMIWhIWWLAIVGFAGMIITWIVKSFDEDVDYYVPVAEIEKLENQhFDEITKAGLK
NGN

