Results summary for protein: id10085_cyto._C_oxidase._Subunit_IV
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 3 Neutral
    His 4 Neutral
    His 9 Neutral
    His 11 Neutral
    His 18 Neutral
Scale: OmpLa
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

29-47 (19) : GATWVSILSIAVLVFLALA
    DG = -12.94 for segment sequence
    DG = -12.94 for most favorable 19AA centered at #38I
    (GATWVSILSIAVLVFLALA)


Number of known transmembrane regions: 0


Working sequence (length = 49):

AShhEITDhKhGEMDIRhQQATFAGFIKGATWVSILSIAVLVFLALANS

