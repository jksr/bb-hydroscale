Results summary for protein: id10018_AChR_pore_delta_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 20 Neutral
    His 26 Neutral
    His 60 Neutral
    His 65 Neutral
    His 194 Neutral
    His 313 Neutral
    His 320 Neutral
    His 339 Neutral
    His 390 Neutral
    His 416 Neutral
    His 497 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

121-139 (19) : LPPAIFRSSCPINVLYFPF
    DG = -0.86 for segment sequence
    DG = -0.86 for most favorable 19AA centered at #130C
    (LPPAIFRSSCPINVLYFPF)

215-250 (36) : VTFYLIIRRKPLFYVINFITPCVLISFLAALAFYLP
    DG = -16.57 for segment sequence
    DG = -12.93 for most favorable 19AA centered at #241F
    (FITPCVLISFLAALAFYLP)

257-275 (19) : MSTAICVLLAQAVFLLLTS
    DG = -7.06 for segment sequence
    DG = -7.06 for most favorable 19AA centered at #266A
    (MSTAICVLLAQAVFLLLTS)

281-310 (30) : TALAVPLIGKYLMFIMSLVTGVVVNCGIVL
    DG = -9.49 for segment sequence
    DG = -8.68 for most favorable 19AA centered at #301G
    (LMFIMSLVTGVVVNCGIVL)

456-474 (19) : LSMFIITPVMVLGTIFIFV
    DG = -14.37 for segment sequence
    DG = -14.37 for most favorable 19AA centered at #465M
    (LSMFIITPVMVLGTIFIFV)


Number of known transmembrane regions: 0


Working sequence (length = 501):

VNEEERLINDLLIVNKYNKhVRPVKhNNEVVNIALSLTLSNLISLKETDETLTTNVWMDh
AWYDhRLTWNASEYSDISILRLRPELIWIPDIVLQNNNDGQYNVAYFCNVLVRPNGYVTW
LPPAIFRSSCPINVLYFPFDWQNCSLKFTALNYNANEISMDLMTDTIDGKDYPIEWIIID
PEAFTENGEWEIIhKPAKKNIYGDKFPNGTNYQDVTFYLIIRRKPLFYVINFITPCVLIS
FLAALAFYLPAESGEKMSTAICVLLAQAVFLLLTSQRLPETALAVPLIGKYLMFIMSLVT
GVVVNCGIVLNFhFRTPSThVLSTRVKQIFLEKLPRILhMSRVDEIEQPDWQNDLKLRRS
SSVGYISKAQEYFNIKSRSELMFEKQSERhGLVPRVTPRIGFGNNNENIAASDQLhDEIK
SGIDSTNYIVKQIKEKNAYDEEVGNWNLVGQTIDRLSMFIITPVMVLGTIFIFVMGNFNR
PPAKPFEGDPFDYSSDhPRCA

