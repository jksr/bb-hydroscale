Results summary for protein: id10055_succinate_dehydrogenase_D_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 14 Neutral
    His 71 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

16-52 (37) : FILVRATAIVLTLYIIYMVGFFATSGELTYEVWIGFF
    DG = -18.34 for segment sequence
    DG = -14.74 for most favorable 19AA centered at #25V
    (FILVRATAIVLTLYIIYMV)

59-77 (19) : VFTLLALFSILIhAWIGMW
    DG = -13.03 for segment sequence
    DG = -13.03 for most favorable 19AA centered at #68I
    (VFTLLALFSILIhAWIGMW)

95-113 (19) : LVIVVALVVYVIYGFVVVW
    DG = -17.49 for segment sequence
    DG = -17.49 for most favorable 19AA centered at #104Y
    (LVIVVALVVYVIYGFVVVW)


Number of known transmembrane regions: 0


Working sequence (length = 115):

MVSNASALGRNGVhDFILVRATAIVLTLYIIYMVGFFATSGELTYEVWIGFFASAFTKVF
TLLALFSILIhAWIGMWQVLTDYVKPLALRLMLQLVIVVALVVYVIYGFVVVWGV

