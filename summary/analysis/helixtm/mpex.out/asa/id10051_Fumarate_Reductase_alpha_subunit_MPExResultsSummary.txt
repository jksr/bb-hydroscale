Results summary for protein: id10051_Fumarate_Reductase_alpha_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 44 Neutral
    His 93 Neutral
    His 114 Neutral
    His 120 Neutral
    His 143 Neutral
    His 182 Neutral
    His 254 Neutral
    His 255 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

35-53 (19) : LFLGLFMIGhMFFVSTILL
    DG = -17.19 for segment sequence
    DG = -17.19 for most favorable 19AA centered at #44h
    (LFLGLFMIGhMFFVSTILL)

78-96 (19) : VVSFLAAFVFAVFIAhAFL
    DG = -15.9 for segment sequence
    DG = -15.9 for most favorable 19AA centered at #87F
    (VVSFLAAFVFAVFIAhAFL)

130-148 (19) : AMTGFAMFFLGSVhLYIMM
    DG = -10.61 for segment sequence
    DG = -10.61 for most favorable 19AA centered at #139L
    (AMTGFAMFFLGSVhLYIMM)

161-190 (30) : FRMVSEWMWPLYLVLLFAVELhGSVGLYRL
    DG = -6.86 for segment sequence
    DG = -9.25 for most favorable 19AA centered at #170P
    (FRMVSEWMWPLYLVLLFAV)

214-232 (19) : TLMSAFLIVLGLLTFGAYV
    DG = -12.94 for segment sequence
    DG = -12.94 for most favorable 19AA centered at #223L
    (TLMSAFLIVLGLLTFGAYV)


Number of known transmembrane regions: 0


Working sequence (length = 256):

MTNESILESYSGVTPERKKSRMPAKLDWWQSATGLFLGLFMIGhMFFVSTILLGDNVMLW
VTKKFELDFIFEGGKPIVVSFLAAFVFAVFIAhAFLAMRKFPINYRQYLTFKThKDLMRh
GDTTLWWIQAMTGFAMFFLGSVhLYIMMTQPQTIGPVSSSFRMVSEWMWPLYLVLLFAVE
LhGSVGLYRLAVKWGWFDGETPDKTRANLKKLKTLMSAFLIVLGLLTFGAYVKKGLEQTD
PNIDYKYFDYKRThhR

