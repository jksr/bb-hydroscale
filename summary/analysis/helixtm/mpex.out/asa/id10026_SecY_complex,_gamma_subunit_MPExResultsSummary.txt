Results summary for protein: id10026_SecY_complex,_gamma_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 55 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

42-60 (19) : LGISLLGIIGYIIhVPATY
    DG = -7.95 for segment sequence
    DG = -7.95 for most favorable 19AA centered at #51G
    (LGISLLGIIGYIIhVPATY)


Number of known transmembrane regions: 0


Working sequence (length = 73):

KTDFNQKIEQLKEFIEECRRVWLVLKKPTKDEYLAVAKVTALGISLLGIIGYIIhVPATY
IKGILKPPTTPRV

