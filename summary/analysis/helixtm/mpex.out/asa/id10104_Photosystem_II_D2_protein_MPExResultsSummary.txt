Results summary for protein: id10104_Photosystem_II_D2_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 61 Neutral
    His 87 Neutral
    His 117 Neutral
    His 189 Neutral
    His 197 Neutral
    His 214 Neutral
    His 268 Neutral
    His 336 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

27-59 (33) : FVFVGWSGILLFPCAYLALGGWLTGTTFVTSWY
    DG = -10.94 for segment sequence
    DG = -12.96 for most favorable 19AA centered at #36L
    (FVFVGWSGILLFPCAYLAL)

109-138 (30) : GLWTFIALhGAFGLIGFMLRQFEIARLVGV
    DG = -9.22 for segment sequence
    DG = -11.98 for most favorable 19AA centered at #118G
    (GLWTFIALhGAFGLIGFML)

144-223 (80) : IAFSAPIAVFVSVFLIYPLGQSSWFFAPSFGVAAIFRFLLFFQGFhNWTLNPFhMMGVAGVLGGALLCAIhGATVENTLF
    DG = -15.67 for segment sequence
    DG = -15.13 for most favorable 19AA centered at #176A
    (WFFAPSFGVAAIFRFLLFF)

256-299 (44) : IFGIAFSNKRWLhFFMLFVPVTGLWMSAIGVVGLALNLRSYDFI
    DG = -7.62 for segment sequence
    DG = -10.8 for most favorable 19AA centered at #278G
    (FFMLFVPVTGLWMSAIGVV)


Number of known transmembrane regions: 0


Working sequence (length = 352):

MTIAIGRAPAERGWFDILDDWLKRDRFVFVGWSGILLFPCAYLALGGWLTGTTFVTSWYT
hGLASSYLEGCNFLTVAVSTPANSMGhSLLLLWGPEAQGDFTRWCQLGGLWTFIALhGAF
GLIGFMLRQFEIARLVGVRPYNAIAFSAPIAVFVSVFLIYPLGQSSWFFAPSFGVAAIFR
FLLFFQGFhNWTLNPFhMMGVAGVLGGALLCAIhGATVENTLFQDGEGASTFRAFNPTQA
EETYSMVTANRFWSQIFGIAFSNKRWLhFFMLFVPVTGLWMSAIGVVGLALNLRSYDFIS
QEIRAAEDPEFETFYTKNLLLNEGIRAWMAPQDQPhENFVFPEEVLPRGNAL

