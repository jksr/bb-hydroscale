Results summary for protein: id10043_Metalloenzyme_pMMO_subunit_pmoA
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 11 Neutral
    His 38 Neutral
    His 40 Neutral
    His 168 Neutral
    His 232 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

25-43 (19) : ALFVVFFVIVGSYhIhAML
    DG = -12.49 for segment sequence
    DG = -12.49 for most favorable 19AA centered at #34V
    (ALFVVFFVIVGSYhIhAML)

61-79 (19) : VTVTPIVLVTFPAAVQSYL
    DG = -2.97 for segment sequence
    DG = -2.97 for most favorable 19AA centered at #70T
    (VTVTPIVLVTFPAAVQSYL)

91-109 (19) : VCVLGLLLGEWINRYFNFW
    DG = -4.43 for segment sequence
    DG = -4.43 for most favorable 19AA centered at #100E
    (VCVLGLLLGEWINRYFNFW)

113-181 (69) : YFPINFVFPASLVPGAIILDTVLMLSGSYLFTAIVGAMGWGLIFYPGNWPIIAPLhVPVEYNGMLMSIA
    DG = -11.67 for segment sequence
    DG = -9.38 for most favorable 19AA centered at #150M
    (YLFTAIVGAMGWGLIFYPG)

217-235 (19) : AFFSAFMSILIYFMWhFIG
    DG = -14.23 for segment sequence
    DG = -14.23 for most favorable 19AA centered at #226L
    (AFFSAFMSILIYFMWhFIG)


Number of known transmembrane regions: 0


Working sequence (length = 247):

MSAAQSAVRShAEAVQVSRTIDWMALFVVFFVIVGSYhIhAMLTMGDWDFWSDWKDRRLW
VTVTPIVLVTFPAAVQSYLWERYRLPWGATVCVLGLLLGEWINRYFNFWGWTYFPINFVF
PASLVPGAIILDTVLMLSGSYLFTAIVGAMGWGLIFYPGNWPIIAPLhVPVEYNGMLMSI
ADIQGYNYVRTGTPEYIRMVEKGTLRTFGKDVAPVSAFFSAFMSILIYFMWhFIGRWFSN
ERFLQST

