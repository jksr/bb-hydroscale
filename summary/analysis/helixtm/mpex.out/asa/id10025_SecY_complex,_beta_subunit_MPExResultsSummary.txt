Results summary for protein: id10025_SecY_complex,_beta_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 32 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

35-53 (19) : GVTVAFVIIEAILTYGRFL
    DG = -7.88 for segment sequence
    DG = -7.88 for most favorable 19AA centered at #44E
    (GVTVAFVIIEAILTYGRFL)


Number of known transmembrane regions: 0


Working sequence (length = 53):

MSKREETGLATSAGLIRYMDETFSKIRVKPEhVIGVTVAFVIIEAILTYGRFL

