Results summary for protein: id10074_cyto._C_oxidase._Subunit_III
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 3 Neutral
    His 6 Neutral
    His 9 Neutral
    His 36 Neutral
    His 70 Neutral
    His 71 Neutral
    His 103 Neutral
    His 122 Neutral
    His 148 Neutral
    His 149 Neutral
    His 158 Neutral
    His 204 Neutral
    His 207 Neutral
    His 226 Neutral
    His 231 Neutral
    His 232 Neutral
    His 243 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

30-58 (29) : GLTMWFhFNSMTLLMIGLTTNMLTMYQWW
    DG = -3.42 for segment sequence
    DG = -5.1 for most favorable 19AA centered at #39S
    (GLTMWFhFNSMTLLMIGLT)

84-102 (19) : ILFIISEVLFFTGFFWAFY
    DG = -17.76 for segment sequence
    DG = -17.76 for most favorable 19AA centered at #93F
    (ILFIISEVLFFTGFFWAFY)

158-186 (29) : hMLQALFITITLGVYFTLLQASEYYEAPF
    DG = -6.76 for segment sequence
    DG = -11.83 for most favorable 19AA centered at #167I
    (hMLQALFITITLGVYFTLL)

202-220 (19) : GFhGLhVIIGSTFLIVCFF
    DG = -9.7 for segment sequence
    DG = -9.7 for most favorable 19AA centered at #211G
    (GFhGLhVIIGSTFLIVCFF)

240-258 (19) : WYWhFVDVVWLFLYVSIYW
    DG = -10.34 for segment sequence
    DG = -10.34 for most favorable 19AA centered at #249W
    (WYWhFVDVVWLFLYVSIYW)


Number of known transmembrane regions: 0


Working sequence (length = 261):

MThQThAYhMVNPSPWPLTGALSALLMTSGLTMWFhFNSMTLLMIGLTTNMLTMYQWWRD
VIRESTFQGhhTPAVQKGLRYGMILFIISEVLFFTGFFWAFYhSSLAPTPELGGCWPPTG
IhPLNPLEVPLLNTSVLLASGVSITWAhhSLMEGDRKhMLQALFITITLGVYFTLLQASE
YYEAPFTISDGVYGSTFFVATGFhGLhVIIGSTFLIVCFFRQLKFhFTSNhhFGFEAGAW
YWhFVDVVWLFLYVSIYWWGS

