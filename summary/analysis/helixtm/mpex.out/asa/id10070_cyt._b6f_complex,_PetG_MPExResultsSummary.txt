Results summary for protein: id10070_cyt._b6f_complex,_PetG
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

8-26 (19) : GLVLGLVFATLGGLFYAAY
    DG = -12.82 for segment sequence
    DG = -12.82 for most favorable 19AA centered at #17T
    (GLVLGLVFATLGGLFYAAY)


Number of known transmembrane regions: 0


Working sequence (length = 37):

MVEPLLDGLVLGLVFATLGGLFYAAYQQYKRPNELGG

