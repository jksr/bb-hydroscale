Results summary for protein: id10034_glycerol-3-phosphate_transporter
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 10 Neutral
    His 147 Neutral
    His 165 Neutral
    His 187 Neutral
    His 285 Neutral
    His 371 Neutral
    His 442 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 11

26-56 (31) : LRWQIFLGIFFGYAAYYLVRKNFALAMPYLV
    DG = -12.33 for segment sequence
    DG = -13.27 for most favorable 19AA centered at #35F
    (LRWQIFLGIFFGYAAYYLV)

65-83 (19) : LGFALSGISIAYGFSKFIM
    DG = -5.64 for segment sequence
    DG = -5.64 for most favorable 19AA centered at #74I
    (LGFALSGISIAYGFSKFIM)

94-112 (19) : VFLPAGLILAAAVMLFMGF
    DG = -15.47 for segment sequence
    DG = -15.47 for most favorable 19AA centered at #103A
    (VFLPAGLILAAAVMLFMGF)

120-138 (19) : IAVMFVLLFLCGWFQGMGW
    DG = -12.07 for segment sequence
    DG = -12.07 for most favorable 19AA centered at #129L
    (IAVMFVLLFLCGWFQGMGW)

174-208 (35) : LLFLLGMAWFNDWhAALYMPAFCAILVALFAFAMM
    DG = -20.78 for segment sequence
    DG = -15.97 for most favorable 19AA centered at #199L
    (LYMPAFCAILVALFAFAMM)

255-273 (19) : LWYIAIANVFVYLLRYGIL
    DG = -13.33 for segment sequence
    DG = -13.33 for most favorable 19AA centered at #264F
    (LWYIAIANVFVYLLRYGIL)

294-312 (19) : AYFLYEYAGIPGTLLCGWM
    DG = -5.97 for segment sequence
    DG = -5.97 for most favorable 19AA centered at #303I
    (AYFLYEYAGIPGTLLCGWM)

323-341 (19) : ATGVFFMTLVTIATIVYWM
    DG = -9.7 for segment sequence
    DG = -9.7 for most favorable 19AA centered at #332V
    (ATGVFFMTLVTIATIVYWM)

352-370 (19) : ICMIVIGFLIYGPVMLIGL
    DG = -15.29 for segment sequence
    DG = -15.29 for most favorable 19AA centered at #361I
    (ICMIVIGFLIYGPVMLIGL)

390-408 (19) : LFGYLGGSVAASAIVGYTV
    DG = -3.88 for segment sequence
    DG = -3.88 for most favorable 19AA centered at #399A
    (LFGYLGGSVAASAIVGYTV)

417-435 (19) : FMVMIGGSILAVILLIVVM
    DG = -15.75 for segment sequence
    DG = -15.75 for most favorable 19AA centered at #426L
    (FMVMIGGSILAVILLIVVM)


Number of known transmembrane regions: 0


Working sequence (length = 452):

MLSIFKPAPhKARLPAAEIDPTYRRLRWQIFLGIFFGYAAYYLVRKNFALAMPYLVEQGF
SRGDLGFALSGISIAYGFSKFIMGSVSDRSNPRVFLPAGLILAAAVMLFMGFVPWATSSI
AVMFVLLFLCGWFQGMGWPPCGRTMVhWWSQKERGGIVSVWNCAhNVGGGIPPLLFLLGM
AWFNDWhAALYMPAFCAILVALFAFAMMRDTPQSCGLPPIEEYKNDYPDDYNEKAEQELT
AKQIFMQYVLPNKLLWYIAIANVFVYLLRYGILDWSPTYLKEVKhFALDKSSWAYFLYEY
AGIPGTLLCGWMSDKVFRGNRGATGVFFMTLVTIATIVYWMNPAGNPTVDMICMIVIGFL
IYGPVMLIGLhALELAPKKAAGTAAGFTGLFGYLGGSVAASAIVGYTVDFFGWDGGFMVM
IGGSILAVILLIVVMIGEKRRhEQLLQERNGG

