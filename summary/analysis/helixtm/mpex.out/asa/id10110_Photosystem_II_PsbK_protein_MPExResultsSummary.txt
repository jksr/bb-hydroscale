Results summary for protein: id10110_Photosystem_II_PsbK_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

12-30 (19) : LVDVLPVIPVLFLALAFVW
    DG = -13.45 for segment sequence
    DG = -13.45 for most favorable 19AA centered at #21V
    (LVDVLPVIPVLFLALAFVW)


Number of known transmembrane regions: 0


Working sequence (length = 37):

KLPEAYAIFDPLVDVLPVIPVLFLALAFVWQAAVGFR

