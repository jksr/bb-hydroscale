Results summary for protein: id10120_Light_Harvesting_Complex._beta_chain
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 17 Neutral
    His 35 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

23-41 (19) : TFSAFIILAAVAhVLVWVW
    DG = -10.28 for segment sequence
    DG = -10.28 for most favorable 19AA centered at #32A
    (TFSAFIILAAVAhVLVWVW)


Number of known transmembrane regions: 0


Working sequence (length = 45):

AERSLSGLTEEEAIAVhDQFKTTFSAFIILAAVAhVLVWVWKPWF

