Results summary for protein: id10066_cyt._b6f_complex,_cytochrome_f
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 26 Neutral
    His 143 Neutral
    His 150 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

114-132 (19) : LLVGPLPGEQYQEIVFPVL
    DG = -0.48 for segment sequence
    DG = -0.48 for most favorable 19AA centered at #123Q
    (LLVGPLPGEQYQEIVFPVL)

256-274 (19) : KWMIAFICLVMLAQLMLIL
    DG = -13.64 for segment sequence
    DG = -13.64 for most favorable 19AA centered at #265V
    (KWMIAFICLVMLAQLMLIL)


Number of known transmembrane regions: 0


Working sequence (length = 289):

YPFWAQQTYPPTPREPTGRIVCANChLAAKPAEVEVPQSVLPDTVFKAVVKIPYDTKLQQ
VAADGSKVGLNVGAVLMLPEGFKIAPEERIPEELKKEVGDVYFQPYKEGQDNVLLVGPLP
GEQYQEIVFPVLSPNPTTDKNIhFGKYAIhLGANRGRGQIYPTGEKSNNNVFTASATGTI
TKIAKEEDEYGNVKYQVSIQTDSGKTVVDTIPAGPELIVSEGQAVKAGEALTNNPNVGGF
GQDDTEIVLQDPNRVKWMIAFICLVMLAQLMLILKKKQVEKVQAAEMNF

