Results summary for protein: id10087_cyto.c_oxidase._Chain_II
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 5 Neutral
    His 8 Neutral
    His 40 Neutral
    His 114 Neutral
    His 117 Neutral
    His 157 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

19-37 (19) : LAFSLAMLFVFIALIAYTL
    DG = -18.59 for segment sequence
    DG = -18.59 for most favorable 19AA centered at #28V
    (LAFSLAMLFVFIALIAYTL)

79-97 (19) : YTVYVLAFAFGYQPNPIEV
    DG = -2.23 for segment sequence
    DG = -2.23 for most favorable 19AA centered at #88F
    (YTVYVLAFAFGYQPNPIEV)


Number of known transmembrane regions: 0


Working sequence (length = 168):

MVDEhKAhKAILAYEKGWLAFSLAMLFVFIALIAYTLAThTAGVIPAGKLERVDPTTVRQ
EGPWADPAQAVVQTGPNQYTVYVLAFAFGYQPNPIEVPQGAEIVFKITSPDVIhGFhVEG
TNINVEVLPGEVSTVRYTFKRPGEYRIICNQYCGLGhQNMFGTIVVKE

