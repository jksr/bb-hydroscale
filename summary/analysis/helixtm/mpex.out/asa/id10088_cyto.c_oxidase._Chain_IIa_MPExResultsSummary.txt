Results summary for protein: id10088_cyto.c_oxidase._Chain_IIa
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

13-31 (19) : LVLTLTILVFWLGVYAVFF
    DG = -18.81 for segment sequence
    DG = -18.81 for most favorable 19AA centered at #22F
    (LVLTLTILVFWLGVYAVFF)


Number of known transmembrane regions: 0


Working sequence (length = 34):

MEEKPKGALAVILVLTLTILVFWLGVYAVFFARG

