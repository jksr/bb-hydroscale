Results summary for protein: id10115_Photosystem_II_PsbN_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 41 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

9-27 (19) : IALAAVCIGVTGYSIYLSF
    DG = -7.46 for segment sequence
    DG = -7.46 for most favorable 19AA centered at #18V
    (IALAAVCIGVTGYSIYLSF)


Number of known transmembrane regions: 0


Working sequence (length = 43):

MEPATVLSIALAAVCIGVTGYSIYLSFGPPSKELADPFDDhED

