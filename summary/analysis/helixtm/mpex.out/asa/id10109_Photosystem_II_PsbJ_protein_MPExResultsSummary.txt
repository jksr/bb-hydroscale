Results summary for protein: id10109_Photosystem_II_PsbJ_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

12-40 (29) : IVATVAGMGVIVIVGLFFYGAYAGLGSSL
    DG = -11.64 for segment sequence
    DG = -13.12 for most favorable 19AA centered at #21V
    (IVATVAGMGVIVIVGLFFY)


Number of known transmembrane regions: 0


Working sequence (length = 40):

MMSEGGRIPLWIVATVAGMGVIVIVGLFFYGAYAGLGSSL

