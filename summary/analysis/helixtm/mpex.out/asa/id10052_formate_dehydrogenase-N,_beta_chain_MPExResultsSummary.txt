Results summary for protein: id10052_formate_dehydrogenase-N,_beta_chain
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 61 Neutral
    His 102 Neutral
    His 185 Neutral
    His 210 Neutral
    His 223 Neutral
    His 229 Neutral
    His 230 Neutral
    His 238 Neutral
    His 275 Neutral
    His 292 Neutral
    His 293 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

260-278 (19) : LAAAGFIATFAGLIFhYIG
    DG = -10.89 for segment sequence
    DG = -10.89 for most favorable 19AA centered at #269F
    (LAAAGFIATFAGLIFhYIG)


Number of known transmembrane regions: 0


Working sequence (length = 294):

MAMETQDIIKRSATNSITPPSQVRDYKAEVAKLIDVSTCIGCKACQVACSEWNDIRDEVG
hCVGVYDNPADLSAKSWTVMRFSETEQNGKLEWLIRKDGCMhCEDPGCLKACPSAGAIIQ
YANGIVDFQSENCIGCGYCIAGCPFNIPRLNKEDNRVYKCTLCVDRVSVGQEPACVKTCP
TGAIhFGTKKEMLELAEQRVAKLKARGYEhAGVYNPEGVGGThVMYVLhhADQPELYhGL
PKDPKIDTSVSLWKGALKPLAAAGFIATFAGLIFhYIGIGPNKEVDDDEEDhhE

