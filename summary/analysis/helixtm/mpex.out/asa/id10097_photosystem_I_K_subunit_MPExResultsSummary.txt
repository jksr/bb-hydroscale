Results summary for protein: id10097_photosystem_I_K_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 67 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

16-34 (19) : GLVVILCNLFAIALGRYAI
    DG = -9.64 for segment sequence
    DG = -9.64 for most favorable 19AA centered at #25F
    (GLVVILCNLFAIALGRYAI)

43-61 (19) : LPIALPALFEGFGLPELLA
    DG = -6.87 for segment sequence
    DG = -6.87 for most favorable 19AA centered at #52E
    (LPIALPALFEGFGLPELLA)

65-83 (19) : FGhLLAAGVVSGLQYAGAL
    DG = -3.3 for segment sequence
    DG = -3.3 for most favorable 19AA centered at #74V
    (FGhLLAAGVVSGLQYAGAL)


Number of known transmembrane regions: 0


Working sequence (length = 83):

MVLATLPDTTWTPSVGLVVILCNLFAIALGRYAIQSRGKGPGLPIALPALFEGFGLPELL
ATTSFGhLLAAGVVSGLQYAGAL

