Results summary for protein: id10019_aquaporin_1
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 69 Neutral
    His 74 Neutral
    His 180 Neutral
    His 204 Neutral
    His 209 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

9-40 (32) : LFWRAVVAEFLATTLFVFISIGSALGFKYPVG
    DG = -8.61 for segment sequence
    DG = -11.77 for most favorable 19AA centered at #18F
    (LFWRAVVAEFLATTLFVFI)

52-70 (19) : VSLAFGLSIATLAQSVGhI
    DG = -0.58 for segment sequence
    DG = -0.58 for most favorable 19AA centered at #61A
    (VSLAFGLSIATLAQSVGhI)

81-112 (32) : LGLLLSCQISIFRALMYIIAQCVGAIVATAIL
    DG = -12.15 for segment sequence
    DG = -9.95 for most favorable 19AA centered at #90S
    (LGLLLSCQISIFRALMYII)

138-156 (19) : GLGIEIIGTLQLVLCVLAT
    DG = -6.17 for segment sequence
    DG = -6.17 for most favorable 19AA centered at #147L
    (GLGIEIIGTLQLVLCVLAT)

168-186 (19) : APLAIGLSVALGhLLAIDY
    DG = -4.6 for segment sequence
    DG = -4.6 for most favorable 19AA centered at #177A
    (APLAIGLSVALGhLLAIDY)

211-229 (19) : IFWVGPFIGGALAVLIYDF
    DG = -10.87 for segment sequence
    DG = -10.87 for most favorable 19AA centered at #220G
    (IFWVGPFIGGALAVLIYDF)


Number of known transmembrane regions: 0


Working sequence (length = 269):

MASEFKKKLFWRAVVAEFLATTLFVFISIGSALGFKYPVGNNQTAVQDNVKVSLAFGLSI
ATLAQSVGhISGAhLNPAVTLGLLLSCQISIFRALMYIIAQCVGAIVATAILSGITSSLT
GNSLGRNDLADGVNSGQGLGIEIIGTLQLVLCVLATTDRRRRDLGGSAPLAIGLSVALGh
LLAIDYTGCGINPARSFGSAVIThNFSNhWIFWVGPFIGGALAVLIYDFILAPRSSDLTD
RVKVWTSGQVEEYDLDADDINSRVEMKPK

