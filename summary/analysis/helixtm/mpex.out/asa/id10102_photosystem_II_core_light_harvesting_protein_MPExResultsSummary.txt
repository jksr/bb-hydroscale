Results summary for protein: id10102_photosystem_II_core_light_harvesting_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 9 Neutral
    His 23 Neutral
    His 26 Neutral
    His 100 Neutral
    His 114 Neutral
    His 142 Neutral
    His 157 Neutral
    His 201 Neutral
    His 202 Neutral
    His 216 Neutral
    His 343 Neutral
    His 455 Neutral
    His 466 Neutral
    His 469 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 8

27-45 (19) : TALVAGWAGSMALYELATF
    DG = -2.82 for segment sequence
    DG = -2.82 for most favorable 19AA centered at #36S
    (TALVAGWAGSMALYELATF)

51-69 (19) : VLNPMWRQGMFVLPFMARL
    DG = -0.98 for segment sequence
    DG = -0.98 for most favorable 19AA centered at #60M
    (VLNPMWRQGMFVLPFMARL)

93-123 (31) : FEGVALAhIVLSGLLFLAACWhWVYWDLELF
    DG = -10.39 for segment sequence
    DG = -9.32 for most favorable 19AA centered at #102V
    (FEGVALAhIVLSGLLFLAA)

138-166 (29) : MFGIhLFLAGLLCFGFGAFhLTGLFGPGM
    DG = -15.31 for segment sequence
    DG = -15.14 for most favorable 19AA centered at #147G
    (MFGIhLFLAGLLCFGFGAF)

201-219 (19) : hhIAAGIVGIIAGLFhILV
    DG = -7.91 for segment sequence
    DG = -7.91 for most favorable 19AA centered at #210I
    (hhIAAGIVGIIAGLFhILV)

241-259 (19) : SIAAVFFAAFVVAGTMWYG
    DG = -8.05 for segment sequence
    DG = -8.05 for most favorable 19AA centered at #250F
    (SIAAVFFAAFVVAGTMWYG)

353-371 (19) : ELFVRRMPAFFESFPVILT
    DG = -1.67 for segment sequence
    DG = -1.67 for most favorable 19AA centered at #362F
    (ELFVRRMPAFFESFPVILT)

450-479 (30) : WFTFAhAVFALLFFFGhIWhGARTLFRDVF
    DG = -10.25 for segment sequence
    DG = -14.58 for most favorable 19AA centered at #459A
    (WFTFAhAVFALLFFFGhIW)


Number of known transmembrane regions: 0


Working sequence (length = 510):

MGLPWYRVhTVLINDPGRLIAAhLMhTALVAGWAGSMALYELATFDPSDPVLNPMWRQGM
FVLPFMARLGVTGSWSGWSITGETGIDPGFWSFEGVALAhIVLSGLLFLAACWhWVYWDL
ELFRDPRTGEPALDLPKMFGIhLFLAGLLCFGFGAFhLTGLFGPGMWVSDPYGLTGSVQP
VAPEWGPDGFNPYNPGGVVAhhIAAGIVGIIAGLFhILVRPPQRLYKALRMGNIETVLSS
SIAAVFFAAFVVAGTMWYGSATTPIELFGPTRYQWDSSYFQQEINRRVQASLASGATLEE
AWSAIPEKLAFYDYIGNNPAKGGLFRTGPMNKGDGIAQAWKGhAVFRNKEGEELFVRRMP
AFFESFPVILTDKNGVVKADIPFRRAESKYSFEQQGVTVSFYGGELNGQTFTDPPTVKSY
ARKAIFGEIFEFDTETLNSDGIFRTSPRGWFTFAhAVFALLFFFGhIWhGARTLFRDVFS
GIDPELSPEQVEWGFYQKVGDVTTRRKEAV

