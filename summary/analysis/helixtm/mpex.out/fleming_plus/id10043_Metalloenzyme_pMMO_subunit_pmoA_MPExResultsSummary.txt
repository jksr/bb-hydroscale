Results summary for protein: id10043_Metalloenzyme_pMMO_subunit_pmoA
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 11 Neutral
    His 38 Neutral
    His 40 Neutral
    His 168 Neutral
    His 232 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

21-39 (19) : IDWMALFVVFFVIVGSYhI
    DG = -7.18 for segment sequence
    DG = -7.18 for most favorable 19AA centered at #30F
    (IDWMALFVVFFVIVGSYhI)

61-79 (19) : VTVTPIVLVTFPAAVQSYL
    DG = -5.23 for segment sequence
    DG = -5.23 for most favorable 19AA centered at #70T
    (VTVTPIVLVTFPAAVQSYL)

85-103 (19) : LPWGATVCVLGLLLGEWIN
    DG = -1.4 for segment sequence
    DG = -1.4 for most favorable 19AA centered at #94L
    (LPWGATVCVLGLLLGEWIN)

113-143 (31) : YFPINFVFPASLVPGAIILDTVLMLSGSYLF
    DG = -15.24 for segment sequence
    DG = -15.09 for most favorable 19AA centered at #122A
    (YFPINFVFPASLVPGAIIL)

149-180 (32) : AMGWGLIFYPGNWPIIAPLhVPVEYNGMLMSI
    DG = -4.17 for segment sequence
    DG = -9.04 for most favorable 19AA centered at #158P
    (AMGWGLIFYPGNWPIIAPL)

212-230 (19) : VAPVSAFFSAFMSILIYFM
    DG = -13.93 for segment sequence
    DG = -13.93 for most favorable 19AA centered at #221A
    (VAPVSAFFSAFMSILIYFM)


Number of known transmembrane regions: 0


Working sequence (length = 247):

MSAAQSAVRShAEAVQVSRTIDWMALFVVFFVIVGSYhIhAMLTMGDWDFWSDWKDRRLW
VTVTPIVLVTFPAAVQSYLWERYRLPWGATVCVLGLLLGEWINRYFNFWGWTYFPINFVF
PASLVPGAIILDTVLMLSGSYLFTAIVGAMGWGLIFYPGNWPIIAPLhVPVEYNGMLMSI
ADIQGYNYVRTGTPEYIRMVEKGTLRTFGKDVAPVSAFFSAFMSILIYFMWhFIGRWFSN
ERFLQST

