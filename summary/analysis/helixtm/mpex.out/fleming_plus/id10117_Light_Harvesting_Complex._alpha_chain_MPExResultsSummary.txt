Results summary for protein: id10117_Light_Harvesting_Complex._alpha_chain
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 31 Neutral
    His 37 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

12-44 (33) : PAIGIPALLGSVTVIAILVhLAILShTTWFPAY
    DG = -5.46 for segment sequence
    DG = -10 for most favorable 19AA centered at #21G
    (PAIGIPALLGSVTVIAILV)


Number of known transmembrane regions: 0


Working sequence (length = 53):

MNQGKIWTVVNPAIGIPALLGSVTVIAILVhLAILShTTWFPAYWQGGVKKAA

