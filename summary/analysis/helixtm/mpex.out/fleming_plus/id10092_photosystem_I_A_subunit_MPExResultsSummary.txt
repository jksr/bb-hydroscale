Results summary for protein: id10092_photosystem_I_A_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 33 Neutral
    His 52 Neutral
    His 56 Neutral
    His 61 Neutral
    His 76 Neutral
    His 79 Neutral
    His 93 Neutral
    His 135 Neutral
    His 179 Neutral
    His 181 Neutral
    His 199 Neutral
    His 200 Neutral
    His 215 Neutral
    His 218 Neutral
    His 240 Neutral
    His 299 Neutral
    His 300 Neutral
    His 301 Neutral
    His 313 Neutral
    His 323 Neutral
    His 332 Neutral
    His 341 Neutral
    His 353 Neutral
    His 373 Neutral
    His 396 Neutral
    His 397 Neutral
    His 411 Neutral
    His 436 Neutral
    His 443 Neutral
    His 454 Neutral
    His 461 Neutral
    His 494 Neutral
    His 539 Neutral
    His 540 Neutral
    His 542 Neutral
    His 547 Neutral
    His 594 Neutral
    His 612 Neutral
    His 633 Neutral
    His 680 Neutral
    His 708 Neutral
    His 734 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

156-174 (19) : FQLYCTAIGGLVMAGLMLF
    DG = -6.15 for segment sequence
    DG = -6.15 for most favorable 19AA centered at #165G
    (FQLYCTAIGGLVMAGLMLF)

236-281 (46) : IPLPhEFILNPSLMAELYPKVDWGFFSGVIPFFTFNWAAYSDFLTF
    DG = -4.4 for segment sequence
    DG = -6.12 for most favorable 19AA centered at #245N
    (IPLPhEFILNPSLMAELYP)

366-394 (29) : LSIIVAQhMYAMPPYPYLATDYPTQLSLF
    DG = -4.35 for segment sequence
    DG = -7.27 for most favorable 19AA centered at #375Y
    (LSIIVAQhMYAMPPYPYLA)

595-613 (19) : VFLGLFWMYNCISVVIFhF
    DG = -7.84 for segment sequence
    DG = -7.84 for most favorable 19AA centered at #604N
    (VFLGLFWMYNCISVVIFhF)

673-701 (29) : GLLFLGAhFIWAFSLMFLFSGRGYWQELI
    DG = -3.42 for segment sequence
    DG = -12.72 for most favorable 19AA centered at #682I
    (GLLFLGAhFIWAFSLMFLF)

735-753 (19) : YLLGGIATTWAFFLARIIS
    DG = -3.44 for segment sequence
    DG = -3.44 for most favorable 19AA centered at #744W
    (YLLGGIATTWAFFLARIIS)


Number of known transmembrane regions: 0


Working sequence (length = 755):

MTISPPEREPKVRVVVDNDPVPTSFEKWAKPGhFDRTLARGPQTTTWIWNLhALAhDFDT
hTSDLEDISRKIFSAhFGhLAVVFIWLSGMYFhGAKFSNYEAWLADPTGIKPSAQVVWPI
VGQGILNGDVGGGFhGIQITSGLFQLWRASGITNEFQLYCTAIGGLVMAGLMLFAGWFhY
hKRAPKLEWFQNVESMLNhhLAGLLGLGSLAWAGhQIhVSLPINKLLDAGVAAKDIPLPh
EFILNPSLMAELYPKVDWGFFSGVIPFFTFNWAAYSDFLTFNGGLNPVTGGLWLSDTAhh
hLAIAVLFIIAGhMYRTNWGIGhSLKEILEAhKGPFTGAGhKGLYEVLTTSWhAQLAINL
AMMGSLSIIVAQhMYAMPPYPYLATDYPTQLSLFThhMWIGGFLVVGGAAhGAIFMVRDY
DPAMNQNNVLDRVLRhRDAIIShLNWVCIFLGFhSFGLYVhNDTMRAFGRPQDMFSDTGI
QLQPVFAQWVQNLhTLAPGGTAPNAAATASVAFGGDVVAVGGKVAMMPIVLGTADFMVhh
IhAFTIhVTVLILLKGVLFARSSRLIPDKANLGFRFPCDGPGRGGTCQVSGWDhVFLGLF
WMYNCISVVIFhFSWKMQSDVWGTVAPDGTVShITGGNFAQSAITINGWLRDFLWAQASQ
VIGSYGSALSAYGLLFLGAhFIWAFSLMFLFSGRGYWQELIESIVWAhNKLKVAPAIQPR
ALSIIQGRAVGVAhYLLGGIATTWAFFLARIISVG

