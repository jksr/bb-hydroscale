Results summary for protein: id10086_cyto.c_oxidase._Chain_I
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 72 Neutral
    His 142 Neutral
    His 233 Neutral
    His 282 Neutral
    His 283 Neutral
    His 298 Neutral
    His 376 Neutral
    His 384 Neutral
    His 386 Neutral
    His 440 Neutral
    His 462 Neutral
    His 552 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 12

23-55 (33) : YFLVLGFLALIVGSLFGPFQALNYGNVDAYPLL
    DG = -11.88 for segment sequence
    DG = -16.59 for most favorable 19AA centered at #32L
    (YFLVLGFLALIVGSLFGPF)

75-93 (19) : LNAIVFTQLFAQAIMVYLP
    DG = -6.61 for segment sequence
    DG = -6.61 for most favorable 19AA centered at #84F
    (LNAIVFTQLFAQAIMVYLP)

107-163 (57) : WLSWWMAFIGLVVAALPLLANEATVLYTFYPPLKGhWAFYLGASVFVLSTWVSIYIV
    DG = -13.22 for segment sequence
    DG = -14.24 for most favorable 19AA centered at #116G
    (WLSWWMAFIGLVVAALPLL)

191-223 (33) : LMWFLASLGLVLEAVLFLLPWSFGLVEGVDPLV
    DG = -17.08 for segment sequence
    DG = -15.88 for most favorable 19AA centered at #200L
    (LMWFLASLGLVLEAVLFLL)

234-252 (19) : PIVYFWLLPAYAIIYTILP
    DG = -21.08 for segment sequence
    DG = -21.08 for most favorable 19AA centered at #243A
    (PIVYFWLLPAYAIIYTILP)

263-281 (19) : PMARLAFLLFLLLSTPVGF
    DG = -13 for segment sequence
    DG = -13 for most favorable 19AA centered at #272F
    (PMARLAFLLFLLLSTPVGF)

300-318 (19) : VLTLFVAVPSLMTAFTVAA
    DG = -8.06 for segment sequence
    DG = -8.06 for most favorable 19AA centered at #309S
    (VLTLFVAVPSLMTAFTVAA)

339-357 (19) : LPWDNPAFVAPVLGLLGFI
    DG = -9.84 for segment sequence
    DG = -9.84 for most favorable 19AA centered at #348A
    (LPWDNPAFVAPVLGLLGFI)

390-408 (19) : ASLVTLTAMGSLYWLLPNL
    DG = -2.98 for segment sequence
    DG = -2.98 for most favorable 19AA centered at #399G
    (ASLVTLTAMGSLYWLLPNL)

421-439 (19) : GLAVVWLWFLGMMIMAVGL
    DG = -11.22 for segment sequence
    DG = -11.22 for most favorable 19AA centered at #430L
    (GLAVVWLWFLGMMIMAVGL)

475-493 (19) : IVLLVALLLFIYGLFSVLL
    DG = -21.88 for segment sequence
    DG = -21.88 for most favorable 19AA centered at #484F
    (IVLLVALLLFIYGLFSVLL)

529-557 (29) : FWFAVAAILVVLAYGPTLVQLFGhLNPVP
    DG = -10.68 for segment sequence
    DG = -14 for most favorable 19AA centered at #538V
    (FWFAVAAILVVLAYGPTLV)


Number of known transmembrane regions: 0


Working sequence (length = 562):

MAVRASEISRVYEAYPEKKATLYFLVLGFLALIVGSLFGPFQALNYGNVDAYPLLKRLLP
FVQSYYQGLTLhGVLNAIVFTQLFAQAIMVYLPARELNMRPNMGLMWLSWWMAFIGLVVA
ALPLLANEATVLYTFYPPLKGhWAFYLGASVFVLSTWVSIYIVLDLWRRWKAANPGKVTP
LVTYMAVVFWLMWFLASLGLVLEAVLFLLPWSFGLVEGVDPLVARTLFWWTGhPIVYFWL
LPAYAIIYTILPKQAGGKLVSDPMARLAFLLFLLLSTPVGFhhQFADPGIDPTWKMIhSV
LTLFVAVPSLMTAFTVAASLEFAGRLRGGRGLFGWIRALPWDNPAFVAPVLGLLGFIPGG
AGGIVNASFTLDYVVhNTAWVPGhFhLQVASLVTLTAMGSLYWLLPNLTGKPISDAQRRL
GLAVVWLWFLGMMIMAVGLhWAGLLNVPRRAYIAQVPDAYPhAAVPMVFNVLAGIVLLVA
LLLFIYGLFSVLLSRERKPELAEAPLPFAEVISGPEDRRLVLAMDRIGFWFAVAAILVVL
AYGPTLVQLFGhLNPVPGWRLW

