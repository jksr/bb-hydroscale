Results summary for protein: id10118_Light_Harvesting_Complex._beta_chain
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 12 Neutral
    His 30 Neutral
    His 41 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

22-40 (19) : FLGLALVAhFLAFSATPWL
    DG = -8.24 for segment sequence
    DG = -8.24 for most favorable 19AA centered at #31F
    (FLGLALVAhFLAFSATPWL)


Number of known transmembrane regions: 0


Working sequence (length = 41):

ATLTAEQSEELhKYVIDGTRVFLGLALVAhFLAFSATPWLh

