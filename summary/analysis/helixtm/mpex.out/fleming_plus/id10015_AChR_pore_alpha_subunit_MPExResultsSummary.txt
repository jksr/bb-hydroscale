Results summary for protein: id10015_AChR_pore_alpha_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 3 Neutral
    His 24 Neutral
    His 25 Neutral
    His 27 Neutral
    His 104 Neutral
    His 134 Neutral
    His 186 Neutral
    His 204 Neutral
    His 299 Neutral
    His 300 Neutral
    His 306 Neutral
    His 385 Neutral
    His 408 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

218-236 (19) : VIIPCLLFSFLTVLVFYLP
    DG = -21.14 for segment sequence
    DG = -21.14 for most favorable 19AA centered at #227F
    (VIIPCLLFSFLTVLVFYLP)

247-265 (19) : ISVLLSLTVFLLVIVELIP
    DG = -14.79 for segment sequence
    DG = -14.79 for most favorable 19AA centered at #256F
    (ISVLLSLTVFLLVIVELIP)

278-296 (19) : MLFTMIFVISSIIVTVVVI
    DG = -12.21 for segment sequence
    DG = -12.21 for most favorable 19AA centered at #287S
    (MLFTMIFVISSIIVTVVVI)

409-427 (19) : ILLCVFMLICIIGTVSVFA
    DG = -12.86 for segment sequence
    DG = -12.86 for most favorable 19AA centered at #418C
    (ILLCVFMLICIIGTVSVFA)


Number of known transmembrane regions: 0


Working sequence (length = 437):

SEhETRLVANLLENYNKVIRPVEhhThFVDITVGLQLIQLINVDEVNQIVETNVRLRQQW
IDVRLRWNPADYGGIKKIRLPSDDVWLPDLVLYNNADGDFAIVhMTKLLLDYTGKIMWTP
PAIFKSYCEIIVThFPFDQQNCTMKLGIWTYDGTKVSISPESDRPDLSTFMESGEWVMKD
YRGWKhWVYYTCCPDTPYLDITYhFIMQRIPLYFVVNVIIPCLLFSFLTVLVFYLPTDSG
EKMTLSISVLLSLTVFLLVIVELIPSTSSAVPLIGKYMLFTMIFVISSIIVTVVVINThh
RSPSThTMPQWVRKIFINTIPNVMFFSTMKRASKEKQENKIFADDIDISDISGKQVTGEV
IFQTPLIKNPDVKSAIEGVKYIAEhMKSDEESSNAAEEWKYVAMVIDhILLCVFMLICII
GTVSVFAGRLIELSQEG

