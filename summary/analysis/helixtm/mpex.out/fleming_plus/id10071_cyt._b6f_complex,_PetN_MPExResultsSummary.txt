Results summary for protein: id10071_cyt._b6f_complex,_PetN
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

5-23 (19) : VLGWVALLVVFTWSIAMVV
    DG = -10.06 for segment sequence
    DG = -10.06 for most favorable 19AA centered at #14V
    (VLGWVALLVVFTWSIAMVV)


Number of known transmembrane regions: 0


Working sequence (length = 29):

MEIDVLGWVALLVVFTWSIAMVVWGRNGL

