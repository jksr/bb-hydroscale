Results summary for protein: id10089_ubiquinol_oxidase_subunit_I
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 13 Neutral
    His 54 Neutral
    His 97 Neutral
    His 98 Neutral
    His 106 Neutral
    His 262 Neutral
    His 284 Neutral
    His 333 Neutral
    His 334 Neutral
    His 378 Neutral
    His 411 Neutral
    His 419 Neutral
    His 421 Neutral
    His 492 Neutral
    His 557 Neutral
    His 559 Neutral
    His 579 Neutral
    His 584 Neutral
    His 609 Neutral
    His 650 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 14

16-44 (29) : IVMVTIAGIILGGLALVGLITYFGKWTYL
    DG = -5.38 for segment sequence
    DG = -7.92 for most favorable 19AA centered at #25I
    (IVMVTIAGIILGGLALVGL)

59-77 (19) : IMYIIVAIVMLLRGFADAI
    DG = -9.41 for segment sequence
    DG = -9.41 for most favorable 19AA centered at #68M
    (IMYIIVAIVMLLRGFADAI)

111-156 (46) : IFFVAMPFVIGLMNLVVPLQIGARDVAFPFLNNLSFWFTVVGVILV
    DG = -17.13 for segment sequence
    DG = -17.64 for most favorable 19AA centered at #120I
    (IFFVAMPFVIGLMNLVVPL)

169-187 (19) : GWLAYPPLSGIEYSPGVGV
    DG = -1.17 for segment sequence
    DG = -1.17 for most favorable 19AA centered at #178G
    (GWLAYPPLSGIEYSPGVGV)

237-255 (19) : VLIIASFPILTVTVALLTL
    DG = -12.62 for segment sequence
    DG = -12.62 for most favorable 19AA centered at #246L
    (VLIIASFPILTVTVALLTL)

287-305 (19) : VYILILPVFGVFSEIAATF
    DG = -12.37 for segment sequence
    DG = -12.37 for most favorable 19AA centered at #296G
    (VYILILPVFGVFSEIAATF)

314-332 (19) : TSLVWATVCITVLSFIVWL
    DG = -5.14 for segment sequence
    DG = -5.14 for most favorable 19AA centered at #323I
    (TSLVWATVCITVLSFIVWL)

353-371 (19) : MIIAIPTGVKIFNWLFTMY
    DG = -3.6 for segment sequence
    DG = -3.6 for most favorable 19AA centered at #362K
    (MIIAIPTGVKIFNWLFTMY)

375-417 (43) : IVFhSAMLWTIGFIVTFSVGGMTGVLLAVPGADFVLhNSLFLI
    DG = -0.83 for segment sequence
    DG = -4.14 for most favorable 19AA centered at #408F
    (VLLAVPGADFVLhNSLFLI)

423-441 (19) : VIIGGVVFGCFAGMTYWWP
    DG = -4.84 for segment sequence
    DG = -4.84 for most favorable 19AA centered at #432C
    (VIIGGVVFGCFAGMTYWWP)

456-474 (19) : AFWFWIIGFFVAFMPLYAL
    DG = -20.93 for segment sequence
    DG = -20.93 for most favorable 19AA centered at #465F
    (AFWFWIIGFFVAFMPLYAL)

502-520 (19) : AVLIALGILCLVIQMYVSI
    DG = -10.62 for segment sequence
    DG = -10.62 for most favorable 19AA centered at #511C
    (AVLIALGILCLVIQMYVSI)

538-556 (19) : TLEWATSSPPPFYNFAVVP
    DG = -2.48 for segment sequence
    DG = -2.48 for most favorable 19AA centered at #547P
    (TLEWATSSPPPFYNFAVVP)

601-630 (30) : IFGFAMIWhIWWLAIVGFAGMIITWIVKSF
    DG = -7.21 for segment sequence
    DG = -9.13 for most favorable 19AA centered at #610I
    (IFGFAMIWhIWWLAIVGFA)


Number of known transmembrane regions: 0


Working sequence (length = 663):

MFGKLSLDAVPFhEPIVMVTIAGIILGGLALVGLITYFGKWTYLWKEWLTSVDhKRLGIM
YIIVAIVMLLRGFADAIMMRSQQALASAGEAGFLPPhhYDQIFTAhGVIMIFFVAMPFVI
GLMNLVVPLQIGARDVAFPFLNNLSFWFTVVGVILVNVSLGVGEFAQTGWLAYPPLSGIE
YSPGVGVDYWIWSLQLSGIGTTLTGINFFVTILKMRAPGMTMFKMPVFTWASLCANVLII
ASFPILTVTVALLTLDRYLGThFFTNDMGGNMMMYINLIWAWGhPEVYILILPVFGVFSE
IAATFSRKRLFGYTSLVWATVCITVLSFIVWLhhFFTMGAGANVNAFFGITTMIIAIPTG
VKIFNWLFTMYQGRIVFhSAMLWTIGFIVTFSVGGMTGVLLAVPGADFVLhNSLFLIAhF
hNVIIGGVVFGCFAGMTYWWPKAFGFKLNETWGKRAFWFWIIGFFVAFMPLYALGFMGMT
RRLSQQIDPQFhTMLMIAASGAVLIALGILCLVIQMYVSIRDRDQNRDLTGDPWGGRTLE
WATSSPPPFYNFAVVPhVhERDAFWEMKEKGEAYKKPDhYEEIhMPKNSGAGIVIAAFST
IFGFAMIWhIWWLAIVGFAGMIITWIVKSFDEDVDYYVPVAEIEKLENQhFDEITKAGLK
NGN

