Results summary for protein: id10106_Photosystem_II_Cytochrome_b559_beta_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 24 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

25-43 (19) : TLAVPTIFFLGAIAAMQFI
    DG = -9.67 for segment sequence
    DG = -9.67 for most favorable 19AA centered at #34L
    (TLAVPTIFFLGAIAAMQFI)


Number of known transmembrane regions: 0


Working sequence (length = 45):

MTSNTPNQEPVSYPIFTVRWVAVhTLAVPTIFFLGAIAAMQFIQR

