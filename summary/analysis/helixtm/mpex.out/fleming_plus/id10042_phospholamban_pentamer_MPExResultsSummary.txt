Results summary for protein: id10042_phospholamban_pentamer
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

31-49 (19) : LFINFCLILICLLLICIIV
    DG = -20.46 for segment sequence
    DG = -20.46 for most favorable 19AA centered at #40I
    (LFINFCLILICLLLICIIV)


Number of known transmembrane regions: 0


Working sequence (length = 52):

MEKVQYLTRSAIRRASTIEMPQQARQKLQNLFINFCLILICLLLICIIVMLL

