Results summary for protein: id10077_cyto._C_oxidase._Subunit_VIc
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 20 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

21-39 (19) : IVGAFMVSLGFATFYKFAV
    DG = -3.92 for segment sequence
    DG = -3.92 for most favorable 19AA centered at #30G
    (IVGAFMVSLGFATFYKFAV)


Number of known transmembrane regions: 0


Working sequence (length = 73):

STALAKPQMRGLLARRLRFhIVGAFMVSLGFATFYKFAVAEKRKKAYADFYRNYDSMKDF
EEMRKAGIFQSAK

