Results summary for protein: id10006_M13_coat_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

21-39 (19) : YIGYAWAMVVVIVGATIGI
    DG = -5.74 for segment sequence
    DG = -5.74 for most favorable 19AA centered at #30V
    (YIGYAWAMVVVIVGATIGI)


Number of known transmembrane regions: 0


Working sequence (length = 50):

AEGDDPAKAAFNSLQASATEYIGYAWAMVVVIVGATIGIKLFKKFTSKAS

