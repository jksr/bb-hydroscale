Results summary for protein: id10125_Reaction_center._Chain_H
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 68 Neutral
    His 98 Neutral
    His 126 Neutral
    His 128 Neutral
    His 141 Neutral
    His 204 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

12-30 (19) : LASLAIYSFWIFLAGLIYY
    DG = -14.59 for segment sequence
    DG = -14.59 for most favorable 19AA centered at #21W
    (LASLAIYSFWIFLAGLIYY)


Number of known transmembrane regions: 0


Working sequence (length = 260):

MVGVTAFGNFDLASLAIYSFWIFLAGLIYYLQTENMREGYPLENEDGTPAANQGPFPLPK
PKTFILPhGRGTLTVPGPESEDRPIALARTAVSEGFPhAPTGDPMKDGVGPASWVARRDL
PELDGhGhNKIKPMKAAAGFhVSAGKNPIGLPVRGCDLEIAGKVVDIWVDIPEQMARFLE
VELKDGSTRLLPMQMVKVQSNRVhVNALSSDLFAGIPTIKSPTEVTLLEEDKICGYVAGG
LMYAAPKRKSVVAAMLAEYA

