Results summary for protein: id10105_Photosystem_II_Cytochrome_b559_alpha_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 22 Neutral
Scale: Fleming
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

24-42 (19) : ITIPALFIAGWLFVSTGLA
    DG = -8.36 for segment sequence
    DG = -8.36 for most favorable 19AA centered at #33G
    (ITIPALFIAGWLFVSTGLA)


Number of known transmembrane regions: 0


Working sequence (length = 83):

AGTTGERPFSDIITSVRYWVIhSITIPALFIAGWLFVSTGLAYDVFGTPRPDSYYAQEQR
SIPLVTDRFEAKQQVETFLEQLK

