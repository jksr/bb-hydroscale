Results summary for protein: id10123_Reaction_center._Chain_L
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 116 Neutral
    His 144 Neutral
    His 153 Neutral
    His 168 Neutral
    His 173 Neutral
    His 190 Neutral
    His 211 Neutral
    His 230 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

31-49 (19) : VGFFGVSAIFFIFLGVSLI
    DG = -11.26 for segment sequence
    DG = -11.26 for most favorable 19AA centered at #40F
    (VGFFGVSAIFFIFLGVSLI)

84-102 (19) : GFWQAITVCALGAFISWML
    DG = -4.23 for segment sequence
    DG = -4.23 for most favorable 19AA centered at #93A
    (GFWQAITVCALGAFISWML)

121-151 (31) : FCVPIFMFCVLQVFRPLLLGSWGhAFPYGIL
    DG = -7.54 for segment sequence
    DG = -9.86 for most favorable 19AA centered at #130V
    (FCVPIFMFCVLQVFRPLLL)

179-197 (19) : FLFVNAMALGLhGGLILSV
    DG = -7.53 for segment sequence
    DG = -7.53 for most favorable 19AA centered at #188G
    (FLFVNAMALGLhGGLILSV)

232-250 (19) : LGLFLASNIFLTGAFGTIA
    DG = -5.24 for segment sequence
    DG = -5.24 for most favorable 19AA centered at #241F
    (LGLFLASNIFLTGAFGTIA)


Number of known transmembrane regions: 0


Working sequence (length = 273):

ALLSFERKYRVRGGTLIGGDLFDFWVGPYFVGFFGVSAIFFIFLGVSLIGYAASQGPTWD
PFAISINPPDLKYGLGAAPLLEGGFWQAITVCALGAFISWMLREVEISRKLGIGWhVPLA
FCVPIFMFCVLQVFRPLLLGSWGhAFPYGILShLDWVNNFGYQYLNWhYNPGhMSSVSFL
FVNAMALGLhGGLILSVANPGDGDKVKTAEhENQYFRDVVGYSIGALSIhRLGLFLASNI
FLTGAFGTIASGPFWTRGWPEWWGWWLDIPFWS

