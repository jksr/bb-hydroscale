Results summary for protein: id10121_LHC-II
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 68 Neutral
    His 120 Neutral
    His 212 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

67-85 (19) : IhCRWAMLGALGCVFPELL
    DG = -1.91 for segment sequence
    DG = -1.91 for most favorable 19AA centered at #76A
    (IhCRWAMLGALGCVFPELL)

118-136 (19) : LVhAQSILAIWACQVILMG
    DG = -3.8 for segment sequence
    DG = -3.8 for most favorable 19AA centered at #127I
    (LVhAQSILAIWACQVILMG)

184-202 (19) : GRLAMFSMFGFFVQAIVTG
    DG = -2.22 for segment sequence
    DG = -2.22 for most favorable 19AA centered at #193G
    (GRLAMFSMFGFFVQAIVTG)


Number of known transmembrane regions: 0


Working sequence (length = 232):

RKTAGKPKTVQSSSPWYGPDRVKYLGPFSGESPSYLTGEFPGDYGWDTAGLSADPETFAK
NRELEVIhCRWAMLGALGCVFPELLARNGVKFGEAVWFKAGSQIFSEGGLDYLGNPSLVh
AQSILAIWACQVILMGAVEGYRIAGGPLGEVVDPLYPGGSFDPLGLADDPEAFAELKVKE
IKNGRLAMFSMFGFFVQAIVTGKGPLENLADhLADPVNNNAWNFATNFVPGK

