Results summary for protein: id10095_photosystem_I_I_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

14-32 (19) : FIPVVCWLMPTVVMGLLFL
    DG = -12.17 for segment sequence
    DG = -12.17 for most favorable 19AA centered at #23P
    (FIPVVCWLMPTVVMGLLFL)


Number of known transmembrane regions: 0


Working sequence (length = 38):

MMGSYAASFLPWIFIPVVCWLMPTVVMGLLFLYIEGEA

