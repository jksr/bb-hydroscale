Results summary for protein: id10092_photosystem_I_A_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 33 Neutral
    His 52 Neutral
    His 56 Neutral
    His 61 Neutral
    His 76 Neutral
    His 79 Neutral
    His 93 Neutral
    His 135 Neutral
    His 179 Neutral
    His 181 Neutral
    His 199 Neutral
    His 200 Neutral
    His 215 Neutral
    His 218 Neutral
    His 240 Neutral
    His 299 Neutral
    His 300 Neutral
    His 301 Neutral
    His 313 Neutral
    His 323 Neutral
    His 332 Neutral
    His 341 Neutral
    His 353 Neutral
    His 373 Neutral
    His 396 Neutral
    His 397 Neutral
    His 411 Neutral
    His 436 Neutral
    His 443 Neutral
    His 454 Neutral
    His 461 Neutral
    His 494 Neutral
    His 539 Neutral
    His 540 Neutral
    His 542 Neutral
    His 547 Neutral
    His 594 Neutral
    His 612 Neutral
    His 633 Neutral
    His 680 Neutral
    His 708 Neutral
    His 734 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 11

72-90 (19) : IFSAhFGhLAVVFIWLSGM
    DG = -4.84 for segment sequence
    DG = -4.84 for most favorable 19AA centered at #81A
    (IFSAhFGhLAVVFIWLSGM)

156-184 (29) : FQLYCTAIGGLVMAGLMLFAGWFhYhKRA
    DG = -0.31 for segment sequence
    DG = -8.1 for most favorable 19AA centered at #165G
    (FQLYCTAIGGLVMAGLMLF)

201-219 (19) : LAGLLGLGSLAWAGhQIhV
    DG = -1.43 for segment sequence
    DG = -1.43 for most favorable 19AA centered at #210L
    (LAGLLGLGSLAWAGhQIhV)

292-320 (29) : LWLSDTAhhhLAIAVLFIIAGhMYRTNWG
    DG = 4.7 for segment sequence
    DG = -3.41 for most favorable 19AA centered at #301h
    (LWLSDTAhhhLAIAVLFII)

356-384 (29) : LAINLAMMGSLSIIVAQhMYAMPPYPYLA
    DG = -1.37 for segment sequence
    DG = -2.13 for most favorable 19AA centered at #365S
    (LAINLAMMGSLSIIVAQhM)

399-417 (19) : WIGGFLVVGGAAhGAIFMV
    DG = -5.34 for segment sequence
    DG = -5.34 for most favorable 19AA centered at #408G
    (WIGGFLVVGGAAhGAIFMV)

440-458 (19) : IIShLNWVCIFLGFhSFGL
    DG = -3.32 for segment sequence
    DG = -3.32 for most favorable 19AA centered at #449I
    (IIShLNWVCIFLGFhSFGL)

513-559 (47) : FGGDVVAVGGKVAMMPIVLGTADFMVhhIhAFTIhVTVLILLKGVLF
    DG = -3.3 for segment sequence
    DG = -8.07 for most favorable 19AA centered at #550V
    (IhAFTIhVTVLILLKGVLF)

595-613 (19) : VFLGLFWMYNCISVVIFhF
    DG = -8.65 for segment sequence
    DG = -8.65 for most favorable 19AA centered at #604N
    (VFLGLFWMYNCISVVIFhF)

673-701 (29) : GLLFLGAhFIWAFSLMFLFSGRGYWQELI
    DG = -6.61 for segment sequence
    DG = -13.12 for most favorable 19AA centered at #682I
    (GLLFLGAhFIWAFSLMFLF)

730-748 (19) : VGVAhYLLGGIATTWAFFL
    DG = -5.91 for segment sequence
    DG = -5.91 for most favorable 19AA centered at #739G
    (VGVAhYLLGGIATTWAFFL)


Number of known transmembrane regions: 0


Working sequence (length = 755):

MTISPPEREPKVRVVVDNDPVPTSFEKWAKPGhFDRTLARGPQTTTWIWNLhALAhDFDT
hTSDLEDISRKIFSAhFGhLAVVFIWLSGMYFhGAKFSNYEAWLADPTGIKPSAQVVWPI
VGQGILNGDVGGGFhGIQITSGLFQLWRASGITNEFQLYCTAIGGLVMAGLMLFAGWFhY
hKRAPKLEWFQNVESMLNhhLAGLLGLGSLAWAGhQIhVSLPINKLLDAGVAAKDIPLPh
EFILNPSLMAELYPKVDWGFFSGVIPFFTFNWAAYSDFLTFNGGLNPVTGGLWLSDTAhh
hLAIAVLFIIAGhMYRTNWGIGhSLKEILEAhKGPFTGAGhKGLYEVLTTSWhAQLAINL
AMMGSLSIIVAQhMYAMPPYPYLATDYPTQLSLFThhMWIGGFLVVGGAAhGAIFMVRDY
DPAMNQNNVLDRVLRhRDAIIShLNWVCIFLGFhSFGLYVhNDTMRAFGRPQDMFSDTGI
QLQPVFAQWVQNLhTLAPGGTAPNAAATASVAFGGDVVAVGGKVAMMPIVLGTADFMVhh
IhAFTIhVTVLILLKGVLFARSSRLIPDKANLGFRFPCDGPGRGGTCQVSGWDhVFLGLF
WMYNCISVVIFhFSWKMQSDVWGTVAPDGTVShITGGNFAQSAITINGWLRDFLWAQASQ
VIGSYGSALSAYGLLFLGAhFIWAFSLMFLFSGRGYWQELIESIVWAhNKLKVAPAIQPR
ALSIIQGRAVGVAhYLLGGIATTWAFFLARIISVG

