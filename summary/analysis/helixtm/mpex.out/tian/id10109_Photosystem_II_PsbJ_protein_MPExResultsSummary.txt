Results summary for protein: id10109_Photosystem_II_PsbJ_protein
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

10-40 (31) : LWIVATVAGMGVIVIVGLFFYGAYAGLGSSL
    DG = -10.75 for segment sequence
    DG = -11.49 for most favorable 19AA centered at #19M
    (LWIVATVAGMGVIVIVGLF)


Number of known transmembrane regions: 0


Working sequence (length = 40):

MMSEGGRIPLWIVATVAGMGVIVIVGLFFYGAYAGLGSSL

