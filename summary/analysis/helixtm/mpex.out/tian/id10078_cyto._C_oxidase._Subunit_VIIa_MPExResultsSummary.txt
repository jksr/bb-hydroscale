Results summary for protein: id10078_cyto._C_oxidase._Subunit_VIIa
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 21 Neutral
    His 57 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

30-48 (19) : ILYRVTMTLCLGGTLYSLY
    DG = -3.57 for segment sequence
    DG = -3.57 for most favorable 19AA centered at #39C
    (ILYRVTMTLCLGGTLYSLY)


Number of known transmembrane regions: 0


Working sequence (length = 59):

FENRVAEKQKLFQEDNGLPVhLKGGATDNILYRVTMTLCLGGTLYSLYCLGWASFPhKK

