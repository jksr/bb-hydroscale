Results summary for protein: id10105_Photosystem_II_Cytochrome_b559_alpha_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 22 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

19-37 (19) : WVIhSITIPALFIAGWLFV
    DG = -7.61 for segment sequence
    DG = -7.61 for most favorable 19AA centered at #28A
    (WVIhSITIPALFIAGWLFV)


Number of known transmembrane regions: 0


Working sequence (length = 83):

AGTTGERPFSDIITSVRYWVIhSITIPALFIAGWLFVSTGLAYDVFGTPRPDSYYAQEQR
SIPLVTDRFEAKQQVETFLEQLK

