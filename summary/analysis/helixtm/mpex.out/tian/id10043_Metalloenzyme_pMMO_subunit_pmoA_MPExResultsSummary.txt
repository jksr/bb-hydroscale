Results summary for protein: id10043_Metalloenzyme_pMMO_subunit_pmoA
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 11 Neutral
    His 38 Neutral
    His 40 Neutral
    His 168 Neutral
    His 232 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

25-43 (19) : ALFVVFFVIVGSYhIhAML
    DG = -9.53 for segment sequence
    DG = -9.53 for most favorable 19AA centered at #34V
    (ALFVVFFVIVGSYhIhAML)

61-79 (19) : VTVTPIVLVTFPAAVQSYL
    DG = -2.21 for segment sequence
    DG = -2.21 for most favorable 19AA centered at #70T
    (VTVTPIVLVTFPAAVQSYL)

84-102 (19) : RLPWGATVCVLGLLLGEWI
    DG = -3.11 for segment sequence
    DG = -3.11 for most favorable 19AA centered at #93V
    (RLPWGATVCVLGLLLGEWI)

119-171 (53) : VFPASLVPGAIILDTVLMLSGSYLFTAIVGAMGWGLIFYPGNWPIIAPLhVPV
    DG = -5.75 for segment sequence
    DG = -6.21 for most favorable 19AA centered at #138S
    (IILDTVLMLSGSYLFTAIV)

217-235 (19) : AFFSAFMSILIYFMWhFIG
    DG = -8.36 for segment sequence
    DG = -8.36 for most favorable 19AA centered at #226L
    (AFFSAFMSILIYFMWhFIG)


Number of known transmembrane regions: 0


Working sequence (length = 247):

MSAAQSAVRShAEAVQVSRTIDWMALFVVFFVIVGSYhIhAMLTMGDWDFWSDWKDRRLW
VTVTPIVLVTFPAAVQSYLWERYRLPWGATVCVLGLLLGEWINRYFNFWGWTYFPINFVF
PASLVPGAIILDTVLMLSGSYLFTAIVGAMGWGLIFYPGNWPIIAPLhVPVEYNGMLMSI
ADIQGYNYVRTGTPEYIRMVEKGTLRTFGKDVAPVSAFFSAFMSILIYFMWhFIGRWFSN
ERFLQST

