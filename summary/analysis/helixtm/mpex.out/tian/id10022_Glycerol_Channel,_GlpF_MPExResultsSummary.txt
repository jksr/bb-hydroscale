Results summary for protein: id10022_Glycerol_Channel,_GlpF
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 66 Neutral
    His 119 Neutral
    His 120 Neutral
    His 142 Neutral
    His 258 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

14-32 (19) : EFLGTGLLIFFGVGCVAAL
    DG = -7.79 for segment sequence
    DG = -7.79 for most favorable 19AA centered at #23F
    (EFLGTGLLIFFGVGCVAAL)

44-62 (19) : ISVIWGLGVAMAIYLTAGV
    DG = -6.52 for segment sequence
    DG = -6.52 for most favorable 19AA centered at #53A
    (ISVIWGLGVAMAIYLTAGV)

63-91 (29) : SGAhLNPAVTIALWLFACFDKRKVIPFIV
    DG = 5.7 for segment sequence
    DG = -1.24 for most favorable 19AA centered at #72T
    (SGAhLNPAVTIALWLFACF)

95-113 (19) : AGAFCAAALVYGLYYNLFF
    DG = -6.51 for segment sequence
    DG = -6.51 for most favorable 19AA centered at #104V
    (AGAFCAAALVYGLYYNLFF)

148-166 (19) : AFAVEMVITAILMGLILAL
    DG = -12.23 for segment sequence
    DG = -12.23 for most favorable 19AA centered at #157A
    (AFAVEMVITAILMGLILAL)

176-194 (19) : GPLAPLLIGLLIAVIGASM
    DG = -8 for segment sequence
    DG = -8 for most favorable 19AA centered at #185L
    (GPLAPLLIGLLIAVIGASM)

233-251 (19) : FLVPLFGPIVGAIVGAFAY
    DG = -8.46 for segment sequence
    DG = -8.46 for most favorable 19AA centered at #242V
    (FLVPLFGPIVGAIVGAFAY)


Number of known transmembrane regions: 0


Working sequence (length = 281):

MSQTSTLKGQCIAEFLGTGLLIFFGVGCVAALKVAGASFGQWEISVIWGLGVAMAIYLTA
GVSGAhLNPAVTIALWLFACFDKRKVIPFIVSQVAGAFCAAALVYGLYYNLFFDFEQThh
IVRGSVESVDLAGTFSTYPNPhINFVQAFAVEMVITAILMGLILALTDDGNGVPRGPLAP
LLIGLLIAVIGASMGPLTGFAMNPARDFGPKVFAWLAGWGNVAFTGGRDIPYFLVPLFGP
IVGAIVGAFAYRKLIGRhLPCDICVVEEKETTTPSEQKASL

