Results summary for protein: id10020_aquaporin_1
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 36 Neutral
    His 71 Neutral
    His 76 Neutral
    His 182 Neutral
    His 206 Neutral
    His 211 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

9-39 (31) : LFWRAVVAEFLAMILFIFISIGSALGFhYPI
    DG = -11.12 for segment sequence
    DG = -13.28 for most favorable 19AA centered at #18F
    (LFWRAVVAEFLAMILFIFI)

83-114 (32) : LGLLLSCQISVLRAIMYIIAQCVGAIVATAIL
    DG = -9.41 for segment sequence
    DG = -7.21 for most favorable 19AA centered at #105V
    (AIMYIIAQCVGAIVATAIL)

140-158 (19) : GLGIEIIGTLQLVLCVLAT
    DG = -6.17 for segment sequence
    DG = -6.17 for most favorable 19AA centered at #149L
    (GLGIEIIGTLQLVLCVLAT)

172-190 (19) : LAIGFSVALGhLLAIDYTG
    DG = -2.68 for segment sequence
    DG = -2.68 for most favorable 19AA centered at #181G
    (LAIGFSVALGhLLAIDYTG)

215-233 (19) : WVGPFIGAALAVLIYDFIL
    DG = -8.63 for segment sequence
    DG = -8.63 for most favorable 19AA centered at #224L
    (WVGPFIGAALAVLIYDFIL)


Number of known transmembrane regions: 0


Working sequence (length = 271):

MASEFKKKLFWRAVVAEFLAMILFIFISIGSALGFhYPIKSNQTTGAVQDNVKVSLAFGL
SIATLAQSVGhISGAhLNPAVTLGLLLSCQISVLRAIMYIIAQCVGAIVATAILSGITSS
LPDNSLGLNALAPGVNSGQGLGIEIIGTLQLVLCVLATTDRRRRDLGGSGPLAIGFSVAL
GhLLAIDYTGCGINPARSFGSSVIThNFQDhWIFWVGPFIGAALAVLIYDFILAPRSSDL
TDRVKVWTSGQVEEYDLDADDINSRVEMKPK

