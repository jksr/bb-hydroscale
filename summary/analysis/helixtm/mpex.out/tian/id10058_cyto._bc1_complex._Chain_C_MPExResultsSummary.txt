Results summary for protein: id10058_cyto._bc1_complex._Chain_C
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 8 Neutral
    His 54 Neutral
    His 68 Neutral
    His 83 Neutral
    His 97 Neutral
    His 182 Neutral
    His 196 Neutral
    His 201 Neutral
    His 221 Neutral
    His 267 Neutral
    His 308 Neutral
    His 345 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 8

36-54 (19) : LLGICLILQILTGLFLAMh
    DG = -12.33 for segment sequence
    DG = -12.33 for most favorable 19AA centered at #45I
    (LLGICLILQILTGLFLAMh)

89-107 (19) : MFFICLYMhVGRGLYYGSY
    DG = -2.15 for segment sequence
    DG = -2.15 for most favorable 19AA centered at #98V
    (MFFICLYMhVGRGLYYGSY)

115-156 (42) : IGVILLLTVMATAFMGYVLPWGQMSFWGATVITNLLSAIPYI
    DG = -9.35 for segment sequence
    DG = -12.43 for most favorable 19AA centered at #124M
    (IGVILLLTVMATAFMGYVL)

182-200 (19) : hFILPFIIMAIAMVhLLFL
    DG = -14.77 for segment sequence
    DG = -14.77 for most favorable 19AA centered at #191A
    (hFILPFIIMAIAMVhLLFL)

229-247 (19) : ILGALLLILALMLLVLFAP
    DG = -20.23 for segment sequence
    DG = -20.23 for most favorable 19AA centered at #238A
    (ILGALLLILALMLLVLFAP)

274-306 (33) : FLFAYAILRSIPNKLGGVLALAFSILILALIPL
    DG = -12.55 for segment sequence
    DG = -14.49 for most favorable 19AA centered at #297S
    (LGGVLALAFSILILALIPL)

320-338 (19) : LSQCLFWALVADLLTLTWI
    DG = -5.91 for segment sequence
    DG = -5.91 for most favorable 19AA centered at #329V
    (LSQCLFWALVADLLTLTWI)

348-377 (30) : ITIGQLASVLYFLLILVLMPTAGTIENKLL
    DG = -6.88 for segment sequence
    DG = -13.75 for most favorable 19AA centered at #357L
    (ITIGQLASVLYFLLILVLM)


Number of known transmembrane regions: 0


Working sequence (length = 379):

MTNIRKShPLMKIVNNAFIDLPAPSNISSWWNFGSLLGICLILQILTGLFLAMhYTSDTT
TAFSSVThICRDVNYGWIIRYMhANGASMFFICLYMhVGRGLYYGSYTFLETWNIGVILL
LTVMATAFMGYVLPWGQMSFWGATVITNLLSAIPYIGTNLVEWIWGGFSVDKATLTRFFA
FhFILPFIIMAIAMVhLLFLhETGSNNPTGISSDVDKIPFhPYYTIKDILGALLLILALM
LLVLFAPDLLGDPDNYTPANPLNTPPhIKPEWYFLFAYAILRSIPNKLGGVLALAFSILI
LALIPLLhTSKQRSMMFRPLSQCLFWALVADLLTLTWIGGQPVEhPYITIGQLASVLYFL
LILVLMPTAGTIENKLLKW

