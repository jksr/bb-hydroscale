Results summary for protein: id10044_Metalloenzyme_pMMO_subunit_pmoB
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 33 Neutral
    His 48 Neutral
    His 72 Neutral
    His 137 Neutral
    His 139 Neutral
    His 192 Neutral
    His 307 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

8-26 (19) : IAKWSAIGLLSAVAATAFY
    DG = -0.56 for segment sequence
    DG = -0.56 for most favorable 19AA centered at #17L
    (IAKWSAIGLLSAVAATAFY)

188-206 (19) : TYFWhAFWFAIGVAWIGYW
    DG = -4.67 for segment sequence
    DG = -4.67 for most favorable 19AA centered at #197A
    (TYFWhAFWFAIGVAWIGYW)

235-253 (19) : VAMGFLAATILIVVMAMSS
    DG = -7.42 for segment sequence
    DG = -7.42 for most favorable 19AA centered at #244I
    (VAMGFLAATILIVVMAMSS)


Number of known transmembrane regions: 0


Working sequence (length = 414):

MKTIKDRIAKWSAIGLLSAVAATAFYAPSASAhGEKSQAAFMRMRTIhWYDLSWSKEKVK
INETVEIKGKFhVFEGWPETVDEPDVAFLNVGMPGPVFIRKESYIGGQLVPRSVRLEIGK
TYDFRVVLKARRPGDWhVhTMMNVQGGGPIIGPGKWITVEGSMSEFRNPVTTLTGQTVDL
ENYNEGNTYFWhAFWFAIGVAWIGYWSRRPIFIPRLLMVDAGRADELVSATDRKVAMGFL
AATILIVVMAMSSANSKYPITIPLQAGTMRGMKPLELPAPTVSVKVEDATYRVPGRAMRM
KLTITNhGNSPIRLGEFYTASVRFLDSDVYKDTTGYPEDLLAEDGLSVSDNSPLAPGETR
TVDVTASDAAWEVYRLSDIIYDPDSRFAGLLFFFDATGNRQVVQIDAPLIPSFM

