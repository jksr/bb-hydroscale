Results summary for protein: id10050_Fumarate_Reductase._13_kD_anchor
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 81 Neutral
    His 84 Neutral
    His 85 Neutral
    His 88 Neutral
    His 93 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

27-56 (30) : IIAPVMILLVGILLPLGLFPGDALSYERVL
    DG = -9.51 for segment sequence
    DG = -16.21 for most favorable 19AA centered at #36V
    (IIAPVMILLVGILLPLGLF)

62-80 (19) : FIGRVFLFLMIVLPLWCGL
    DG = -12.98 for segment sequence
    DG = -12.98 for most favorable 19AA centered at #71M
    (FIGRVFLFLMIVLPLWCGL)

99-117 (19) : WVFYGLAAILTVVTLIGVV
    DG = -11.35 for segment sequence
    DG = -11.35 for most favorable 19AA centered at #108L
    (WVFYGLAAILTVVTLIGVV)


Number of known transmembrane regions: 0


Working sequence (length = 119):

MINPNPKRSDEPVFWGLFGAGGMWSAIIAPVMILLVGILLPLGLFPGDALSYERVLAFAQ
SFIGRVFLFLMIVLPLWCGLhRMhhAMhDLKIhVPAGKWVFYGLAAILTVVTLIGVVTI

