Results summary for protein: id10051_Fumarate_Reductase_alpha_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 44 Neutral
    His 93 Neutral
    His 114 Neutral
    His 120 Neutral
    His 143 Neutral
    His 182 Neutral
    His 254 Neutral
    His 255 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

35-53 (19) : LFLGLFMIGhMFFVSTILL
    DG = -13.67 for segment sequence
    DG = -13.67 for most favorable 19AA centered at #44h
    (LFLGLFMIGhMFFVSTILL)

78-96 (19) : VVSFLAAFVFAVFIAhAFL
    DG = -12.37 for segment sequence
    DG = -12.37 for most favorable 19AA centered at #87F
    (VVSFLAAFVFAVFIAhAFL)

130-148 (19) : AMTGFAMFFLGSVhLYIMM
    DG = -7.08 for segment sequence
    DG = -7.08 for most favorable 19AA centered at #139L
    (AMTGFAMFFLGSVhLYIMM)

163-181 (19) : MVSEWMWPLYLVLLFAVEL
    DG = -8.2 for segment sequence
    DG = -8.2 for most favorable 19AA centered at #172Y
    (MVSEWMWPLYLVLLFAVEL)

214-232 (19) : TLMSAFLIVLGLLTFGAYV
    DG = -10.73 for segment sequence
    DG = -10.73 for most favorable 19AA centered at #223L
    (TLMSAFLIVLGLLTFGAYV)


Number of known transmembrane regions: 0


Working sequence (length = 256):

MTNESILESYSGVTPERKKSRMPAKLDWWQSATGLFLGLFMIGhMFFVSTILLGDNVMLW
VTKKFELDFIFEGGKPIVVSFLAAFVFAVFIAhAFLAMRKFPINYRQYLTFKThKDLMRh
GDTTLWWIQAMTGFAMFFLGSVhLYIMMTQPQTIGPVSSSFRMVSEWMWPLYLVLLFAVE
LhGSVGLYRLAVKWGWFDGETPDKTRANLKKLKTLMSAFLIVLGLLTFGAYVKKGLEQTD
PNIDYKYFDYKRThhR

