Results summary for protein: id10027_Rhomboid_Protease_GlpG
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 32 Neutral
    His 73 Neutral
    His 79 Neutral
    His 141 Neutral
    His 145 Neutral
    His 150 Neutral
    His 254 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

96-114 (19) : VTWVMMIACVVVFIAMQIL
    DG = -11.16 for segment sequence
    DG = -11.16 for most favorable 19AA centered at #105V
    (VTWVMMIACVVVFIAMQIL)

143-161 (19) : LMhFSLMhILFNLLWWWYL
    DG = -9.85 for segment sequence
    DG = -9.85 for most favorable 19AA centered at #152L
    (LMhFSLMhILFNLLWWWYL)

174-192 (19) : LIVITLISALLSGYVQQKF
    DG = -2.41 for segment sequence
    DG = -2.41 for most favorable 19AA centered at #183L
    (LIVITLISALLSGYVQQKF)

195-213 (19) : PWFGGLSGVVYALMGYVWL
    DG = -4.91 for segment sequence
    DG = -4.91 for most favorable 19AA centered at #204V
    (PWFGGLSGVVYALMGYVWL)

229-247 (19) : LIIFALIWIVAGWFDLFGM
    DG = -12.34 for segment sequence
    DG = -12.34 for most favorable 19AA centered at #238V
    (LIIFALIWIVAGWFDLFGM)

249-267 (19) : MANGAhIAGLAVGLAMAFV
    DG = -3.18 for segment sequence
    DG = -3.18 for most favorable 19AA centered at #258L
    (MANGAhIAGLAVGLAMAFV)


Number of known transmembrane regions: 0


Working sequence (length = 276):

MLMITSFANPRVAQAFVDYMATQGVILTIQQhNQSDVWLADESQAERVRAELARFLENPA
DPRYLAASWQAGhTGSGLhYRRYPFFAALRERAGPVTWVMMIACVVVFIAMQILGDQEVM
LWLAWPFDPTLKFEFWRYFThALMhFSLMhILFNLLWWWYLGGAVEKRLGSGKLIVITLI
SALLSGYVQQKFSGPWFGGLSGVVYALMGYVWLRGERDPQSGIYLQRGLIIFALIWIVAG
WFDLFGMSMANGAhIAGLAVGLAMAFVDSLNARKRK

