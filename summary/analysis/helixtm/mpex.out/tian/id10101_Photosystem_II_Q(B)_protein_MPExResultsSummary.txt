Results summary for protein: id10101_Photosystem_II_Q(B)_protein
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 92 Neutral
    His 118 Neutral
    His 190 Neutral
    His 195 Neutral
    His 198 Neutral
    His 215 Neutral
    His 252 Neutral
    His 272 Neutral
    His 304 Neutral
    His 332 Neutral
    His 337 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

35-63 (29) : VIMIPTLLAATICFVIAFIAAPPVDIDGI
    DG = -6.24 for segment sequence
    DG = -12.08 for most favorable 19AA centered at #44A
    (VIMIPTLLAATICFVIAFI)

105-123 (19) : WLYNGGPYQLIIFhFLLGA
    DG = -3.41 for segment sequence
    DG = -3.41 for most favorable 19AA centered at #114L
    (WLYNGGPYQLIIFhFLLGA)

143-161 (19) : ICVAYSAPLASAFAVFLIY
    DG = -6.06 for segment sequence
    DG = -6.06 for most favorable 19AA centered at #152A
    (ICVAYSAPLASAFAVFLIY)

200-218 (19) : LGVAGVFGGALFCAMhGSL
    DG = -2.87 for segment sequence
    DG = -2.87 for most favorable 19AA centered at #209A
    (LGVAGVFGGALFCAMhGSL)

271-289 (19) : LhFFLAAWPVVGVWFTALG
    DG = -7.42 for segment sequence
    DG = -7.42 for most favorable 19AA centered at #280V
    (LhFFLAAWPVVGVWFTALG)


Number of known transmembrane regions: 0


Working sequence (length = 360):

MTTTLQRRESANLWERFCNWVTSTDNRLYVGWFGVIMIPTLLAATICFVIAFIAAPPVDI
DGIREPVSGSLLYGNNIITGAVVPSSNAIGLhFYPIWEAASLDEWLYNGGPYQLIIFhFL
LGASCYMGRQWELSYRLGMRPWICVAYSAPLASAFAVFLIYPIGQGSFSDGMPLGISGTF
NFMIVFQAEhNILMhPFhQLGVAGVFGGALFCAMhGSLVTSSLIRETTETESANYGYKFG
QEEETYNIVAAhGYFGRLIFQYASFNNSRSLhFFLAAWPVVGVWFTALGISTMAFNLNGF
NFNhSVIDAKGNVINTWADIINRANLGMEVMhERNAhNFPLDLASAESAPVAMIAPSING

