Results summary for protein: id10070_cyt._b6f_complex,_PetG
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

5-23 (19) : LLDGLVLGLVFATLGGLFY
    DG = -10.45 for segment sequence
    DG = -10.45 for most favorable 19AA centered at #14V
    (LLDGLVLGLVFATLGGLFY)


Number of known transmembrane regions: 0


Working sequence (length = 37):

MVEPLLDGLVLGLVFATLGGLFYAAYQQYKRPNELGG

