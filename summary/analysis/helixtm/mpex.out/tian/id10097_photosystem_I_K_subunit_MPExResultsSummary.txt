Results summary for protein: id10097_photosystem_I_K_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 67 Neutral
Scale: Tian
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

16-34 (19) : GLVVILCNLFAIALGRYAI
    DG = -7.85 for segment sequence
    DG = -7.85 for most favorable 19AA centered at #25F
    (GLVVILCNLFAIALGRYAI)

43-77 (35) : LPIALPALFEGFGLPELLATTSFGhLLAAGVVSGL
    DG = -4.34 for segment sequence
    DG = -5.17 for most favorable 19AA centered at #52E
    (LPIALPALFEGFGLPELLA)


Number of known transmembrane regions: 0


Working sequence (length = 83):

MVLATLPDTTWTPSVGLVVILCNLFAIALGRYAIQSRGKGPGLPIALPALFEGFGLPELL
ATTSFGhLLAAGVVSGLQYAGAL

