Results summary for protein: id10081_cyto._C_oxidase._Subunit_VIII
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 36 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

17-35 (19) : IGLSVTFLSFLLPAGWVLY
    DG = -6.84 for segment sequence
    DG = -6.84 for most favorable 19AA centered at #26F
    (IGLSVTFLSFLLPAGWVLY)


Number of known transmembrane regions: 0


Working sequence (length = 46):

ITAKPARTPTSPKEQAIGLSVTFLSFLLPAGWVLYhLDNYKKSSAA

