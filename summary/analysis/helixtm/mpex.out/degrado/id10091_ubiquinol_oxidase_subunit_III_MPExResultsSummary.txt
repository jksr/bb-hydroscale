Results summary for protein: id10091_ubiquinol_oxidase_subunit_III
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 8 Neutral
    His 12 Neutral
    His 14 Neutral
    His 16 Neutral
    His 18 Neutral
    His 19 Neutral
    His 121 Neutral
    His 122 Neutral
    His 146 Neutral
    His 149 Neutral
    His 185 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

26-56 (31) : IFGFWIYLMSDCILFSILFATYAVLVNGTAG
    DG = -9.51 for segment sequence
    DG = -7.53 for most favorable 19AA centered at #35S
    (IFGFWIYLMSDCILFSILF)

73-91 (19) : TFLLLFSSITYGMAAIAMY
    DG = -6.7 for segment sequence
    DG = -6.7 for most favorable 19AA centered at #82T
    (TFLLLFSSITYGMAAIAMY)

99-128 (30) : ISWLALTWLFGAGFIGMEIYEFhhLIVNGM
    DG = -5.87 for segment sequence
    DG = -6.38 for most favorable 19AA centered at #108F
    (ISWLALTWLFGAGFIGMEI)

138-169 (32) : AFFALVGThGLhVTSGLIWMAVLMVQIARRGL
    DG = -4.48 for segment sequence
    DG = -4.86 for most favorable 19AA centered at #147G
    (AFFALVGThGLhVTSGLIW)

182-200 (19) : LFWhFLDVVWICVFTVVYL
    DG = -7.4 for segment sequence
    DG = -7.4 for most favorable 19AA centered at #191W
    (LFWhFLDVVWICVFTVVYL)


Number of known transmembrane regions: 0


Working sequence (length = 204):

MATDTLThATAhAhEhGhhDAGGTKIFGFWIYLMSDCILFSILFATYAVLVNGTAGGPTG
KDIFELPFVLVETFLLLFSSITYGMAAIAMYKNNKSQVISWLALTWLFGAGFIGMEIYEF
hhLIVNGMGPDRSGFLSAFFALVGThGLhVTSGLIWMAVLMVQIARRGLTSTNRTRIMCL
SLFWhFLDVVWICVFTVVYLMGAM

