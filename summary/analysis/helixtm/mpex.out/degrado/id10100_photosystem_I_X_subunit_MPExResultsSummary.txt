Results summary for protein: id10100_photosystem_I_X_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

14-32 (19) : FWAVLLLAINFLVAAYYFA
    DG = -8.71 for segment sequence
    DG = -8.71 for most favorable 19AA centered at #23N
    (FWAVLLLAINFLVAAYYFA)


Number of known transmembrane regions: 0


Working sequence (length = 35):

ATKSAKPTYAFRTFWAVLLLAINFLVAAYYFAAAA

