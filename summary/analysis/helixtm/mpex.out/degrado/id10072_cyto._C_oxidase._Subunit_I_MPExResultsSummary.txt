Results summary for protein: id10072_cyto._C_oxidase._Subunit_I
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 12 Neutral
    His 61 Neutral
    His 138 Neutral
    His 151 Neutral
    His 233 Neutral
    His 240 Neutral
    His 256 Neutral
    His 290 Neutral
    His 291 Neutral
    His 328 Neutral
    His 368 Neutral
    His 376 Neutral
    His 378 Neutral
    His 395 Neutral
    His 413 Neutral
    His 429 Neutral
    His 503 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 12

18-49 (32) : LYLLFGAWAGMVGTALSLLIRAELGQPGTLLG
    DG = -5.35 for segment sequence
    DG = -7.45 for most favorable 19AA centered at #27G
    (LYLLFGAWAGMVGTALSLL)

63-94 (32) : FVMIFFMVMPIMIGGFGNWLVPLMIGAPDMAF
    DG = -7.51 for segment sequence
    DG = -6.1 for most favorable 19AA centered at #72P
    (FVMIFFMVMPIMIGGFGNW)

100-118 (19) : MSFWLLPPSFLLLLASSMV
    DG = -5.55 for segment sequence
    DG = -5.55 for most favorable 19AA centered at #109F
    (MSFWLLPPSFLLLLASSMV)

147-176 (30) : IFSLhLAGVSSILGAINFITTIINMKPPAM
    DG = -2.63 for segment sequence
    DG = -4.89 for most favorable 19AA centered at #156S
    (IFSLhLAGVSSILGAINFI)

184-215 (32) : FVWSVMITAVLLLLSLPVLAAGITMLLTDRNL
    DG = -7.16 for segment sequence
    DG = -7.46 for most favorable 19AA centered at #193V
    (FVWSVMITAVLLLLSLPVL)

219-237 (19) : FFDPAGGGDPILYQhLFWF
    DG = -0.63 for segment sequence
    DG = -0.63 for most favorable 19AA centered at #228P
    (FFDPAGGGDPILYQhLFWF)

243-261 (19) : VYILILPGFGMIShIVTYY
    DG = -5.13 for segment sequence
    DG = -5.13 for most favorable 19AA centered at #252G
    (VYILILPGFGMIShIVTYY)

270-288 (19) : YMGMVWAMMSIGFLGFIVW
    DG = -7.76 for segment sequence
    DG = -7.76 for most favorable 19AA centered at #279S
    (YMGMVWAMMSIGFLGFIVW)

309-327 (19) : TMIIAIPTGVKVFSWLATL
    DG = -3.8 for segment sequence
    DG = -3.8 for most favorable 19AA centered at #318V
    (TMIIAIPTGVKVFSWLATL)

340-358 (19) : WALGFIFLFTVGGLTGIVL
    DG = -8.18 for segment sequence
    DG = -8.18 for most favorable 19AA centered at #349T
    (WALGFIFLFTVGGLTGIVL)

379-436 (58) : YVLSMGAVFAIMGGFVhWFPLFSGYTLNDTWAKIhFAIMFVGVNMTFFPQhFLGLSGM
    DG = -9.52 for segment sequence
    DG = -6.6 for most favorable 19AA centered at #388A
    (YVLSMGAVFAIMGGFVhWF)

455-483 (29) : SMGSFISLTAVMLMVFIIWEAFASKREVL
    DG = -3.76 for segment sequence
    DG = -7.18 for most favorable 19AA centered at #464A
    (SMGSFISLTAVMLMVFIIW)


Number of known transmembrane regions: 0


Working sequence (length = 514):

MFINRWLFSTNhKDIGTLYLLFGAWAGMVGTALSLLIRAELGQPGTLLGDDQIYNVVVTA
hAFVMIFFMVMPIMIGGFGNWLVPLMIGAPDMAFPRMNNMSFWLLPPSFLLLLASSMVEA
GAGTGWTVYPPLAGNLAhAGASVDLTIFSLhLAGVSSILGAINFITTIINMKPPAMSQYQ
TPLFVWSVMITAVLLLLSLPVLAAGITMLLTDRNLNTTFFDPAGGGDPILYQhLFWFFGh
PEVYILILPGFGMIShIVTYYSGKKEPFGYMGMVWAMMSIGFLGFIVWAhhMFTVGMDVD
TRAYFTSATMIIAIPTGVKVFSWLATLhGGNIKWSPAMMWALGFIFLFTVGGLTGIVLAN
SSLDIVLhDTYYVVAhFhYVLSMGAVFAIMGGFVhWFPLFSGYTLNDTWAKIhFAIMFVG
VNMTFFPQhFLGLSGMPRRYSDYPDAYTMWNTISSMGSFISLTAVMLMVFIIWEAFASKR
EVLTVDLTTTNLEWLNGCPPPYhTFEEPTYVNLK

