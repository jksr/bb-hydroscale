Results summary for protein: id10056_Mito._Respiratory_Complex_protein_CybL
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 26 Neutral
    His 42 Neutral
    His 70 Neutral
    His 98 Neutral
    His 105 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

45-73 (29) : TGIALSAGVSLFGLSALLLPGNFEShLEL
    DG = -3.18 for segment sequence
    DG = -6.26 for most favorable 19AA centered at #54S
    (TGIALSAGVSLFGLSALLL)

82-110 (29) : TLIYTAKFGIVFPLMYhTWNGIRhLIWDL
    DG = -2.35 for segment sequence
    DG = -3.47 for most favorable 19AA centered at #91I
    (TLIYTAKFGIVFPLMYhTW)

122-140 (19) : SGVVVLILTVLSSVGLAAM
    DG = -6.04 for segment sequence
    DG = -6.04 for most favorable 19AA centered at #131V
    (SGVVVLILTVLSSVGLAAM)


Number of known transmembrane regions: 0


Working sequence (length = 140):

LGTTAKEEMERFWNKNLGSNRPLSPhITIYRWSLPMAMSIChRGTGIALSAGVSLFGLSA
LLLPGNFEShLELVKSLCLGPTLIYTAKFGIVFPLMYhTWNGIRhLIWDLGKGLTIPQLT
QSGVVVLILTVLSSVGLAAM

