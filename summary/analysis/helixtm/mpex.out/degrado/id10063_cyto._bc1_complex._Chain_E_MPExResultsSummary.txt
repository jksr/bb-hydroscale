Results summary for protein: id10063_cyto._bc1_complex._Chain_E
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 2 Neutral
    His 100 Neutral
    His 122 Neutral
    His 141 Neutral
    His 161 Neutral
    His 164 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

34-72 (39) : GFSYLVTATTTVGVAYAAKNVVSQFVSSMSASADVLAMS
    DG = -3.93 for segment sequence
    DG = -3.07 for most favorable 19AA centered at #43T
    (GFSYLVTATTTVGVAYAAK)

132-160 (29) : WVILIGVCThLGCVPIANAGDFGGYYCPC
    DG = -2.99 for segment sequence
    DG = -3.34 for most favorable 19AA centered at #141h
    (WVILIGVCThLGCVPIANA)


Number of known transmembrane regions: 0


Working sequence (length = 196):

ShTDIKVPDFSDYRRPEVLDSTKSSKESSEARKGFSYLVTATTTVGVAYAAKNVVSQFVS
SMSASADVLAMSKIEIKLSDIPEGKNMAFKWRGKPLFVRhRTKKEIDQEAAVEVSQLRDP
QhDLERVKKPEWVILIGVCThLGCVPIANAGDFGGYYCPChGShYDASGRIRKGPAPLNL
EVPSYEFTSDDMVIVG

