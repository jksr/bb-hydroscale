Results summary for protein: id10016_AChR_pore_beta_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 105 Neutral
    His 112 Neutral
    His 119 Neutral
    His 160 Neutral
    His 190 Neutral
    His 305 Neutral
    His 306 Neutral
    His 312 Neutral
    His 387 Neutral
    His 460 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

25-43 (19) : VGDKVTVRVGLTLTSLLIL
    DG = -1.13 for segment sequence
    DG = -1.13 for most favorable 19AA centered at #34G
    (VGDKVTVRVGLTLTSLLIL)

100-118 (19) : FEITLhVNVLVQhTGAVSW
    DG = -0.65 for segment sequence
    DG = -0.65 for most favorable 19AA centered at #109L
    (FEITLhVNVLVQhTGAVSW)

208-277 (70) : TFYLIIQRKPLFYIVYTIVPCILISILAILVFYLPPDAGEKMSLSISALLALTVFLLLLADKVPETSLSV
    DG = -6.57 for segment sequence
    DG = -7.79 for most favorable 19AA centered at #258A
    (MSLSISALLALTVFLLLLA)

279-297 (19) : IIISYLMFIMILVAFSVIL
    DG = -8.89 for segment sequence
    DG = -8.89 for most favorable 19AA centered at #288M
    (IIISYLMFIMILVAFSVIL)

438-456 (19) : LFLYIFITMCSIGTFSIFL
    DG = -7.85 for segment sequence
    DG = -7.85 for most favorable 19AA centered at #447C
    (LFLYIFITMCSIGTFSIFL)


Number of known transmembrane regions: 0


Working sequence (length = 469):

SVMEDTLLSVLFENYNPKVRPSQTVGDKVTVRVGLTLTSLLILNEKNEEMTTSVFLNLAW
TDYRLQWDPAAYEGIKDLSIPSDDVWQPDIVLMNNNDGSFEITLhVNVLVQhTGAVSWhP
SAIYRSSCTIKVMYFPFDWQNCTMVFKSYTYDTSEVILQhALDAKGEREVKEIMINQDAF
TENGQWSIEhKPSRKNWRSDDPSYEDVTFYLIIQRKPLFYIVYTIVPCILISILAILVFY
LPPDAGEKMSLSISALLALTVFLLLLADKVPETSLSVPIIISYLMFIMILVAFSVILSVV
VLNLhhRSPNThTMPNWIRQIFIETLPPFLWIQRPVTTPSPDSKPTIISRANDEYFIRKP
AGDFVCPVDNARVAVQPERLFSEMKWhLNGLTQPVTLPQDLKEAVEAIKYIAEQLESASE
FDDLKKDWQYVAMVADRLFLYIFITMCSIGTFSIFLDAShNVPPDNPFA

