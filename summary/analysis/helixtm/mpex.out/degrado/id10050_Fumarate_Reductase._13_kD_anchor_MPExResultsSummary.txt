Results summary for protein: id10050_Fumarate_Reductase._13_kD_anchor
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 81 Neutral
    His 84 Neutral
    His 85 Neutral
    His 88 Neutral
    His 93 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

22-50 (29) : GMWSAIIAPVMILLVGILLPLGLFPGDAL
    DG = -7 for segment sequence
    DG = -6.82 for most favorable 19AA centered at #31V
    (GMWSAIIAPVMILLVGILL)

62-80 (19) : FIGRVFLFLMIVLPLWCGL
    DG = -6.43 for segment sequence
    DG = -6.43 for most favorable 19AA centered at #71M
    (FIGRVFLFLMIVLPLWCGL)

99-117 (19) : WVFYGLAAILTVVTLIGVV
    DG = -8.04 for segment sequence
    DG = -8.04 for most favorable 19AA centered at #108L
    (WVFYGLAAILTVVTLIGVV)


Number of known transmembrane regions: 0


Working sequence (length = 119):

MINPNPKRSDEPVFWGLFGAGGMWSAIIAPVMILLVGILLPLGLFPGDALSYERVLAFAQ
SFIGRVFLFLMIVLPLWCGLhRMhhAMhDLKIhVPAGKWVFYGLAAILTVVTLIGVVTI

