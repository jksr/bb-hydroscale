Results summary for protein: id10064_cyt._b6f_complex,_cyto_b6_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 29 Neutral
    His 86 Neutral
    His 100 Neutral
    His 187 Neutral
    His 202 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

39-82 (44) : ITLTCFLIQFATGFAMTFYYKPTVTEAYASVQYIMNEVSFGWLI
    DG = -5.18 for segment sequence
    DG = -5.64 for most favorable 19AA centered at #48F
    (ITLTCFLIQFATGFAMTFY)

88-106 (19) : WSASMMVLMMILhVFRVYL
    DG = -4.87 for segment sequence
    DG = -4.87 for most favorable 19AA centered at #97M
    (WSASMMVLMMILhVFRVYL)

116-148 (33) : LTWISGVILAVITVSFGVTGYSLPWDQVGYWAV
    DG = -7.67 for segment sequence
    DG = -6.57 for most favorable 19AA centered at #125A
    (LTWISGVILAVITVSFGVT)

157-175 (19) : AIPVVGVLISDLLRGGSSV
    DG = -1.37 for segment sequence
    DG = -1.37 for most favorable 19AA centered at #166S
    (AIPVVGVLISDLLRGGSSV)

188-206 (19) : TFVLPWLIAVFMLLhFLMI
    DG = -7.77 for segment sequence
    DG = -7.77 for most favorable 19AA centered at #197V
    (TFVLPWLIAVFMLLhFLMI)


Number of known transmembrane regions: 0


Working sequence (length = 215):

MANVYDWFQERLEIQALADDVTSKYVPPhVNIFYCLGGITLTCFLIQFATGFAMTFYYKP
TVTEAYASVQYIMNEVSFGWLIRSIhRWSASMMVLMMILhVFRVYLTGGFKKPRELTWIS
GVILAVITVSFGVTGYSLPWDQVGYWAVKIVSGVPEAIPVVGVLISDLLRGGSSVGQATL
TRYYSAhTFVLPWLIAVFMLLhFLMIRKQGISGPL

