Results summary for protein: id10109_Photosystem_II_PsbJ_protein
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

1-40 (40) : MMSEGGRIPLWIVATVAGMGVIVIVGLFFYGAYAGLGSSL
    DG = -8.53 for segment sequence
    DG = -8 for most favorable 19AA centered at #20G
    (WIVATVAGMGVIVIVGLFF)


Number of known transmembrane regions: 0


Working sequence (length = 40):

MMSEGGRIPLWIVATVAGMGVIVIVGLFFYGAYAGLGSSL

