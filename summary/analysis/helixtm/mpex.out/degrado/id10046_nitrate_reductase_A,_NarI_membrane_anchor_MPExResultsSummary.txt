Results summary for protein: id10046_nitrate_reductase_A,_NarI_membrane_anchor
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 56 Neutral
    His 66 Neutral
    His 74 Neutral
    His 150 Neutral
    His 170 Neutral
    His 176 Neutral
    His 187 Neutral
    His 205 Neutral
    His 225 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

7-35 (29) : FFFDIYPYIAGAVFLIGSWLRYDYGQYTW
    DG = -4.96 for segment sequence
    DG = -6.16 for most favorable 19AA centered at #16A
    (FFFDIYPYIAGAVFLIGSW)

54-83 (30) : LFhIGILGIFVGhFFGMLTPhWMYEAWLPI
    DG = -6.26 for segment sequence
    DG = -6.08 for most favorable 19AA centered at #63F
    (LFhIGILGIFVGhFFGMLT)

91-109 (19) : MFAGGASGVLCLIGGVLLL
    DG = -6.31 for segment sequence
    DG = -6.31 for most favorable 19AA centered at #100L
    (MFAGGASGVLCLIGGVLLL)

128-146 (19) : LILSLLVIQCALGLLTIPF
    DG = -5.58 for segment sequence
    DG = -5.58 for most favorable 19AA centered at #137C
    (LILSLLVIQCALGLLTIPF)

156-211 (56) : MMKLVGWAQSVVTFhGGASQhLDGVAFIFRLhLVLGMTLFLLFPFSRLIhIWSVPV
    DG = -5.54 for segment sequence
    DG = -6.81 for most favorable 19AA centered at #189V
    (VAFIFRLhLVLGMTLFLLF)


Number of known transmembrane regions: 0


Working sequence (length = 225):

MQFLNMFFFDIYPYIAGAVFLIGSWLRYDYGQYTWRAASSQMLDRKGMNLASNLFhIGIL
GIFVGhFFGMLTPhWMYEAWLPIEVKQKMAMFAGGASGVLCLIGGVLLLKRRLFSPRVRA
TTTGADILILSLLVIQCALGLLTIPFSAQhMDGSEMMKLVGWAQSVVTFhGGASQhLDGV
AFIFRLhLVLGMTLFLLFPFSRLIhIWSVPVEYLTRKYQLVRARh

