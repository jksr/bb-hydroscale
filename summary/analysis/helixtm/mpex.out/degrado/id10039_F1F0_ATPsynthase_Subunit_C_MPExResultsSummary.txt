Results summary for protein: id10039_F1F0_ATPsynthase_Subunit_C
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

8-36 (29) : LLYMAAAVMMGLAAIGAAIGIGILGGKFL
    DG = -8.4 for segment sequence
    DG = -6.82 for most favorable 19AA centered at #17M
    (LLYMAAAVMMGLAAIGAAI)

53-71 (19) : FFIVMGLVDAIPMIAVGLG
    DG = -5.12 for segment sequence
    DG = -5.12 for most favorable 19AA centered at #62A
    (FFIVMGLVDAIPMIAVGLG)


Number of known transmembrane regions: 0


Working sequence (length = 79):

MENLNMDLLYMAAAVMMGLAAIGAAIGIGILGGKFLEGAARQPDLIPLLRTQFFIVMGLV
DAIPMIAVGLGLYVMFAVA

