Results summary for protein: id10019_aquaporin_1
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 69 Neutral
    His 74 Neutral
    His 180 Neutral
    His 204 Neutral
    His 209 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

9-40 (32) : LFWRAVVAEFLATTLFVFISIGSALGFKYPVG
    DG = -6.46 for segment sequence
    DG = -5.94 for most favorable 19AA centered at #18F
    (LFWRAVVAEFLATTLFVFI)

52-70 (19) : VSLAFGLSIATLAQSVGhI
    DG = -3.4 for segment sequence
    DG = -3.4 for most favorable 19AA centered at #61A
    (VSLAFGLSIATLAQSVGhI)

81-124 (44) : LGLLLSCQISIFRALMYIIAQCVGAIVATAILSGITSSLTGNSL
    DG = -7.48 for segment sequence
    DG = -5.4 for most favorable 19AA centered at #103V
    (ALMYIIAQCVGAIVATAIL)

138-156 (19) : GLGIEIIGTLQLVLCVLAT
    DG = -3.61 for segment sequence
    DG = -3.61 for most favorable 19AA centered at #147L
    (GLGIEIIGTLQLVLCVLAT)

166-194 (29) : GSAPLAIGLSVALGhLLAIDYTGCGINPA
    DG = -2.57 for segment sequence
    DG = -4.2 for most favorable 19AA centered at #175S
    (GSAPLAIGLSVALGhLLAI)

209-238 (30) : hWIFWVGPFIGGALAVLIYDFILAPRSSDL
    DG = -4.62 for segment sequence
    DG = -6.65 for most favorable 19AA centered at #218I
    (hWIFWVGPFIGGALAVLIY)


Number of known transmembrane regions: 0


Working sequence (length = 269):

MASEFKKKLFWRAVVAEFLATTLFVFISIGSALGFKYPVGNNQTAVQDNVKVSLAFGLSI
ATLAQSVGhISGAhLNPAVTLGLLLSCQISIFRALMYIIAQCVGAIVATAILSGITSSLT
GNSLGRNDLADGVNSGQGLGIEIIGTLQLVLCVLATTDRRRRDLGGSAPLAIGLSVALGh
LLAIDYTGCGINPARSFGSAVIThNFSNhWIFWVGPFIGGALAVLIYDFILAPRSSDLTD
RVKVWTSGQVEEYDLDADDINSRVEMKPK

