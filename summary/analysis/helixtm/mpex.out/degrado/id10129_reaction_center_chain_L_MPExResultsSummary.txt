Results summary for protein: id10129_reaction_center_chain_L
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 124 Neutral
    His 152 Neutral
    His 161 Neutral
    His 174 Neutral
    His 176 Neutral
    His 181 Neutral
    His 198 Neutral
    His 219 Neutral
    His 238 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

33-64 (32) : FFGVVGFCFTLLGVLLIVWGATIGPTGPTSDL
    DG = -7.61 for segment sequence
    DG = -9.07 for most favorable 19AA centered at #42T
    (FFGVVGFCFTLLGVLLIVW)

92-110 (19) : GLWQIITICAAGAFISWAL
    DG = -5.88 for segment sequence
    DG = -5.88 for most favorable 19AA centered at #101A
    (GLWQIITICAAGAFISWAL)

123-175 (53) : FhVPFAFSFAIGAYLVLVFVRPLLMGAWGhGFPYGILShLDWVSNVGYQFLhF
    DG = -8.65 for segment sequence
    DG = -6.87 for most favorable 19AA centered at #132A
    (FhVPFAFSFAIGAYLVLVF)

185-203 (19) : ISFFFTNCLALSMhGSLIL
    DG = -4.71 for segment sequence
    DG = -4.71 for most favorable 19AA centered at #194A
    (ISFFFTNCLALSMhGSLIL)

240-275 (36) : LGLFLALSAAFWSAVCILISGPFWTRGWPEWWNWWL
    DG = -9.26 for segment sequence
    DG = -8.21 for most favorable 19AA centered at #249A
    (LGLFLALSAAFWSAVCILI)


Number of known transmembrane regions: 0


Working sequence (length = 280):

AMLSFEKKYRVRGGTLIGGDLFDFWVGPFYVGFFGVVGFCFTLLGVLLIVWGATIGPTGP
TSDLQTYNLWRISIAPPDLSYGLRMAPLTEGGLWQIITICAAGAFISWALREVEICRKLG
IGFhVPFAFSFAIGAYLVLVFVRPLLMGAWGhGFPYGILShLDWVSNVGYQFLhFhYNPA
hMLAISFFFTNCLALSMhGSLILSVTNPQRGEPVKTSEhENTFFRDIVGYSIGALAIhRL
GLFLALSAAFWSAVCILISGPFWTRGWPEWWNWWLELPLW

