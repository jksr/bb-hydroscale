Results summary for protein: id10028_Rhomboid_peptidase
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 39 Neutral
    His 56 Neutral
    His 60 Neutral
    His 65 Neutral
    His 134 Neutral
    His 135 Neutral
    His 169 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

12-30 (19) : TLILTALCVLIYLAQQLGF
    DG = -4.8 for segment sequence
    DG = -4.8 for most favorable 19AA centered at #21L
    (TLILTALCVLIYLAQQLGF)

58-102 (45) : LVhLSNLhILFNLSWFFIFGGMIERTFGSVKLLMLYVVASAITGY
    DG = -7.9 for segment sequence
    DG = -5.36 for most favorable 19AA centered at #67L
    (LVhLSNLhILFNLSWFFIF)

110-128 (19) : PAFFGLSGVVYAVLGYVFI
    DG = -6.64 for segment sequence
    DG = -6.64 for most favorable 19AA centered at #119V
    (PAFFGLSGVVYAVLGYVFI)

142-160 (19) : GFFTMLLVGIALGFISPLF
    DG = -7.01 for segment sequence
    DG = -7.01 for most favorable 19AA centered at #151I
    (GFFTMLLVGIALGFISPLF)

164-182 (19) : MGNAAhISGLIVGLIWGFI
    DG = -4.8 for segment sequence
    DG = -4.8 for most favorable 19AA centered at #173L
    (MGNAAhISGLIVGLIWGFI)


Number of known transmembrane regions: 0


Working sequence (length = 192):

MKNFLAQQGKITLILTALCVLIYLAQQLGFEDDIMYLMhYPAYEEQDSEVWRYIShTLVh
LSNLhILFNLSWFFIFGGMIERTFGSVKLLMLYVVASAITGYVQNYVSGPAFFGLSGVVY
AVLGYVFIRDKLNhhLFDLPEGFFTMLLVGIALGFISPLFGVEMGNAAhISGLIVGLIWG
FIDSKLRKNSLE

