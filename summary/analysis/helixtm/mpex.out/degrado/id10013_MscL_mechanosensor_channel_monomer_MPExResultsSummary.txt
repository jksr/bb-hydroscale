Results summary for protein: id10013_MscL_mechanosensor_channel_monomer
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 132 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

14-43 (30) : IVDLAVAVVIGTAFTALVTKFTDSIITPLI
    DG = -4.82 for segment sequence
    DG = -5.5 for most favorable 19AA centered at #23I
    (IVDLAVAVVIGTAFTALVT)

72-90 (19) : LLSAAINFFLIAFAVYFLV
    DG = -8.41 for segment sequence
    DG = -8.41 for most favorable 19AA centered at #81L
    (LLSAAINFFLIAFAVYFLV)


Number of known transmembrane regions: 0


Working sequence (length = 151):

MLKGFKEFLARGNIVDLAVAVVIGTAFTALVTKFTDSIITPLINRIGVNAQSDVGILRIG
IGGGQTIDLNVLLSAAINFFLIAFAVYFLVVLPYNTLRKKGEVEQPGDTQVVLLTEIRDL
LAQTNGDSPGRhGGRGTPSPTDGPRASTESQ

