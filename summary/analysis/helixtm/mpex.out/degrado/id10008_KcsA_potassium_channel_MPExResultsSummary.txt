Results summary for protein: id10008_KcsA_potassium_channel
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 20 Neutral
    His 25 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

31-49 (19) : AATVLLVIVLLAGSYLAVL
    DG = -7.76 for segment sequence
    DG = -7.76 for most favorable 19AA centered at #40L
    (AATVLLVIVLLAGSYLAVL)

66-84 (19) : LWWSVETATTVGYGDLYPV
    DG = -2.09 for segment sequence
    DG = -2.09 for most favorable 19AA centered at #75T
    (LWWSVETATTVGYGDLYPV)

97-115 (19) : VAGITSFGLVTAALATWFV
    DG = -6.75 for segment sequence
    DG = -6.75 for most favorable 19AA centered at #106V
    (VAGITSFGLVTAALATWFV)


Number of known transmembrane regions: 0


Working sequence (length = 119):

MPPMLSGLLARLVKLLLGRhGSALhWRAAGAATVLLVIVLLAGSYLAVLAERGAPGAQLI
TYPRALWWSVETATTVGYGDLYPVTLWGRCVAVVVMVAGITSFGLVTAALATWFVGREQ

