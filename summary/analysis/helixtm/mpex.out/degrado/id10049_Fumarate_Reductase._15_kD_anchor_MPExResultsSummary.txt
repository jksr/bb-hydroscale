Results summary for protein: id10049_Fumarate_Reductase._15_kD_anchor
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 83 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

32-61 (30) : TAVPAVWFSIELIFGLFALKNGPEAWAGFV
    DG = -3.91 for segment sequence
    DG = -5.87 for most favorable 19AA centered at #41I
    (TAVPAVWFSIELIFGLFAL)

70-88 (19) : VIINLITLAAALLhTKTWF
    DG = -3.9 for segment sequence
    DG = -3.9 for most favorable 19AA centered at #79A
    (VIINLITLAAALLhTKTWF)

113-131 (19) : LWAVTVVATIVILFVALYW
    DG = -9.16 for segment sequence
    DG = -9.16 for most favorable 19AA centered at #122I
    (LWAVTVVATIVILFVALYW)


Number of known transmembrane regions: 0


Working sequence (length = 131):

MTTKRKPYVRPMTSTWWKKLPFYRFYMLREGTAVPAVWFSIELIFGLFALKNGPEAWAGF
VDFLQNPVIVIINLITLAAALLhTKTWFELAPKAANIIVKDEKMGPEPIIKSLWAVTVVA
TIVILFVALYW

