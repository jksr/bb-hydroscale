Results summary for protein: id10018_AChR_pore_delta_subunit
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 20 Neutral
    His 26 Neutral
    His 60 Neutral
    His 65 Neutral
    His 194 Neutral
    His 313 Neutral
    His 320 Neutral
    His 339 Neutral
    His 390 Neutral
    His 416 Neutral
    His 497 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

30-48 (19) : VVNIALSLTLSNLISLKET
    DG = -0.49 for segment sequence
    DG = -0.49 for most favorable 19AA centered at #39L
    (VVNIALSLTLSNLISLKET)

104-137 (34) : VAYFCNVLVRPNGYVTWLPPAIFRSSCPINVLYF
    DG = -1.19 for segment sequence
    DG = -0.95 for most favorable 19AA centered at #113R
    (VAYFCNVLVRPNGYVTWLP)

215-254 (40) : VTFYLIIRRKPLFYVINFITPCVLISFLAALAFYLPAESG
    DG = -4.82 for segment sequence
    DG = -6.11 for most favorable 19AA centered at #235P
    (LFYVINFITPCVLISFLAA)

257-275 (19) : MSTAICVLLAQAVFLLLTS
    DG = -5.22 for segment sequence
    DG = -5.22 for most favorable 19AA centered at #266A
    (MSTAICVLLAQAVFLLLTS)

281-310 (30) : TALAVPLIGKYLMFIMSLVTGVVVNCGIVL
    DG = -6.59 for segment sequence
    DG = -5.77 for most favorable 19AA centered at #301G
    (LMFIMSLVTGVVVNCGIVL)

456-474 (19) : LSMFIITPVMVLGTIFIFV
    DG = -6.95 for segment sequence
    DG = -6.95 for most favorable 19AA centered at #465M
    (LSMFIITPVMVLGTIFIFV)


Number of known transmembrane regions: 0


Working sequence (length = 501):

VNEEERLINDLLIVNKYNKhVRPVKhNNEVVNIALSLTLSNLISLKETDETLTTNVWMDh
AWYDhRLTWNASEYSDISILRLRPELIWIPDIVLQNNNDGQYNVAYFCNVLVRPNGYVTW
LPPAIFRSSCPINVLYFPFDWQNCSLKFTALNYNANEISMDLMTDTIDGKDYPIEWIIID
PEAFTENGEWEIIhKPAKKNIYGDKFPNGTNYQDVTFYLIIRRKPLFYVINFITPCVLIS
FLAALAFYLPAESGEKMSTAICVLLAQAVFLLLTSQRLPETALAVPLIGKYLMFIMSLVT
GVVVNCGIVLNFhFRTPSThVLSTRVKQIFLEKLPRILhMSRVDEIEQPDWQNDLKLRRS
SSVGYISKAQEYFNIKSRSELMFEKQSERhGLVPRVTPRIGFGNNNENIAASDQLhDEIK
SGIDSTNYIVKQIKEKNAYDEEVGNWNLVGQTIDRLSMFIITPVMVLGTIFIFVMGNFNR
PPAKPFEGDPFDYSSDhPRCA

