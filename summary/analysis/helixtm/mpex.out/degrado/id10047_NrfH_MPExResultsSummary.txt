Results summary for protein: id10047_NrfH
Hydropathy Analysis Results
All Asp charged: true
All Glu charged: true
All His charged: false
    His 47 Neutral
    His 56 Neutral
    His 61 Neutral
    His 70 Neutral
    His 73 Neutral
    His 97 Neutral
    His 120 Neutral
    His 140 Neutral
    His 145 Neutral
Scale: DeGrado
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

15-33 (19) : LVLGGATLGVVALATVAFG
    DG = -6.42 for segment sequence
    DG = -6.42 for most favorable 19AA centered at #24V
    (LVLGGATLGVVALATVAFG)


Number of known transmembrane regions: 0


Working sequence (length = 159):

MSEEKSRNGPARLKLVLGGATLGVVALATVAFGMKYTDQRPFCTSChIMNPVGVThKLSG
hANISCNDChAPhNLLAKLPFKAIAGARDVYMNTLGhPGDLILAGMETKEVVNANCKACh
TMTNVEVASMEAKKYCTDChRNVQhMRMKPISTREVADE

