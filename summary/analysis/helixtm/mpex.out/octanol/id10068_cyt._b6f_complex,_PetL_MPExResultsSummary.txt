Results summary for protein: id10068_cyt._b6f_complex,_PetL
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

6-24 (19) : VFYIVFIALFFGIAVGIIF
    DG = -14.19 for segment sequence
    DG = -14.19 for most favorable 19AA centered at #15F
    (VFYIVFIALFFGIAVGIIF)


Number of known transmembrane regions: 0


Working sequence (length = 32):

MILGAVFYIVFIALFFGIAVGIIFAIKSIKLI

