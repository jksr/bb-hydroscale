Results summary for protein: id10100_photosystem_I_X_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

13-31 (19) : TFWAVLLLAINFLVAAYYF
    DG = -12.58 for segment sequence
    DG = -12.58 for most favorable 19AA centered at #22I
    (TFWAVLLLAINFLVAAYYF)


Number of known transmembrane regions: 0


Working sequence (length = 35):

ATKSAKPTYAFRTFWAVLLLAINFLVAAYYFAAAA

