Results summary for protein: id10013_MscL_mechanosensor_channel_monomer
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 132 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

17-35 (19) : LAVAVVIGTAFTALVTKFT
    DG = -1.93 for segment sequence
    DG = -1.93 for most favorable 19AA centered at #26A
    (LAVAVVIGTAFTALVTKFT)

79-97 (19) : FFLIAFAVYFLVVLPYNTL
    DG = -13.52 for segment sequence
    DG = -13.52 for most favorable 19AA centered at #88F
    (FFLIAFAVYFLVVLPYNTL)


Number of known transmembrane regions: 0


Working sequence (length = 151):

MLKGFKEFLARGNIVDLAVAVVIGTAFTALVTKFTDSIITPLINRIGVNAQSDVGILRIG
IGGGQTIDLNVLLSAAINFFLIAFAVYFLVVLPYNTLRKKGEVEQPGDTQVVLLTEIRDL
LAQTNGDSPGRhGGRGTPSPTDGPRASTESQ

