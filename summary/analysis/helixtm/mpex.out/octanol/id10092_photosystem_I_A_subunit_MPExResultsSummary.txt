Results summary for protein: id10092_photosystem_I_A_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 33 Neutral
    His 52 Neutral
    His 56 Neutral
    His 61 Neutral
    His 76 Neutral
    His 79 Neutral
    His 93 Neutral
    His 135 Neutral
    His 179 Neutral
    His 181 Neutral
    His 199 Neutral
    His 200 Neutral
    His 215 Neutral
    His 218 Neutral
    His 240 Neutral
    His 299 Neutral
    His 300 Neutral
    His 301 Neutral
    His 313 Neutral
    His 323 Neutral
    His 332 Neutral
    His 341 Neutral
    His 353 Neutral
    His 373 Neutral
    His 396 Neutral
    His 397 Neutral
    His 411 Neutral
    His 436 Neutral
    His 443 Neutral
    His 454 Neutral
    His 461 Neutral
    His 494 Neutral
    His 539 Neutral
    His 540 Neutral
    His 542 Neutral
    His 547 Neutral
    His 594 Neutral
    His 612 Neutral
    His 633 Neutral
    His 680 Neutral
    His 708 Neutral
    His 734 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 17

38-56 (19) : LARGPQTTTWIWNLhALAh
    DG = -1.86 for segment sequence
    DG = -1.86 for most favorable 19AA centered at #47W
    (LARGPQTTTWIWNLhALAh)

72-100 (29) : IFSAhFGhLAVVFIWLSGMYFhGAKFSNY
    DG = -8.08 for segment sequence
    DG = -9.11 for most favorable 19AA centered at #81A
    (IFSAhFGhLAVVFIWLSGM)

134-152 (19) : FhGIQITSGLFQLWRASGI
    DG = -2.79 for segment sequence
    DG = -2.79 for most favorable 19AA centered at #143L
    (FhGIQITSGLFQLWRASGI)

163-181 (19) : IGGLVMAGLMLFAGWFhYh
    DG = -7.07 for segment sequence
    DG = -7.07 for most favorable 19AA centered at #172M
    (IGGLVMAGLMLFAGWFhYh)

204-222 (19) : LLGLGSLAWAGhQIhVSLP
    DG = -3.42 for segment sequence
    DG = -3.42 for most favorable 19AA centered at #213A
    (LLGLGSLAWAGhQIhVSLP)

236-254 (19) : IPLPhEFILNPSLMAELYP
    DG = -0.59 for segment sequence
    DG = -0.59 for most favorable 19AA centered at #245N
    (IPLPhEFILNPSLMAELYP)

258-288 (31) : WGFFSGVIPFFTFNWAAYSDFLTFNGGLNPV
    DG = -7.91 for segment sequence
    DG = -9.56 for most favorable 19AA centered at #267F
    (WGFFSGVIPFFTFNWAAYS)

292-325 (34) : LWLSDTAhhhLAIAVLFIIAGhMYRTNWGIGhSL
    DG = -4.74 for segment sequence
    DG = -6.44 for most favorable 19AA centered at #301h
    (LWLSDTAhhhLAIAVLFII)

352-383 (32) : WhAQLAINLAMMGSLSIIVAQhMYAMPPYPYL
    DG = -8.12 for segment sequence
    DG = -5.7 for most favorable 19AA centered at #361A
    (WhAQLAINLAMMGSLSIIV)

387-416 (30) : YPTQLSLFThhMWIGGFLVVGGAAhGAIFM
    DG = -6.73 for segment sequence
    DG = -7.83 for most favorable 19AA centered at #396h
    (YPTQLSLFThhMWIGGFLV)

440-468 (29) : IIShLNWVCIFLGFhSFGLYVhNDTMRAF
    DG = -6.91 for segment sequence
    DG = -10.52 for most favorable 19AA centered at #449I
    (IIShLNWVCIFLGFhSFGL)

480-498 (19) : IQLQPVFAQWVQNLhTLAP
    DG = -4.02 for segment sequence
    DG = -4.02 for most favorable 19AA centered at #489W
    (IQLQPVFAQWVQNLhTLAP)

526-566 (41) : MMPIVLGTADFMVhhIhAFTIhVTVLILLKGVLFARSSRLI
    DG = -6.43 for segment sequence
    DG = -11.14 for most favorable 19AA centered at #545T
    (FMVhhIhAFTIhVTVLILL)

595-613 (19) : VFLGLFWMYNCISVVIFhF
    DG = -13.88 for segment sequence
    DG = -13.88 for most favorable 19AA centered at #604N
    (VFLGLFWMYNCISVVIFhF)

644-662 (19) : ITINGWLRDFLWAQASQVI
    DG = -1.51 for segment sequence
    DG = -1.51 for most favorable 19AA centered at #653F
    (ITINGWLRDFLWAQASQVI)

674-706 (33) : LLFLGAhFIWAFSLMFLFSGRGYWQELIESIVW
    DG = -11.74 for segment sequence
    DG = -15.5 for most favorable 19AA centered at #683W
    (LLFLGAhFIWAFSLMFLFS)

734-752 (19) : hYLLGGIATTWAFFLARII
    DG = -7.11 for segment sequence
    DG = -7.11 for most favorable 19AA centered at #743T
    (hYLLGGIATTWAFFLARII)


Number of known transmembrane regions: 0


Working sequence (length = 755):

MTISPPEREPKVRVVVDNDPVPTSFEKWAKPGhFDRTLARGPQTTTWIWNLhALAhDFDT
hTSDLEDISRKIFSAhFGhLAVVFIWLSGMYFhGAKFSNYEAWLADPTGIKPSAQVVWPI
VGQGILNGDVGGGFhGIQITSGLFQLWRASGITNEFQLYCTAIGGLVMAGLMLFAGWFhY
hKRAPKLEWFQNVESMLNhhLAGLLGLGSLAWAGhQIhVSLPINKLLDAGVAAKDIPLPh
EFILNPSLMAELYPKVDWGFFSGVIPFFTFNWAAYSDFLTFNGGLNPVTGGLWLSDTAhh
hLAIAVLFIIAGhMYRTNWGIGhSLKEILEAhKGPFTGAGhKGLYEVLTTSWhAQLAINL
AMMGSLSIIVAQhMYAMPPYPYLATDYPTQLSLFThhMWIGGFLVVGGAAhGAIFMVRDY
DPAMNQNNVLDRVLRhRDAIIShLNWVCIFLGFhSFGLYVhNDTMRAFGRPQDMFSDTGI
QLQPVFAQWVQNLhTLAPGGTAPNAAATASVAFGGDVVAVGGKVAMMPIVLGTADFMVhh
IhAFTIhVTVLILLKGVLFARSSRLIPDKANLGFRFPCDGPGRGGTCQVSGWDhVFLGLF
WMYNCISVVIFhFSWKMQSDVWGTVAPDGTVShITGGNFAQSAITINGWLRDFLWAQASQ
VIGSYGSALSAYGLLFLGAhFIWAFSLMFLFSGRGYWQELIESIVWAhNKLKVAPAIQPR
ALSIIQGRAVGVAhYLLGGIATTWAFFLARIISVG

