Results summary for protein: id10073_cyto._C_oxidase._Subunit_II
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 22 Neutral
    His 24 Neutral
    His 26 Neutral
    His 52 Neutral
    His 102 Neutral
    His 161 Neutral
    His 204 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 4

28-46 (19) : LMIVFLISSLVLYIISLML
    DG = -15.28 for segment sequence
    DG = -15.28 for most favorable 19AA centered at #37L
    (LMIVFLISSLVLYIISLML)

63-81 (19) : TIWTILPAIILILIALPSL
    DG = -12.82 for segment sequence
    DG = -12.82 for most favorable 19AA centered at #72I
    (TIWTILPAIILILIALPSL)

90-108 (19) : INNPSLTVKTMGhQWYWSY
    DG = -1.01 for segment sequence
    DG = -1.01 for most favorable 19AA centered at #99T
    (INNPSLTVKTMGhQWYWSY)

201-219 (19) : GSNhSFMPIVLELVPLKYF
    DG = -0.85 for segment sequence
    DG = -0.85 for most favorable 19AA centered at #210V
    (GSNhSFMPIVLELVPLKYF)


Number of known transmembrane regions: 0


Working sequence (length = 227):

MAYPMQLGFQDATSPIMEELLhFhDhTLMIVFLISSLVLYIISLMLTTKLThTSTMDAQE
VETIWTILPAIILILIALPSLRILYMMDEINNPSLTVKTMGhQWYWSYEYTDYEDLSFDS
YMIPTSELKPGELRLLEVDNRVVLPMEMTIRMLVSSEDVLhSWAVPSLGLKTDAIPGRLN
QTTLMSSRPGLYYGQCSEICGSNhSFMPIVLELVPLKYFEKWSASML

