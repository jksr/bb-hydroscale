Results summary for protein: id10119_Light_Harvesting_Complex._alpha_chain
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 34 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

9-48 (40) : IWLVINPSTWLPVIWIVATVVAIAVhAAVLAAPGSNWIAL
    DG = -14.5 for segment sequence
    DG = -12.04 for most favorable 19AA centered at #18W
    (IWLVINPSTWLPVIWIVAT)


Number of known transmembrane regions: 0


Working sequence (length = 56):

SNPKDDYKIWLVINPSTWLPVIWIVATVVAIAVhAAVLAAPGSNWIALGAAKSAAK

