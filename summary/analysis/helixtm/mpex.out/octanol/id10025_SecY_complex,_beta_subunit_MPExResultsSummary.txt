Results summary for protein: id10025_SecY_complex,_beta_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 32 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

32-50 (19) : hVIGVTVAFVIIEAILTYG
    DG = -2.45 for segment sequence
    DG = -2.45 for most favorable 19AA centered at #41V
    (hVIGVTVAFVIIEAILTYG)


Number of known transmembrane regions: 0


Working sequence (length = 53):

MSKREETGLATSAGLIRYMDETFSKIRVKPEhVIGVTVAFVIIEAILTYGRFL

