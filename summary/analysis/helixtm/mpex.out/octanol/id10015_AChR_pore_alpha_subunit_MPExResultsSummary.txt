Results summary for protein: id10015_AChR_pore_alpha_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 3 Neutral
    His 24 Neutral
    His 25 Neutral
    His 27 Neutral
    His 104 Neutral
    His 134 Neutral
    His 186 Neutral
    His 204 Neutral
    His 299 Neutral
    His 300 Neutral
    His 306 Neutral
    His 385 Neutral
    His 408 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 8

25-43 (19) : hThFVDITVGLQLIQLINV
    DG = -2.3 for segment sequence
    DG = -2.3 for most favorable 19AA centered at #34G
    (hThFVDITVGLQLIQLINV)

100-135 (36) : FAIVhMTKLLLDYTGKIMWTPPAIFKSYCEIIVThF
    DG = -0.49 for segment sequence
    DG = -2.45 for most favorable 19AA centered at #126S
    (MWTPPAIFKSYCEIIVThF)

187-205 (19) : WVYYTCCPDTPYLDITYhF
    DG = -1.09 for segment sequence
    DG = -1.09 for most favorable 19AA centered at #196T
    (WVYYTCCPDTPYLDITYhF)

218-236 (19) : VIIPCLLFSFLTVLVFYLP
    DG = -14.74 for segment sequence
    DG = -14.74 for most favorable 19AA centered at #227F
    (VIIPCLLFSFLTVLVFYLP)

243-274 (32) : MTLSISVLLSLTVFLLVIVELIPSTSSAVPLI
    DG = -11.24 for segment sequence
    DG = -12.08 for most favorable 19AA centered at #252S
    (MTLSISVLLSLTVFLLVIV)

278-307 (30) : MLFTMIFVISSIIVTVVVINThhRSPSThT
    DG = -7.69 for segment sequence
    DG = -12.49 for most favorable 19AA centered at #287S
    (MLFTMIFVISSIIVTVVVI)

311-329 (19) : WVRKIFINTIPNVMFFSTM
    DG = -5.43 for segment sequence
    DG = -5.43 for most favorable 19AA centered at #320I
    (WVRKIFINTIPNVMFFSTM)

408-426 (19) : hILLCVFMLICIIGTVSVF
    DG = -11.77 for segment sequence
    DG = -11.77 for most favorable 19AA centered at #417I
    (hILLCVFMLICIIGTVSVF)


Number of known transmembrane regions: 0


Working sequence (length = 437):

SEhETRLVANLLENYNKVIRPVEhhThFVDITVGLQLIQLINVDEVNQIVETNVRLRQQW
IDVRLRWNPADYGGIKKIRLPSDDVWLPDLVLYNNADGDFAIVhMTKLLLDYTGKIMWTP
PAIFKSYCEIIVThFPFDQQNCTMKLGIWTYDGTKVSISPESDRPDLSTFMESGEWVMKD
YRGWKhWVYYTCCPDTPYLDITYhFIMQRIPLYFVVNVIIPCLLFSFLTVLVFYLPTDSG
EKMTLSISVLLSLTVFLLVIVELIPSTSSAVPLIGKYMLFTMIFVISSIIVTVVVINThh
RSPSThTMPQWVRKIFINTIPNVMFFSTMKRASKEKQENKIFADDIDISDISGKQVTGEV
IFQTPLIKNPDVKSAIEGVKYIAEhMKSDEESSNAAEEWKYVAMVIDhILLCVFMLICII
GTVSVFAGRLIELSQEG

