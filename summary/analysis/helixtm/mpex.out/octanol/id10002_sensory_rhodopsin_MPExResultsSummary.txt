Results summary for protein: id10002_sensory_rhodopsin
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 8 Neutral
    His 21 Neutral
    His 69 Neutral
    His 219 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 5

5-23 (19) : SLLhWIYVAGMTIGALhFW
    DG = -9.49 for segment sequence
    DG = -9.49 for most favorable 19AA centered at #14G
    (SLLhWIYVAGMTIGALhFW)

38-56 (19) : LVAMFIPIWSGLAYMAMAI
    DG = -9.09 for segment sequence
    DG = -9.09 for most favorable 19AA centered at #47S
    (LVAMFIPIWSGLAYMAMAI)

76-117 (42) : WMVTTPLLLLSLSWTAMQFIKKDWTLIGFLMSTQIVVITSGL
    DG = -10.5 for segment sequence
    DG = -10.86 for most favorable 19AA centered at #85L
    (WMVTTPLLLLSLSWTAMQF)

129-147 (19) : YLWYICGVCAFLIILWGIW
    DG = -15.33 for segment sequence
    DG = -15.33 for most favorable 19AA centered at #138A
    (YLWYICGVCAFLIILWGIW)

168-218 (51) : LVTYFTVLWIGYPIVWIIGPSGFGWINQTIDTFLFCLLPFFSKVGFSFLDL
    DG = -18.45 for segment sequence
    DG = -12.73 for most favorable 19AA centered at #177I
    (LVTYFTVLWIGYPIVWIIG)


Number of known transmembrane regions: 0


Working sequence (length = 261):

MNLESLLhWIYVAGMTIGALhFWSLSRNPRGVPQYEYLVAMFIPIWSGLAYMAMAIDQGK
VEAAGQIAhYARYIDWMVTTPLLLLSLSWTAMQFIKKDWTLIGFLMSTQIVVITSGLIAD
LSERDWVRYLWYICGVCAFLIILWGIWNPLRAKTRTQSSELANLYDKLVTYFTVLWIGYP
IVWIIGPSGFGWINQTIDTFLFCLLPFFSKVGFSFLDLhGLRNLNDSRQTTGDRFAENTL
QFVENITLFANSRRQQSRRRV

