Results summary for protein: id10128_reaction_center_chain_H
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 7 Neutral
    His 71 Neutral
    His 122 Neutral
    His 218 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

12-30 (19) : AQITIWAFWLFFFGLIIYL
    DG = -16.79 for segment sequence
    DG = -16.79 for most favorable 19AA centered at #21L
    (AQITIWAFWLFFFGLIIYL)


Number of known transmembrane regions: 0


Working sequence (length = 259):

MPAGIThYIDAAQITIWAFWLFFFGLIIYLRREDKREGYPLDSNRTERSGGRYKVVGFPD
LPDPKTFVLPhNGGTVVAPRVEAPVAVNATPFSPAPGSPLVPNGDPMLSGFGPAASPDRP
KhCDLTFEGLPKIVPMRVAKEFSIAEGDPDPRGMTVVGLDGEVAGTVSDVWVDRSEPQIR
YLEVEVAANKKKVLLPIGFSRFDKKARKVKVDAIKAAhFANVPTLSNPDQVTLYEEDKVC
AYYAGGKLYATAERAGPLL

