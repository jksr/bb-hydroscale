Results summary for protein: id10024_SecY_complex,_alpha_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 236 Neutral
    His 285 Neutral
    His 314 Neutral
    His 367 Neutral
    His 426 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 9

28-46 (19) : WTGIVLVLYFIMGCIDVYT
    DG = -6.71 for segment sequence
    DG = -6.71 for most favorable 19AA centered at #37F
    (WTGIVLVLYFIMGCIDVYT)

54-100 (47) : IFEFWQTITASRIGTLITLGIGPIVTAGIIMQLLVGSGIIQMDLSIP
    DG = -4.14 for segment sequence
    DG = -6.37 for most favorable 19AA centered at #78V
    (LITLGIGPIVTAGIIMQLL)

113-131 (19) : LSIIMCFVEAVLFVGAGAF
    DG = -4.05 for segment sequence
    DG = -4.05 for most favorable 19AA centered at #122A
    (LSIIMCFVEAVLFVGAGAF)

137-155 (19) : LLAFLVIIQIAFGSIILIY
    DG = -12.93 for segment sequence
    DG = -12.93 for most favorable 19AA centered at #146I
    (LLAFLVIIQIAFGSIILIY)

169-199 (31) : IGLFIAAGVSQTIFVGALGPEGYLWKFLNSL
    DG = -1.85 for segment sequence
    DG = -2.62 for most favorable 19AA centered at #178S
    (IGLFIAAGVSQTIFVGALG)

206-234 (29) : IEYIAPIIGTIIVFLMVVYAECMRVEIPL
    DG = -1.29 for segment sequence
    DG = -7.48 for most favorable 19AA centered at #215T
    (IEYIAPIIGTIIVFLMVVY)

258-286 (29) : VILAAALFANIQLWGLALYRMGIPILGhY
    DG = -8.7 for segment sequence
    DG = -8.19 for most favorable 19AA centered at #267N
    (VILAAALFANIQLWGLALY)

316-334 (19) : IVYMIAMIITCVMFGIFWV
    DG = -13.33 for segment sequence
    DG = -13.33 for most favorable 19AA centered at #325T
    (IVYMIAMIITCVMFGIFWV)

372-411 (40) : YIPPLTVMSSAFVGFLATIANFIGALGGGTGVLLTVSIVY
    DG = -7.84 for segment sequence
    DG = -6.61 for most favorable 19AA centered at #381S
    (YIPPLTVMSSAFVGFLATI)


Number of known transmembrane regions: 0


Working sequence (length = 435):

KKLIPILEKIPEVELPVKEITFKEKLKWTGIVLVLYFIMGCIDVYTAGAQIPAIFEFWQT
ITASRIGTLITLGIGPIVTAGIIMQLLVGSGIIQMDLSIPENRALFQGCQKLLSIIMCFV
EAVLFVGAGAFGILTPLLAFLVIIQIAFGSIILIYLDEIVSKYGIGSGIGLFIAAGVSQT
IFVGALGPEGYLWKFLNSLIQGVPNIEYIAPIIGTIIVFLMVVYAECMRVEIPLAhGRIK
GAVGKYPIKFVYVSNIPVILAAALFANIQLWGLALYRMGIPILGhYEGGRAVDGIAYYLS
TPYGLSSVISDPIhAIVYMIAMIITCVMFGIFWVETTGLDPKSMAKRIGSLGMAIKGFRK
SEKAIEhRLKRYIPPLTVMSSAFVGFLATIANFIGALGGGTGVLLTVSIVYRMYEQLLRE
KVSELhPAIAKLLNK

