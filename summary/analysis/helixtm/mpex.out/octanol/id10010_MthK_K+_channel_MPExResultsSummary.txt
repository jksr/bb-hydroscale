Results summary for protein: id10010_MthK_K+_channel
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 11 Neutral
    His 39 Neutral
    His 116 Neutral
    His 160 Neutral
    His 192 Neutral
    His 285 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 3

23-41 (19) : ILLLVLAVIIYGTAGFhFI
    DG = -10.87 for segment sequence
    DG = -10.87 for most favorable 19AA centered at #32I
    (ILLLVLAVIIYGTAGFhFI)

45-63 (19) : SWTVSLYWTFVTIATVGYG
    DG = -6.34 for segment sequence
    DG = -6.34 for most favorable 19AA centered at #54F
    (SWTVSLYWTFVTIATVGYG)

70-88 (19) : LGMYFTVTLIVLGIGTFAV
    DG = -7.47 for segment sequence
    DG = -7.47 for most favorable 19AA centered at #79I
    (LGMYFTVTLIVLGIGTFAV)


Number of known transmembrane regions: 0


Working sequence (length = 335):

MVLVIEIIRKhLPRVLKVPATRILLLVLAVIIYGTAGFhFIEGESWTVSLYWTFVTIATV
GYGDSPSTPLGMYFTVTLIVLGIGTFAVAVERLLEFLINREQMKLMGLIDVAKSRhVVIC
GWSESTLECLRELRGSEVFVLAEDENVRKKVLRSGANFVhGDPTRVSDLEKANVRGARAV
IVDLESDSETIhCILGIRKIDESVRIIAEAERYENIEQLRMAGADQVISPFVISGRLMSR
SIDDGYEAMFVQDVLAEESTRRMVEVPIPEGSKLEGVSVLDADIhDVTGVIIIGVGRGDE
LIIDPPRDYSFRAGDIILGIGKPEEIERLKNYISA

