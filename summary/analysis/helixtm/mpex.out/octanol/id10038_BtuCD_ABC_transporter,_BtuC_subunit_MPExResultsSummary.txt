Results summary for protein: id10038_BtuCD_ABC_transporter,_BtuC_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 139 Neutral
    His 262 Neutral
    His 272 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 9

1-33 (33) : MLTLARQQQRQNIRWLLCLSVLMLLALLLSLCA
    DG = -8.02 for segment sequence
    DG = -13.84 for most favorable 19AA centered at #24L
    (WLLCLSVLMLLALLLSLCA)

50-81 (32) : LFVWQIRLPRTLAVLLVGAALAISGAVMQALF
    DG = -7.7 for segment sequence
    DG = -5.62 for most favorable 19AA centered at #59R
    (LFVWQIRLPRTLAVLLVGA)

101-143 (43) : LIAAVLLGQGQLPNWALGLCAIAGALIITLILLRFARRhLSTS
    DG = -6.04 for segment sequence
    DG = -9.54 for most favorable 19AA centered at #124G
    (WALGLCAIAGALIITLILL)

152-180 (29) : LGIICSALMTWAIYFSTSVDLRQLMYWMM
    DG = -8.58 for segment sequence
    DG = -7.49 for most favorable 19AA centered at #161T
    (LGIICSALMTWAIYFSTSV)

188-219 (32) : WRQSWLMLALIPMLLWICCQSRPMNMLALGEI
    DG = -8.11 for segment sequence
    DG = -12.46 for most favorable 19AA centered at #197L
    (WRQSWLMLALIPMLLWICC)

224-242 (19) : LGLPLWFWRNVLVAATGWM
    DG = -8.22 for segment sequence
    DG = -8.22 for most favorable 19AA centered at #233N
    (LGLPLWFWRNVLVAATGWM)

252-270 (19) : AIGFIGLVIPhILRLCGLT
    DG = -5.41 for segment sequence
    DG = -5.41 for most favorable 19AA centered at #261P
    (AIGFIGLVIPhILRLCGLT)

275-293 (19) : LLPGCALAGASALLLADIV
    DG = -0.06 for segment sequence
    DG = -0.06 for most favorable 19AA centered at #284A
    (LLPGCALAGASALLLADIV)

303-321 (19) : LPIGVVTATLGAPVFIWLL
    DG = -8.34 for segment sequence
    DG = -8.34 for most favorable 19AA centered at #312L
    (LPIGVVTATLGAPVFIWLL)


Number of known transmembrane regions: 0


Working sequence (length = 326):

MLTLARQQQRQNIRWLLCLSVLMLLALLLSLCAGEQWISPGDWFSPRGELFVWQIRLPRT
LAVLLVGAALAISGAVMQALFENPLAEPGLLGVSNGAGVGLIAAVLLGQGQLPNWALGLC
AIAGALIITLILLRFARRhLSTSRLLLAGVALGIICSALMTWAIYFSTSVDLRQLMYWMM
GGFGGVDWRQSWLMLALIPMLLWICCQSRPMNMLALGEISARQLGLPLWFWRNVLVAATG
WMVGVSVALAGAIGFIGLVIPhILRLCGLTDhRALLPGCALAGASALLLADIVARLALAA
AELPIGVVTATLGAPVFIWLLLKAGR

