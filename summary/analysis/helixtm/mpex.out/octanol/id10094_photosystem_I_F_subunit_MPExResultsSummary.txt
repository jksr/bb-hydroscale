Results summary for protein: id10094_photosystem_I_F_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 50 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

63-81 (19) : FLIPSVLFLYIAGWIGWVG
    DG = -11.79 for segment sequence
    DG = -11.79 for most favorable 19AA centered at #72Y
    (FLIPSVLFLYIAGWIGWVG)

103-121 (19) : VPLAIKCMLTGFAWPLAAL
    DG = -4.59 for segment sequence
    DG = -4.59 for most favorable 19AA centered at #112T
    (VPLAIKCMLTGFAWPLAAL)


Number of known transmembrane regions: 0


Working sequence (length = 141):

DVAGLVPCKDSPAFQKRAAAAVNTTADPASGQKRFERYSQALCGEDGLPhLVVDGRLSRA
GDFLIPSVLFLYIAGWIGWVGRAYLIAVRNSGEANEKEIIIDVPLAIKCMLTGFAWPLAA
LKELASGELTAKDNEITVSPR

