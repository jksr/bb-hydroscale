Results summary for protein: id10020_aquaporin_1
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 36 Neutral
    His 71 Neutral
    His 76 Neutral
    His 182 Neutral
    His 206 Neutral
    His 211 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

18-36 (19) : FLAMILFIFISIGSALGFh
    DG = -11.41 for segment sequence
    DG = -11.41 for most favorable 19AA centered at #27I
    (FLAMILFIFISIGSALGFh)

54-72 (19) : VSLAFGLSIATLAQSVGhI
    DG = -2.31 for segment sequence
    DG = -2.31 for most favorable 19AA centered at #63A
    (VSLAFGLSIATLAQSVGhI)

83-115 (33) : LGLLLSCQISVLRAIMYIIAQCVGAIVATAILS
    DG = -7.24 for segment sequence
    DG = -7.44 for most favorable 19AA centered at #92S
    (LGLLLSCQISVLRAIMYII)

141-159 (19) : LGIEIIGTLQLVLCVLATT
    DG = -2.6 for segment sequence
    DG = -2.6 for most favorable 19AA centered at #150Q
    (LGIEIIGTLQLVLCVLATT)

168-186 (19) : GSGPLAIGFSVALGhLLAI
    DG = -2.14 for segment sequence
    DG = -2.14 for most favorable 19AA centered at #177S
    (GSGPLAIGFSVALGhLLAI)

211-229 (19) : hWIFWVGPFIGAALAVLIY
    DG = -11.04 for segment sequence
    DG = -11.04 for most favorable 19AA centered at #220I
    (hWIFWVGPFIGAALAVLIY)


Number of known transmembrane regions: 0


Working sequence (length = 271):

MASEFKKKLFWRAVVAEFLAMILFIFISIGSALGFhYPIKSNQTTGAVQDNVKVSLAFGL
SIATLAQSVGhISGAhLNPAVTLGLLLSCQISVLRAIMYIIAQCVGAIVATAILSGITSS
LPDNSLGLNALAPGVNSGQGLGIEIIGTLQLVLCVLATTDRRRRDLGGSGPLAIGFSVAL
GhLLAIDYTGCGINPARSFGSSVIThNFQDhWIFWVGPFIGAALAVLIYDFILAPRSSDL
TDRVKVWTSGQVEEYDLDADDINSRVEMKPK

