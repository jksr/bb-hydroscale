Results summary for protein: id10001_halorhodopsin
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 95 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

32-50 (19) : SLWVNVALAGIAILVFVYM
    DG = -9.05 for segment sequence
    DG = -9.05 for most favorable 19AA centered at #41G
    (SLWVNVALAGIAILVFVYM)

63-92 (30) : WGATLMIPLVSISSYLGLLSGLTVGMIEMP
    DG = -5.24 for segment sequence
    DG = -7.85 for most favorable 19AA centered at #72V
    (WGATLMIPLVSISSYLGLL)

109-127 (19) : YLTWALSTPMILLALGLLA
    DG = -9.59 for segment sequence
    DG = -9.59 for most favorable 19AA centered at #118M
    (YLTWALSTPMILLALGLLA)

131-149 (19) : LGSLFTVIAADIGMCVTGL
    DG = -0.26 for segment sequence
    DG = -0.26 for most favorable 19AA centered at #140A
    (LGSLFTVIAADIGMCVTGL)

158-189 (32) : LLFRWAFYAISCAFFVVVLSALVTDWAASASS
    DG = -8.21 for segment sequence
    DG = -12.14 for most favorable 19AA centered at #167I
    (LLFRWAFYAISCAFFVVVL)

198-216 (19) : TLRVLTVVLWLGYPIVWAV
    DG = -9.21 for segment sequence
    DG = -9.21 for most favorable 19AA centered at #207W
    (TLRVLTVVLWLGYPIVWAV)

222-261 (40) : ALVQSVGVTSWAYSVLDVFAKYVFAFILLRWVANNERTVA
    DG = 1.66 for segment sequence
    DG = -6.49 for most favorable 19AA centered at #241A
    (WAYSVLDVFAKYVFAFILL)


Number of known transmembrane regions: 0


Working sequence (length = 274):

MSITSVPGVVDAGVLGAQSAAAVRENALLSSSLWVNVALAGIAILVFVYMGRTIRPGRPR
LIWGATLMIPLVSISSYLGLLSGLTVGMIEMPAGhALAGEMVRSQWGRYLTWALSTPMIL
LALGLLADVDLGSLFTVIAADIGMCVTGLAAAMTTSALLFRWAFYAISCAFFVVVLSALV
TDWAASASSAGTAEIFDTLRVLTVVLWLGYPIVWAVGVEGLALVQSVGVTSWAYSVLDVF
AKYVFAFILLRWVANNERTVAVAGQTLGTMSSDD

