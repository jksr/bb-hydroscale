Results summary for protein: id10085_cyto._C_oxidase._Subunit_IV
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 3 Neutral
    His 4 Neutral
    His 9 Neutral
    His 11 Neutral
    His 18 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

31-49 (19) : TWVSILSIAVLVFLALANS
    DG = -8.44 for segment sequence
    DG = -8.44 for most favorable 19AA centered at #40V
    (TWVSILSIAVLVFLALANS)


Number of known transmembrane regions: 0


Working sequence (length = 49):

AShhEITDhKhGEMDIRhQQATFAGFIKGATWVSILSIAVLVFLALANS

