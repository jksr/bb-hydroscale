Results summary for protein: id10122_Reaction_center._Chain_H
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 3 Neutral
    His 9 Neutral
    His 72 Neutral
    His 178 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

14-32 (19) : QLVWYAQWLVIWTVVLLYL
    DG = -14.61 for segment sequence
    DG = -14.61 for most favorable 19AA centered at #23V
    (QLVWYAQWLVIWTVVLLYL)


Number of known transmembrane regions: 0


Working sequence (length = 258):

MYhGALAQhLDIAQLVWYAQWLVIWTVVLLYLRREDRREGYPLVEPLGLVKLAPEDGQVY
ELPYPKTFVLPhGGTVTVPRRRPETRELKLAQTDGFEGAPLQPTGNPLVDAVGPASYAER
AEVVDATVDGKAKIVPLRVATDFSIAEGDVDPRGLPVVAADGVEAGTVTDLWVDRSEhYF
RYLELSVAGSARTALIPLGFCDVKKDKIVVTSILSEQFANVPRLQSRDQITLREEDKVSA
YYAGGLLYATPERAESLL

