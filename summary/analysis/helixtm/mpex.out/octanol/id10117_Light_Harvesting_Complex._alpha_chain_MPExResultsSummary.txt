Results summary for protein: id10117_Light_Harvesting_Complex._alpha_chain
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 31 Neutral
    His 37 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

27-45 (19) : AILVhLAILShTTWFPAYW
    DG = -10.23 for segment sequence
    DG = -10.23 for most favorable 19AA centered at #36S
    (AILVhLAILShTTWFPAYW)


Number of known transmembrane regions: 0


Working sequence (length = 53):

MNQGKIWTVVNPAIGIPALLGSVTVIAILVhLAILShTTWFPAYWQGGVKKAA

