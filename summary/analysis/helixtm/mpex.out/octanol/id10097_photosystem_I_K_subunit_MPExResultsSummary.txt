Results summary for protein: id10097_photosystem_I_K_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 67 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 2

11-29 (19) : WTPSVGLVVILCNLFAIAL
    DG = -8.59 for segment sequence
    DG = -8.59 for most favorable 19AA centered at #20I
    (WTPSVGLVVILCNLFAIAL)

43-77 (35) : LPIALPALFEGFGLPELLATTSFGhLLAAGVVSGL
    DG = -0.96 for segment sequence
    DG = -2.4 for most favorable 19AA centered at #68L
    (LLATTSFGhLLAAGVVSGL)


Number of known transmembrane regions: 0


Working sequence (length = 83):

MVLATLPDTTWTPSVGLVVILCNLFAIALGRYAIQSRGKGPGLPIALPALFEGFGLPELL
ATTSFGhLLAAGVVSGLQYAGAL

