Results summary for protein: id10099_photosystem_I_M_subunit
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 1

8-26 (19) : VYVALVIALLPAVLAFRLS
    DG = -7.22 for segment sequence
    DG = -7.22 for most favorable 19AA centered at #17L
    (VYVALVIALLPAVLAFRLS)


Number of known transmembrane regions: 0


Working sequence (length = 31):

MALTDTQVYVALVIALLPAVLAFRLSTELYK

