Results summary for protein: id10058_cyto._bc1_complex._Chain_C
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 8 Neutral
    His 54 Neutral
    His 68 Neutral
    His 83 Neutral
    His 97 Neutral
    His 182 Neutral
    His 196 Neutral
    His 201 Neutral
    His 221 Neutral
    His 267 Neutral
    His 308 Neutral
    His 345 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 7

33-63 (31) : FGSLLGICLILQILTGLFLAMhYTSDTTTAF
    DG = -7.5 for segment sequence
    DG = -10.62 for most favorable 19AA centered at #42I
    (FGSLLGICLILQILTGLFL)

77-107 (31) : WIIRYMhANGASMFFICLYMhVGRGLYYGSY
    DG = -6.2 for segment sequence
    DG = -7.52 for most favorable 19AA centered at #86G
    (WIIRYMhANGASMFFICLY)

117-199 (83) : VILLLTVMATAFMGYVLPWGQMSFWGATVITNLLSAIPYIGTNLVEWIWGGFSVDKATLTRFFAFhFILPFIIMAIAMVhLLF
    DG = -26.55 for segment sequence
    DG = -15.51 for most favorable 19AA centered at #190M
    (FhFILPFIIMAIAMVhLLF)

229-247 (19) : ILGALLLILALMLLVLFAP
    DG = -13.54 for segment sequence
    DG = -13.54 for most favorable 19AA centered at #238A
    (ILGALLLILALMLLVLFAP)

272-290 (19) : WYFLFAYAILRSIPNKLGG
    DG = -3.56 for segment sequence
    DG = -3.56 for most favorable 19AA centered at #281L
    (WYFLFAYAILRSIPNKLGG)

291-309 (19) : VLALAFSILILALIPLLhT
    DG = -11.82 for segment sequence
    DG = -11.82 for most favorable 19AA centered at #300I
    (VLALAFSILILALIPLLhT)

320-377 (58) : LSQCLFWALVADLLTLTWIGGQPVEhPYITIGQLASVLYFLLILVLMPTAGTIENKLL
    DG = -8.49 for segment sequence
    DG = -11.78 for most favorable 19AA centered at #356V
    (YITIGQLASVLYFLLILVL)


Number of known transmembrane regions: 0


Working sequence (length = 379):

MTNIRKShPLMKIVNNAFIDLPAPSNISSWWNFGSLLGICLILQILTGLFLAMhYTSDTT
TAFSSVThICRDVNYGWIIRYMhANGASMFFICLYMhVGRGLYYGSYTFLETWNIGVILL
LTVMATAFMGYVLPWGQMSFWGATVITNLLSAIPYIGTNLVEWIWGGFSVDKATLTRFFA
FhFILPFIIMAIAMVhLLFLhETGSNNPTGISSDVDKIPFhPYYTIKDILGALLLILALM
LLVLFAPDLLGDPDNYTPANPLNTPPhIKPEWYFLFAYAILRSIPNKLGGVLALAFSILI
LALIPLLhTSKQRSMMFRPLSQCLFWALVADLLTLTWIGGQPVEhPYITIGQLASVLYFL
LILVLMPTAGTIENKLLKW

