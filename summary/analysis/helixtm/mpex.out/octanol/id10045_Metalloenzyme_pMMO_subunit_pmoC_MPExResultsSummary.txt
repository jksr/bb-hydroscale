Results summary for protein: id10045_Metalloenzyme_pMMO_subunit_pmoC
Hydropathy Analysis Results
All Asp charged: false
All Glu charged: false
All His charged: false
    His 2 Neutral
    His 21 Neutral
    His 134 Neutral
    His 160 Neutral
    His 173 Neutral
    His 231 Neutral
    His 245 Neutral
Scale: WW Octanol (Oct)
DCONH value: 0.0
Mode: Locate
Partitioning: water to bilayer
Window size: 19

Number of hydropathy predicted segments: 6

50-78 (29) : WLTFALAIYTVFYLWVRWYEGVYGWSAGL
    DG = -10.77 for segment sequence
    DG = -14.3 for most favorable 19AA centered at #59T
    (WLTFALAIYTVFYLWVRWY)

94-112 (19) : YTEIVLEIVTASILWGYLW
    DG = -3.76 for segment sequence
    DG = -3.76 for most favorable 19AA centered at #103T
    (YTEIVLEIVTASILWGYLW)

134-152 (19) : hLVWLVAYAWAIYWGASYF
    DG = -10.93 for segment sequence
    DG = -10.93 for most favorable 19AA centered at #143W
    (hLVWLVAYAWAIYWGASYF)

177-195 (19) : FYLSYPIYIITGFAAFIYA
    DG = -10.2 for segment sequence
    DG = -10.2 for most favorable 19AA centered at #186I
    (FYLSYPIYIITGFAAFIYA)

206-236 (31) : ISLPYLVLVVGPFMILPNVGLNEWGhTFWFM
    DG = -11.67 for segment sequence
    DG = -9.29 for most favorable 19AA centered at #215V
    (ISLPYLVLVVGPFMILPNV)

239-269 (31) : LFVAPLhYGFVIFGWLALAVMGTLTQTFYSF
    DG = -14.3 for segment sequence
    DG = -10.92 for most favorable 19AA centered at #248F
    (LFVAPLhYGFVIFGWLALA)


Number of known transmembrane regions: 0


Working sequence (length = 289):

MhETKQGGEKRFTGAICRCShRYNSMEVKMAATTIGGAAAAEAPLLDKKWLTFALAIYTV
FYLWVRWYEGVYGWSAGLDSFAPEFETYWMNFLYTEIVLEIVTASILWGYLWKTRDRNLA
ALTPREELRRNFThLVWLVAYAWAIYWGASYFTEQDGTWhQTIVRDTDFTPShIIEFYLS
YPIYIITGFAAFIYAKTRLPFFAKGISLPYLVLVVGPFMILPNVGLNEWGhTFWFMEELF
VAPLhYGFVIFGWLALAVMGTLTQTFYSFAQGGLGQSLCEAVDEGLIAK

