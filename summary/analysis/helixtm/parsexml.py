import xml.etree.ElementTree as ET




tree = ET.parse('mptopoAlphaHlxTbl.xml')
root = tree.getroot()

def parsetmsegs(tmsegs):
	rtn = ''
	for tmseg in tmsegs:
		rtn += tmseg[1].text + '-' + tmseg[2].text + '||'
	return rtn[:-2]



idint = 10000
for subgroup in root[1][0][1]:#.findall('mptopoProtein'):
	#print subgroup[0].text
	for mptopoprotein in subgroup[1]:
		id = 'id'+str(idint)
		idint += 1
		name = mptopoprotein.findall('proteinName')[0].text.replace(' ', '_')
		name = name.replace('/','.')
		prefix = id+'_'+name
		with open('data/fasta/'+prefix+'.fasta','w') as fout:
			fout.write('>'+prefix+'\n')
			fout.write(mptopoprotein.findall('sequence')[0].text+'\n')
		with open('data/tmsegs/'+prefix+'.tmsegs','w') as fout:
			fout.write(parsetmsegs(mptopoprotein.findall('tmSegments')[0])+'\n')

		#print '\t',mptopoprotein.findall('proteinName')[0].text
		#print '\t',parsetmsegs(mptopoprotein.findall('tmSegments')[0])
		#print '\t',mptopoprotein.findall('sequence')[0].text
		#print ''
		print '>'+prefix
		print mptopoprotein.findall('sequence')[0].text

