import numpy as np

np.set_printoptions(linewidth=10000)
dat = np.genfromtxt('tm_info.txt',dtype=None)
print 'coef', np.corrcoef( dat[1:,5].astype(float), dat[1:,8].astype(float) )[0,1]

within_idx = dat[1:,9]=='TRUE'
outside_idx = dat[1:,9]!='TRUE'
print 'within opm err: true', sum(within_idx), 'false', sum(outside_idx)

print 'opm tilt', np.mean( dat[1:,5].astype(float) ), 'dt', np.mean( dat[1:,4].astype(float) )
print 'my tilt', np.mean( dat[1:,8].astype(float) )

diff = np.absolute( dat[1:,5].astype(float) - dat[1:,8].astype(float) )
print 'all delta tilt from opm', np.mean(diff), np.max(diff)
print 'within delta tilt from opm', np.mean(diff[within_idx]), np.max(diff[within_idx])
print 'outside delta tilt from opm', np.mean(diff[outside_idx]), np.max(diff[outside_idx])

import matplotlib.pyplot as plt

