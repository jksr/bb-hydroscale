import Bio.PDB
import numpy as np
import math
import csv
import sys
sys.path.append('../../../../pylib')
import twbio.AminoAcid as AA
import scipy.optimize
import grid_optimization
import calcE

#np.set_printoptions(precision=4,linewidth=1000,suppress=True)
np.set_printoptions(linewidth=1000,suppress=True)


def get_tm(pdb, chains):
	coords = calcE.get_coords(pdb,chains,True,'ca')
	#coords = calcE.get_coords(pdb,chains,True,'caalz')

	def mycalc_e(x):
		return calcE.calc_e(x,coords)

	# initial values
	x0 = np.array([0, 0, 0, 0, 12])
	result = grid_optimization.minimize(mycalc_e, x0, bounds=((0,1),(0,1e-10),(0,2*math.pi),(0,1e-10),(11.5,11.50000001)), stepnums=[100, 1, 20, 1, 1] )

	#result = scipy.optimize.minimize(mycalc_e, x0, method='SLSQP', bounds=((1e-5,1),(0,1e-10),(0,2*math.pi),(-0.5,0.5),(11,12.5)))
	#result = scipy.optimize.minimize(mycalc_e, x0, method='BFGS', bounds=((0,0.8),(0,2*math.pi),(-5,5),(10.5,13.5)))
	#result = scipy.optimize.minimize(mycalc_e, x0, method='L-BFGS-B', bounds=((0,0.8),(0,2*math.pi),(-5,5),(10.5,13.5)))
	#result = scipy.optimize.minimize(mycalc_e, x0, method='TNC', bounds=((0,0.8),(0,2*math.pi),(-5,5),(10.5,13.5)))
	#result = scipy.optimize.minimize(mycalc_e, x0, method='COBYLA', bounds=((0,0.8),(0,2*math.pi),(-5,5),(10.5,13.5)))
	print pdb, result.x, result.success, chains# ,result.message, calc_e(result.x)
	#print result.x[0]

pdbchains = [ ('1a0s', ['P','Q','R']), ('1bxw', 'A'), ('1e54', ['A','C','E']), ('1ek9', ['A','C','B']), ('1fep', 'A'), ('1i78', 'A'), ('1k24', 'A'), ('1kmo', 'A'), ('1nqe', 'A'), ('1p4t', 'A'), ('1prn', ['A','B','C']), ('1qd6', ['C','D']), ('1qj8', 'A'), ('1t16', 'A'), ('1thq', 'A'), ('1tly', 'A'), ('1uyn', 'X'), ('1xkw', 'A'), ('1yc9', ['A','B','C']), ('2erv', 'A'), ('2f1c', 'A'), ('2f1t', 'A'), ('2fcp', 'A'), ('2gr8', ['A','C','D']), ('2lhf', 'A'), ('2lme', ['A','B','C']), ('2mlh', 'A'), ('2mpr', ['A','B','C']), ('2o4v', ['A','B','C']), ('2omf', ['A','B','C']), ('2por', ['A','B','C']), ('2qdz', 'A'), ('2vqi', 'A'), ('2wjr', 'A'), ('2ynk', 'A'), ('3aeh', 'A'), ('3bs0', 'A'), ('3csl', 'A'), ('3dwo', 'X'), ('3dzm', 'A'), ('3fid', 'A'), ('3kvn', 'A'), ('3pik', ['A','B','C']), ('3rbh', 'A'), ('3rfz', 'B'), ('3syb', 'A'), ('3szv', 'A'), ('3v8x', 'A'), ('3vzt', ['A','B','C']), ('4c00', 'A'), ('4e1s', 'A'), ('4gey', 'A'), ('4pr7', 'A'), ('4q35', 'A'), ('4k3c', 'A'), ('7ahl', ['A','B','C','D','E','F','G']), ('3b07', ['A','B','C','D','E','F','G','H']), ('3o44', ['A','B','C','D','E','F','G']) ]

for pdb,chains in pdbchains:
	get_tm(pdb, chains)
