import matplotlib.pyplot as plt
import math
import numpy as np
import Bio.PDB
import sys
sys.path.append('../../../../pylib')
import twbio.AminoAcid as AA

coefs = np.array( [ #fit -1 1
[ 0., 0., 0., 0.],
[-0.15151454,-2.06943225,-0.21790426, 2.44046509],
[-0.26314788,-2.310217  ,-0.02231672, 2.59213116],
[ 0.30752556,-2.76316747,-0.1713528 , 2.97560555],
[-0.74780005,-1.10793332,-0.12789133, 0.50366347],
[ 0.63411568,-1.83698818,-0.8107452 , 1.84203819],
[ 0.32025769,-1.80797849,-0.16605687, 2.12956088],
[ 0.10177953,-0.62047399, 0.05434983, 0.84099404],
[-0.12032056,-1.70605271, 0.08074856, 1.07619281],
[ 0.03549713, 0.29308042, 0.05861763,-1.30221755],
[ 0.10804827, 0.55790986, 0.09402692,-1.81781299],
[ 0.78092954,-2.25505132,-1.26771728, 2.74613667],
[-0.1346412 , 0.05118752,-0.02675078,-0.68173952],
[-0.04955572,-0.26738981, 0.12381683,-1.26509095],
[ 0.48768372,-1.16219032,-0.18000281, 1.202926  ],
[-0.05042103,-1.41498419,-0.05168586, 1.84641425],
[-0.071056  ,-1.00204713, 0.04027208, 0.98559014],
[-0.09188023,-1.14919685, 0.01111042,-0.16038689],
[-0.24204931,-0.93869862, 0.0701271 ,-0.08360912],
[-0.01231274, 0.17970904, 0.09085752,-1.02498986]
] )

# get coord array for each amino acids from opm pdb w/ or w/o loops
def get_coords(pdb,chains,withloop=True,dirn='ca'):
	centopos = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.cen.topo').astype(int)
	strandends = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.ss').astype(int)
	tm_outfacing_res = []
	tm_infacing_res = []
	for i in range(len(centopos)):
		cen,topo = centopos[i]
		if topo==2:
			tm_outfacing_res += range(cen, strandends[i][0]-1,-2)+range(cen,strandends[i][1]+1,2)
			tm_infacing_res += range(cen-1, strandends[i][0]-1,-2)+range(cen+1,strandends[i][1]+1,2)
		else:
			tm_outfacing_res += range(cen-1, strandends[i][0]-1,-2)+range(cen+1,strandends[i][1]+1,2)
			tm_infacing_res += range(cen, strandends[i][0]-1,-2)+range(cen,strandends[i][1]+1,2)
	tm_outfacing_res = set(tm_outfacing_res)
	tm_infacing_res = set(tm_infacing_res)

	pdbparser = Bio.PDB.PDBParser()
	pdbstructure = pdbparser.get_structure('', 'pdbs/'+dirn+'/'+pdb+'_'+dirn+'.pdb')
	pdbmodel = pdbstructure[0]

	# get coords of all non in-facing res
	# coords = [ 0: 3 x n0 mat, 1: 3 x n1 mat, ..., 19: 3 x n19 mat]
	coords = [ [] for i in range(20) ]
	for chain in chains:
		pdbchain = pdbmodel[chain]
		for itm in pdbchain:
			seqid = itm.get_id()[1]
			if withloop:
				if seqid in tm_infacing_res:
					continue
				aa = AA.three_to_index(itm.resname)
				coords[aa].append( itm['CA'].get_coord() )
			else:
				if seqid not in tm_outfacing_res:
					continue
				aa = AA.three_to_index(itm.resname)
				coords[aa].append( itm['CA'].get_coord() )
	for i in range(20):
		coords[i] = np.array(coords[i]).T
	return coords

# calculate energy
def calc_e(x,coords):
	alpha, beta, gamma, dz, hh = x
	e = 0
	for i in range(1,20):
		if len(coords[i])==0:
			continue
		# rotation matrix
		rxa = np.array( [ [1,0,0], [0,math.cos(alpha),-math.sin(alpha)], [0,math.sin(alpha),math.cos(alpha)] ] )
		ryb = np.array( [ [math.cos(beta),0,math.sin(beta)], [0,1,0] , [-math.sin(beta),0,math.cos(beta)]] )
		rzg = np.array( [ [math.cos(gamma),-math.sin(gamma),0], [math.sin(gamma),math.cos(gamma),0], [0,0,1] ] )
		# displacement
		dzv = np.array( [ [0],[0],[dz] ] )
		# relative z coords

		zs = ( np.dot( np.dot( np.dot(rxa, ryb),rzg) , coords[i] ) + dzv )[2]
		zs = zs / hh
		e_resitype = np.polyval( coefs[i], zs)
		e_resitype[zs>1]=0
		e_resitype[zs<-1]=0
		# use fitted poly to compute energy
		e += np.sum(e_resitype)
	#e /= np.sum(zs!=0) # TODO
	return e
