import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
import math
import numpy as np
import Bio.PDB
import sys
sys.path.append('../../../../pylib')
import twbio.AminoAcid as AA
import calcE


#pdbchains = [ ('1a0s', ['P','Q','R'], 54), ('1bxw', 'A', 8), ('1e54', ['A','C','E'], 16), ('1ek9', ['A','C','B'], 12), ('1fep', 'A',22), ('1i78', 'A',10), ('1k24', 'A',10), ('1kmo', 'A',22), ('1nqe', 'A',22), ('1p4t', 'A',8), ('1prn', ['A','B','C'], 48), ('1qd6', ['C','D'], 24), ('1qj8', 'A', 8), ('1t16', 'A', 14), ('1thq', 'A', 8), ('1tly', 'A', 12), ('1uyn', 'X', 12), ('1xkw', 'A', 22), ('1yc9', ['A','B','C'], 12), ('2erv', 'A', 8), ('2f1c', 'A', 14), ('2f1t', 'A', 8), ('2fcp', 'A', 22), ('2gr8', ['A','C','D'], 12), ('2lhf', 'A', 8), ('2lme', ['A','B','C'], 12), ('2mlh', 'A', 8), ('2mpr', ['A','B','C'], 54), ('2o4v', ['A','B','C'], 48), ('2omf', ['A','B','C'], 48), ('2por', ['A','B','C'], 48), ('2qdz', 'A', 16), ('2vqi', 'A', 24), ('2wjr', 'A', 12), ('2ynk', 'A', 18), ('3aeh', 'A', 12), ('3bs0', 'A', 14), ('3csl', 'A', 22), ('3dwo', 'X',14), ('3dzm', 'A',8), ('3fid', 'A',12), ('3kvn', 'A', 12), ('3pik', ['A','B','C'], 12), ('3rbh', 'A', 18), ('3rfz', 'B', 24), ('3syb', 'A', 18), ('3szv', 'A', 18), ('3v8x', 'A', 22), ('3vzt', ['A','B','C'], 48), ('4c00', 'A', 16), ('4e1s', 'A', 22), ('4gey', 'A', 16), ('4pr7', 'A', 12), ('4q35', 'A', 26), ('4k3c', 'A', 16), ('7ahl', ['A','B','C','D','E','F','G'], 14), ('3b07', ['A','B','C','D','E','F','G','H'], 16), ('3o44', ['A','B','C','D','E','F','G'], 14) ]
pdbmeta = [
('1a0s',	['P', 'Q', 'R']							,			54	,	-1.55413026664	,	3.99659901685	),
('1bxw',	'A'										,			8	,	-3.75831670864	,	4.79623330228	),
('1e54',	['A', 'C', 'E']							,			16	,	-8.53978916206	,	8.82492898222	),
('1ek9',	['A', 'C', 'B']							,			12	,	-4.62449833709	,	23.4874044226	),
('1fep',	'A'										,			22	,	-2.64509944061	,	8.26148340683	),
('1i78',	'A'										,			10	,	-3.48862161182	,	7.77720700604	),
('1k24',	'A'										,			10	,	-3.65724222172	,	6.95838965647	),
('1kmo',	'A'										,			22	,	-2.48626816973	,	7.87016475694	),
('1nqe',	'A'										,			22	,	-1.9468627213	,	5.27542457825	),
('1p4t',	'A'										,			8	,	-3.83437549963	,	4.5613031827 	),
('1prn',	['A', 'B', 'C']							,			48	,	-3.09621240023	,	2.08648405277	),
('1qd6',	['C', 'D']								,			24	,	-3.30001107419	,	3.90346446935	),
('1qj8',	'A'										,			8	,	-3.5282738946	,	4.59284806547	),
('1t16',	'A'										,			14	,	-3.01400745012	,	9.24824557339	),
('1thq',	'A'										,			8	,	-3.58955755586	,	5.61451060734	),
('1tly',	'A'										,			12	,	-3.26566755772	,	5.24257708144	),
('1uyn',	'X'										,			12	,	-2.87773699473	,	7.44426935425	),
('1xkw',	'A'										,			22	,	-2.71436680292	,	7.25152018257	),
('1yc9',	['A', 'B', 'C']							,			12	,	-5.18029782228	,	15.0481437332	),
('2erv',	'A'										,			8	,	-3.7398987164	,	4.25236324728	),
('2f1c',	'A'										,			14	,	-3.57057359344	,	4.08085485041	),
('2f1t',	'A'										,			8	,	-3.65241146856	,	6.48787807137	),
('2fcp',	'A'										,			22	,	-2.28828859723	,	7.4867956255 	),
('2gr8',	['A', 'C', 'D']							,			12	,	-2.21521101652	,	6.59898372347	),
('2lhf',	'A'										,			8	,	-2.27262930658	,	8.52258190768	),
('2lme',	['A', 'B', 'C']							,			12	,	-2.77675186645	,	8.18769187221	),
('2mlh',	'A'										,			8	,	-3.51950841879	,	8.82434433733	),
('2mpr',	['A', 'B', 'C']							,			54	,	-1.89212446636	,	3.72268068599	),
('2o4v',	['A', 'B', 'C']							,			48	,	-2.47280168357	,	4.73618447548	),
('2omf',	['A', 'B', 'C']							,			48	,	-1.90975338393	,	3.73214903226	),
('2por',	['A', 'B', 'C']							,			48	,	-2.63807635814	,	2.61846964151	),
('2qdz',	'A'										,			16	,	-3.26108312604	,	7.8012853831 	),
('2vqi',	'A'										,			24	,	-1.53918776491	,	4.54586429064	),
('2wjr',	'A'										,			12	,	-2.91803549803	,	4.48885560981	),
('2ynk',	'A'										,			18	,	-2.33531972878	,	7.17601048771	),
('3aeh',	'A'										,			12	,	-2.55720691729	,	6.0218795339 	),
('3bs0',	'A'										,			14	,	-3.22006350166	,	5.18466795076	),
('3csl',	'A'										,			22	,	-2.74102009858	,	9.82558902548	),
('3dwo',	'X'										,			14	,	-3.22206253533	,	9.22178580511	),
('3dzm',	'A'										,			8	,	-5.45807126919	,	3.73680882813	),
('3fid',	'A'										,			12	,	-3.35779794528	,	6.6351544421 	),
('3kvn',	'A'										,			12	,	-3.04683268517	,	15.2058427279	),
('3pik',	['A', 'B', 'C']							,			12	,	-3.54176832425	,	17.363320973 	),
('3rbh',	'A'										,			18	,	-2.07162410017	,	7.85944861113	),
('3rfz',	'B'										,			24	,	-2.04654898207	,	6.64669719215	),
('3syb',	'A'										,			18	,	-2.56972939251	,	5.96701661305	),
('3szv',	'A'										,			18	,	-3.24662631082	,	4.43079743527	),
('3v8x',	'A'										,			22	,	-2.50826800362	,	11.2006307115	),
('3vzt',	['A', 'B', 'C']							,			48	,	-1.4286092956	,	4.12248052233	),
('4c00',	'A'										,			16	,	-1.80506362969	,	9.35435781351	),
('4e1s',	'A'										,			22	,	-1.60086631355	,	2.92539418552	),
('4gey',	'A'										,			16	,	-2.93481105462	,	7.24812923526	),
('4pr7',	'A'										,			12	,	-2.98185440367	,	2.09139058493	),
('4q35',	'A'										,			26	,	-1.82199157834	,	7.39903441289	),
('4k3c',	'A'										,			16	,	-3.21492469278	,	10.1296467933	),
('7ahl',	['A', 'B', 'C', 'D', 'E', 'F', 'G']		,			14	,	-2.38799087519	,	26.6648125143	),
('3b07',	['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],			16	,	-1.2696155591	,	21.4265527626	),
('3o44',	['A', 'B', 'C', 'D', 'E', 'F', 'G']		,			14	,	-2.861702906	,	38.2643386428	)
]


alphas = np.linspace(-0.5*math.pi,0.5*math.pi,120)
betas = np.linspace(-0.5*math.pi,0.5*math.pi,120)
gammas = np.linspace(-0.5*math.pi,0.5*math.pi,120)
X,Y = np.meshgrid(alphas,betas)

for pdb,chains,strandn,vmin,vmax in pdbmeta:
	pdbeminmax = []
	for wl in [True]:
	#for wl in [True, False]:
		coords = calcE.get_coords(pdb,chains,wl,dirn='ca')
		#coords = calcE.get_coords(pdb,chains,wl,dirn='caalz')
    
		vals = np.empty(X.shape)
    
		for x in range(len(alphas)):
			for y in range(len(betas)):
				vals[y][x] = calcE.calc_e(np.array([alphas[x], betas[y],0, 0, 11.5]),coords)/strandn

		# get the indices of the maximum values for plotting
		maxval_idx = np.unravel_index(vals.argmax(), vals.shape)
		print pdb, maxval_idx

	#	#tmp = vals.ravel()
	#	#pdbeminmax.append(min(tmp))
	#	#pdbeminmax.append(max(tmp))
	#	plt.close('all')
	#	#plt.pcolor(X,Y,vals,vmin=vmin,vmax=vmax,cmap='hot')
	#	#plt.xlim(min(alphas),max(alphas))
	#	#plt.ylim(min(betas),max(betas))
	#	plt.pcolor(np.degrees(X),np.degrees(Y),vals,vmin=vals.min(),vmax=vals.min()+1,cmap='hot')
	#	#plt.pcolor(np.degrees(X),np.degrees(Y),vals,vmin=vmin,vmax=vmax/2,cmap='hot')
	#	plt.xlim(min(np.degrees(alphas)),max(np.degrees(alphas)))
	#	plt.ylim(min(np.degrees(betas)),max(np.degrees(betas)))
	#	plt.xlabel(r'$\alpha$')
	#	plt.ylabel(r'$\beta$')
	#	cb = plt.colorbar()
	#	plt.savefig(pdb+'_heat_'+str(wl)+'.png')

		# plot funnel landscape
		plt.close('all')
		fig = plt.figure()
		ax = fig.gca(projection='3d')
		ax.plot_surface(np.degrees(X),np.degrees(Y),vals, rstride=2, cstride=2, alpha=0.3, edgecolor='grey')
		cset = ax.contour(np.degrees(X),np.degrees(Y),vals, zdir='z', offset=np.min(vals), cmap=cm.coolwarm)
		cset = ax.contour(np.degrees(X),np.degrees(Y),vals, zdir='x', offset=-120, cmap=cm.coolwarm)
		cset = ax.contour(np.degrees(X),np.degrees(Y),vals, zdir='y', offset=120, cmap=cm.coolwarm)
		ax.view_init(elev=30, azim=-40)
		ax.set_xlabel(r'$\alpha$')
		ax.set_ylabel(r'$\beta$')
		ax.set_zlabel(r'$\Delta\Delta G$')
		plt.savefig(pdb+'_landscape_'+str(wl)+'.png')
		#plt.savefig(pdb+'_landscape_'+str(wl)+'.eps')

	#print pdb,chains,strandn,min(pdbeminmax),max(pdbeminmax)
	#print min(pdbeminmax),max(pdbeminmax)






