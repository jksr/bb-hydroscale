import Bio.PDB
import numpy as np
import math
import sys
sys.path.append('../../../../pylib')
import twbio.AminoAcid as AA


def write_pdb_tm(pdb, chains, alpha, gamma, dz, hh):
	pdbparser = Bio.PDB.PDBParser()
	pdbstructure = pdbparser.get_structure('', 'pdbs/al/'+pdb+'_caal.pdb')
	pdbmodel = pdbstructure[0]
	
	for chain in chains:
		pdbchain = pdbmodel[chain]
		coords = [  ]
		for itm in pdbchain:
			if isinstance(itm.get_id()[1], int):
				coords.append( itm['CA'].get_coord() )
		coords = np.array(coords).T
    
		rxa = np.array( [ [1,0,0], [0,math.cos(alpha),-math.sin(alpha)], [0,math.sin(alpha),math.cos(alpha)] ] )
		rzg = np.array( [ [math.cos(gamma),-math.sin(gamma),0], [math.sin(gamma),math.cos(gamma),0], [0,0,1] ] )
		dzv = np.array( [ [0],[0],[dz] ] )
		newcoords = ( np.dot( np.dot(rxa,rzg), coords ) + dzv ).T
		
		i = 0
		for itm in pdbchain:
			if isinstance(itm.get_id()[1], int):
				itm['CA'].set_coord(newcoords[i])
				i+=1

	io = Bio.PDB.PDBIO()
	io.set_structure(pdbstructure)
	foutn = 'pdbs/tm/'+pdb+'_tm.pdb'
	io.save(foutn, write_end=False)
	
	with open('membrane.dat') as f:
		lines = f.readlines()
	with open(foutn, 'a') as fout:
		hhstr = '%.3f'%hh
		for line in lines:
			fout.write(line[:-1]+hhstr+'                          \n')


if __name__ == '__main__':
	if len(sys.argv)!=2:
		print 'Usage:', sys.argv[0], 'optimized_rlt_file'
		sys.exit(0)
	with open(sys.argv[1]) as f:
		lines = f.readlines()
	for line in lines:
		line = line.replace('[','')
		line = line.replace(']','')
		split = line.split()
		pdb = split[0]
		alpha = float(split[1])
		gamma = float(split[2])
		dz = float(split[3])
		hh = float(split[4])
		success = split[5]
		chains = split[6:]
		for i in range(len(chains)):
			chains[i]=chains[i].strip(',')
			chains[i]=chains[i].strip('\'')
		if success=='False':
			continue
		write_pdb_tm(pdb, chains, alpha, gamma, dz, hh)
