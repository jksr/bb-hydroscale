import Bio.PDB
import numpy as np
import math
import csv
import sys
sys.path.append('../../../../pylib')
import twbio.AminoAcid as AA
import scipy.optimize
import grid_optimization



coefs = np.array( [ #fit -4 4
	[ 0., 0., 0., 0.],
	[ -2.36741469e-03, -1.29339516e-01, -5.44760653e-02, 2.44046509e+00],
	[-0.00411169,-0.14438856,-0.00557918, 2.59213116],
	[ 0.00480509,-0.17269797,-0.0428382,  2.97560555],
	[-0.01168438,-0.06924583,-0.03197283, 0.50366347],
	[ 0.00990806,-0.11481176,-0.2026863,  1.84203819],
	[ 0.00500403,-0.11299866,-0.04151422, 2.12956088],
	[ 0.00159031,-0.03877962, 0.01358746, 0.84099404],
	[-0.00188001,-0.10662829, 0.02018714, 1.07619281],
	[  5.54642731e-04,  1.83175261e-02,  1.46544070e-02, -1.30221755e+00],
	[  1.68825418e-03,  3.48693661e-02,  2.35067308e-02, -1.81781299e+00],
	[ 0.01220202,-0.14094071,-0.31692932, 2.74613667],
	[-0.00210377, 0.00319922,-0.0066877, -0.68173952],
	[ -7.74308121e-04, -1.67118631e-02,  3.09542066e-02, -1.26509095e+00],
	[ 0.00762006,-0.07263689,-0.0450007,  1.202926  ],
	[ -7.87828640e-04, -8.84365118e-02, -1.29214644e-02,  1.84641425e+00],
	[-0.00111025,-0.06262795, 0.01006802, 0.98559014],
	[-0.00143563,-0.0718248 , 0.00277761,-0.16038689],
	[-0.00378202,-0.05866866, 0.01753178,-0.08360912],
	[ -1.92386639e-04,  1.12318147e-02,  2.27143788e-02, -1.02498986e+00]
] )

coefs = np.array( [ #fit -1 1
[ 0., 0., 0., 0.],
[-0.15151454,-2.06943225,-0.21790426, 2.44046509],
[-0.26314788,-2.310217  ,-0.02231672, 2.59213116],
[ 0.30752556,-2.76316747,-0.1713528 , 2.97560555],
[-0.74780005,-1.10793332,-0.12789133, 0.50366347],
[ 0.63411568,-1.83698818,-0.8107452 , 1.84203819],
[ 0.32025769,-1.80797849,-0.16605687, 2.12956088],
[ 0.10177953,-0.62047399, 0.05434983, 0.84099404],
[-0.12032056,-1.70605271, 0.08074856, 1.07619281],
[ 0.03549713, 0.29308042, 0.05861763,-1.30221755],
[ 0.10804827, 0.55790986, 0.09402692,-1.81781299],
[ 0.78092954,-2.25505132,-1.26771728, 2.74613667],
[-0.1346412 , 0.05118752,-0.02675078,-0.68173952],
[-0.04955572,-0.26738981, 0.12381683,-1.26509095],
[ 0.48768372,-1.16219032,-0.18000281, 1.202926  ],
[-0.05042103,-1.41498419,-0.05168586, 1.84641425],
[-0.071056  ,-1.00204713, 0.04027208, 0.98559014],
[-0.09188023,-1.14919685, 0.01111042,-0.16038689],
[-0.24204931,-0.93869862, 0.0701271 ,-0.08360912],
[-0.01231274, 0.17970904, 0.09085752,-1.02498986]
] )


def get_coords(pdb,chains):
	centopos = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.cen.topo').astype(int)
	strandends = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.ss').astype(int)
	tm_outfacing_res = []
	tm_infacing_res = []
	for i in range(len(centopos)):
		cen,topo = centopos[i]
		if topo==2:
			tm_outfacing_res += range(cen, strandends[i][0]-1,-2)+range(cen,strandends[i][1]+1,2)
			tm_infacing_res += range(cen-1, strandends[i][0]-1,-2)+range(cen+1,strandends[i][1]+1,2)
		else:
			tm_outfacing_res += range(cen-1, strandends[i][0]-1,-2)+range(cen+1,strandends[i][1]+1,2)
			tm_infacing_res += range(cen, strandends[i][0]-1,-2)+range(cen,strandends[i][1]+1,2)
	tm_outfacing_res = set(tm_outfacing_res)
	tm_infacing_res = set(tm_infacing_res)

	pdbparser = Bio.PDB.PDBParser()
	pdbstructure = pdbparser.get_structure('', 'pdbs/al/'+pdb+'_caal.pdb')
	pdbmodel = pdbstructure[0]

	# get coords of all non in-facing res
	# coords = [ 0: 3 x n0 mat, 1: 3 x n1 mat, ..., 19: 3 x n19 mat]
	coords = [ [] for i in range(20) ]
	for chain in chains:
		pdbchain = pdbmodel[chain]
		for itm in pdbchain:
			seqid = itm.get_id()[1]
			if seqid in tm_infacing_res:
				continue
			aa = AA.three_to_index(itm.resname)
			coords[aa].append( itm['CA'].get_coord() )
	for i in range(20):
		coords[i] = np.array(coords[i]).T
	return coords



def get_tm(pdb, chains):
	coords = get_coords(pdb,chains)

	# calculate energy
	def calc_e(x):
		alpha, gamma, dz, hh = x
		e = 0
		for i in range(1,20):
			if len(coords[i])==0:
				continue
			# rotation matrix
			rxa = np.array( [ [1,0,0], [0,math.cos(alpha),-math.sin(alpha)], [0,math.sin(alpha),math.cos(alpha)] ] )
			rzg = np.array( [ [math.cos(gamma),-math.sin(gamma),0], [math.sin(gamma),math.cos(gamma),0], [0,0,1] ] )
			# displacement
			dzv = np.array( [ [0],[0],[dz] ] )
			# relative z coords
			zs = ( np.dot( np.dot(rxa,rzg), coords[i] ) + dzv )[2]
			zs = zs / hh
			e_resitype = np.polyval( coefs[i], zs)
			e_resitype[zs>1]=0
			e_resitype[zs<-1]=0
			# use fitted poly to compute energy
			e += np.sum(e_resitype)
		#e /= np.sum(zs!=0) # TODO
		return e

	# initial values
	x0 = np.array([0, 0, 0, 12])
	#result = grid_optimization.minimize(calc_e, x0, bounds=((0,0.8),(0,2*math.pi),(-1,1),(10.5,12.5)), stepnums=[20, 10, 10, 20] )

	result = scipy.optimize.minimize(calc_e, x0, method='SLSQP', bounds=((0,0.8),(0,2*math.pi),(-0.5,0.5),(11,12.5)))
	#result = scipy.optimize.minimize(calc_e, x0, method='BFGS', bounds=((0,0.8),(0,2*math.pi),(-5,5),(10.5,13.5)))
	#result = scipy.optimize.minimize(calc_e, x0, method='L-BFGS-B', bounds=((0,0.8),(0,2*math.pi),(-5,5),(10.5,13.5)))
	#result = scipy.optimize.minimize(calc_e, x0, method='TNC', bounds=((0,0.8),(0,2*math.pi),(-5,5),(10.5,13.5)))
	#result = scipy.optimize.minimize(calc_e, x0, method='COBYLA', bounds=((0,0.8),(0,2*math.pi),(-5,5),(10.5,13.5)))
	print pdb, result.x, result.success, chains# ,result.message, calc_e(result.x)




pdbchains = [ ('1a0s', ['P','Q','R']), ('1bxw', 'A'), ('1e54', ['A','C','E']), ('1ek9', ['A','C','B']), ('1fep', 'A'), ('1i78', 'A'), ('1k24', 'A'), ('1kmo', 'A'), ('1nqe', 'A'), ('1p4t', 'A'), ('1prn', ['A','B','C']), ('1qd6', ['C','D']), ('1qj8', 'A'), ('1t16', 'A'), ('1thq', 'A'), ('1tly', 'A'), ('1uyn', 'X'), ('1xkw', 'A'), ('1yc9', ['A','B','C']), ('2erv', 'A'), ('2f1c', 'A'), ('2f1t', 'A'), ('2fcp', 'A'), ('2gr8', ['A','C','D']), ('2lhf', 'A'), ('2lme', ['A','B','C']), ('2mlh', 'A'), ('2mpr', ['A','B','C']), ('2o4v', ['A','B','C']), ('2omf', ['A','B','C']), ('2por', ['A','B','C']), ('2qdz', 'A'), ('2vqi', 'A'), ('2wjr', 'A'), ('2ynk', 'A'), ('3aeh', 'A'), ('3bs0', 'A'), ('3csl', 'A'), ('3dwo', 'X'), ('3dzm', 'A'), ('3fid', 'A'), ('3kvn', 'A'), ('3pik', ['A','B','C']), ('3rbh', 'A'), ('3rfz', 'B'), ('3syb', 'A'), ('3szv', 'A'), ('3v8x', 'A'), ('3vzt', ['A','B','C']), ('4c00', 'A'), ('4e1s', 'A'), ('4gey', 'A'), ('4pr7', 'A'), ('4q35', 'A'), ('4k3c', 'A'), ('7ahl', ['A','B','C','D','E','F','G']), ('3b07', ['A','B','C','D','E','F','G','H']), ('3o44', ['A','B','C','D','E','F','G']) ]

for pdb,chains in pdbchains:
	get_tm(pdb, chains)
