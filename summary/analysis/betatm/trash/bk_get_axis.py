import numpy as np
import sys
import Bio.PDB
import warnings
from Bio import BiopythonWarning
warnings.simplefilter('ignore', BiopythonWarning)
from collections import Counter

def barrelaxis(pdb, chains, pdbfn, details = False):
	cens = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.cen.topo').astype(int)[:,0].tolist()
	pos_seqid_dict = {0:cens}
	for pos in range(-4,0)+range(1,5):
		pos_seqid_dict[pos]=[]
		for i in range(len(pos_seqid_dict[0])):
			if i%2==0:
				pos_seqid_dict[pos].append(pos_seqid_dict[0][i]+pos)
			else:
				pos_seqid_dict[pos].append(pos_seqid_dict[0][i]-pos)
	seqid_pos_dict = {}
	for pos in range(-4,5):
		for seqid in pos_seqid_dict[pos]:
			seqid_pos_dict[seqid] = pos

	## calc axis of barrel
	pdbparser = Bio.PDB.PDBParser()
	pdbstructure = pdbparser.get_structure('', pdbfn)
	pdbmodel = pdbstructure[0]
    
	pos_coord_dict = {pos:[] for pos in range(-4,5)}
	for chain in chains:
		pdbchain = pdbmodel[chain]
		for itm in pdbchain:
			seqid = itm.get_id()[1]
			if isinstance(seqid, int):
				if seqid in seqid_pos_dict:
					pos_coord_dict[seqid_pos_dict[seqid]].append( itm['CA'].get_coord() )
    
	common = Counter([ len(pos_coord_dict[pos]) for pos in range(-4,5) ]).most_common(1)[0][0]

	data = []
	for pos in range(-4,5):
		if len(pos_coord_dict[pos])==common:
			data.append( np.mean(pos_coord_dict[pos], axis=0) )
	data = np.array(data)
    
	datamean = data.mean(axis=0)
	uu, dd, vv = np.linalg.svd(data - datamean)
	axis = vv[0]/np.linalg.norm(vv[0])

	if details:
		return axis, data, datamean, pos_coord_dict
	else:
		return axis

 















if __name__=='__main__':

	import math
	import glob
	import matplotlib.pyplot as plt
	import mpl_toolkits.mplot3d as m3d
	
	if len(sys.argv)<2 or len(sys.argv)>3:
		print 'Usage', sys.argv[0],'pdbdir [0 degree/1 radian (default 0)]'
		sys.exit(0)
	dirn = sys.argv[1]
	if len(sys.argv)==3:
		isradian = bool(int(sys.argv[2]))
	else:
		isradian = False
	


	pdbchains = [ ('1a0s', ['P','Q','R']), ('1bxw', 'A'), ('1e54', ['A','C','E']), ('1ek9', ['A','C','B']), ('1fep', 'A'), ('1i78', 'A'), ('1k24', 'A'), ('1kmo', 'A'), ('1nqe', 'A'), ('1p4t', 'A'), ('1prn', ['A','B','C']), ('1qd6', ['C','D']), ('1qj8', 'A'), ('1t16', 'A'), ('1thq', 'A'), ('1tly', 'A'), ('1uyn', 'X'), ('1xkw', 'A'), ('1yc9', ['A','B','C']), ('2erv', 'A'), ('2f1c', 'A'), ('2f1t', 'A'), ('2fcp', 'A'), ('2gr8', ['A','C','D']), ('2lhf', 'A'), ('2lme', ['A','B','C']), ('2mlh', 'A'), ('2mpr', ['A','B','C']), ('2o4v', ['A','B','C']), ('2omf', ['A','B','C']), ('2por', ['A','B','C']), ('2qdz', 'A'), ('2vqi', 'A'), ('2wjr', 'A'), ('2ynk', 'A'), ('3aeh', 'A'), ('3bs0', 'A'), ('3csl', 'A'), ('3dwo', 'X'), ('3dzm', 'A'), ('3fid', 'A'), ('3kvn', 'A'), ('3pik', ['A','B','C']), ('3rbh', 'A'), ('3rfz', 'B'), ('3syb', 'A'), ('3szv', 'A'), ('3v8x', 'A'), ('3vzt', ['A','B','C']), ('4c00', 'A'), ('4e1s', 'A'), ('4gey', 'A'), ('4pr7', 'A'), ('4q35', 'A'), ('4k3c', 'A'), ('7ahl', ['A','B','C','D','E','F','G']), ('3b07', ['A','B','C','D','E','F','G','H']), ('3o44', ['A','B','C','D','E','F','G']) ]
	
	for pdb, chains in pdbchains:
		try:
		#if True:
			#pdbfn = glob.glob(dirn+'/'+pdb+'*tm*.pdb')[0]
			pdbfn = glob.glob(dirn+'/'+pdb+'*.pdb')[0]
			vv, data, datamean,pos_coord_dict = barrelaxis(pdb, chains, pdbfn, True)
			print data.shape
			print datamean.shape
			# I use -15, 27 to fit the spread of the data
			# and we want it to have mean 0 (like the points we did
			# the svd on). Also, it's a straight line, so we only need 2 points.
			linepts = vv * np.mgrid[-15:27:2j][:, np.newaxis]
	
	
			ez = np.array([0,0,1]).astype(float)
			degree = math.degrees(math.acos(np.dot(ez,vv)/(np.linalg.norm(vv)*1.0)))
			if degree > 90:
				degree = abs(degree-180)

			if isradian:
				print pdb, np.radians((degree))
				#print np.radians((degree))
			else:
				print pdb, degree
        
			### shift by the mean to get the line in the right place
			linepts += datamean
			
			coords = []
			for pos in range(-4,5):
				coords+=pos_coord_dict[pos]
			coords = np.array(coords)
			
			ax = m3d.Axes3D(plt.figure())
			ax.scatter3D(*data.T, color='b')
			ax.scatter3D(*coords.T, color='r')
			ax.plot3D(*linepts.T, color='b')
			plt.show()
			break
		except:
			print pdb, 'n/a'
			pass


