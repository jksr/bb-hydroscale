import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
np.set_printoptions(linewidth=1000000,suppress=True)

data = np.array([
	[0,1,1],
	[0,1,2],
	[0,1,3],
	[0,1,4],
	[1,1,4],
	[2,1,4],
	[3,1,4],
	[4,1,4],
	[5,1,4],
	[6,1,4],
	]).T
print data
alpha = math.pi/4
beta = 0
gamma = math.pi/4
rxa = np.array( [ [1,0,0], [0,math.cos(alpha),-math.sin(alpha)], [0,math.sin(alpha),math.cos(alpha)] ] )
ryb = np.array( [ [math.cos(beta),0,math.sin(beta)], [0,1,0] , [-math.sin(beta),0,math.cos(beta)]] )
rzg = np.array( [ [math.cos(gamma),-math.sin(gamma),0], [math.sin(gamma),math.cos(gamma),0], [0,0,1] ] )

print rxa
print ryb
print rzg

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(data[0,:],data[1,:],data[2,:])
ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
#plt.show()

#plt.close('all')
#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#data = np.dot(np.dot( rzg, np.dot(ryb, rxa) ), data)
data = np.dot( np.dot(np.dot(rxa,ryb),rzg),data)
print data
#data[2] = data[2]-5
ax.scatter(data[0,:],data[1,:],data[2,:],color='r')
ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
plt.show()
