import math, sys
import numpy as np
import Bio.PDB
import calcE
sys.path.append('../../../../pylib')
import twbio.AminoAcid as AA


def get_orientation(coords, strandn, alpha, alpha_range, beta, beta_range, dz, h):
	perdegree = 0.0174532925
	alpha_stepnum = alpha_range/perdegree*2+1
	beta_stepnum = beta_range/perdegree*2+1
	alphas = np.linspace(alpha-alpha_range, alpha+alpha_range, alpha_stepnum)
	betas = np.linspace(beta-beta_range, beta+beta_range, beta_stepnum)
	X,Y = np.meshgrid(alphas,betas)
	vals = np.empty(X.shape)
	for x in range(len(alphas)):
		for y in range(len(betas)):
			vals[y][x] = calcE.calc_e(np.array([alphas[x], betas[y],0, dz, h]),coords)/strandn

	# get the indices of the minimum values
	minval_idx = np.unravel_index(vals.argmin(), vals.shape)
	mymin = vals.min()
	# flood out from the minimum to find the area
	floodarea = np.where(vals<=mymin+0.5)
	myalpha = np.mean(X[floodarea])
	mybeta = np.mean(Y[floodarea])

	return myalpha, mybeta


def get_position(coords, strandn, alpha, beta, dz, dz_range, h, h_range):
	perdist = 0.2
	dz_stepnum = dz_range/perdist*2+3
	h_stepnum = h_range/perdist*2+3

	dzs = np.linspace(dz-dz_range, dz+dz_range, dz_stepnum)
	hs = np.linspace(h-h_range, h+h_range, h_stepnum)
	X,Y = np.meshgrid(dzs,hs)
	vals = np.empty(X.shape)
	for x in range(len(dzs)):
		for y in range(len(hs)):
			vals[y][x] = calcE.calc_e(np.array([alpha, beta, 0, dzs[x], hs[y]]),coords)/strandn

	# get the indices of the minimum values
	minval_idx = np.unravel_index(vals.argmin(), vals.shape)
	mymin = vals.min()
	# flood out from the minimum to find the area
	floodarea = np.where(vals<=mymin+0.5)
	mydz = np.mean(X[floodarea])
	myh = np.mean(Y[floodarea])

	return mydz, myh


pdbchains = [ ('1a0s', ['P','Q','R'], 54), ('1bxw', 'A', 8), ('1e54', ['A','C','E'], 16), ('1ek9', ['A','C','B'], 12), ('1fep', 'A',22), ('1i78', 'A',10), ('1k24', 'A',10), ('1kmo', 'A',22), ('1nqe', 'A',22), ('1p4t', 'A',8), ('1prn', ['A','B','C'], 48), ('1qd6', ['C','D'], 24), ('1qj8', 'A', 8), ('1t16', 'A', 14), ('1thq', 'A', 8), ('1tly', 'A', 12), ('1uyn', 'X', 12), ('1xkw', 'A', 22), ('1yc9', ['A','B','C'], 12), ('2erv', 'A', 8), ('2f1c', 'A', 14), ('2f1t', 'A', 8), ('2fcp', 'A', 22), ('2gr8', ['A','C','D'], 12), ('2lhf', 'A', 8), ('2lme', ['A','B','C'], 12), ('2mlh', 'A', 8), ('2mpr', ['A','B','C'], 54), ('2o4v', ['A','B','C'], 48), ('2omf', ['A','B','C'], 48), ('2por', ['A','B','C'], 48), ('2qdz', 'A', 16), ('2vqi', 'A', 24), ('2wjr', 'A', 12), ('2ynk', 'A', 18), ('3aeh', 'A', 12), ('3bs0', 'A', 14), ('3csl', 'A', 22), ('3dwo', 'X',14), ('3dzm', 'A',8), ('3fid', 'A',12), ('3kvn', 'A', 12), ('3pik', ['A','B','C'], 12), ('3rbh', 'A', 18), ('3rfz', 'B', 24), ('3syb', 'A', 18), ('3szv', 'A', 18), ('3v8x', 'A', 22), ('3vzt', ['A','B','C'], 48), ('4c00', 'A', 16), ('4e1s', 'A', 22), ('4gey', 'A', 16), ('4pr7', 'A', 12), ('4q35', 'A', 26), ('4k3c', 'A', 16), ('7ahl', ['A','B','C','D','E','F','G'], 14), ('3b07', ['A','B','C','D','E','F','G','H'], 16), ('3o44', ['A','B','C','D','E','F','G'], 14) ]

pdbchains = sorted(pdbchains)


for pdb,chains,strandn in pdbchains:
	coords = calcE.get_coords(pdb,chains,True,dirn='caalz')
	myalpha = 0
	mybeta = 0
	mydz = 0
	myh = 11.5
	alpha_range = 0.5*math.pi
	beta_range = 0.5*math.pi
	dz_range = 1
	h_range = 1.5
	changed = True
	for i in range(10):
		if not changed:
			break
		myalpha, mybeta = get_orientation(coords, strandn, myalpha, alpha_range, mybeta, beta_range, mydz, myh)
		mydz, myh = get_position(coords,strandn, myalpha, mybeta, mydz, dz_range, myh, h_range)
		changed = False
		if alpha_range > math.pi/18:
			alpha_range/=2
			changed = True
		if beta_range > math.pi/18:
			beta_range/=2
			changed = True
		if dz_range > 0.2:
			dz_range*=0.8
			changed = True
		if h_range > 0.2:
			h_range*=0.8
			changed = True
		#print pdb, i, np.degrees(myalpha), np.degrees(mybeta), mydz, myh, '||', np.degrees(alpha_range), np.degrees(beta_range), dz_range, h_range
	#print '='*40
	print pdb, np.degrees(myalpha), np.degrees(mybeta), mydz, myh



