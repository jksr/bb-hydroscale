import numpy as np
import sys, glob, math
import Bio.PDB
import warnings
from Bio import BiopythonWarning
warnings.simplefilter('ignore', BiopythonWarning)



def segment_tilt(pdb, chains, pdbfn, h, details = False):
	lencut = 3-1
	ss = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.ss').astype(int)

	pdbparser = Bio.PDB.PDBParser()
	pdbstructure = pdbparser.get_structure('', pdbfn)
	pdbmodel = pdbstructure[0]

	chainseqid_coord_dict = {}
	for chain in chains:
		chainseqid_coord_dict[chain] = {}
		pdbchain = pdbmodel[chain]
		for itm in pdbchain:
			seqid = itm.get_id()[1]
			if isinstance(seqid, int):
				chainseqid_coord_dict[chain][seqid] = itm['CA'].get_coord()
	
	allangle = []

	for chain in chains:
		for i in range(len(ss)):
			if i%2==0:
				for dseqid in range(3):
					if abs(chainseqid_coord_dict[chain][ss[i][0]+dseqid][2]) <= h:
						start = ss[i][0]+dseqid
						end = start+lencut if start+lencut <= ss[i][1] else ss[i][1]
						break
			else:
				for dseqid in range(3):
					if abs(chainseqid_coord_dict[chain][ss[i][1]-dseqid][2]) <= h:
						start = ss[i][1]-dseqid
						end = start-lencut if start-lencut <= ss[i][0] else ss[i][0]
						start, end = end, start
						break
			data = []
			for seqid in range(start,end+1):
				data.append(chainseqid_coord_dict[chain][seqid])
			#if i%2==0:
			#	for seqid in range(start,end+1):
			#		data.append(chainseqid_coord_dict[chain][seqid])
			#else:
			#	for seqid in range(start,end+1):
			#		data.append(-chainseqid_coord_dict[chain][seqid])

			# calc axis of current strand seg
			data = np.array(data)
			datamean = data.mean(axis=0)
			uu, dd, vv = np.linalg.svd(data - datamean)
			vv = vv[0]/np.linalg.norm(vv[0])

			# calc anlge of current strand seg to z axis
			ez = np.array([0,0,1]).astype(float)
			degree = math.degrees(math.acos(np.dot(ez,vv)/(np.linalg.norm(vv)*1.0)))
			if degree>90:
				degree = 180-degree

			allangle.append(degree)

	if details:
		allangle = np.mean(allangle), np.std(allangle), np.min(allangle), np.max(allangle)
	else:
		allangle = np.mean(allangle)

	return allangle
 

if __name__=='__main__':

	pdb_pos_ang = {}
	with open('position_orientation.txt') as f:
		lines = f.readlines()
	for line in lines:
		pdb, alpha, beta, dz, h = line.split()
		alpha = np.radians(float(alpha))
		beta = np.radians(float(beta))
		dz = float(dz)
		h = float(h)
		pdb_pos_ang[pdb]=(alpha,beta,dz,h)


	pdbchains = [ ('1a0s', ['P','Q','R']), ('1bxw', 'A'), ('1e54', ['A','C','E']), ('1ek9', ['A','C','B']), ('1fep', 'A'), ('1i78', 'A'), ('1k24', 'A'), ('1kmo', 'A'), ('1nqe', 'A'), ('1p4t', 'A'), ('1prn', ['A','B','C']), ('1qd6', ['C','D']), ('1qj8', 'A'), ('1t16', 'A'), ('1thq', 'A'), ('1tly', 'A'), ('1uyn', 'X'), ('1xkw', 'A'), ('1yc9', ['A','B','C']), ('2erv', 'A'), ('2f1c', 'A'), ('2f1t', 'A'), ('2fcp', 'A'), ('2gr8', ['A','C','D']), ('2lhf', 'A'), ('2lme', ['A','B','C']), ('2mlh', 'A'), ('2mpr', ['A','B','C']), ('2o4v', ['A','B','C']), ('2omf', ['A','B','C']), ('2por', ['A','B','C']), ('2qdz', 'A'), ('2vqi', 'A'), ('2wjr', 'A'), ('2ynk', 'A'), ('3aeh', 'A'), ('3bs0', 'A'), ('3csl', 'A'), ('3dwo', 'X'), ('3dzm', 'A'), ('3fid', 'A'), ('3kvn', 'A'), ('3pik', ['A','B','C']), ('3rbh', 'A'), ('3rfz', 'B'), ('3syb', 'A'), ('3szv', 'A'), ('3v8x', 'A'), ('3vzt', ['A','B','C']), ('4c00', 'A'), ('4e1s', 'A'), ('4gey', 'A'), ('4pr7', 'A'), ('4q35', 'A'), ('4k3c', 'A'), ('7ahl', ['A','B','C','D','E','F','G']), ('3b07', ['A','B','C','D','E','F','G','H']), ('3o44', ['A','B','C','D','E','F','G']) ]
	pdbchains = sorted(pdbchains)
	
	dirn = 'pdbs/rotca/'
	for pdb, chains in pdbchains:
		if pdb=='1bxw':
			pdbfn = glob.glob(dirn+'/'+pdb+'*.pdb')[0]
			h = pdb_pos_ang[pdb][3]
			#h = 12.6 # for pdbs/ca
			print dirn, pdb, segment_tilt(pdb, chains, pdbfn, h, True)
		elif pdb=='2fcp':
			pdbfn = glob.glob(dirn+'/'+pdb+'*.pdb')[0]
			h = pdb_pos_ang[pdb][3]
			#h = 12.2 # for pdbs/ca
			print dirn, pdb, segment_tilt(pdb, chains, pdbfn, h, True)
		else:
			pass
	dirn = 'pdbs/ca/'
	for pdb, chains in pdbchains:
		if pdb=='1bxw':
			pdbfn = glob.glob(dirn+'/'+pdb+'*.pdb')[0]
			#h = pdb_pos_ang[pdb][3]
			h = 12.6 # for pdbs/ca
			print dirn, pdb, segment_tilt(pdb, chains, pdbfn, h, True)
		elif pdb=='2fcp':
			pdbfn = glob.glob(dirn+'/'+pdb+'*.pdb')[0]
			#h = pdb_pos_ang[pdb][3]
			h = 12.2 # for pdbs/ca
			print dirn, pdb, segment_tilt(pdb, chains, pdbfn, h, True)
		else:
			pass

