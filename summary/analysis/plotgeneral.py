import numpy as np
import matplotlib.pyplot as plt
import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')
import twbio.AminoAcid

means = np.loadtxt('HD/gram_negative_mean_profile.txt').T
stds = np.loadtxt('HD/gram_negative_mean_profile_std.txt').T
#means = np.loadtxt('HD/sym_gram_negative_mean_profile.txt').T
#stds = np.loadtxt('HD/sym_gram_negative_mean_profile_std.txt').T


fits = []
coeffs = []
for i in range(len(means)):
	#coeff = np.polyfit(range(-4,5), means[i],3)
	#fitted = np.polyval(coeff,range(-4,5))
	coeff = np.polyfit(np.arange(-4,5)/4.0, means[i],3)#will
	fitted = np.polyval(coeff,np.arange(-4,5)/4.0)
	coeffs.append(coeff)
	fits.append(fitted)


# plot depth dependent transfer energy

plotorder = [ 0, 10, 1, 11, 8, 9, 19, 3, 2, 15, 12, 13, 6, 5, 16, 18, 17, 7, 14, 4 ]
fig = plt.figure()
fig.set_size_inches(15,12)
mainax = fig.add_subplot(111) # the big subplot
mainax.set_xlabel('\n'+r'Position index ($i$)'+'\n'+'GeTFEP', fontsize=20)
#mainax.set_xlabel('\n'+r'Position index ($i$)'+'\n'+'sGeTFEP', fontsize=20)
mainax.set_ylabel(r'$\mathbf{\Delta\Delta G_{aa(i)}}$(kcal/mol)'+'\n', fontsize=20)
# Turn off axis lines and ticks of the big subplot
mainax.patch.set_visible(False)
mainax.spines['top'].set_visible(False)
mainax.spines['bottom'].set_visible(False)
mainax.spines['left'].set_visible(False)
mainax.spines['right'].set_visible(False)
mainax.set_xticklabels([])
mainax.set_yticklabels([])

for iprox in range(20):
	i = plotorder[iprox]
	curax = fig.add_subplot(4,5,iprox+1)

	curax.axhline(0, linestyle='-.', color = '#808080')
	curax.set_title(twbio.AminoAcid.index_to_three(i).title(), fontsize=20)
	curax.set_ylim(-3.5,7)
	curax.set_xlim(-4.5,4.5)
	#curax.set_xlim(-1.1,1.1)#will

	#curax.plot(np.arange(-4,5)/4.0,fits[i],linewidth = 3, color = '#de2d26')#will
	curax.plot(range(-4,5),fits[i],linewidth = 3, color = '#de2d26')
	#curax.plot(range(-4,5),means[i],linewidth = 3, color = '#de2d26')
	curax.errorbar(range(-4,5), means[i], yerr=stds[i], fmt='o', color='#2171b5', ecolor='#2171b5')

	curax.tick_params(labelsize=18)

plt.tight_layout(pad=0.2,w_pad = 0.4, h_pad = 0.4)
#plt.savefig('general_hydroi.png')
plt.savefig('a.svg')


