from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys


#print np.linspace(-3,6,81)
#np.set_printoptions(linewidth=10000000000)

#fn = 'outlier_resampled_single_value.pickle'
#dat = pickle.load(open(fn,'rb'))
#for res in range(20):
#	hist = []
#	for dep in range(-4,5):
#		print res, dep
#		currhist = np.histogram(dat[dep][res], bins=np.linspace(-3,6,81))
#		print currhist[0]


with open('distribution.txt') as f:
	line = f.readline()
	lines = f.readlines()

energies = np.array(line[1:-2].split()).astype(float)  # 81

everything = {}
for i in range(len(lines)/2):
	res, dep = np.array(lines[i*2].split()).astype(int)
	count = np.array(lines[i*2+1][1:-2].split()).astype(int)
	if res not in everything:
		everything[res] = {}
	everything[res][dep] = count


aaids = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
aas = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']
colors = ['#8dd3c7', '#ffffb3', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#b3de69', '#fccde5', '#d9d9d9']

for aaid, aa in zip(aaids,aas):
	plt.close('all')
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	for dep in range(-4,4):
		ax.bar(energies[:-1], everything[aaid][dep], zs=dep, zdir='y', color=colors[dep+4], alpha=0.8, width = energies[1]-energies[0], linewidth=None)

	ax.set_xlabel('TFE')
	ax.set_ylabel('depth')
	ax.set_zlabel('count')
	plt.title(aa)
	plt.savefig('distribution_'+aa+'.eps')
	plt.savefig('distribution_'+aa+'.png')
	#plt.show()	


