import glob
import numpy as np

all_res_pool=[]
#for fn in glob.glob('*_svp.txt'):
for fn in glob.glob('*_svz.txt'):
	pdb = fn[:4]
	with open(fn) as f:
		lines = f.readlines()
	pdbcount = 0
	for line in lines:
		if not line.startswith('\t'):
			continue
		split = line.split()
		if len(split)!=4:
			continue
		seqid = int(split[0])
		res = split[1]
		dep = int(split[2])
		score = float(split[3])
		all_res_pool.append(res)

		print pdb, seqid
		pdbcount+=1
	print '-'*40

	print pdb, 'total', pdbcount
	print '-'*40
	


all_res_pool = np.array(all_res_pool)

for aa in ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']:
	print aa, sum(all_res_pool==aa)
print '-'*40


## too many outliers in L, P, I seems because of the long tail
## try the following examples
#a=np.array([1,2,3,4,5,6,7,8,9,10])
#print scipy.stats.norm.cdf( (a-np.mean(a))/np.std(a) )
#a=np.array([1,2,3,4,5,6,7,8,9,10, 5])
#print scipy.stats.norm.cdf( (a-np.mean(a))/np.std(a) )
#a=np.array([1,2,3,4,5,6,7,8,9,10, 5,6])
#print scipy.stats.norm.cdf( (a-np.mean(a))/np.std(a) )

