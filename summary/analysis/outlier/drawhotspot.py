import numpy as np
import sys
sys.path.append('../../../../pylib')
import twbio.AminoAcid as AA
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import glob
import math


def getmap(pdb, neutral=0):
	strands = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.ss').astype(int)
	centopos = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.cen.topo').astype(int)
	cens = centopos[:,0].tolist()
	centopos = centopos[:,1].tolist()
	regs = np.genfromtxt('../../../inputs/'+pdb+'/'+pdb+'.registration',skip_footer=1)[:,5].astype(int).tolist()

	N = len(cens)

	map = []
	for i in range(N+1):
		i = (i-1)%N
		if i%2==0:
			map.append(range(strands[i][0],strands[i][1]+5))
		else:
			map.append( range(strands[i][1],strands[i][0]-1,-1) )

	map = map[::-1]
	cens = ([cens[-1]]+cens)[::-1]
	centopos = ([centopos[-1]]+centopos)[::-1]
	regs = ([regs[-1]]+regs)[::-1]

	#for i in range(N+1):
	#	print map[i]
	#print '-'*30

	#print cens
	#print centopos
	#print regs

	relpos = [0]
	for i in range(1,N+1):
		relpos.append(relpos[i-1]+regs[i])

	relpos = np.array(relpos)
	relpos -= relpos.min()
	#print relpos

	maxlen = 0;
	for i in range(N+1):
		map[i] = [neutral]*relpos[i] + map[i]
		if len(map[i])>maxlen:
			maxlen = len(map[i])
	for i in range(N+1):
		map[i] += [neutral]*(maxlen-len(map[i]))

	#for i in range(N+1):
	#	cen = cens[i]
	#	centopo = centopos[i]
	#	for j in range(len(map[i])):
	#		if map[i][j]==neutral:
	#			continue
	#		if (map[i][j]-cen)%2==0 and centopo == 1:
	#				map[i][j]=neutral
	#		elif (map[i][j]-cen)%2!=0 and centopo == 2:
	#				map[i][j]=neutral

	map = np.array(map).T

	#np.set_printoptions(linewidth=130)
	#print np.flipud(map)
	return map

def gettextmap(map,neutral=0):
	textmap = np.chararray(map.shape, itemsize=4)
	textmap[:] = ''
	textmap=np.array(textmap).astype(str)
	res = [0]+np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.res').astype(int).tolist()
	for i in range(map.shape[0]):
		for j in range(map.shape[1]):
			if map[i][j]!=neutral:
				if map[i][j]>=len(res):
					continue
				if res[ map[i][j] ]==200:
					continue
				textmap[i][j] = str(map[i][j])+AA.index_to_one(res[ map[i][j] ])
	return textmap

def getvalmap(map, scorefn, neutral=0):
	with open(scorefn) as f:
		lines = f.readlines()
	seqid_score_dict = {}
	seqid_set = set()
	lines = lines[4:]
	for line in lines:
		split = line.split()
		if len(split)!=4:
			continue
		seqid_score_dict[ int(split[0]) ] = float(split[3])
		seqid_set.add(int(split[0]))

	valmap = np.zeros(map.shape)
	for i in range(valmap.shape[0]):
		for j in range(valmap.shape[1]):
			if map[i][j] in seqid_set:
				valmap[i][j] = seqid_score_dict[map[i][j]]-0.5
				#valmap[i][j] = abs(seqid_score_dict[map[i][j]]-0.5)
				if valmap[i][j]<0:
					valmap[i][j]=-math.exp(-valmap[i][j]*10)
				else:
					valmap[i][j]=math.exp(valmap[i][j]*10)
	return valmap

def drawfigure(pdb, scorefn, outfn):
	neutral = 0

	map = getmap(pdb, neutral)
	textmap = gettextmap(map,neutral)
	valmap = getvalmap(map,scorefn,neutral)
	np.set_printoptions(linewidth=130)

	plt.close('all')
	
	## heat map
	#heatmap = plt.pcolor(valmap, cmap='RdBu_r', vmin=-0.5, vmax=0.5)
	heatmap = plt.pcolor(valmap, cmap='RdBu_r', norm=colors.SymLogNorm(linthresh=0.45, linscale=0.01, vmin=-0.5, vmax=0.5))
	
	## text
	for y in range(map.shape[0]):
		for x in range(map.shape[1]):
			plt.text(x + 0.5, y + 0.5, textmap[y, x], horizontalalignment='center', verticalalignment='center',)
	
	plt.xlim(-0.05,map.shape[1]+0.05)
	plt.ylim(-0.05,map.shape[0]+0.05)
	
	## border
	for i in range(map.shape[1]):
		for first in range(map.shape[0]):
			if map[first][i]!=neutral:
				break
		for last in range(map.shape[0]-1,-1,-1):
			if map[last][i]!=neutral:
				break
		ymin = float(first)/float(map.shape[0])
		ymax = float(last+1)/float(map.shape[0])
		xmin = float(i)/float(map.shape[1])
		xmax = float(i+1)/float(map.shape[1])
		plt.axvline(x=i,ymin=ymin,ymax=ymax,color='grey')
		plt.axvline(x=i+1,ymin=ymin,ymax=ymax,color='grey')
		plt.axhline(y=first,xmin=xmin,xmax=xmax,color='grey')
		plt.axhline(y=last+1,xmin=xmin,xmax=xmax,color='grey')
	
	
	plt.colorbar()

	# put the major ticks at the middle of each cell
	plt.gca().set_xticks(np.arange(map.shape[1]) + 0.5, minor=False)
	plt.gca().set_yticks(np.arange(map.shape[0]) + 0.5, minor=False)
	#lebels
	plt.gca().set_xticklabels(range(map.shape[1]-1,0,-1)+[map.shape[1]-1], minor=False)
	plt.gca().set_yticklabels([], minor=False)
	
	plt.gca().spines['top'].set_visible(False)
	plt.gca().spines['right'].set_visible(False)
	plt.gca().spines['bottom'].set_visible(False)
	plt.gca().spines['left'].set_visible(False)
	
	plt.gcf().set_size_inches(30, 20)
	plt.savefig(outfn)
	#plt.show()

if __name__ == '__main__':
	for fn in glob.glob('scores/*.txt'):
		print fn
		print fn+'.svg'
		pdb = fn[ fn.find('scores/')+len('scores/'): fn.find('_')]
		drawfigure(pdb, fn, fn+'.png')

neutral = 0
pdb = '1thq'
pdb = '2erv'
pdb = '2omf'
pdb = '1qd6'
#pdb = '2por'
#pdb = '1a0s'
#pdb = '1bxw'
map = getmap(pdb, neutral)
textmap = gettextmap(map,neutral)
valmap = getvalmap(map,pdb+'_svz.txt',neutral)
np.set_printoptions(linewidth=130)

print valmap
## drawmap

import matplotlib.pyplot as plt
import matplotlib.colors as colors

## heat map
heatmap = plt.pcolor(valmap, cmap='RdBu_r', vmin=-148, vmax=148)
#heatmap = plt.pcolor(valmap, cmap='RdBu_r', vmin=-0.5, vmax=0.5)

## text
for y in range(map.shape[0]):
	for x in range(map.shape[1]):
		plt.text(x + 0.5, y + 0.5, textmap[y, x], horizontalalignment='center', verticalalignment='center',)

plt.xlim(-0.05,map.shape[1]+0.05)
plt.ylim(-0.05,map.shape[0]+0.05)

## border
for i in range(map.shape[1]):
	print map.shape
	for first in range(map.shape[0]):
		if map[first][i]!=neutral:
			break
	for last in range(map.shape[0]-1,-1,-1):
		if map[last][i]!=neutral:
			break
	ymin = float(first)/float(map.shape[0])
	ymax = float(last+1)/float(map.shape[0])
	xmin = float(i)/float(map.shape[1])
	xmax = float(i+1)/float(map.shape[1])
	plt.axvline(x=i,ymin=ymin,ymax=ymax,color='grey')
	plt.axvline(x=i+1,ymin=ymin,ymax=ymax,color='grey')
	plt.axhline(y=first,xmin=xmin,xmax=xmax,color='grey')
	plt.axhline(y=last+1,xmin=xmin,xmax=xmax,color='grey')


#cbar = plt.gcf().colorbar(heatmap,ticks=[-0.5,0,0.5])
#cbar.ax.set_yticklabels(['lu', 'he', 'ha'])
#plt.colorbar()

# put the major ticks at the middle of each cell
plt.gca().set_xticks(np.arange(map.shape[1]) + 0.5, minor=False)
plt.gca().set_yticks(np.arange(map.shape[0]) + 0.5, minor=False)
#lebels
plt.gca().set_xticklabels(range(map.shape[1]-1,0,-1)+[map.shape[1]-1], minor=False)
plt.gca().set_yticklabels([], minor=False)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.gca().spines['bottom'].set_visible(False)
plt.gca().spines['left'].set_visible(False)

plt.show()

