import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as ss
import sys

getfepmid = np.loadtxt('getfepmid.hydro')
fleming = np.loadtxt('fleming.hydro')
lin = np.loadtxt('lin.hydro')


theirnames = ['GeTFEP', 'Moon-Fleming', 'Lin']

getfepmid = getfepmid

aa = ["A", "R", "N", "D", "C", "Q", "E", "G", "H", "I", "L", "K", "M", "F", "P", "S", "T", "W", "Y", "V", "X"]

fig = plt.figure(figsize =(10,10),dpi=150 )
ax1 = fig.add_subplot(1,1,1)

tmpfleming = []
tmpgetfepmid = []
tmplin = []
tmpaa = []

for quadruplet in sorted(zip(getfepmid,fleming,lin,aa)):
	tmpgetfepmid.append(quadruplet[0])
	tmpfleming.append(quadruplet[1])
	tmplin.append(quadruplet[2])
	tmpaa.append(quadruplet[3])

x = np.arange(20)
x = x + 0.3
width = 0.25
leglin = ax1.bar(x,tmplin,width=width,color='grey',linewidth=0)
leggetfepmid = ax1.bar(x+width,tmpgetfepmid,width=width,color='#2b8cbe')
legfleming = ax1.bar(x+2*width,tmpfleming,width=width,color='#e34a33')

ax1.set_xticks(x+1.5*width)
ax1.set_xticklabels(tmpaa, fontsize = 20)
ax1.set_ylabel(r"$\Delta\Delta G^{210}$(kcal/mol)", fontsize = 20)
ax1.set_xlim([0,19+2*0.3+2*width])
ax1.legend( (leglin,leggetfepmid[0],legfleming[0]), ('Lin', 'GetFEP-mid', 'Moon-Fleming'), loc=0, fontsize = 20)
plt.savefig('bar_three.svg',bbox_inches='tight')


