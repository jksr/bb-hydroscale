flemingdepth = [-4, -2, 0, 2, 3, 4 ]
flemingL = [-1.21, -2.16, -1.81, -2.62, -1.83, -1.21 ]
flemingLstd = [0.21, 0.09, 0.10, 0.21, 0.11, 0.04 ]

flemingR = [0.80, 2.35, 3.71, 3.06, 2.07, 0.61 ]
flemingRstd = [0.27, 0.12, 0.11, 0.16, 0.23, 0.02 ]


omplaR = [ 0.1244,  0.1147, 2.9978, 3.2808, 2.0730,  0.7510]
omplaL = [-1.1854, -1.8882,-2.0788,-2.2874,-3.1136, -1.6323]


import numpy as np
#getfepmean = np.loadtxt('../HD/gram_negative_mean_profile.txt')
#getfepstd = np.loadtxt('../HD/gram_negative_mean_profile_std.txt')

#getfepL = getfepmean[:,10]
#getfepR = getfepmean[:,1]

#getfepstdL = getfepstd[:,10]
#getfepstdR = getfepstd[:,1]

#depth = range(-4,5)

#coefL = np.polyfit(depth,getfepL,3)
#coefR = np.polyfit(depth,getfepR,3)

#fittedL = np.polyval(coefL,depth)
#fittedR = np.polyval(coefR,depth)

import matplotlib
matplotlib.use('Agg')
#tmpl = []
#tmpl.append(getfepL[0])
#tmpl.append(getfepL[2])
#tmpl.append(getfepL[4])
#tmpl.append(getfepL[6])
#tmpl.append(getfepL[7])
#tmpl.append(getfepL[8])
#tmpr = []
#tmpr.append(getfepR[0])
#tmpr.append(getfepR[2])
#tmpr.append(getfepR[4])
#tmpr.append(getfepR[6])
#tmpr.append(getfepR[7])
#tmpr.append(getfepR[8])
#print np.corrcoef(flemingL,tmpl)[0,1]
#print np.corrcoef(flemingR,tmpr)[0,1]

import matplotlib.pyplot as plt
#plt.plot(depth,fittedL,'-', color='#74add1', linewidth=2)
#plt.errorbar(depth,getfepL,yerr=getfepstdL, fmt='o', color='#74add1', ecolor='#74add1', markersize=8)
plt.plot(flemingdepth,omplaL, 'o', color='#74add1', markersize=8)
#plt.plot(flemingdepth,  flemingL, 'o', color='#f46d43', markersize=8)
plt.errorbar(flemingdepth,flemingL,yerr=flemingLstd, fmt='o', color='#f46d43', ecolor='#f46d43', markersize=8)

plt.text(-4.3,-3.2, 'GeTFEP Leu', color='#74add1', fontsize=16)
plt.text(-4.3,-4.2, 'MF-scale Leu', color='#f46d43', fontsize=16)

#plt.plot(depth,fittedR,'-', color='#5aae61', linewidth=2)
#plt.errorbar(depth,getfepR,yerr=getfepstdR, fmt='o', color='#5aae61', ecolor='#5aae61', markersize=8)
plt.plot(flemingdepth,omplaR, 'o', color='#5aae61', markersize=8)
#plt.plot(flemingdepth,  flemingR, 'o', color='#9970ab', markersize=8)
plt.errorbar(flemingdepth,flemingR,yerr=flemingRstd, fmt='o', color='#9970ab', ecolor='#9970ab', markersize=8)

plt.text(-4.3,5, 'GeTFEP Arg', color='#5aae61', fontsize=16)
plt.text(-4.3,4, 'MF-scale Arg', color='#9970ab', fontsize=16)

plt.axhline(0,linestyle='--',color='grey')
plt.ylim([-4.5,6])
plt.xlim([-4.5,4.5])

plt.ylabel( r"$\Delta\Delta G\ $(kcal/mol)")
plt.xlabel( "depth")
plt.savefig("arg_leu_depth_ompla.svg")
plt.show()
