import numpy as np
# value from MD simulation
# Partitioning of Amino Acid Side Chains into Lipid Bilayers: Results from Computer Simulations and Comparison to Experiment
# values seem to be ridiculus
#Leu -14.1 -15.2
#Ile -20.6 -22.1
#Val -12.2 -13.8
#Phe -14.9 -12.8
#Ala -6.8 -8.4
#Trp -21.6 -4.9
#Met -10.5 -4.4
#Cys -6.6 -3.4
#Tyr -14.0 6.6
#Thr -4.2 13.9
#Ser -0.7 15.8
#Gln -8.9 20.2
#Lys -18.6 19.9
#Asn -6.5 23.9
#Glu -1.68 21.1
#Asp 1.6 31.0
#Arg -21.2 58.1
dat = [
[ -14.1, -15.2 ],
[ -20.6, -22.1 ],
[ -12.2, -13.8 ],
[ -14.9, -12.8 ],
[ -6.8, -8.4   ],
[ -21.6, -4.9  ],
[ -10.5, -4.4  ],
[ -6.6, -3.4   ],
[ -14.0, 6.6   ],
[ -4.2, 13.9   ],
[ -0.7, 15.8   ],
[ -8.9, 20.2   ],
[ -18.6, 19.9  ],
[ -6.5, 23.9   ],
[ -1.68, 21.1  ],
[ 1.6, 31.0    ],
[ -21.2, 58.1  ]
]
dat = np.array(dat)
dat = dat - dat[4,:]
dat = dat*0.238
name = ['Leu', 'Ile', 'Val', 'Phe', 'Ala', 'Trp', 'Met', 'Cys', 'Tyr', 'Thr', 'Ser', 'Gln', 'Lys', 'Asn', 'Glu', 'Asp', 'Arg']
for i in range(len(dat)):
	print name[i], dat[i]


