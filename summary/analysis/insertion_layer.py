import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')

from hydrodata import Hydrodata
import hydrodata
import pickle

import twbio.AminoAcid

import matplotlib.pyplot as plt
#from outlierdetect import detectoutliers
import numpy as np
import mean_profile



########################################################################
# check if mean profile can be used in calculating insertion free energy
def check_mean_profile():
	mean_profile_dict = mean_profile.read_mean_profile()
	for pdb in open('../../finished.list').readlines():
		pdb = pdb.strip()
		hddata = pickle.load(open('HD/'+pdb+'_hd.pickle','rb'))

		ownfe = 0
		for seqid in hddata.outfacing_list:
			res = twbio.AminoAcid.one_to_index( hddata.records[seqid]['resname'] )
			ownfe += hddata.records[seqid]['profile'][res]

		meanfe = 0
		for dep in range(-4,5):
			for seqid in hddata.depth_index_dict[dep]:
				res = twbio.AminoAcid.one_to_index( hddata.records[seqid]['resname'] )
				meanfe += mean_profile_dict[dep][res]

		print pdb, ownfe, meanfe

#check_mean_profile()
#sys.exit(0)
# mean profile does not work for this purpose
########################################################################


def single_lipid_depth_profile(hddata, outliers=[]):
	profile = { dep:np.zeros((20,)) for dep in range(-12,13) }
	# profile is from -12 to 12 with only -4 to 4 have non-zero values
	for i in range(-4,5):
		meanprofile = np.zeros((20,))
		resinum = 0
		# get avg TFE of non-outlier residues in i-th depth
		for res in hddata.depth_index_dict[i]:
			if res not in outliers:
				meanprofile +=  hddata.records[res]['profile']
				resinum += 1
		profile[i] = meanprofile / resinum
	return profile


def mean_lipid_depth_profile(mean_profile_dict):
	profile = { dep:np.zeros((20,)) for dep in range(-12,13) }
	# profile is from -12 to 12 with only -4 to 4 have non-zero values
	for dep in range(-4,5):
		profile[dep] = mean_profile_dict[dep]
	return profile


#a = mean_profile.read_mean_profile()
#a = mean_lipid_depth_profile(a)
#for dep in range(-12,13):
#	print len(a[dep])
#sys.exit(0)


def omp_layer_to_bilayer_depth_TFE(hddata, profile, ompi, lipidi):
	'''TFE that i-th layer of OMP inserts into i-th depth of bilayer
	'''
	tfe = 0
	for res in hddata.depth_index_dict[ompi]:
		if res in hddata.outfacing_list:
			aaid = twbio.AminoAcid.one_to_index(hddata.records[res]['resname'])
			tfe += profile[lipidi][aaid]
	return tfe


def omp_to_bilayer_depth(hddata, profile, lipidi, native_orientation = True):
	''' compute TFE that midplane inserts to i-th depth (from -8 to 8)
		native_orientation: omp orientation is native or "upside down"
		return values TFEs are always inserted from peri to extra however
		orientation of omp is

		return a (9,) array with 1st value is the energy of the upper most layer,
		so on so forth
	'''
	tfes = []
	if native_orientation:
		for ompi in range(4,-5,-1):
			tfes.append(omp_layer_to_bilayer_depth_TFE(hddata, profile, ompi, ompi+lipidi))
	else:
		for ompi in range(-4,5):
			tfes.append(omp_layer_to_bilayer_depth_TFE(hddata, profile, ompi, -ompi+lipidi))
	return np.array(tfes)


def compute_insertion_process_energies(code, remove_outliers = True, native_orientation = True, strand_avg = True, use_mean_profile = False):
	''' compute the energies of the insertion process from peri to extra
		
		return a (9,17) array. 9 layers of omp, 17 insertion position of omp into lipid
	'''
	hddata = pickle.load(open('HD/'+code+'_hd.pickle','rb'))
	outliers = []

	if remove_outliers:
		# TODO
		outliers = []

	if not use_mean_profile:
		profile = single_lipid_depth_profile(hddata, outliers)
	else:
		profile = mean_lipid_depth_profile( mean_profile.read_mean_profile() )

	process_tfes = []
	for dep in range(-8,9):
		process_tfes.append( omp_to_bilayer_depth(hddata, profile, dep, native_orientation) )

	process_tfes = np.array( process_tfes ).T
	if strand_avg:
		process_tfes = process_tfes/hddata.subunit_strand_num
	return process_tfes



def plot_insertion_energies(process_tfes, fn, native_orientation = True):
	''' plot insertion energy
		layer_wise: contribution of each layer
	'''

	plt.clf()
	if native_orientation:
		colors = ['#252525', '#ef3b2c', '#6a51a3', '#238b45']
		labels = ['whole', 'extra head', 'core', 'peri head']
		#labels = ['normed whole', 'extracellular', 'core', 'periplasmic']
	else:
		colors = ['#252525', '#238b45', '#6a51a3', '#ef3b2c']
		labels = ['whole', 'peri head', 'core', 'extra head']
		#labels = ['normed whole', 'periplasmic', 'core', 'extracellular']
	
	plt.plot(range(-8,9), np.sum(process_tfes, axis=0), color = colors[0], label=labels[0], linewidth=3)

	#plt.plot(range(-8,9), np.sum(process_tfes[0:1,:], axis=0), color = colors[1], label=labels[1], linestyle='--')
	#plt.plot(range(-8,9), np.sum(process_tfes[2:6,:], axis=0), color = colors[2], label=labels[2], linestyle='--')
	#plt.plot(range(-8,9), np.sum(process_tfes[7:8,:], axis=0), color = colors[3], label=labels[3], linestyle='--')
	plt.plot(range(-8,9), np.sum(process_tfes[0:2,:], axis=0), color = colors[1], label=labels[1], linestyle='--')
	plt.plot(range(-8,9), np.sum(process_tfes[2:7,:], axis=0), color = colors[2], label=labels[2], linestyle='--')
	plt.plot(range(-8,9), np.sum(process_tfes[7:9,:], axis=0), color = colors[3], label=labels[3], linestyle='--')
	plt.ylim([-4.5,1])



	plt.legend(loc=4)
	#plt.show()
	plt.savefig(fn+'.png')







if __name__ == '__main__':

	with open('../finished.list') as f:
		pdbs = f.readlines()
	pdbs.remove('1uun\n') # which is gram positive
	pdbs.remove('2fgr\n') # which is redundent
	pdbs.remove('4c4v\n') # which is redundent of bama
	pdbs.remove('4n75\n') # which is redundent of bama

	for i in range(len(pdbs)):
		pdbs[i] = pdbs[i].strip()

		#tfes = compute_insertion_process_energies(pdbs[i], native_orientation = True, use_mean_profile = False)
		#plot_insertion_energies(tfes,'insertion_no_spec_'+pdbs[i],native_orientation = True)

		#tfes = compute_insertion_process_energies(pdbs[i], native_orientation = False, use_mean_profile = False)
		#plot_insertion_energies(tfes,'insertion_nno_spec_'+pdbs[i],native_orientation = True)

		tfes = compute_insertion_process_energies(pdbs[i], native_orientation = True, use_mean_profile = True)
		plot_insertion_energies(tfes,'insertion_natori_mean_'+pdbs[i],native_orientation = True)

		tfes = compute_insertion_process_energies(pdbs[i], native_orientation = False, use_mean_profile = True)
		plot_insertion_energies(tfes,'insertion_nonnatori_mean_'+pdbs[i],native_orientation = False)

		#tfes = compute_insertion_process_energies(pdbs[i], native_orientation = False)

