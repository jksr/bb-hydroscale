clt = [ 2 , 3 , 4 , 5 , 6 , 7 ]
sil = [ 0.445160892129 , 0.412392997632 , 0.252741661085 , 0.234220898747 , 0.208420701761 , 0.195670956163 ]
import matplotlib.pyplot as plt

plt.figure(figsize=(4.5,6))
plt.plot(clt,sil,'-o',color='#4393c3')
plt.gca().tick_params(labelsize=18)
plt.xlim([1.5,7.5])
plt.ylabel('Silhouette score',fontsize=20)
plt.xlabel('Cluster num',fontsize=20)
plt.savefig('silhouette.svg')
