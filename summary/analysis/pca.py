import sys
sys.path.append('.')
import clustering
from hydrodata import *
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import mean_profile
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

lblue = '#4393c3'
lred = '#d6604d'
dblue = '#08306b'
dred = '#a50f15'
R = 10 # sphere radius that includes points belongs to the main cluster

if __name__ == '__main__':
	data_matrix, pdbs = clustering.compile_data()
	maskeddata = np.ma.array(data_matrix,mask=np.isnan(data_matrix))
	cov = np.ma.cov(maskeddata,rowvar=False,allow_masked=True)
	evals, evecs = sp.linalg.eigh(cov)
	idx = np.argsort(evals)[::-1]
	evecs = evecs[:,idx]
	evals = evals[idx]

	evecs_1d = evecs[:,:1]
	evecs_2d = evecs[:,:2]
	evecs_3d = evecs[:,:3]

	pcaed_data_1d = np.dot(data_matrix, evecs_1d)
	pcaed_data_2d = np.dot(data_matrix, evecs_2d)
	pcaed_data_3d = np.dot(data_matrix, evecs_3d)

	tmp = mean_profile.read_mean_profile()
	general = []
	for i in [ 10, 1, 11, 8, 9, 19, 3, 2, 15, 12, 13, 6, 5, 16, 18, 17, 7, 14, 4 ]:
		for dep in range(-4,5):
			general.append(tmp[dep][i])
	general = np.array(general)

	pcaed_general_1d = np.dot(general, evecs_1d)
	pcaed_general_2d = np.dot(general, evecs_2d)
	pcaed_general_3d = np.dot(general, evecs_3d)

	# 1d
	plt.close('all')
	fig = plt.figure(figsize=(20,2.4))
	ax = fig.add_subplot(111)

	ax.plot(pcaed_data_1d[:, 0], [0]*len(pcaed_data_1d), 'o', color = lblue)
	for i in range(len(pcaed_data_1d)):
		ax.text(pcaed_data_1d[:, 0][i]-0.1,1,pdbs[i],rotation=90)

	ax.plot(pcaed_general_1d, [0], 'o', color=lred)
	ax.text(pcaed_general_1d-0.1,-0.7,'general',rotation=90)

	ax.set_xlim([-17,-2])
	ax.set_ylim([-1.7,2])
	ax.set_yticks([])
	collection = PatchCollection( [mpatches.Ellipse((pcaed_general_1d[0],0), R, 0.7)], alpha=0.3,color=lred )
	ax.add_collection(collection)
	plt.tight_layout()
	plt.savefig('cluster/pca_1d.svg')
	plt.show()

	# 2d
	plt.close('all')
	fig = plt.figure(figsize=(10,10))
	#fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.plot(pcaed_data_2d[:, 0], pcaed_data_2d[:, 1], 'o', color=lblue)
	ax.plot([pcaed_general_2d[0]], [pcaed_general_2d[1]], 'o', color=lred)
	collection = PatchCollection( [mpatches.Circle((pcaed_general_2d[0],pcaed_general_2d[1]), R)], alpha=0.3, color=lred )
	ax.add_collection(collection)

	ax.set_xlim([-20,-1])
	ax.set_ylim([-18,1])
	ax.set_aspect(1)
	for i in range(len(pcaed_data_2d)):
		ax.text(pcaed_data_2d[:, 0][i]+0.1,pcaed_data_2d[:, 1][i]+0.1,pdbs[i])
	plt.tight_layout()
	plt.savefig('cluster/pca_2d.svg')
	plt.show()

	# 3d
	plt.close('all')
	fig = plt.figure(figsize=(6,6))
	ax = Axes3D(fig)
	xs = pcaed_data_3d[:,0]
	ys = pcaed_data_3d[:,1]
	zs = pcaed_data_3d[:,2]
	ax.scatter(xs, ys, zs, 'o', color=lblue) # make a scatter plot of blue dots from the data
	for i in range(len(xs)):
		if pdbs[i]=='7ahl' or pdbs[i]=='3b07':
			ax.text(xs[i],ys[i],zs[i]+0.1,pdbs[i], fontsize = 20)
		else:
			ax.text(xs[i],ys[i],zs[i]+0.1,pdbs[i], fontsize = 16)

	ax.scatter([pcaed_general_3d[0]], [pcaed_general_3d[1]], [pcaed_general_3d[2]], 'o', color=dred)
	ax.text(pcaed_general_3d[0], pcaed_general_3d[1], pcaed_general_3d[2]+0.1, 'general')

	# u and v are parametric variables. from 0 to 2*pi
	u=np.r_[0:2*np.pi:20j]
	v=np.r_[0:np.pi:20j]
	# x, y, and z are the coordinates of the points for plotting
	spherex=R*np.outer(np.cos(u),np.sin(v))
	spherey=R*np.outer(np.sin(u),np.sin(v))
	spherez=R*np.outer(np.ones(np.size(u)),np.cos(v))
	spherex=spherex+pcaed_general_3d[0]
	spherey=spherey+pcaed_general_3d[1]
	spherez=spherez+pcaed_general_3d[2]
	ax.plot_wireframe(spherex,spherey,spherez, color=lred, alpha=0.5)

	ax.set_xlim([-17,1])
	ax.set_ylim([-16,2])
	ax.set_zlim([-13,5])
	ax.tick_params(labelsize=16)
	ax.view_init(elev=23, azim=-145)
	plt.savefig('cluster/pca_3d.svg')



