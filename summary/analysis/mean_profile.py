## utils for computing mean profile from a bunch of omps (main cluster of omps)
# created by Wei Tian, Feb 16, 2015

import sys
sys.dont_write_bytecode = True

from hydrodata import *
import pickle

import numpy as np

# compute mean profile from a bunch of omps
def get_mean_profile(hd_list):
	mean_profile_dict = { dep:None for dep in range(-4,5) }
	mean_profile_std_dict = { dep:None for dep in range(-4,5) }
	for dep in range(-4,5):
		tmp = []
		for hd in hd_list:
			for res in hd.depth_index_dict[dep]:
				tmp.append(hd.records[res]['profile'])
		mean_profile_dict[dep] = np.mean(tmp, axis = 0)
		mean_profile_std_dict[dep] = np.std(tmp, axis = 0)
	return mean_profile_dict, mean_profile_std_dict


# write the mean profile to a file
def write_mean_profile(mean_profile_dict, fn):
	with open(fn, 'w') as f:
		for dep in range(-4,5):
			f.write(str(mean_profile_dict[dep][0]))
			for res in range(1,20):
				f.write(' '+str(mean_profile_dict[dep][res]))
			f.write('\n')


# read the mean profile from a file
def read_mean_profile(fn='HD/gram_negative_mean_profile.txt'):
	mtx = np.loadtxt(fn).astype(float)
	mean_profile_dict = { dep:[] for dep in range(-4,5) }
	for dep in range(-4,5):
		for res in range(20):
			mean_profile_dict[dep].append(mtx[dep+4][res])
	return mean_profile_dict


if __name__ == '__main__':
	pdbs = open('../finished.list').readlines()
	pdbs.remove('1uun\n') # which is gram positive
	pdbs.remove('2fgr\n') # which is redundent
	pdbs.remove('4c4v\n') # which is redundent of bama
	pdbs.remove('4n75\n') # which is redundent of bama
	#pdbs.remove('3b07\n') # toxin not same cluster
	#pdbs.remove('3o44\n') # toxin not same cluster
	#pdbs.remove('7ahl\n') # toxin not same cluster
	hds = [ pickle.load(open('HD/'+pdb.strip()+'_hd.pickle','rb')) for pdb in pdbs ]
	mean_profile_dict,mean_profile_std_dict = get_mean_profile(hds)
	write_mean_profile(mean_profile_dict, 'HD/gram_negative_mean_profile.txt')
	write_mean_profile(mean_profile_std_dict, 'HD/gram_negative_mean_profile_std.txt')
	print 'HD/gram_negative_mean_profile.txt written'

