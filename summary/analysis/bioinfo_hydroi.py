import glob
import os
import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')

import numpy as np
import matplotlib.pyplot as plt
from hydrodata import *
import pickle
#from outlierdetect import *

import twbio.AminoAcid






##############################################################
def depthdepend(dicts, code):
	fig = plt.figure()
	fig.set_size_inches(15,12)
	mainax = fig.add_subplot(111) # the big subplot
	#mainax.set_xlabel('\n'+r'Position index ($i$)'+'\n'+code, fontsize=18)
	mainax.set_ylabel(r'$\mathbf{\Delta\Delta G^i}$(kcal/mol)'+'\n', fontsize=18)
	# Turn off axis lines and ticks of the big subplot
	mainax.patch.set_visible(False)
	mainax.spines['top'].set_visible(False)
	mainax.spines['bottom'].set_visible(False)
	mainax.spines['left'].set_visible(False)
	mainax.spines['right'].set_visible(False)
	mainax.set_xticklabels([])
	mainax.set_yticklabels([])
 
	for idx in range(-4,5):
		energy = []
		std = []
		aa = [ 'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V' ]
		for i in range(20):
			energy.append(np.mean(dicts[i][idx]))
			std.append(np.std(dicts[i][idx]))

		mean_std_aa = sorted(zip(energy, std, aa))
		for i in range(20):
			energy[i] = mean_std_aa[i][0]
			std[i] = mean_std_aa[i][1]
			aa[i] = mean_std_aa[i][2]

		ind = np.arange(20)
		barwidth = 0.8
		curax = fig.add_subplot(3,3,idx+5)
		if idx<0:
			clr = '#91cf60'
		elif idx>0:
			clr = '#fc8d59'
		else:
			clr = '#ffffbf'
		curax.bar(ind, energy, barwidth, color=clr, yerr=std, error_kw=dict(ecolor='k'))
		curax.set_title('Position '+str(idx))
		curax.set_xticks(ind+barwidth/2)
		curax.set_xticklabels( aa )
		curax.set_ylim(-3,6)
		curax.set_xlim(0,19+barwidth)

	plt.tight_layout(pad=0.2,w_pad = 0.4, h_pad = 0.4)
	plt.savefig(code+'_DepthDepend.png')
	plt.show()
	sys.exit(0)
##############################################################


##############################################################
##############################################################
##############################################################
##############################################################
##############################################################
##############################################################
##############################################################
##############################################################


##############################################################
def plot_hydroi(dicts, code, fitted = None, err=True, fit_color='#de2d26'):
	# plot depth dependent transfer energy
	figname = code+'_hydroi.eps'
	if os.path.exists(figname):
		return

	plotorder = [ 0, 10, 1, 11, 8, 9, 19, 3, 2, 15, 12, 13, 6, 5, 16, 18, 17, 7, 14, 4 ]
	fig = plt.figure()
	fig.set_size_inches(10,8)
	mainax = fig.add_subplot(111) # the big subplot
	mainax.set_xlabel('\n'+r'Position index ($i$)'+'\n'+code, fontsize=12)
	mainax.set_ylabel(r'$\mathbf{\Delta\Delta G_{aa(i)}}$(kcal/mol)'+'\n', fontsize=12)
	# Turn off axis lines and ticks of the big subplot
	mainax.patch.set_visible(False)
	mainax.spines['top'].set_visible(False)
	mainax.spines['bottom'].set_visible(False)
	mainax.spines['left'].set_visible(False)
	mainax.spines['right'].set_visible(False)
	mainax.set_xticklabels([])
	mainax.set_yticklabels([])
 
	for iprox in range(20):
		i = plotorder[iprox]
		resmean = np.empty(9)
		resstd = np.empty(9)
		for j in range(-4,5):
			resmean[j+4] = np.mean(np.array(dicts[i][j]))
			resstd[j+4] = np.std(np.array(dicts[i][j]))
		curax = fig.add_subplot(4,5,iprox+1)
		if err:
			curax.errorbar(range(-4,5), resmean, yerr=resstd, fmt='o', color='#2171b5', ecolor='#2171b5')
		else:
			curax.plot(range(-4,5), resmean, 'o')

		# plot outliers
		#for j in range(-4,5):
		#	for k in outdicts[i][j]:
		#		curax.plot([j],[k],'+')
		curax.axhline(0, linestyle='-.', color = '#808080')
		curax.set_title(twbio.AminoAcid.index_to_three(i).title(), fontsize=12)
		curax.set_ylim(-3.5,7)
		curax.set_xlim(-4.5,4.5)

		if not fitted == None:
			curax.plot(range(-4,5),fitted[i],linewidth = 3, color = fit_color)


	plt.tight_layout(pad=0.2,w_pad = 0.4, h_pad = 0.4)
	plt.savefig(figname)
	plt.clf()
##############################################################



##############################################################
def get_hydroi_data(pdb, outliers=[]):
	#hydroi_data[res id]= { position:list_of_vals }
	hydroi_data = {}
	for res_id in range(20):
		hydroi_data[res_id] = {}
		for depthidx in range(-4,5):
			hydroi_data[res_id][depthidx]=[]
	#outliers = detectoutliers(pdb)

	data = pickle.load(open('HD/'+pdb+'_hd.pickle','rb'))
	for depthidx in range(-4,5):
		for seqid in data.depth_index_dict[depthidx]:
			if seqid not in outliers:
				for res_id in range(20):
					hydroi_data[res_id][depthidx].append(data.records[seqid]['profile'][res_id])
	return hydroi_data
##############################################################


##############################################################
def compile_hydroi_data(pdblist):
	# hydroi_data[res_id][dep_id] = list_of_vals
	hydroi_data = {}
	for res_id in range(20):
		hydroi_data[res_id] = {}
		for depthidx in range(-4,5):
			hydroi_data[res_id][depthidx]=[]

	for i in range(len(pdblist)):
		data = get_hydroi_data(pdblist[i])
		for res_id in range(20):
			for depthidx in range(-4,5):
				hydroi_data[res_id][depthidx] = hydroi_data[res_id][depthidx] + data[res_id][depthidx]
	
	return hydroi_data
##############################################################


##############################################################
def fit_hydroi_data(hydroi_data):
	#prepare data to be fitted
	dat_to_fit = {}
	for res_id in range(20):
		depth_ddG_list = []
		for depthidx in range(-4,5):
			#mean_ddG = np.mean(hydroi_data[res_id][depthidx])
			#depth_ddG_list.append([depthidx,mean_ddG])
			for ddG in hydroi_data[res_id][depthidx]:
				depth_ddG_list.append([depthidx,ddG])
		dat_to_fit[res_id] = np.array(depth_ddG_list)
	
	#fitted data
	fitted = {}
	for res_id in range(20):
		coeff = np.polyfit(dat_to_fit[res_id][:,0], dat_to_fit[res_id][:,1],3)
		fitted[res_id] = np.polyval(coeff,range(-4,5))
	
	return fitted
##############################################################

import mean_profile

if __name__ == '__main__':
	codes = open('../finished.list').readlines()
	for i in range(len(codes)):
		codes[i] = codes[i].strip()
	codes = ['4q35']

	tmp = mean_profile.read_mean_profile()
	mean_prof = { res:[] for res in range(20) }
	for res in range(20):
		for dep in range(-4,5):
			mean_prof[res].append( tmp[dep][res] )
	for code in codes:
		print code
		hydroi_data = get_hydroi_data(code)
		## plot against nothing
		#plot_hydroi(hydroi_data, code)
		## plot against mean profile
		plot_hydroi(hydroi_data, code, fitted = None)
		## plot against fitted profile of self
		#plot_hydroi(hydroi_data, code, fitted = fit_hydroi_data(hydroi_data),err=True, fit_color='#2171b5')
	
	##############################################

	#totaldicts = compile_hydroi_data(codes)
	#plot_hydroi(totaldicts, "out", err = True)
	#depthdepend(totaldicts, "ALL")


