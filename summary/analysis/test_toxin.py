import sys
sys.dont_write_bytecode = True
from hydrodata import *
import pickle
import numpy as np
import mean_profile
import random



pdbs = open('../finished.list').readlines()
pdbs.remove('1uun\n') # which is gram positive
pdbs.remove('2fgr\n') # which is redundent
pdbs.remove('4c4v\n') # which is redundent of bama
pdbs.remove('4n75\n') # which is redundent of bama
# test 7ahl 3b07, so exclude it
pdbs.remove('7ahl\n')
pdbs.remove('3b07\n')
#pdbs.remove('4q35\n')

## get 7ahl profile
hd = pickle.load(open('HD/7ahl_hd.pickle','rb'))
hpres = hd.strands[0]+hd.strands[1]
ahl_profile = []
for dep in range(-4,5):
	for res in hpres:
		if hd.records[res]['depidx']==dep:
			ahl_profile.append(hd.records[res]['profile'])
			break
ahl_profile = np.hstack(ahl_profile)

## get 3b07 profile
hd = pickle.load(open('HD/3b07_hd.pickle','rb'))
hpres = hd.strands[0]+hd.strands[1]
b07_profile = []
for dep in range(-4,5):
	for res in hpres:
		if hd.records[res]['depidx']==dep:
			b07_profile.append(hd.records[res]['profile'])
			break
b07_profile = np.hstack(b07_profile)




## construct hairpin profile pool
hd_list = [ pickle.load(open('HD/'+pdb.strip()+'_hd.pickle','rb')) for pdb in pdbs ]
hairpin_pool = []
for hd in hd_list:
	if hd.subunit_strand_num==2:
		eff_hairpin_num = hd.subunit_strand_num-1
	else:
		eff_hairpin_num = hd.subunit_strand_num
	for i in range(eff_hairpin_num):
		hpres = hd.strands[i]+hd.strands[(i+1)%hd.subunit_strand_num]
		if len(hpres)==9:
			hairpin_profile = []
			for dep in range(-4,5):
				for res in hpres:
					if hd.records[res]['depidx']==dep:
						hairpin_profile.append(hd.records[res]['profile'])
						break
			hairpin_profile = np.hstack(hairpin_profile)
			hairpin_pool.append(hairpin_profile)


## get mean profile
mean_dict = mean_profile. read_mean_profile(fn='HD/gram_negative_mean_profile.txt')
mean_profile = []
for dep in range(-4,5):
	mean_profile.append(mean_dict[dep])
mean_profile=np.hstack(mean_profile)


## 7ahl dist
ahl_dis = np.linalg.norm(ahl_profile-mean_profile)

## 3b07 dist
b07_dis = np.linalg.norm(b07_profile-mean_profile)

## resample distance
dis = []
for i in range(100000):
	dis.append( np.linalg.norm(random.choice(hairpin_pool)-mean_profile) )


import matplotlib.pyplot as plt
import scipy.stats as ss


fig = plt.figure(figsize=(4.5,6))
ax = fig.add_subplot(111)
ax.tick_params(labelsize=18)
plt.hist(dis,30,histtype='stepfilled',color='#4393c3',normed=True)
for pdb,tox_dis,os,clr in [('7ahl',ahl_dis,0,'#d6604d'),('3b07',b07_dis,0.02,'#006d2c')]:
	plt.axvline(tox_dis+os*3, color=clr,linewidth=1.2)
	plt.text(tox_dis+0.5, 0.15-os, pdb+" %.2f"%(ss.percentileofscore(dis,tox_dis)*0.01), color=clr, fontsize=20)
plt.xlabel('Euclidean distance', fontsize=20)
plt.ylabel('Frequency', fontsize=20)
#plt.savefig('cluster/tox_.eps')
plt.savefig('cluster/tox.svg')
#plt.show()



