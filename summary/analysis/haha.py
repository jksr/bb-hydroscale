import numpy as np
import glob

pdbs = np.loadtxt('../finished.list',dtype='str').tolist()

cens = []
aroundcens = []
for pdb in pdbs:
	cenids = np.loadtxt('../../inputs/'+pdb+'/'+pdb+'.cen.topo').astype(int)
	cenids = cenids[cenids[:,1]==2][:,0]
	aroundcenids = (cenids-2).tolist() + (cenids+2).tolist()

	dat = np.loadtxt('../../inputs/'+pdb+'/'+pdb+'.sidechain',dtype='str')
	dat = dat[dat[:,3]=='OUT']

	for i in range(len(dat)):
		if int(dat[i][0]) in cenids:
			cens.append(dat[i])
		if int(dat[i][0]) in aroundcenids:
			aroundcens.append(dat[i])
cens = np.array(cens)
aroundcens = np.array(aroundcens)

print len(cens), sum(cens[:,2]=='H'), sum(cens[:,2]=='R'), sum(cens[:,2]=='K')
print len(aroundcens), sum(aroundcens[:,2]=='H'), sum(aroundcens[:,2]=='R'), sum(aroundcens[:,2]=='K')
print cens[:,2]
