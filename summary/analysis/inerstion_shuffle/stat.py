import numpy as np
import sys

dat = np.loadtxt(sys.argv[1],dtype='str')
idx = dat[:,1]=='o'
print 'not step 0:', sum( dat[idx][:,3].astype(float) ) / ( len(dat[idx])*2000 )
print 'larger e:', sum(dat[idx][:,4].astype(float)>0)
print 'e mean,std:', np.mean( dat[idx][:,4].astype(float) ), np.std( dat[idx][:,4].astype(float) )
