## output
## pdb
## shuffled_minE_depth, shuffled_minE, 0, zero_depth_E

import sys
sys.dont_write_bytecode = True
sys.path.append('../../../../pylib')
sys.path.append('../')

#from hydrodata import Hydrodata
#import pickle

import twbio.AminoAcid
from twbio.dsspdata import DSSPData
import Bio.PDB

import matplotlib.pyplot as plt
import numpy as np
import mean_profile
import random


def shuffle_out(resi_out_dict, count_out_dict):
	allresis = []
	for resis in resi_out_dict.values():
		allresis+= resis
	random.shuffle(allresis)
	new_resi_out_dict = {dep:[] for dep in range(-4,5)}

	nowpos = 0
	for dep in range(-4,5):
		num = count_out_dict[dep]
		new_resi_out_dict[dep] = allresis[nowpos:nowpos+num]
		nowpos+=num
	return new_resi_out_dict


def shuffle_all(resi_out_dict, resi_in_dict, count_out_dict):
	allresis = []
	for resis in resi_out_dict.values():
		allresis+= resis
	for resis in resi_in_dict.values():
		allresis+= resis
	random.shuffle(allresis)
	new_resi_out_dict = {dep:[] for dep in range(-4,5)}

	nowpos = 0
	for dep in range(-4,5):
		num = count_out_dict[dep]
		new_resi_out_dict[dep] = allresis[nowpos:nowpos+num]
		nowpos+=num
	return new_resi_out_dict


def omp_layer_to_bilayer_depth_TFE(resi_out_dict, profile, ompi, lipidi):
	'''TFE that i-th layer of OMP inserts into i-th depth of bilayer
	'''
	tfe = 0
	for resi in resi_out_dict[ompi]:
		aaid = twbio.AminoAcid.one_to_index(resi)
		tfe += profile[lipidi][aaid]
	return tfe


def omp_to_bilayer_depth(resi_out_dict, profile, lipidi, native_orientation = True):
	''' compute TFE that midplane inserts to i-th depth (from -8 to 8)
		native_orientation: omp orientation is native or "upside down"
		return values TFEs are always inserted from peri to extra however
		orientation of omp is

		return a (9,) array with 1st value is the energy of the upper most layer,
		so on so forth
	'''
	tfes = []
	if native_orientation:
		for ompi in range(4,-5,-1):
			tfes.append(omp_layer_to_bilayer_depth_TFE(resi_out_dict, profile, ompi, ompi+lipidi))
	else:
		for ompi in range(-4,5):
			tfes.append(omp_layer_to_bilayer_depth_TFE(resi_out_dict, profile, ompi, -ompi+lipidi))
	return np.array(tfes)


def mean_lipid_depth_profile(mean_profile_dict):
	profile = { dep:np.zeros((20,)) for dep in range(-12,13) }
	# profile is from -12 to 12 with only -4 to 4 have non-zero values
	for dep in range(-4,5):
		profile[dep] = mean_profile_dict[dep]
	return profile


def compute_insertion_process_energies(resi_out_dict, native_orientation = True, strand_num = None, sym_mean_profile=False):
	''' compute the energies of the insertion process from peri to extra
		
		return a (9,17) array. 9 layers of omp, 17 insertion position of omp into lipid
	'''
	
	if sym_mean_profile:
		profile = mean_lipid_depth_profile( mean_profile.read_mean_profile('../HD/sym_gram_negative_mean_profile.txt') )
	else:
		profile = mean_lipid_depth_profile( mean_profile.read_mean_profile('../HD/gram_negative_mean_profile.txt') )
	#if sym_mean_profile:#will TODO
	#	profile = mean_lipid_depth_profile( mean_profile.read_mean_profile('no_tyrtrphis.txt') )
	#else:
	#	profile = mean_lipid_depth_profile( mean_profile.read_mean_profile('no_s_tyrtrphis.txt') )

	process_tfes = []
	for dep in range(-8,9):
		process_tfes.append( omp_to_bilayer_depth(resi_out_dict, profile, dep, native_orientation) )

	process_tfes = np.array( process_tfes ).T
	if strand_num is not None:
		process_tfes = process_tfes/strand_num
	return process_tfes


def plot_insertion_energies(process_tfes, fn, native_orientation = True):
	''' plot insertion energy
		layer_wise: contribution of each layer
	'''

	plt.clf()
	if native_orientation:
		colors = ['#252525', '#ef3b2c', '#6a51a3', '#238b45']
		labels = ['whole', 'extra head', 'core', 'peri head']
		#labels = ['normed whole', 'extracellular', 'core', 'periplasmic']
	else:
		colors = ['#252525', '#238b45', '#6a51a3', '#ef3b2c']
		labels = ['whole', 'peri head', 'core', 'extra head']
		#labels = ['normed whole', 'periplasmic', 'core', 'extracellular']
	
	plt.plot(range(-8,9), np.sum(process_tfes, axis=0), color = colors[0], label=labels[0], linewidth=3)

	#plt.plot(range(-8,9), np.sum(process_tfes[0:1,:], axis=0), color = colors[1], label=labels[1], linestyle='--')
	#plt.plot(range(-8,9), np.sum(process_tfes[2:6,:], axis=0), color = colors[2], label=labels[2], linestyle='--')
	#plt.plot(range(-8,9), np.sum(process_tfes[7:8,:], axis=0), color = colors[3], label=labels[3], linestyle='--')
	plt.plot(range(-8,9), np.sum(process_tfes[0:2,:], axis=0), color = colors[1], label=labels[1], linestyle='--')
	plt.plot(range(-8,9), np.sum(process_tfes[2:7,:], axis=0), color = colors[2], label=labels[2], linestyle='--')
	plt.plot(range(-8,9), np.sum(process_tfes[7:9,:], axis=0), color = colors[3], label=labels[3], linestyle='--')
	plt.ylim([-4.5,1])
	plt.xlabel('Depth')
	plt.ylabel(r'$\Delta\Delta G$(kcal/mol)')


	plt.legend(loc=4)
	#plt.show()
	plt.savefig(fn+'.png')





if __name__ == '__main__':

	datadict = { '1a0s':'P', '1bxw':'A', '1e54':'A', '1ek9':'A', '1fep':'A', '1i78':'A', '1k24':'A', '1kmo':'A', '1nqe':'A', '1p4t':'A', '1prn':'A', '1qd6':'C', '1qj8':'A', '1t16':'A', '1thq':'A', '1tly':'A', '1uyn':'X', '1xkw':'A', '1yc9':'A', '2erv':'A', '2f1c':'A', '2f1t':'A', '2fcp':'A', '2gr8':'A', '2lhf':'A', '2lme':'A', '2mlh':'A', '2mpr':'A', '2o4v':'A', '2omf':'A', '2por':'A', '2qdz':'A', '2vqi':'A', '2wjr':'A', '2ynk':'A', '3aeh':'A', '3bs0':'A', '3csl':'A', '3dwo':'X', '3dzm':'A', '3fid':'A', '3kvn':'A', '3pik':'A', '3rbh':'A', '3rfz':'B', '3syb':'A', '3szv':'A', '3v8x':'A', '3vzt':'A', '4c00':'A', '4e1s':'A', '4gey':'A', '4pr7':'A', '4q35':'A', '4k3c':'A', '7ahl':'A', '3b07':'A', '3o44':'A' }

	structure_dict = {}
	tot_resi_in_dict = {dep:[] for dep in range(-4,5)}
	tot_resi_out_dict = {dep:[] for dep in range(-4,5)}

	for pdb in datadict.keys():
		print pdb
		chain = datadict[pdb]
		dssp = DSSPData('../../../inputs/'+pdb+'/'+pdb+'.dssp')

		resi_in_dict={dep:[] for dep in range(-4,5)}
		resi_out_dict={dep:[] for dep in range(-4,5)}
		count_in_dict={dep:0 for dep in range(-4,5)}
		count_out_dict={dep:0 for dep in range(-4,5)}

		centopos = np.loadtxt('../../../inputs/'+pdb+'/'+pdb+'.cen.topo').astype(int)
		strand_num = len(centopos)

		for i in range(strand_num):
			cen,topo = centopos[i]
			if i%2==0:
				d = 1
			else:
				d = -1

			if topo == 2:
				for dep in [-4,-2,0,2,4]:
					tmpseqid = cen + dep*d
					try:
						resi = dssp.get(chain,tmpseqid,'aa')
						if resi == 'X':
							resi = 'M'
						resi_out_dict[dep].append(resi)
						count_out_dict[dep]+=1
					except:
						pass
				for dep in [-3,-1,1,3]:
					tmpseqid = cen + dep*d
					try:
						resi = dssp.get(chain,tmpseqid,'aa')
						if resi == 'X':
							resi = 'M'
						resi_in_dict[dep]. append(resi)
						count_in_dict[dep]+=1
					except:
						pass
			else:
				for dep in [-4,-2,0,2,4]:
					tmpseqid = cen + dep*d
					try:
						resi = dssp.get(chain,tmpseqid,'aa')
						if resi == 'X':
							resi = 'M'
						resi_in_dict[dep]. append(resi)
						count_in_dict[dep]+=1
					except:
						pass
				for dep in [-3,-1,1,3]:
					tmpseqid = cen + dep*d
					try:
						resi = dssp.get(chain,tmpseqid,'aa')
						if resi == 'X':
							resi = 'M'
						resi_out_dict[dep].append(resi)
						count_out_dict[dep]+=1
					except:
						pass

		native_orientation = True
		sym_mean_profile = False
		if pdb in ['3b07','7ahl','3o44']:
			sym_mean_profile = True
		# insertion energy plot for wt
		tfes = compute_insertion_process_energies(resi_out_dict, native_orientation, strand_num, sym_mean_profile)

		tfesum = np.sum(tfes, axis=0)
		zeroe = tfesum[8]
		tfesum = sorted(zip(tfesum, range(-8,9)))
		print '      wt', tfesum[0][1], tfesum[0][0]*strand_num, 0, zeroe*strand_num
		wtmine = tfesum[0][0]
		wtminpos = tfesum[0][1]

		plot_insertion_energies(tfes, pdb+'_wt')
		continue#will TODO

		# insertion energy plot for shuffle
		#for tmp in range(0,2000):
		for tmp in range(0,1):
			new_resi_out_dict = shuffle_out(resi_out_dict, count_out_dict)
			#new_resi_out_dict = shuffle_all(resi_out_dict, resi_in_dict, count_out_dict)
			tfes = compute_insertion_process_energies(new_resi_out_dict, native_orientation, strand_num, sym_mean_profile)

			tfesum = np.sum(tfes, axis=0)
			zeroe = tfesum[8]
			tfesum = sorted(zip(tfesum, range(-8,9)))
			print 'shuffled', tfesum[0][1], tfesum[0][0]*strand_num, 0, zeroe*strand_num
			#if tfesum[0][1]!=0: 
			#	plot_insertion_energies(tfes, pdb+'_suffled_'+str(tmp).zfill(2))
			#plot_insertion_energies(tfes, pdb+'_suffled_'+str(tmp).zfill(2))

		print '-'*40


		# prepare tot
		#structure_dict[pdb]=(count_out_dict)
		#for dep in range(-4,5):
		#	tot_resi_in_dict[dep] += resi_in_dict[dep]
		#	tot_resi_out_dict[dep] += resi_out_dict[dep]


