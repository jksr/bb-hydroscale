## output
# pdb  if_0_is_minE_depth  wt_minE  

import sys
import numpy as np

with open('minenergy4.txt') as f:
	lines = f.readlines()

pdbnum = 58
shuffles = 50

for i in range(pdbnum):
	pdb = lines[i*(shuffles+3)].strip()
	sys.stdout.write(pdb)
	wtsplit = lines[i*(shuffles+3)+1].split()
	if wtsplit[1]!='0':
		sys.stdout.write(' x ')
	else:
		sys.stdout.write(' o ')
	wtmin = float(wtsplit[2])
	sys.stdout.write('%.2f'%wtmin+' ')

	# min miss 0 depth
	count_mis = 0
	shufflemins = []
	for j in range(2,(shuffles+3)-1):
		shufflesplit = lines[i*(shuffles+3)+j].split()
		shuffle_mindepth = shufflesplit[1]
		if shuffle_mindepth!='0':
			count_mis+=1
		else:
			shufflemins.append(shufflesplit[2])
	shufflemins = np.array(shufflemins).astype(float)
	shufflemins -= wtmin
	shufflemins_mean = np.mean(shufflemins)
	shufflemins_std = np.std(shufflemins)
	sys.stdout.write( str(count_mis).zfill(2) +' '+ '%.2f'%(shufflemins_mean)+'\n')
