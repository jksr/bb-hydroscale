import numpy as np
import matplotlib.pyplot as plt
import pickle
import glob
import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')

import twbio.AminoAcid
from hydrodata import *
from scipy.optimize import curve_fit

##############################################################
def get_hydroz_data(pdb,outliers=[]):
	# { res: [ col0, col1 ] }, col0 z coords, col1 ddG
	hydroz_data = {i:[] for i in range(20)}

	data = pickle.load(open('HD/'+pdb+'_hd.pickle','rb'))

	for res in data.outfacing_list:
		try:
			if res not in outliers:
				relz = data.records[res]['zcoord']/(24.11/2)
				#relz = data.records[res]['zcoord']/(data.membrane_thickness/2)
				for j in range(20):
					hydroz_data[j].append( ( relz, data.records[res]['profile'][j] ) )
					#hydroz_data[j].append( np.array( [ relz, data.records[res]['profile'][j] ]) )
		except:
			pass
	return hydroz_data
##############################################################



##############################################################
def compile_hydroz_data(pdblist):
	hydroz_data = {i:[] for i in range(20)}
	for pdb in pdblist:
		prt_aa_recs = get_hydroz_data(pdb)
		for i in range(20):
			hydroz_data[i] = hydroz_data[i] + prt_aa_recs[i]
	for i in range(20):
		hydroz_data[i] = np.array(sorted(hydroz_data[i]))

	return hydroz_data

##############################################################



##############################################################
def fit_hydroz_data(hydroz_data):
	fitted = {i:[] for i in range(20)}
	coef = {i:[] for i in range(20)}
	for i in range(20):
		coef[i] = np.polyfit(hydroz_data[i][:,0], hydroz_data[i][:,1],3)
		fitted[i] = np.polyval(coef[i], hydroz_data[i][:,0])
	return fitted, coef
##############################################################



##############################################################
def plot_hydroz(hydroz_data, figname, fitted_coef = None):

	plotorder = [ 0, 10, 1, 11, 8, 9, 19, 3, 2, 15, 12, 13, 6, 5, 16, 18, 17, 7, 14, 4 ]
	fig = plt.figure()
	fig.set_size_inches(15,12)
	mainax = fig.add_subplot(111) # the big subplot
	mainax.set_xlabel('\nZ position\n'+figname, fontsize=18)
	mainax.set_ylabel(r'$\mathbf{\Delta\Delta G_{aa}}$(kcal/mol)'+'\n', fontsize=18)
	# Turn off axis lines and ticks of the big subplot
	mainax.patch.set_visible(False)
	mainax.spines['top'].set_visible(False)
	mainax.spines['bottom'].set_visible(False)
	mainax.spines['left'].set_visible(False)
	mainax.spines['right'].set_visible(False)
	mainax.set_xticklabels([])
	mainax.set_yticklabels([])
 
	for iprox in range(20):
		i = plotorder[iprox]
		curax = fig.add_subplot(4,5,iprox+1)
		curax.scatter(hydroz_data[i][:,0], hydroz_data[i][:,1], color='#2171b5')

		if not fitted_coef == None:
			curax.plot(hydroz_data[i][:,0],np.polyval(fitted_coef[i], hydroz_data[i][:,0]),linewidth = 3, color = '#de2d26')

		curax.axhline(0, linestyle='-.', color = '#808080')
		curax.axhline(4, linestyle='-.', color = '#808080')
		curax.axhline(-2.5, linestyle='-.', color = '#808080')
		curax.set_title(twbio.AminoAcid.index_to_three(i).title())
		curax.set_ylim(-3.5,7)
		curax.set_xlim(-1.1,1.1)
	plt.tight_layout(pad=0.2,w_pad = 0.4, h_pad = 0.4)
	plt.savefig(figname+'_hydroz.png')
	plt.close()
##############################################################


# different size
list_8 = ['1bxw', '1p4t', '1qj8', '1thq', '2erv', '2f1t', '2lhf', '2mlh', '3dzm']
list_10 = ['1k24', '1i78']
list_12 = ['1qd6', '1uyn', '1tly', '2wjr', '3aeh', '3fid', '3kvn', '4e1s', '4pr7']
list_12_ = ['1ek9', '1yc9', '2lme', '3pik', '2gr8' ]
list_14 = ['1t16', '2f1c', '3bs0', '3dwo']
list_16 = ['1e54', '1prn', '1uun', '2fgr','2o4v', '2omf', '2por', '2qdz', '3vzt', '4c00', '4gey', '4k3c']
list_18 = ['1a0s', '2mpr', '2ynk', '3rbh', '3syb', '3szv']
list_22 = ['1fep', '1nqe', '1kmo', '1xkw', '2fcp', '3csl', '3v8x']
list_24 = ['2vqi', '3rfz']


# different organsim
list_STyphimurium = [ '1a0s', '2mpr', '3fid' ]
list_PPutida = [ '3bs0', '4gey' ]
list_PAeruginosa = [ '2erv', '1xkw', '2lhf', '2o4v', '3dwo', '3kvn', '3rbh', '3syb', '3szv' ]
list_NMeningitidis = [ '1k24', '1p4t', '1uyn', '3v8x', '3vzt' ]
list_EColi = [ '1bxw', '1ek9', '1fep', '1i78', '1kmo', '1nqe', '1qd6', '1qj8', '1t16', '1thq', '1tly', '2f1c', '2f1t', '2fcp', '2omf', '2vqi', '2wjr', '2ynk', '3aeh', '3pik', '4c00', '4e1s', '3rfz']
list_Others = [ '1e54', '1uun', '1yc9', '2fgr', '2gr8', '2lme', '2mlh', '1prn', '2por', '2qdz', '3csl', '3dzm', '4k3c', '4pr7' ]


# different assembly state
list_11 = [ '1bxw', '1fep', '1i78', '1k24', '1kmo', '1nqe', '1p4t', '1qd6', '1qj8', '1t16', '1thq', '1tly', '1uyn', '1xkw', '2erv', '2f1c', '2f1t', '2fcp', '2lhf', '2mlh', '2qdz', '2vqi', '2wjr', '2ynk', '3aeh', '3bs0', '3csl', '3dwo', '3dzm', '3fid', '3kvn', '3rbh', '3syb', '3szv', '3v8x', '4c00', '4e1s', '4gey', '4k3c', '4pr7', '3rfz' ]
list_31 = [ '1ek9', '1yc9', '2gr8', '2lme', '3pik' ] 
list_33 = [ '1a0s', '1e54', '1prn', '2fgr', '2mpr', '2o4v', '2omf', '2por', '3vzt' ]
list_81 = [ '1uun' ]


list_all = list_8+list_10+list_12+list_12_+list_14+list_16+list_18+list_22+list_24
	


if __name__ == '__main__':

	#############################################################
	#############################################################
	### this part is the basic hydroz plot
#	plot_hydroz(compile_hydroz_data(list_8),'siz_8_strands')
#	plot_hydroz(compile_hydroz_data(list_10),'siz_10_strands')
#	plot_hydroz(compile_hydroz_data(list_12),'siz_12_strands')
#	plot_hydroz(compile_hydroz_data(list_14),'siz_14_strands')
#	plot_hydroz(compile_hydroz_data(list_16),'siz_16_strands')
#	plot_hydroz(compile_hydroz_data(list_18),'siz_18_strands')
#	plot_hydroz(compile_hydroz_data(list_22),'siz_22_strands')
#	plot_hydroz(compile_hydroz_data(list_24),'siz_24_strands')
#	#plot_hydroz(compile_hydroz_data(list_all),'All')

#	plot_hydroz(compile_hydroz_data(list_STyphimurium), 'tax_S.Typhimurium')
#	plot_hydroz(compile_hydroz_data(list_PPutida), 'tax_P.Putida')
#	plot_hydroz(compile_hydroz_data(list_PAeruginosa), 'tax_P.Aeruginosa')
#	plot_hydroz(compile_hydroz_data(list_NMeningitidis), 'tax_N.Meningitidis')
#	plot_hydroz(compile_hydroz_data(list_EColi), 'tax_E.Coli')
#	plot_hydroz(compile_hydroz_data(list_Others), 'tax_Others')

#	plot_hydroz(compile_hydroz_data(list_11),'fol_11')
#	plot_hydroz(compile_hydroz_data(list_31),'fol_31')
#	plot_hydroz(compile_hydroz_data(list_33),'fol_33')
#	plot_hydroz(compile_hydroz_data(list_81),'fol_81')

	
	list_all = open('../finished.list').readlines()
	for i in range(len(list_all)):
		list_all[i] = list_all[i].strip()
	#list_all = ['1a0s', '1bxw', '1e54', '1ek9', '1fep', '1i78', '1k24', '1kmo', '1nqe', '1p4t', '1prn', '1qd6', '1qj8', '1t16', '1thq', '1tly', '1uun', '1uyn', '1xkw', '1yc9', '2erv', '2f1c', '2f1t', '2fcp', '2fgr', '2gr8', '2lhf', '2lme', '2mlh', '2mpr', '2o4v', '2omf', '2por', '2qdz', '2vqi', '2wjr', '2ynk', '3aeh', '3bs0', '3csl', '3dwo', '3dzm', '3fid', '3kvn', '3pik', '3rbh', '3rfz', '3syb', '3szv', '3v8x', '3vzt', '4c00', '4e1s', '4gey', '4k3c', '4pr7', '7ahl', '3b07', '3o44']#, '4q35']

	data_of_all = compile_hydroz_data(list_all)
	dummy, fitted_coef = fit_hydroz_data(data_of_all)

	for pdb in list_all:
		if os.path.exists('pdb_'+pdb+'_hydroz.png'):
			continue
		hydroz_data = compile_hydroz_data([pdb])
		#fitted, dummy = fit_hydroz_data(hydroz_data)
		#plot_hydroz(hydroz_data, pdb, fitted_coef)
		plot_hydroz(hydroz_data, 'pdb_'+pdb)
	#############################################################
	#############################################################


