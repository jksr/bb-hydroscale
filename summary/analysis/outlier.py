import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')

from hydrodata import *
import pickle
import os
import random

import twbio.AminoAcid
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import math
import mean_profile


np.random.seed()
random.seed()

###########################################################################
## prepare dataset for resampling
def compile_datset_to_resample(hds):
	# for each depth, profile_vector_dict stores the ddG vectors of that depth from the given hds
	profile_vector_dict = { dep:[] for dep in range(-4,5) }
	# for each depth, single_vector_dict stores the ddG values for each a.a. residue of that depth from the given hds
	single_value_dict = { dep:{res:[] for res in range(20)} for dep in range(-4,5) }
	
	for dat in hds:
		for dep in range(-4,5):
			for seqid in dat.depth_index_dict[dep]:
				profile_vector_dict[dep].append( dat.records[seqid]['profile'] )
				for res in range(20):
					single_value_dict[dep][res].append( dat.records[seqid]['profile'][res] )
	
	return single_value_dict, profile_vector_dict


###########################################################################
def resample_single_value(single_value_dict, resampling_size = 100000):
	# single value resampling data
	fn = 'outlier/outlier_resampled_single_value.pickle'
	if os.path.exists(fn):
		print 'existing single value resample pickle loaded.'
		sgl_dat = pickle.load(open(fn,'rb'))
	else:
		print 'single value resample pickle does not exist. resampling...'
		sgl_dat = { dep:{res:[] for res in range(20)} for dep in range(-4,5) }
		for dep in range(-4,5):
			for res in range(0,20):
				sgl_dat[dep][res] = np.random.choice(single_value_dict[dep][res], size=resampling_size, replace=True)
				#if dep==3 and res==5: TODO
					#plt.hist(sgl_dat[dep][res], bins=20, normed=True)
					#plt.show()
		pickle.dump(sgl_dat, open(fn,'wb'))
		print 'resampling done.'
	return sgl_dat


###########################################################################
def resample_profile_vector(profile_vector_dict, mean_profile_dict, resampling_size = 100000):
	# profile vector resampling data
	fn = 'outlier/outlier_resampled_profile_vector.pickle'
	if os.path.exists(fn):
		print 'existing vector resample pickle loaded.'
		vec_dat = pickle.load(open(fn,'rb'))
	else:
		print 'vector resample pickle does not exist. resampling...'
		vec_dat = { dep:[] for dep in range(20) }
		for dep in range(-4,5):
			vec_dat[dep] = [ random.choice(profile_vector_dict[dep]) for i in range(resampling_size) ]
		pickle.dump(vec_dat, open(fn,'wb'))
		print 'resampling done.'
	return vec_dat



##########################################################################################
##########################################################################################
# detect single value abnormality according to percentile

# criteria to determine if one residue is significant different from others
# criteria = { depth:{ resi:(p5,p95) }
single_criteria_percentile = { dep:{} for dep in range(-4,5) }
def detect_single_value_abnormal_percentile(hd, sgl_dat):
	h = 0.05
	h = 0.10

	# initialize criteria if it has not been done. '3' is arbitary
	if len(single_criteria_percentile[3])==0:
		for dep in range(-4,5):
			for res in range(1,20):
				p5 = np.percentile(sgl_dat[dep][res], int(h*100) )
				p95 = np.percentile(sgl_dat[dep][res], int(100-h*100) )
				single_criteria_percentile[dep][res] = (p5,p95)

	outliers = []
	for dep in range(-4,5):
		for seqid in hd.depth_index_dict[dep]:
			if hd.code == '4c4v' and seqid == 810:
				continue
			res = twbio.AminoAcid.one_to_index(hd.records[seqid]['resname'])
			if res==0:
				continue
			val = hd.records[seqid]['profile'][res]
			if True:
			#if val < single_criteria_percentile[dep][res][0] or val > single_criteria_percentile[dep][res][1]:
				score = 0.01 * scipy.stats.percentileofscore(sgl_dat[dep][res], val)
				outliers.append( (seqid,score) )
	outliers.sort(key=lambda x:x[1])
	return outliers



##########################################################################################
##########################################################################################
# detect single value abnormality according to zscore

# criteria to determine if one residue is significant different from others
# criteria = { depth:{ resi:(mean, std) }
criteria_single_zscore = { dep:{} for dep in range(-4,5) }
def detect_single_value_abnormal_zscore(hd, sgl_dat):
	# initialize criteria if it has not been done. '3' is arbitary
	if len(criteria_single_zscore[3])==0:
		for dep in range(-4,5):
			for res in range(1,20):
				mean = np.mean(sgl_dat[dep][res])
				std = np.std(sgl_dat[dep][res])
				criteria_single_zscore[dep][res] = (mean,std)

	h = 0.05 # z score +/-2
	h = 0.10
	outliers = []
	for dep in range(-4,5):
		for seqid in hd.depth_index_dict[dep]:
			if hd.code == '4c4v' and seqid == 810:
				continue
			res = twbio.AminoAcid.one_to_index(hd.records[seqid]['resname'])
			if res==0:
				continue
			val = hd.records[seqid]['profile'][res]
			mean, std = criteria_single_zscore[dep][res]
			zscore = (val-mean) / std
			prob = scipy.stats.norm.cdf(zscore)
			if True:
			#if prob < h or 1-prob < h:
				outliers.append( (seqid,prob) )
	outliers.sort(key=lambda x:x[1])
	return outliers


##########################################################################################
##########################################################################################
# detect profile vector abnormality according to coef to the general profile

# criteria to determine if one residue is significant different from others
# criteria = { depth:(p10, [coefs,...]) }
criteria_vector_coef = { dep:{} for dep in range(-4,5) }

def detect_profile_vector_abnormal_coef(hd, vec_dat, mean_profile_dict):
	# initialize criteria if it has not been done. '3' is arbitary
	if len(criteria_vector_coef[3])==0:
		for dep in range(-4,5):
			coefs = np.array([ np.corrcoef(vec, mean_profile_dict[dep])[0][1] for vec in vec_dat[dep] ])
			coefs = coefs[np.logical_not(np.isnan(coefs))]
			### plot histogram of coefs for criteria selection
			#density, bins = np.histogram(coefs, bins=[-1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1], density=True)
			#widths = bins[1:]-bins[:-1]
			#lefts = bins[:-1]
			#freqs = np.multiply(density,widths)
			#plt.bar(lefts, freqs, widths, color = '#08519c', edgecolor='w')
			#plt.title('depth: '+str(dep))
			#plt.xlabel('corr coef')
			#plt.ylabel('freq')
			#plt.savefig('outlier/hist_coef_dep_'+str(dep)+'.png')
			#plt.show()
			
			p10 = np.percentile(coefs,10)
			#p10 = 0.7
			criteria_vector_coef[dep] = (p10, coefs)

	outliers = []
	for dep in range(-4,5):
		for seqid in hd.depth_index_dict[dep]:
			if hd.code == '4c4v' and seqid == 810:
				continue
			coef = np.corrcoef( hd.records[seqid]['profile'], mean_profile_dict[dep] )[0][1]
			if True:
			#if coef < criteria_vector_coef[dep][0]:
				score = 0.01 * scipy.stats.percentileofscore(criteria_vector_coef[dep][1], coef)
				outliers.append( (seqid,coef) )
				#outliers.append( (seqid,score,coef) )####
	outliers.sort(key=lambda x:x[1])
	return outliers


##########################################################################################
##########################################################################################
# detect profile vector abnormality according to distance to the general profile

# criteria to determine if one residue is significant different from others
# criteria = { depth:(p5, p95, [distances,...]) }
criteria_vector_dist = { dep:{} for dep in range(-4,5) }

def detect_profile_vector_abnormal_dist(hd, vec_dat, mean_profile_dict):
	# initialize criteria if it has not been done. '3' is arbitary
	if len(criteria_vector_dist[3])==0:
		for dep in range(-4,5):
			dists = np.array([ np.linalg.norm( vec - mean_profile_dict[dep] ) for vec in vec_dat[dep] ])
			dists = dists[np.logical_not(np.isnan(dists))]
			p90 = np.percentile(dists,90)
			criteria_vector_dist[dep] = (p90, dists)

	outliers = []
	for dep in range(-4,5):
		for seqid in hd.depth_index_dict[dep]:
			if hd.code == '4c4v' and seqid == 810:
				continue
			dist = np.linalg.norm( hd.records[seqid]['profile'] - mean_profile_dict[dep] )
			if True:
			#if dist > criteria_vector_dist[dep][0]:
				score = 0.01 * scipy.stats.percentileofscore(criteria_vector_dist[dep][1], dist)
				outliers.append( (seqid,score) )
	outliers.sort(key=lambda x:x[1])
	return outliers



def main_contributors_of_profile_deviation(hd, seqid, mean_profile_dict, to_str=False):
	for dep in range(-4,5):
		if seqid in hd.depth_index_dict[dep]:
			break
	diffsq = np.square( hd.records[seqid]['profile'] - mean_profile_dict[dep] )
	rtn = diffsq / np.sum(diffsq)
	if to_str:
		return str(rtn)
	else:
		return rtn


if __name__ == '__main__':

	pdbs = open('../finished.list').readlines()
	pdbs.remove('1uun\n') # which is gram positive
	pdbs.remove('2fgr\n') # which is redundent
	pdbs.remove('4c4v\n') # which is redundent of bama
	pdbs.remove('4k3c\n') # which is redundent of bama
	#pdbs.remove('4n75\n') # which is redundent of bama
	#pdbs.remove('7ahl\n')
	#pdbs.remove('3o44\n')
	#pdbs.remove('3b07\n')
	#pdbs = ['1qd6', '1i78', '1thq', '2erv', '3fid', '3aeh', '3kvn']
	#pdbs = ['1qd6', '1i78', '1thq', '2erv', '3fid', '3aeh', '3kvn', '4q35']

	hds = [ pickle.load(open('HD/'+pdb.strip()+'_hd.pickle','rb')) for pdb in pdbs ]
	mean_profile_dict = mean_profile.read_mean_profile()

	# get resampling data
	sgl_datset, vec_datset = compile_datset_to_resample(hds)

	resampling_size = 1000000
	sgl_resampled = resample_single_value(sgl_datset, resampling_size)
	#vec_resampled = resample_profile_vector(vec_datset, mean_profile_dict, resampling_size)


	for hd in hds:
		print hd.code

		#ff = open('outlier/{code}_outliers.txt'.format(code = hd.code),'w')
		#ff.write('## {code}\n'.format(code = hd.code))

		h = 0.05
		h = 0.0013 # Z=-3
		#h = 0.0228 # Z=-2

		ff = open('outlier/scores/{code}_svp.txt'.format(code = hd.code),'w')
		ff.write('##############################################\n')
		ff.write('##############################################\n')
		ff.write('## single value percentile\n')
		belowdone=False
		overdone=False
		ff.write('# single value: \n')
		abnormals = detect_single_value_abnormal_percentile(hd, sgl_resampled)
		for ab in abnormals:
			for dep in range(-4,5):
				if ab[0] in hd.depth_index_dict[dep]:
					break
			if not belowdone and ab[1]>h:
				ff.write('\t'+'A'*25+'\n')
				belowdone = True
			if not overdone and ab[1]>=1-h:
				ff.write('\t'+'V'*25+'\n')
				overdone = True
			if belowdone and not overdone:
				continue
			ff.write( '\t{seqid}\t{res}\t{dep}\t{p}\n'.format(
					seqid = str(ab[0]).rjust(3),
					res = hd.records[ab[0]]['resname'],
					dep = str(dep).rjust(2),
					p = str(ab[1]))
				)
		ff.close()


		ff = open('outlier/scores/{code}_svz.txt'.format(code = hd.code),'w')
		ff.write('##############################################\n')
		ff.write('##############################################\n')
		ff.write('## single value zscore\n')
		belowdone=False
		overdone=False
		ff.write('# single value: \n')
		abnormals = detect_single_value_abnormal_zscore(hd, sgl_resampled)
		for ab in abnormals:
			for dep in range(-4,5):
				if ab[0] in hd.depth_index_dict[dep]:
					break
			if not belowdone and ab[1]>h:
				ff.write('\t'+'A'*25+'\n')
				belowdone = True
			if not overdone and ab[1]>=1-h:
				ff.write('\t'+'V'*25+'\n')
				overdone = True
			if belowdone and not overdone:
				continue
			ff.write( '\t{seqid}\t{res}\t{dep}\t{p}\n'.format(
					seqid = str(ab[0]).rjust(3),
					res = hd.records[ab[0]]['resname'],
					dep = str(dep).rjust(2),
					p = str(ab[1]))
				)
		ff.close()

		continue

		h = 0.1
		ff = open('outlier/scores/{code}_vpc.txt'.format(code = hd.code),'w')
		ff.write('##############################################\n')
		ff.write('##############################################\n')
		ff.write('## vector profile coef\n')
		belowdone=False
		overdone=False
		ff.write('# vector profile: \n')
		abnormals = detect_profile_vector_abnormal_coef(hd, vec_resampled, mean_profile_dict)
		for ab in abnormals:
			for dep in range(-4,5):
				if ab[0] in hd.depth_index_dict[dep]:
					break
			if not belowdone and ab[1]>h:
				ff.write('\t'+'A'*25+'\n')
				belowdone = True
			if not overdone and ab[1]>=1-h:
				ff.write('\t'+'V'*25+'\n')
				overdone = True
			if belowdone and not overdone:
				continue
			ff.write( '\t{seqid}\t{res}\t{dep}\t{p}\n'.format(
					seqid = str(ab[0]).rjust(3),
					res = hd.records[ab[0]]['resname'],
					dep = str(dep).rjust(2),
					p = str(ab[1]))
				)
			#contributors = main_contributors_of_profile_deviation(hd, ab[0], mean_profile_dict)
			#contributors_str =  np.array_str( contributors, max_line_width=500, precision=2, suppress_small=True)
			#ff.write( ' {dists}\n'.format(dists=contributors_str ))
		ff.close()

		h = 0.1

		ff = open('outlier/scores/{code}_vpd.txt'.format(code = hd.code),'w')
		ff.write('##############################################\n')
		ff.write('##############################################\n')
		ff.write('## vector profile dist\n')
		belowdone=False
		overdone=False
		ff.write('# vector profile: \n')
		abnormals = detect_profile_vector_abnormal_dist(hd, vec_resampled, mean_profile_dict)
		for ab in abnormals:
			for dep in range(-4,5):
				if ab[0] in hd.depth_index_dict[dep]:
					break
			if not belowdone and ab[1]>h:
				ff.write('\t'+'A'*25+'\n')
				belowdone = True
			if not overdone and ab[1]>=1-h:
				ff.write('\t'+'V'*25+'\n')
				overdone = True
			if belowdone and not overdone:
				continue
			ff.write( '\t{seqid}\t{res}\t{dep}\t{p}\n'.format(
					seqid = str(ab[0]).rjust(3),
					res = hd.records[ab[0]]['resname'],
					dep = str(dep).rjust(2),
					p = str(ab[1]))
				)
			#contributors = main_contributors_of_profile_deviation(hd, ab[0], mean_profile_dict)
			#contributors_str =  np.array_str( contributors, max_line_width=500, precision=2, suppress_small=True)
			#ff.write( ' {dists}\n'.format(dists=contributors_str ))
		ff.close()
