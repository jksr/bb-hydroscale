import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')

from hydrodata import *
import os

import twbio.AminoAcid
import numpy as np
import scipy.stats
import math
import mean_profile
import pickle



##########################################################################################
##########################################################################################
# detect single value abnormality according to zscore

# criteria to determine if one residue is significant different from others
# criteria = { depth:{ resi:(mean, std) }
criteria_single_zscore = { dep:{} for dep in range(-4,5) }
def detect_single_value_abnormal_zscore(hd, mean_profile_dict, std_profile_dict):
	# initialize criteria if it has not been done. '3' is arbitary
	if len(criteria_single_zscore[3])==0:
		for dep in range(-4,5):
			for res in range(1,20):
				criteria_single_zscore[dep][res] = (mean_profile_dict[dep][res], std_profile_dict[dep][res])

	h = 0.05 # z score +/-2
	h = 0.10
	outliers = []
	for dep in range(-4,5):
		for seqid in hd.depth_index_dict[dep]:
			if hd.code == '4c4v' and seqid == 810:
				continue
			res = twbio.AminoAcid.one_to_index(hd.records[seqid]['resname'])
			if res==0:
				continue
			val = hd.records[seqid]['profile'][res]
			mean, std = criteria_single_zscore[dep][res]
			zscore = (val-mean) / std
			prob = scipy.stats.norm.cdf(zscore)
			if True:
			#if prob < h or 1-prob < h:
				outliers.append( (seqid,prob) )
	outliers.sort(key=lambda x:x[1])
	return outliers


if __name__ == '__main__':

	pdbs = open('../finished.list').readlines()
	pdbs.remove('1uun\n') # which is gram positive
	pdbs.remove('2fgr\n') # which is redundent
	pdbs.remove('4c4v\n') # which is redundent of bama
	#pdbs.remove('4k3c\n') # which is redundent of bama
	pdbs.remove('4n75\n') # which is redundent of bama
	#pdbs.remove('7ahl\n')
	#pdbs.remove('3o44\n')
	#pdbs.remove('3b07\n')
	#pdbs = ['1qd6', '1i78', '1thq', '2erv', '3fid', '3aeh', '3kvn']
	#pdbs = ['1qd6', '1i78', '1thq', '2erv', '3fid', '3aeh', '3kvn', '4q35']

	hds = [ pickle.load(open('HD/'+pdb.strip()+'_hd.pickle','rb')) for pdb in pdbs ]
	mean_profile_dict = mean_profile.read_mean_profile()
	std_profile_dict = mean_profile.read_mean_profile('HD/gram_negative_mean_profile_std.txt')

	for hd in hds:
		print hd.code


		h = 0.0013 # Z=-3
		h = 0.0228 # Z=-2
		h = 0.05 # Z=-1.64

		ff = open('outlier/scores/{code}_svzlite.txt'.format(code = hd.code),'w')
		ff.write('##############################################\n')
		ff.write('##############################################\n')
		ff.write('## single value zscore\n')
		belowdone=False
		overdone=False
		ff.write('# single value: \n')
		abnormals = detect_single_value_abnormal_zscore(hd, mean_profile_dict, std_profile_dict)
		for ab in abnormals:
			for dep in range(-4,5):
				if ab[0] in hd.depth_index_dict[dep]:
					break
			if not belowdone and ab[1]>h:
				ff.write('\t'+'A'*25+'\n')
				belowdone = True
			if not overdone and ab[1]>=1-h:
				ff.write('\t'+'V'*25+'\n')
				overdone = True
			if belowdone and not overdone:
				continue
			ff.write( '\t{seqid}\t{res}\t{dep}\t{p}\n'.format(
					seqid = str(ab[0]).rjust(3),
					res = hd.records[ab[0]]['resname'],
					dep = str(dep).rjust(2),
					p = str(ab[1]))
				)
		ff.close()
