import pickle
from hydrodata import *
import numpy as np
import scipy.spatial.distance as ssd
import lof
import box


def detectoutliers(code, alpha=0.7):
	if __name__ == '__main__':
		print code
	space = '  '
	data = pickle.load(open('HD/'+code+'_hd.pickle','rb'))
	rtn = []
	for i in range(-4,5):
		if __name__ == '__main__':
			print space, '~~~~~~~~ '+str(i)+' ~~~~~~~~~~~'

		# data matrix: each row is a host res, column a ddG
		mtx = np.array( [data.records[j]['profile'] for j in data.depth_index_dict[i]] ) 
		# sum of inverse of corrcoef : sum of distance to others
		if mtx.shape[0]<2:
			continue
		#sumcors = np.sum(np.corrcoef(mtx), axis = 1)
		sumcors = 1-np.mean(np.corrcoef(mtx), axis = 1)
		if __name__ == '__main__':
			for j in range(len(sumcors)):
				print space, data.depth_index_dict[i][j], data.records[data.depth_index_dict[i][j]]['resname'], "%.2f"%sumcors[j]

		sumcorlst = sumcors.tolist()

		# normalize = True will cause exception
		# (x,) must add that dummy or exception caused
		#sumcorsdummy = [(x,) for x in sumcorlst]
		#outers = lof.outliers(int(round(sumcors.size*alpha)), sumcorsdummy, normalize=False)
		#rtn += [ data.depth_index_dict[i][ sumcorlst.index(outlier["instance"][0]) ] for outlier in outers ]
		#if __name__ == '__main__':
		#	for x in outers:
		#		print x["instance"],x["lof"]
		#		x = data.depth_index_dict[i][ sumcorlst.index(x["instance"][0])]
		#		print space, '##', ','.join( [str(x)+data.records[x]['resname']] )

		outvals = box.outliers(sumcorlst)
		outers = [ data.depth_index_dict[i][ sumcorlst.index(outval)]  for outval in outvals ]
		rtn += outers
		if __name__ == '__main__':
			print space, '##', ','.join( [str(x)+data.records[x]['resname'] for x in outers] )
	return sorted(rtn)


if __name__ == '__main__':
	import sys
	import glob
	if len(sys.argv)<2:
		sys.argv.append(0.55)
	#for bb in sorted(glob.glob('????'))[:-1]:
	for bb in ['1qd6','2erv','1i78','1thq','3aeh','3fid','3kvn']:
		otlrs = detectoutliers(bb, float(sys.argv[1]))
		print bb, ':', len(otlrs), ':', ', '.join( str(otlr) for otlr in otlrs )
		print 'show sphere, (' + bb + ' and resi ' + '+'.join( str(otlr) for otlr in otlrs ) + ')'
		print "#"*40


