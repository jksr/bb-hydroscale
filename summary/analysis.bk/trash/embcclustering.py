import glob
import os
import sys
sys.dont_write_bytecode = True

import numpy as np
import matplotlib.pyplot as plt
from hydrodata import *
import pickle
import hydroi

from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from sklearn.metrics import silhouette_samples, silhouette_score

import collections


##############################################################
def get_omp_profile(dicts):
	plotorder = [ 10, 1, 11, 8, 9, 19, 3, 2, 15, 12, 13, 6, 5, 16, 18, 17, 7, 14, 4 ]
	#plotorder = [ 0, 10, 1, 11, 8, 9, 19, 3, 2, 15, 12, 13, 6, 5, 16, 18, 17, 7, 14, 4 ]
	meanvec = []
	stdvec = []
	for iprox in range(19):
		i = plotorder[iprox]
		resmean = np.empty(9)
		resstd = np.empty(9)
		for j in range(-4,5):
			resmean[j+4] = np.mean(np.array(dicts[i][j]))
			resstd[j+4] = np.std(np.array(dicts[i][j]))
		meanvec.append(resmean)
		stdvec.append(resstd)
	meanvec = np.array(meanvec).ravel()
	stdvec = np.array(stdvec).ravel()
	return meanvec, stdvec


##############################################################
def compile_data():
	codes = ['1a0s', '1e54', '1ek9', '1prn', '1qd6', '1t16', '1tly', '1uyn', '1yc9', '2f1c', '2gr8', '2lme', '2mpr', '2o4v', '2omf', '2por', '2qdz', '2wjr', '2ynk', '3aeh', '3bs0', '3dwo', '3fid', '3kvn', '3pik', '3rbh', '3syb', '3szv', '3vzt', '4c00', '4e1s', '4gey', '4pr7', '4k3c', '7ahl', '3b07', '3o44']
	#codes = open('../finished.list').readlines()
	for i in range(len(codes)):
		codes[i] = codes[i].strip()
	data_matrix = []
	pdbs = []
	for code in codes:
		if code in ['1uun','4n75','4c4v','2fgr']:
			continue
		dicts = hydroi.get_hydroi_data(code)
		meanvec, stdvec = get_omp_profile(dicts)
		data_matrix.append( meanvec )
		pdbs.append(code)
	data_matrix = np.array(data_matrix)
	return data_matrix, pdbs


##############################################################
def nanpdist(data, metric):
	size = len(data)
	data_dist = []
	for i in range(size):
		for j in range(i+1,size):
			subdata = np.array([data[i], data[j]])
			subdata = subdata[:,~np.isnan(subdata).any(axis=0)]
			data_dist.append(pdist(subdata,metric)[0])
	data_dist = np.array(data_dist)
	if metric == 'correlation':
		data_dist[ np.isnan(data_dist) ] = 0
	return np.array(data_dist)



##############################################################
##############################################################
# show that if all omps belong to one cluster

if __name__ == '__main__':
	data_matrix, pdbs = compile_data()

	for metric in ['euclidean', 'correlation']:

		# computing the distance 
		data_dist = nanpdist(data_matrix,metric)
		# distance matrix
		data_dist_sqr = squareform(data_dist)
    
		for method in ['single' ,'complete', 'average', 'weighted']:
			# computing the linkage
			data_link = linkage(data_dist,method=method,metric=metric)


			# plot linkage figure
			plt.close('all')
			fig = plt.figure()
			ax = fig.add_subplot(111)
			ax.plot(range(len(data_link[:,2])), data_link[:,2],'o',color='#4393c3')
			ax.set_xlim([-1,58])
			ax.set_xticks([])
			plt.savefig('embccluster/linkage_{metric}_{method}.png'.format(metric=metric,method=method))
			#plt.show()

			for maxclust in [2, 3, 4, 5, 6, 7]:
				cluster_labels= fcluster(data_link, maxclust, 'maxclust')
				# silhoutte score
				silhouette_avg = silhouette_score(data_dist_sqr, cluster_labels, 'precomputed')
				# silhoutte coef for each value
				sample_silhouette_values = silhouette_samples(data_dist_sqr, cluster_labels, 'precomputed')


				# show minorities
				print '-'*30
				print maxclust, method, metric, silhouette_avg
				if maxclust < 5:
					most_common = collections.Counter(cluster_labels).most_common(1)[0][0]
					for item in reversed(zip(cluster_labels, pdbs)):
						if item[0]!=most_common:
							print item


				#plot silhouette
				plt.close('all')
				fig, ax = plt.subplots()
				# The silhouette coefficient can range from -1, 1 but in this example all
				ax.set_xlim([-0.3, 1])
				# The (n_clusters+1)*10 is for inserting blank space between silhouette
				# plots of individual clusters, to demarcate them clearly.
				ax.set_ylim([0, len(data_matrix) + (maxclust + 1) * 5])
				y_lower = 5
				for i in range(1,maxclust+1):
					# Aggregate the silhouette scores for samples belonging to
					# cluster i, and sort them
					ith_cluster_silhouette_values = sample_silhouette_values[cluster_labels == i]
					ith_cluster_silhouette_values.sort()
					size_cluster_i = ith_cluster_silhouette_values.shape[0]
					y_upper = y_lower + size_cluster_i
					color = plt.cm.spectral(float(i) / maxclust)
					if size_cluster_i==1 and ith_cluster_silhouette_values[0]==0:
						# just to make sample with 0 silhouette coef visible
						ax.fill_betweenx(np.arange(y_lower, y_upper), 0, 0.01, facecolor=color, edgecolor=color, alpha=0.7)
					else:
						ax.fill_betweenx(np.arange(y_lower, y_upper), 0, ith_cluster_silhouette_values, facecolor=color, edgecolor=color, alpha=0.7)
					# Label the silhouette plots with their cluster numbers at the middle
					ax.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
					# Compute the new y_lower for next plot
					y_lower = y_upper + 5  # 10 for the 0 samples
				ax.set_title("The silhouette plot for the various clusters.")
				ax.set_xlabel("The silhouette coefficient values")
				ax.set_ylabel("Cluster label")
				# The vertical line for average silhoutte score of all the values
				ax.axvline(x=silhouette_avg, color="red", linestyle="--")
				plt.suptitle(("Silhouette analysis of hierachical clustering w/ {metric} distance and {method} linkage").format(metric=metric,method=method),
							 fontsize=14, fontweight='bold')
				#plt.show()
				plt.savefig('embccluster/silhouete_'+metric+'_'+method+'_'+str(maxclust)+'.png')


			# plot dendrogram and heat map
			plt.close('all')
			fig = plt.figure(figsize=(14,10))
			ax1 = fig.add_axes([0.05,0.1,0.2,0.85])
			#z=dendrogram(data_link,orientation='right',labels=pdbs,leaf_rotation=30)
			z=dendrogram(data_link,orientation='right',labels=pdbs,leaf_rotation=30, color_threshold=10)
			axmatrix = fig.add_axes([0.3,0.1,0.6,0.85])
			idx = z['leaves']
			D = data_dist_sqr[idx,:]
			D = D[:,idx]
			im = axmatrix.matshow(D, aspect='auto', origin='lower', cmap=plt.cm.Blues)
			axmatrix.set_xticks([])
			axmatrix.set_yticks([])
			axcolor = fig.add_axes([0.91,0.1,0.02,0.85])
			plt.colorbar(im, cax=axcolor)
			plt.suptitle('TFE profile hierachical clustering w/ {metric} distance and {method} linkage'.format(metric=metric,method=method), fontweight='bold', fontsize=14);
			plt.savefig('embccluster/clustering_'+metric+'_'+method+'.png')


			# plot dendrogram
			plt.close('all')
			fig = plt.figure()
			fig.set_size_inches(15,3)
			#dendrogram(data_link,orientation='top',labels=pdbs,leaf_rotation=70, leaf_font_size=20)
			dendrogram(data_link,orientation='top',labels=pdbs,leaf_rotation=70, leaf_font_size=20, color_threshold=10)
			plt.gca().tick_params(labelsize=20)
			plt.ylabel('Distance',fontsize=20)
			plt.tight_layout()
			plt.savefig('embccluster/dendro_'+metric+'_'+method+'.png')



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#print "## based on the results above, the possible candidates that make more than 1 cluster are \
#['7ahl', '2lme', '3rbh', '3b07', '2lhf']\n\
## now we test if they are really far away from others\n\
## we also test 3o44 for it is another toxin"

#pdb_to_test = ['7ahl', '2lme', '3rbh', '3b07', '2lhf', '3o44']
#indexes = []
#for pdb in pdb_to_test:
#	indexes.append(pdbs.index(pdb))

#for method in ['single' ,'complete', 'average', 'weighted']:
#	for metric in ['seuclidean' ,'correlation']:
#		data_dist = pdist(data_matrix,metric) # computing the distance 
#		data_link = linkage(data_dist,method=method,metric=metric) # computing the linkage
#		dist_mean = np.mean(data_dist)
#		dist_std = np.std(data_dist)
#		print method, metric, 'dist avg.', dist_mean, 'dist std.', dist_std
#		square_data_dist = squareform(data_dist)
#		for i in range(len(pdb_to_test)):
#			print pdb_to_test[i], np.mean(square_data_dist[indexes[i]])



##############################################################
##############################################################
## try to see which resi and which depth give the variance of data
#from sklearn.decomposition import PCA
#pca = PCA()
#pca.fit(data_matrix)
#print data_matrix.shape
#print pca.components_.shape
#print pca.explained_variance_ratio_.shape
## not working coz too few samples and too many features



