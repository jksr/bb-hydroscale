import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')

from hydrodata import *
import pickle
import os
import random

import twbio.AminoAcid
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import math
import mean_profile


np.random.seed()
random.seed()

def compile_datset_to_resample(hds):
	profile_vector_dict = { dep:[] for dep in range(-4,5) }
	single_value_dict = { dep:{res:[] for res in range(20)} for dep in range(-4,5) }
	
	for dat in hds:
		for dep in range(-4,5):
			for seqid in dat.depth_index_dict[dep]:
				profile_vector_dict[dep].append( dat.records[seqid]['profile'] )
				for res in range(20):
					single_value_dict[dep][res].append( dat.records[seqid]['profile'][res] )
	
	return single_value_dict, profile_vector_dict


def resample_single_value(single_value_dict, resampling_size = 100000):
	# single value resampling data
	if not os.path.exists('HD/outlier_zscore_resampled_single_value.pickle'):
	#if os.path.exists('HD/outlier_zscore_resampled_single_value.pickle'):
		sgl_dat = pickle.load(open('HD/outlier_zscore_resampled_single_value.pickle','rb'))
	else:
		sgl_dat = { dep:{res:[] for res in range(20)} for dep in range(-4,5) }
		for dep in range(-4,5):
			for res in range(0,20):
				resample = np.random.choice(single_value_dict[dep][res], size=resampling_size, replace=True)
				plt.hist(resample, bins=20, normed=True)
				plt.show()
				sgl_dat[dep][res] = ( np.mean( resample ), np.std( resample ) )
		#pickle.dump(sgl_dat, open('HD/outlier_zscore_resampled_single_value.pickle','wb'))
	return sgl_dat


def resample_profile_vector(profile_vector_dict, mean_profile_dict, resampling_size = 100000):
	# profile vector resampling data
	if os.path.exists('HD/outlier_zscore_resampled_profile_vector.pickle'):
		vec_dat = pickle.load(open('HD/outlier_zscore_resampled_profile_vector.pickle','rb'))
	else:
		vec_dat = { dep:[] for dep in range(20) }
		for dep in range(-4,5):
			resample = []
			for i in range(resampling_size):
				dist = np.linalg.norm( random.choice(profile_vector_dict[dep]) - mean_profile_dict[dep] )
				resample.append( dist )
			vec_dat[dep] = ( np.mean( resample ), np.std( resample ) )
		pickle.dump(vec_dat, open('HD/outlier_zscore_resampled_profile_vector.pickle','wb'))
	return vec_dat

def resample_profile_vector_corrcoef(profile_vector_dict, mean_profile_dict, resampling_size = 100000):
	# profile vector resampling data
	if os.path.exists('HD/resampled_profile_vector.pickle'):
		vec_dat = pickle.load(open('HD/resampled_profile_vector.pickle','rb'))
	else:
		vec_dat = { dep:[] for dep in range(20) }
		for dep in range(-4,5):
			resample = []
			for i in range(resampling_size):
				corrcoef = np.corrcoef( random.choice(profile_vector_dict[dep]), mean_profile_dict[dep] )
				corrcoef = corrcoef[0][1]
				resample.append( corrcoef )
			vec_dat[dep] = ( np.mean( resample ), np.std( resample ) )
	return vec_dat



def detect_single_value_abnormal(hd, sgl_dat, h = 0.05):
	print hd.code
	outliers = []
	for dep in range(-4,5):
		for seqid in hd.depth_index_dict[dep]:
			if hd.code == '4c4v' and seqid == 810:
				continue
			res = twbio.AminoAcid.one_to_index(hd.records[seqid]['resname'])
			if res==0:
				continue
			val = hd.records[seqid]['profile'][res]
			mean, std = sgl_dat[dep][res]
			zscore = (val-mean) / std
			prob = scipy.stats.norm.cdf(zscore)
			if prob < h or 1-prob < h:
				outliers.append( (seqid,prob) )
			#outliers.append( (seqid,prob) )####
	#return sorted(outliers)
	outliers.sort(key=lambda x:x[1])
	return outliers


def detect_profile_vector_abnormal(hd, vec_dat, mean_profile_dict, h = 0.05):
	outliers = []
	for dep in range(-4,5):
		for seqid in hd.depth_index_dict[dep]:
			if hd.code == '4c4v' and seqid == 810:
				continue
			dist = np.linalg.norm( hd.records[seqid]['profile'] - mean_profile_dict[dep] )
			mean, std = vec_dat[dep]
			zscore = (dist-mean) / std
			prob = scipy.stats.norm.cdf(zscore)
			if prob < h or 1-prob < h:
				outliers.append( (seqid,prob) )
			#outliers.append( (seqid,prob) )####
	#return sorted(outliers)
	outliers.sort(key=lambda x:x[1])
	return outliers

def detect_profile_vector_abnormal_corrcoef(hd, vec_dat, mean_profile_dict, h = 0.05):
	outliers = []
	for dep in range(-4,5):
		for seqid in hd.depth_index_dict[dep]:
			corrcoef = np.corrcoef( hd.records[seqid]['profile'], mean_profile_dict[dep] )
			corrcoef = corrcoef[0][1]
			mean, std = vec_dat[dep]
			zscore = (corrcoef-mean) / std
			prob = scipy.stats.norm.cdf(zscore)
			#if prob < h or 1-prob < h:
			#	outliers.append( (seqid,prob) )
			outliers.append( (seqid,prob) )####
	#return sorted(outliers)
	outliers.sort(key=lambda x:x[1])
	return outliers


def get_mean_profile(hd_list):
	mean_profile_dict = { dep:None for dep in range(-4,5) }
	for dep in range(-4,5):
		tmp = []
		for hd in hd_list:
			for res in hd.depth_index_dict[dep]:
				tmp.append(hd.records[res]['profile'])
		mean_profile_dict[dep] = np.mean(tmp, axis = 0)
	return mean_profile_dict


def main_contributors_of_profile_deviation(hd, seqid, mean_profile_dict, to_str=False):
	for dep in range(-4,5):
		if seqid in hd.depth_index_dict[dep]:
			break
	diffsq = np.square( hd.records[seqid]['profile'] - mean_profile_dict[dep] )
	rtn = diffsq / np.sum(diffsq)
	if to_str:
		return str(rtn)
	else:
		return rtn


if __name__ == '__main__':

	pdbs = open('../finished.list').readlines()
	pdbs.remove('1uun\n') # which is gram positive
	pdbs.remove('2fgr\n') # which is redundent
	pdbs.remove('4c4v\n') # which is redundent for bama
	pdbs.remove('7ahl\n')
	pdbs.remove('3b07\n')
	#pdbs.remove('3o44\n')
	#pdbs = ['1qd6', '1i78', '1thq', '2erv', '3fid', '3aeh', '3kvn']
	#pdbs = ['1qd6', '1i78', '1thq', '2erv', '3fid', '3aeh', '3kvn', '4q35']

	hds = [ pickle.load(open('HD/'+pdb.strip()+'_hd.pickle','rb')) for pdb in pdbs ]
	mean_profile_dict = get_mean_profile(hds)

	sgl_datset, vec_datset = compile_datset_to_resample(hds)

	resampling_size = 5000#0000
	sgl_resampled = resample_single_value(sgl_datset, resampling_size)
	vec_resampled = resample_profile_vector(vec_datset, mean_profile_dict, resampling_size)

	h= 0.05

#	for hd in hds:
#		abnormals = detect_single_value_abnormal(hd, sgl_resampled, h)
#		print '\tsingle'
#		for ab in sorted(abnormals):
#			print '\t\t',ab[0]
#		abnormals = detect_profile_vector_abnormal(hd, vec_resampled, mean_profile_dict, h)
#		print '\tvector'
#		for ab in sorted(abnormals):
#			print '\t\t',ab[0]
#	sys.exit(0)

	for hd in hds:
		ff = open('outliers/{code}_outliers.txt'.format(code = hd.code),'w')
		ff.write('## {code}\n'.format(code = hd.code))

		belowdone=False
		overdone=False
		ff.write('# single value: \n')
		abnormals = detect_single_value_abnormal(hd, sgl_resampled, h)
		for ab in abnormals:
			for dep in range(-4,5):
				if ab[0] in hd.depth_index_dict[dep]:
					break
			if not belowdone and ab[1]>h:
				ff.write('\t'+'A'*25+'\n')
				belowdone = True
			if not overdone and ab[1]>=1-h:
				ff.write('\t'+'V'*25+'\n')
				overdone = True
			ff.write( '\t{seqid}\t{res}\t{dep}\t{p}\n'.format(
					seqid = str(ab[0]).rjust(3),
					res = hd.records[ab[0]]['resname'],
					dep = str(dep).rjust(2),
					p = str(ab[1]))
				)

		belowdone=False
		overdone=False
		ff.write('# vector profile: \n')
		abnormals = detect_profile_vector_abnormal(hd, vec_resampled, mean_profile_dict, h)
		for ab in abnormals:
			for dep in range(-4,5):
				if ab[0] in hd.depth_index_dict[dep]:
					break
			if not belowdone and ab[1]>h:
				ff.write('\t'+'A'*25+'\n')
				belowdone = True
			if not overdone and ab[1]>=1-h:
				ff.write('\t'+'V'*25+'\n')
				overdone = True
			ff.write( '\t{seqid}\t{res}\t{dep}\t{p}\n'.format(
					seqid = str(ab[0]).rjust(3),
					res = hd.records[ab[0]]['resname'],
					dep = str(dep).rjust(2),
					p = str(ab[1]))
				)
			contributors = main_contributors_of_profile_deviation(hd, ab[0], mean_profile_dict)
			contributors_str =  np.array_str( contributors, max_line_width=500, precision=2, suppress_small=True)
			ff.write( ' {dists}\n'.format(dists=contributors_str ))
		ff.close()


	#datset,dum = compile_datset_to_resample(hd_list)
	#re_sample = resample_single_value(datset)

	#print detect_single_value_abnormal(hd_list[11], re_sample)
