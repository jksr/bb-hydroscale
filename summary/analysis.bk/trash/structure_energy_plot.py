import sys
sys.dont_write_bytecode = True

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import math


def plot_flowerdiagram(omp_hd=None):
	strand_num = 8
	w = 1.0
	r0 = 2.4
	r = r0+9*w
	w_angle = math.asin(w/2/r0)*2
	cmap = plt.get_cmap('RdBu_r')

	def draw_strand(starting_angle, ending_angle, color_pattern, seq, cell_num=9 ):
		for i in range(cell_num):
			lower_left_xy = np.array((r0*math.cos(starting_angle), r0*math.sin(starting_angle)))
			upper_left_xy = np.array((r0*math.cos(ending_angle), r0*math.sin(ending_angle)))
			height = np.linalg.norm(lower_left_xy - upper_left_xy)
			width = r-r0-i*w
			if (color_pattern+i)%2==0:
				color = 'black'
			else:
				color = 'gray'
			rectangle = patches.Rectangle(lower_left_xy, width, height, angle = math.degrees((starting_angle+ending_angle)/2 ), facecolor=color)
			ax.add_patch(rectangle)

	def draw_loop(starting_angle, ending_angle, direction):
		if direction == 0: # outer loop
			starting_xy = np.array(( r*math.cos(starting_angle), r*math.sin(starting_angle) ))
			ending_xy = np.array(( r*math.cos(ending_angle), r*math.sin(ending_angle) ))
			theta1 = math.degrees((starting_angle+ending_angle)/2) - 90
		elif direction == 1: # inner loop
			starting_xy = np.array(( r0*math.cos(starting_angle), r0*math.sin(starting_angle) ))
			ending_xy = np.array(( r0*math.cos(ending_angle), r0*math.sin(ending_angle) ))
			theta1 = math.degrees((starting_angle+ending_angle)/2) + 90
		theta2 = theta1 + 180
		center = (starting_xy+ending_xy)/2
		radius = np.linalg.norm(center-starting_xy)
		wedge = patches.Wedge(center, radius, theta1, theta2, width=0.1, color = 'grey')
		ax.add_patch(wedge)


	fig,ax = plt.subplots()

	# draw strands
	# the strands drawn here are from n-1 to 0
	for i in range(strand_num):
		base_angle = 2*math.pi/strand_num
		central_angle = i*base_angle
		starting_angle = central_angle-base_angle/2
		ending_angle = central_angle+base_angle/2
		diff_angle = ((ending_angle - starting_angle) - w_angle)/2
		starting_angle = starting_angle+diff_angle
		ending_angle = ending_angle-diff_angle
		seq='AAAAAAAAA'
		draw_strand(starting_angle, ending_angle, i, seq, cell_num=9 )

	# draw loops
	for i in range(strand_num-1):
		base_angle = 2*math.pi/strand_num
		starting_angle = i*base_angle
		ending_angle = (i+1)*base_angle
		direction = (i+1)%2
		draw_loop(starting_angle, ending_angle, direction)


plot_flowerdiagram()
#plt.gca().add_patch( patches.Rectangle((1.8477590650225735, -0.7653668647301796), 9, 1.53073372946, 0.0, facecolor='red' ))
plt.gca().set_xlim([-21,21])
plt.gca().set_ylim([-21,21])
plt.show()



