# this script is to calcluate the running time of the hydroscale progame.
# modified by Wei Tian, Jan 3 2015

import glob
import numpy as np

# the lists of OMP with different number of strands.
################ IMPORTANT ################
# DO NOT add other OMPs with same strand number to the list, they may be 
# computed on different server.
# all these OMPs are done on biocluster01.bioe.uic.edu
################ IMPORTANT ################
# for some OMPs, the CASE is important. e.g., 1BXW is done on biocluster01.bioe.uic.edu
# but 1bxw is NOT! be careful with these cases. the upper-case versions are stored in
# folder summary.bak
###########################################
lst8  = [' 8-strand OMP', ['1BXW', '1P4T', '1QJ8', '1THQ', '2F1T']]
lst10 = ['10-strand OMP', ['1K24','1I78']]
lst12 = ['12-strand OMP', ['1QD6', '1UYN']]
lst14 = ['14-strand OMP', ['1T16', '2F1C']]
lst16 = ['16-strand OMP', ['1E54', '1PRN', '2O4V', '2OMF', '2POR' ]]
lst18 = ['18-strand OMP', ['1A0S', '2MPR']]
lst22 = ['22-strand OMP', ['1FEP', '1NQE', '1KMO', '1XKW', '2FCP']]
lst24 = ['24-strand OMP', ['2vqi_time']]
lst26 = ['26-strand OMP', ['4q35']]

codelist = lst24

timelist = []
for code in codelist[1]:
	for fn in glob.glob('../'+code+'/*'):
		with open(fn) as f:
			time = f.readlines()[3].split()[-1]
		timelist.append(float(time))
timearray = np.array(timelist)

mean = np.mean(timearray)#.astype(int)
std = np.std(timearray)#.astype(int)
np.set_printoptions(precision=3)
print '\trunning time of', codelist[0]
print '\tavg: ', mean, 'sec |', mean/60, 'min |', mean/3600, 'hr'
print '\tstd: ',  std, 'sec |',  std/60, 'min |',  std/3600, 'hr'

