import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')

import os
from twbio.dsspdata import DSSPData

import numpy as np
import xlwt



def construct_beta_barrel(pdb, chain):
	dssp = DSSPData('../../inputs/{pdb}/{pdb}.dssp'.format(pdb = pdb))
	ss = np.loadtxt('../../inputs/{pdb}/{pdb}.ss'.format(pdb = pdb)).astype(int)
	centopo = np.loadtxt('../../inputs/{pdb}/{pdb}.cen.topo'.format(pdb = pdb)).astype(int)

	strd_num = len(ss)

	## construct strand
	strands = [ [] for i in range(strd_num+1) ]
	for i in range(strd_num+1):
		strands[i] = range(ss[(i-1)%strd_num][0], ss[(i-1)%strd_num][1]+1)
		if (i-1)%2==0:
			strands[i] = list(reversed(strands[i]))

	## determine residues
	residues = [ [] for i in range(strd_num+1) ]
	for i in range(strd_num+1):
		for j in range(len(strands[i])):
			residues[i].append( dssp.get(chain, strands[i][j], 'aa' ) )

	## determine sidechain direction
	## 1 pore facing, 2 lipid facing
	outfacing = [ [] for i in range(strd_num+1) ]
	for i in range(strd_num+1):
		for seqid in strands[i]:
			if (seqid-centopo[(i-1)%strd_num][0])%2==0:
				outfacing[i].append(centopo[(i-1)%strd_num][1])
			else:
				outfacing[i].append(3-centopo[(i-1)%strd_num][1])

#	## determine connection
#	connection = [ [] for i in range(strd_num+1) ]
#	for i in range(strd_num+1):
#		for j in range(len(strands[i])): 
#			seqid = strands[i][j]
#			bp1 = dssp.get(chain, seqid, 'bp1')
#			bp2 = dssp.get(chain, seqid, 'bp2')
#			bp1 = 0 if bp1==0 else dssp.records[bp1]['resnum']
#			bp2 = 0 if bp2==0 else dssp.records[bp2]['resnum']
#			connection[i].append((bp1,bp2))
#	# check beta bulge
#	for i in range(strd_num):
#		for s in range(strd_num):
#			if connection[i][s][0]!=0 and connection[i][s][1]!=0:
#				break
#		for e in range(strd_num,0,-1):
#			if connection[i][e][0]!=0 and connection[i][e][1]!=0:
#				break
#		print strands[i][s],strands[i][e]
#		for j in range(s,e):
#			if abs(connection[i][j][0]-connection[i][j+1][0])!=1 or abs(connection[i][j][1]-connection[i][j+1][1])!=1:
#				raise Exception( 'beta bugle: ' + pdb + ' ' + chain + ' ' + str(strands[i][j])+'::::'+str(i)+' '+str(j) )




	
	## determine connection and absolute registration
	connection = [ [] for i in range(strd_num) ]
	abs_reg = [0]
	#connection = [ [0 for j in range(len(strands[i]))] for i in range(strd_num) ]
	for i in range(strd_num):
		cen = centopo[(i-1)%strd_num][0]

		for offset in [0,-1,1,-2,2,-3,3]:
			if cen+offset not in strands[i]:
				continue
			cen_pos = strands[i].index(cen+offset) # index in this strand list
			bp1 = dssp.get(chain, cen+offset, 'bp1')
			bp2 = dssp.get(chain, cen+offset, 'bp2')
			bp1 = 0 if bp1==0 else dssp.records[bp1]['resnum']
			bp2 = 0 if bp2==0 else dssp.records[bp2]['resnum']
			if bp1 in strands[i+1]:
				conn_pos = strands[i+1].index(bp1) # connect to the index in next strand list
				break
			elif bp2 in strands[i+1]:
				conn_pos = strands[i+1].index(bp2) # connect to the index in next strand list
				break

#		cen_pos = strands[i].index(cen) # index in this strand list
#		bp1 = dssp.get(chain, cen, 'bp1')
#		bp2 = dssp.get(chain, cen, 'bp2')
#		bp1 = 0 if bp1==0 else dssp.records[bp1]['resnum']
#		bp2 = 0 if bp2==0 else dssp.records[bp2]['resnum']
#		print cen, bp1, bp2
#		if bp1 in strands[i+1]:
#			conn_pos = strands[i+1].index(bp1) # connect to the index in next strand list
#		elif bp2 in strands[i+1]:
#			conn_pos = strands[i+1].index(bp2) # connect to the index in next strand list
#		else:

		abs_reg.append( abs_reg[-1] + len(strands[i])-cen_pos-len(strands[i+1])+conn_pos )

		# connection type
		# strong H 1, non H 0
		if strands[i][0] > strands[i][-1]: # peri -> extra
			# if outfacing[i][cen_pos]==2: conn_typ = 0
			# elif outfacing[i][cen_pos]==1: conn_typ = 1
			conn_typ = 2-outfacing[i][cen_pos]
		else: # extra -> peri
			# if outfacing[i][cen_pos]==2: conn_typ = 1
			# elif outfacing[i][cen_pos]==1: conn_typ = 0
			conn_typ = outfacing[i][cen_pos]-1

		for j in range(len(strands[i])):
			if j-cen_pos+conn_pos < 0 or j-cen_pos+conn_pos >= len(strands[i+1]):
				connection[i].append(None)
			elif (j-cen_pos)%2==0:
				connection[i].append(conn_typ)
			else:
				connection[i].append(1-conn_typ)

#	for i in range(len(strands)):
#		print i
#		print '  ', strands[i]
#		print '  ', outfacing[i]
#		if i == len(strands)-1:
#			break
#		print '  ', connection[i]
#	print abs_reg

	return strands, residues, centopo[:,0].tolist(), outfacing, abs_reg, connection


def barrel_to_xls(pdb, strands, residues, centers, outfacing, abs_reg, connection):
	book = xlwt.Workbook()

	xlwt.add_palette_colour('polar_color',0x21)
	xlwt.add_palette_colour('arom_color',0x22)
	xlwt.add_palette_colour('gp_color',0x23)
	book.set_colour_RGB(0x21,222,45,38)
	book.set_colour_RGB(0x22,65,171,93)
	book.set_colour_RGB(0x23,106,81,163)

	font_def = 'font: bold 1, name Calibri, height 300'
	fontcolor_in = ', color gray25'
	fontcolor_polar = ', color polar_color'
	fontcolor_arom = ', color arom_color'
	fontcolor_gp = ', color gp_color'

	align_def = 'align: vertical center, horizontal center, wrap on'

	border_def = 'borders: left thin, right thin, top thin, bottom thin'
	border_cen = 'borders: left thick, right thick, top thick, bottom thick'
	highlight_out = 'pattern: pattern solid, pattern_fore_colour gray25, pattern_back_colour gray25'
	highlight_nh = 'pattern: pattern solid, pattern_fore_colour blue_gray, pattern_back_colour blue_gray'
	highlight_sh = 'pattern: pattern solid, pattern_fore_colour dark_red, pattern_back_colour dark_red'

	style_default = xlwt.easyxf( ';'.join([ font_def, align_def ]) )
	style_in =  xlwt.easyxf( ';'.join([ font_def+fontcolor_in, align_def, border_def ]) )

	style_out = xlwt.easyxf( ';'.join([ font_def, align_def, border_def, highlight_out ]) )
	style_out_polar = xlwt.easyxf( ';'.join([ font_def+fontcolor_polar, align_def, border_def, highlight_out ]) )
	style_out_arom = xlwt.easyxf( ';'.join([ font_def+fontcolor_arom, align_def, border_def, highlight_out ]) )
	style_out_gp = xlwt.easyxf( ';'.join([ font_def+fontcolor_gp, align_def, border_def, highlight_out ]) )
	style_out_cen = xlwt.easyxf( ';'.join( ([ font_def, align_def, border_cen, highlight_out ]) ) )
	style_out_cen_polar = xlwt.easyxf( ';'.join( ([ font_def+fontcolor_polar, align_def, border_cen, highlight_out ]) ) )
	style_out_cen_arom = xlwt.easyxf( ';'.join( ([ font_def+fontcolor_arom, align_def, border_cen, highlight_out ]) ) )
	style_out_cen_gp = xlwt.easyxf( ';'.join( ([ font_def+fontcolor_gp, align_def, border_cen, highlight_out ]) ) )

	style_in_cen =  xlwt.easyxf( ';'.join( ([ font_def+fontcolor_in, align_def, border_cen ]) ) )
	style_nh = xlwt.easyxf( ';'.join( ([ font_def, align_def, highlight_nh ]) ) )
	style_sh = xlwt.easyxf( ';'.join( ([ font_def, align_def, highlight_sh ]) ) )

	sheet = book.add_sheet(pdb)

	upper = 0
	bottom = 0
	for i in range(len(strands)):
		upper = max(upper, len(strands[i])+abs_reg[i])
		bottom = max(bottom, -abs_reg[i])
	row_num = upper + bottom + 3 # 2 additional rows for strand num and direction, 1 for space

	col_num = len(strands)+len(connection) + 2 # 2 additional rows position ruler
	rowruler = range(row_num+np.min(abs_reg)-3, np.min(abs_reg)-3,-1)
	row_zero = rowruler.index(0)

	for r in range(row_num):
		sheet.write(r, 0, rowruler[r], style_default)
		sheet.write(r, col_num-1, rowruler[r], style_default)
	strd_num = len(strands)-1
	for c in range(1,col_num,2):
		strd_idx = (strd_num-(c+1)/2)%strd_num
		sheet.write(row_num-2, c, strd_idx, style_default )
		sheet.write(row_num-1, c, u'\u25B2' if strd_idx%2==0 else u'\u25BC', style_default )

	for i in range(len(strands)):
		c = col_num-2-2*i
		start_r = row_zero-abs_reg[i]
		strd_len = len(strands[i])

		for j in range(strd_len):
			seqid = strands[i][strd_len-j-1]
			res = residues[i][strd_len-j-1]
			if outfacing[i][strd_len-j-1]==2:
				style_out_str = ';'.join([ align_def, highlight_out, font_def ])
				if res in ['Y','F','W']:
					style_out_str += fontcolor_arom
				elif res in ['N','D','H','C','M','R','K','Q','T','E','S']:
					style_out_str += fontcolor_polar
				elif res in ['G','P']:
					style_out_str += fontcolor_gp

				if seqid==centers[(i-1)%strd_num]:
					style_out_str = ';'.join([ style_out_str, border_cen ])
				else:
					style_out_str = ';'.join([ style_out_str, border_def ])

				res_sty = xlwt.easyxf(style_out_str)
			else:
				res_sty = style_in
				if seqid==centers[(i-1)%strd_num]:
					res_sty = style_in_cen
			sheet.write(start_r-j, c, str(seqid)+residues[i][strd_len-j-1], res_sty)

			if i == len(strands)-1: 
				continue
			if connection[i][strd_len-j-1] == 1:
				bond_sty = style_sh
			elif connection[i][strd_len-j-1] == 0:
				bond_sty = style_nh
			else:
				continue
			sheet.write(start_r-j, c-1, None, bond_sty)
	
	for i in range(2, col_num-1, 2):
		sheet.col(i).width = 4*256
	for i in range(1, col_num-1, 2):
		sheet.col(i).width = 16*256

	# legend
	sheet.write(row_num+2,1,'In', style_in)
	sheet.write(row_num+2,3,'Out', style_out)
	sheet.write(row_num+2,5,'Midplane', style_out_cen)
	sheet.write(row_num+2,7,'SH', style_sh)
	sheet.write(row_num+2,9,'NH', style_nh)
	sheet.write(row_num+3,1,'Polar', style_out_polar)
	sheet.write(row_num+3,3,'Aromartic', style_out_arom)
	sheet.write(row_num+3,5,'GP', style_out_gp)

	book.save(pdb+'.xls')









if __name__ == '__main__':
	with open('../../pdb_info.list') as f: 
		pdbinfo_list = f.readlines()
	for pdbinfo in pdbinfo_list[1:]:
		pdb = pdbinfo.split()[0]
		chain = pdbinfo.split()[1]
		print pdb
		kwargs = construct_beta_barrel(pdb,chain)
		barrel_to_xls(pdb, *kwargs)

