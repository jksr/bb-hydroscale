import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')
#sys.path.append('..')

import twbio.AminoAcid
from hydrodata import *
import matplotlib.pyplot as plt
import numpy as np


def get_strand_insertion_free_energy(hd):
	# single strand insertion free energy
	single = []
	# accumulated insertion free energy
	accumN = [0]
	for strand in hd.strands:
		tmpsgl = 0
		for seqid in strand:
			if seqid in hd.outfacing_list: 
				res = twbio.AminoAcid.one_to_index(hd.records[seqid]['resname'])
				tmpsgl += hd.records[seqid]['profile'][res]
		single.append(tmpsgl)
		accumN.append( accumN[-1]+tmpsgl )
	accumN.pop(0)

	accumC = [0]
	for strand in reversed(hd.strands):
		tmpsgl = 0
		for seqid in strand:
			if seqid in hd.outfacing_list: 
				res = twbio.AminoAcid.one_to_index(hd.records[seqid]['resname'])
				tmpsgl += hd.records[seqid]['profile'][res]
		accumC.append( accumC[-1]+tmpsgl )
	accumC.pop(0)

	return single, accumN, accumC


def plot_strand_insertion_free_energy(hd):
	plt.clf()

	plt.cla()
	single, accumN, accumC = get_strand_insertion_free_energy(hd)
	lin_sgl, = plt.plot(range(1,hd.subunit_strand_num+1), single, 'o', color='#fc9272')
	lin_acc, = plt.plot(range(1,hd.subunit_strand_num+1), accumN, color='#3182bd', linewidth=2)
	plt.title(hd.code)
	plt.xlim([0,hd.subunit_strand_num+1])
	plt.ylim([-70,10])
	plt.xticks(range(1,hd.subunit_strand_num+1))
	plt.xlabel('strand num')
	plt.ylabel(r'insertion free energy $\Delta\Delta G$')
	plt.axhline(0, linewidth=3, linestyle=':', color='#737373')


	lin_acc2, = plt.plot(range(1,hd.subunit_strand_num+1), accumC, color='#6a51a3', linewidth=2)
	plt.legend( [lin_sgl,lin_acc,lin_acc2], ['single strand','accumN','accumC'], loc=3 )

#	# plot normalized insertion free energy
#	ax2= plt.gca().twinx()
#	accum = np.array(accum) / hd.subunit_strand_num
#	lin_norm_acc, = ax2.plot(range(1,hd.subunit_strand_num+1), accum, color='#6a51a3', linewidth=2, linestyle='--')
#	ax2.set_ylim([-4.5,0.5])
#	ax2.set_xlim([0,hd.subunit_strand_num+1])
#	ax2.set_ylabel(r'normalized insertion free energy $\Delta\Delta G$')
#	plt.legend( [lin_sgl,lin_acc,lin_norm_acc], ['single strand','accum','normed accum'], loc=3 )
#	#plt.show()
	plt.savefig('insertion_strand_'+hd.code+'.png')


if __name__ == '__main__':
	for pdb in open('../finished.list').readlines():
		print pdb
		pdb = pdb.strip()
		hd = pickle.load(open('HD/'+pdb+'_hd.pickle','rb'))
		plot_strand_insertion_free_energy(hd)
		#sys.exit(0)
