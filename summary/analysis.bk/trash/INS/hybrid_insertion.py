########################################
############# obtain data ##############

with open('insert.list') as f:
	pdbs = f.readlines()

records = {}
for pdb in pdbs:
	pdb = pdb.strip()

	records[pdb] = {'C':{},'N':{}}

	for strdn in [2,4,6,8,10,12]:
		try:
			with open('../../4c4v_{pdb}_C/4c4v_{pdb}_{strdn}_C.insertion'.format(pdb=pdb, strdn=str(strdn))) as f:
				f.readline()
				f.readline()
				f.readline()
				G = f.readline()
				G = float(G[G.rindex(':')+1:])
				records[pdb]['C'][strdn] = G
			with open('../../4c4v_{pdb}_N/4c4v_{pdb}_{strdn}_N.insertion'.format(pdb=pdb, strdn=str(strdn))) as f:
				f.readline()
				f.readline()
				f.readline()
				G = f.readline()
				G = float(G[G.rindex(':')+1:])
				records[pdb]['N'][strdn] = G
		except:
			break

########################################
############# compute data ##############

pdbGs = {
	'4c4v': -151.1780,
	'1bxw': -85.4600,
	'1qj8': -81.7765,
	'1thq': -80.7143,
	'2f1t': -79.5801,
	'1i78': -98.3120,
	'1qd6': -116.7163,
	'1tly': -106.5739,
	'2wjr': -123.1391,
	'3aeh': -105.4330,
	'4e1s': -111.3182
	}

energies = {}

for pdb in records.keys():

	energies[pdb] = {'C':[],'N':[]}

	energies[pdb]['C'].append(pdbGs['4c4v'])
	energies[pdb]['N'].append(pdbGs['4c4v'])

	for strdn in sorted(records[pdb]['C'].keys()):
		energies[pdb]['C'].append(records[pdb]['C'][strdn])
		energies[pdb]['N'].append(records[pdb]['N'][strdn])

	energies[pdb]['C'].append(pdbGs['4c4v']+pdbGs[pdb])
	energies[pdb]['N'].append(pdbGs['4c4v']+pdbGs[pdb])

########################################
############# print data ##############

import matplotlib.pyplot as plt
import numpy as np

for pdb in energies.keys():
	plt.clf()
	x = np.arange(len(energies[pdb]['C']))*2
	plt.plot(x,energies[pdb]['C'],c='r')
	plt.plot(x,energies[pdb]['N'],c='b')
	plt.title(pdb)
	plt.savefig('4c4v_{pdb}.png'.format(pdb=pdb))



for pdb in energies.keys():
	plt.clf()
	x = np.arange(1,len(energies[pdb]['C']))*2
	plt.plot(x,np.diff(energies[pdb]['C']),c='r')
	plt.plot(x,np.diff(energies[pdb]['N']),c='b')
	plt.title(pdb+'_dG')
	plt.savefig('dG_4c4v_{pdb}.png'.format(pdb=pdb))

