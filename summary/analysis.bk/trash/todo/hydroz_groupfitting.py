import numpy as np
import matplotlib.pyplot as plt
import pickle
import glob
import sys
sys.dont_write_bytecode = True
sys.path.append('../../../pylib')

import twbio.AminoAcid
from hydrodata import *
from scipy.optimize import curve_fit
import outlierdetect

import hydroz




##############################################################
def plot_hydroz_fitted(list_list_pdb, labels,  figname):
	list_hydrozdata = []
	list_fitted = []
	for i in range(len(list_list_pdb)):
		list_hydrozdata.append( hydroz.compile_hydroz_data(list_list_pdb[i]) )
		fitted, dummy = hydroz.fit_hydroz_data(list_hydrozdata[i])
		list_fitted.append( fitted )

	plotorder = [ 0, 10, 1, 11, 8, 9, 19, 3, 2, 15, 12, 13, 6, 5, 16, 18, 17, 7, 14, 4 ]
	fig = plt.figure()
	fig.set_size_inches(15,12)
	mainax = fig.add_subplot(111) # the big subplot
	mainax.set_xlabel('\nZ position\n'+figname, fontsize=18)
	mainax.set_ylabel(r'$\mathbf{\Delta\Delta G_{aa}}$(kcal/mol)'+'\n', fontsize=18)
	# Turn off axis lines and ticks of the big subplot
	mainax.patch.set_visible(False)
	mainax.spines['top'].set_visible(False)
	mainax.spines['bottom'].set_visible(False)
	mainax.spines['left'].set_visible(False)
	mainax.spines['right'].set_visible(False)
	mainax.set_xticklabels([])
	mainax.set_yticklabels([])

	legend_handles = []
 
	for iprox in range(20):
		i = plotorder[iprox]
		curax = fig.add_subplot(4,5,iprox+1)
		curax.axhline(0, linestyle='-.', color = '#808080')
		curax.set_title(twbio.AminoAcid.index_to_three(i).title())
		curax.set_ylim(-3.5,7)
		curax.set_xlim(-1.1,1.1)
		curax.set_color_cycle(['#66c2a5', '#fc8d62', '#8da0cb', '#e78ac3', '#a6d854', '#ffd92f', '#e5c494', '#b3b3b3'])
		for j in range(len(list_list_pdb)):
			h, = curax.plot(list_hydrozdata[j][i][:,0], list_fitted[j][i], linewidth = 3, label = labels[j])
			if i == 0:
				curax.legend(loc = 2)

	plt.tight_layout(pad=0.2,w_pad = 0.4, h_pad = 0.4)
	plt.savefig(figname+'_hydroz.png')
	plt.close()
##############################################################




#plot_hydroz_fitted([hydroz.list_11, hydroz.list_31, hydroz.list_33, hydroz.list_81 ], ['1 subunit 1 pore','3 subunits 1 pore','3 subunits 3 pores','8 subunits 1 pore'],  'Assembly State')

#plot_hydroz_fitted( [ hydroz.list_8, hydroz.list_10, hydroz.list_12+hydroz.list_12_, hydroz.list_14, hydroz.list_16, hydroz.list_18, hydroz.list_22, hydroz.list_24 ], [ ' 8 strands', '10 strands', '12 strands', '14 strands', '16 strands', '18 strands', '22 strands', '24 strands' ], 'Size' )

#plot_hydroz_fitted( [ hydroz.list_STyphimurium, hydroz.list_PPutida, hydroz.list_PAeruginosa, hydroz.list_NMeningitidis, hydroz.list_EColi, hydroz.list_Others ], [ 'S. Typhimurium', 'P. Putida', 'P. Aeruginosa', 'N. Meningitidis', 'E. Coli', 'Others' ], 'Organism' )




plot_hydroz_fitted( [ hydroz.list_11 ], [ 'all' ], 'All' )
