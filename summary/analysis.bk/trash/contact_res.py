import Bio.PDB
import glob
import sys
import numpy as np

import warnings
from Bio import BiopythonWarning
warnings.simplefilter('ignore', BiopythonWarning)


pdb_info = {}
_chain_id = 0
_subunit_num = 1
_barrel_num = 2
_tm_strand_num = 3


def get_pdb_info():
	with open('../../pdb_info.list') as f:
		lines = f.readlines()
	lines.pop(0)
	for line in lines:
		splt = line.split()
		pdb_info[splt[0]]=splt[1:]


# get tm res from .ss file
def get_tm_res(pdb):
	tmres = []
	with open('../../inputs/'+pdb+'/'+pdb+'.ss') as f:
		lines = f.readlines()
	for line in lines:
		splt = line.split()
		tmres += range(int(splt[0]),int(splt[1])+1)
	return tmres


# get outfacing tm res from .cen.topo file
def get_tm_res2(pdb):
	tmres = []
	with open('../../inputs/'+pdb+'/'+pdb+'.cen.topo') as f:
		lines = f.readlines()
	for line in lines:
		cen, direction = line.split()
		cen, direction = int(cen), int(direction)
		if direction == 2:
			tmres+=[cen-4,cen-2,cen,cen+2,cen+4]
		else:
			tmres+=[cen-3,cen-1,cen+1,cen+3]
	return tmres


def detect_contact_res(pdb, anchor_res_list, threshold=7):
	anchor_chain_id = pdb_info[pdb][_chain_id]
	protein = {}
	for pdbchain in Bio.PDB.PDBParser().get_structure('MyStruct', '../../inputs/'+pdb+'/'+pdb+'.pdb')[0]:
		chain_id = pdbchain.get_id()
		chain = {}
		for pdbres in pdbchain:
			if not pdbres.child_dict.has_key('CA'):
				continue
			chain[pdbres.get_id()[1]] = pdbres['CA'].get_coord()
		protein[chain_id] = chain

	contacts = set()

	tmres = get_tm_res(pdb)

	anchor_chain = protein[anchor_chain_id]
	for anchor_resid in anchor_res_list:
	#for resid in anchor_chain.keys():
		try:
			anchor_res_coord = np.array(anchor_chain[anchor_resid])
		except:
			continue

		for another_chain_id in protein.keys():
			another_chain = protein[another_chain_id]
			for another_resid in another_chain.keys():
				if another_chain_id == anchor_chain_id:
					# dont count those connect directly to tm region
					if another_resid in tmres:
						continue
					elif another_resid+1 in tmres:
						continue
					elif another_resid-1 in tmres:
						continue
					#elif another_resid+2 in tmres:
					#	continue
					#elif another_resid-2 in tmres:
					#	continue
				another_res_coord = np.array(another_chain[another_resid])

				if np.linalg.norm(anchor_res_coord-another_res_coord)<=threshold:
					contacts.add(anchor_resid)
					#contacts.add((anchor_resid, another_chain_id, another_resid))

	return contacts




if __name__ == '__main__':

	get_pdb_info()

	with open('../finished.list') as f:
		pdbs = f.readlines()
	for pdb in pdbs:
		pdb = pdb.strip()
		anchor_res_list = get_tm_res2(pdb)
		print pdb, sorted(list(detect_contact_res(pdb, anchor_res_list)))





	#if len(sys.argv)<2:
	#	print "Usage:", sys.argv[0], "pdb"
	#	sys.exit(0)

	#get_pdb_info()
	#anchor_res_list = get_tm_res2(sys.argv[1])
	#print sorted(list(detect_contact_res(sys.argv[1], anchor_res_list)))

