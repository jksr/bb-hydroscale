import sys
sys.dont_write_bytecode = True
from hydrodata import *
import pickle
import matplotlib.pyplot as plt
import numpy as np
from outlierdetect import *


with open('finished.list') as f:
	codes = f.readlines()

alldata = []
for code in codes:
	code = code.strip()
	outers = detectoutliers(code)
	outers = []
	data = pickle.load(open('HD/'+code+'_hd.pickle','rb'))
	resi_idx0 = data.depthindexdict[0]
	for resi in resi_idx0:
		if resi in outers:
			continue
		alldata.append(data.recs[resi]['profile'])

alldata = np.vstack(alldata)
mean = np.mean(alldata, axis=0)
std = np.std(alldata, axis=0)

aa = [ 'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V' ]

zipped = sorted(zip(mean.tolist(),std.tolist(),aa))
for i in range(len(zipped)):
	mean[i] = zipped[i][0]
	std[i] = zipped[i][1]
	aa[i] = zipped[i][2]

upstd = np.copy(std)
upstd[mean>0]=0
lostd = np.copy(std)
lostd[mean<0]=0

fig = plt.figure(figsize =(10,10),dpi=150 )
#mainax = fig.add_subplot(1,1,1)
ax1 = fig.add_subplot(1,1,1)
#mainax.set_xlabel('\n'+r'Position index ($i$)'+'\n'+code, fontsize=18)
#mainax.set_ylabel(r'$\mathbf{\Delta\Delta G_{aa(i)}}$(kcal/mol)'+'\n', fontsize=18)
## Turn off axis lines and ticks of the big subplot
#mainax.patch.set_visible(False)
#mainax.spines['top'].set_visible(False)
#mainax.spines['bottom'].set_visible(False)
#mainax.spines['left'].set_visible(False)
#mainax.spines['right'].set_visible(False)
#mainax.set_xticklabels([])
#mainax.set_yticklabels([])
#
#ax1 = fig.add_subplot(1,1,1)


x = np.arange(20)
x = x + 0.3
width = 0.6
legours = ax1.bar(x,mean,width=width,color='#2b8cbe',yerr=[upstd,lostd],error_kw=dict(ecolor='gray', lw=2, capsize=5))
ax1.set_xticks(x+width)
ax1.set_xticklabels(aa, fontsize = 20)
ax1.set_ylabel(r"$\Delta\Delta G$(kcal/mol)", fontsize = 20)
ax1.set_xlim([0,19+0.3*2+width])
#ax1.legend( (legours[0]), ('Ours'), loc=0, fontsize = 20)
#plt.show()
plt.savefig('common.eps',format='eps',dpi=150)


