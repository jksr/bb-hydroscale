import numpy as np

def outliers( dat, cutoff = 'mild' ):
	q1,q2,q3 = np.percentile( dat, [35, 50, 65], interpolation = 'linear' )
	iq = q3-q1
	l1 = q1-1.5*iq
	l2 = q1-3.0*iq
	u1 = q3+1.5*iq
	u2 = q3+3.0*iq
	rtn = []
	if cutoff == 'mild':
		for d in dat:
			#if d < l1 or d > u1:
			if d > u1:
				rtn.append(d)
	elif cutoff == 'extreme':
		for d in dat:
			#if d < l2 or d > u2:
			if d > u2:
				rtn.append(d)
	else:
		raise Exception()
	return rtn





#a=[ 2.1965528 , 2.40337934, 3.92434472, 5.07451703 ,3.34708363, 4.66904143,3.5429678  ,4.78477375]
#print outliers(a)
#a=[ 8.94608357, 9.10990729, 9.30461554, 8.71386736 ,8.92698569, 8.83699428,8.29186603 ,9.24855791, 9.11362083, 9.1853923 ]
#print outliers(a)
#a=[ 7.30347629, 7.21562703, 7.32128266, -3.2183665, 7.2723884, 6.67591777, 6.4460229, 7.04561989, 7.50397964, 6.68122514]
#print outliers(a)
