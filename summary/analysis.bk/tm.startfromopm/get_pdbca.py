#import sys
#sys.path.append('../../../../pylib')


def get_pdb_ca(pdb, outfn, chains):
	pdbfn = '../../../inputs/'+pdb+'/'+pdb+'.pdb'
	with open(outfn,'w') as fout:
		with open(pdbfn) as fin:
			while True:
				line = fin.readline()
				if not line:
					break
				if len(chains)>1:
					if line.startswith('ATOM') and line[13:15] == 'CA' and line[21] in chains:
						fout.write(line)
				else:
					if line.startswith('ATOM') and line[13:15] == 'CA' and line[21]==chains:
						fout.write(line)




pdbchains = [ ('1a0s', ['P','Q','R']), ('1bxw', 'A'), ('1e54', ['A','C','E']), ('1ek9', ['A','C','B']), ('1fep', 'A'), ('1i78', 'A'), ('1k24', 'A'), ('1kmo', 'A'), ('1nqe', 'A'), ('1p4t', 'A'), ('1prn', ['A','B','C']), ('1qd6', ['C','D']), ('1qj8', 'A'), ('1t16', 'A'), ('1thq', 'A'), ('1tly', 'A'), ('1uyn', 'X'), ('1xkw', 'A'), ('1yc9', ['A','B','C']), ('2erv', 'A'), ('2f1c', 'A'), ('2f1t', 'A'), ('2fcp', 'A'), ('2gr8', ['A','C','D']), ('2lhf', 'A'), ('2lme', ['A','B','C']), ('2mlh', 'A'), ('2mpr', ['A','B','C']), ('2o4v', ['A','B','C']), ('2omf', ['A','B','C']), ('2por', ['A','B','C']), ('2qdz', 'A'), ('2vqi', 'A'), ('2wjr', 'A'), ('2ynk', 'A'), ('3aeh', 'A'), ('3bs0', 'A'), ('3csl', 'A'), ('3dwo', 'X'), ('3dzm', 'A'), ('3fid', 'A'), ('3kvn', 'A'), ('3pik', ['A','B','C']), ('3rbh', 'A'), ('3rfz', 'B'), ('3syb', 'A'), ('3szv', 'A'), ('3v8x', 'A'), ('3vzt', ['A','B','C']), ('4c00', 'A'), ('4e1s', 'A'), ('4gey', 'A'), ('4pr7', 'A'), ('4q35', 'A'), ('4k3c', 'A'), ('7ahl', ['A','B','C','D','E','F','G']), ('3b07', ['A','B','C','D','E','F','G','H']), ('3o44', ['A','B','C','D','E','F','G']) ]



for pdb,chains in pdbchains:
	#get_pdb_ca(pdb, 'pdbs/'+pdb+'_'+chain+'_ca.pdb', chains)
	get_pdb_ca(pdb, 'pdbs/'+pdb+'_ca.pdb', chains)
