def minimize(func,x0, bounds, stepnums=10):
	'''stepnums can be an integer or a list
	'''
	assert len(bounds)==4
	xn = len(bounds)
	if isinstance(stepnums, int):
		stepnums = [stepnums]*xn
	minv = func(x0)
	minvx = x0
	import numpy as np
	for p0 in np.linspace(bounds[0][0],bounds[0][1],num=stepnums[0]):
		for p1 in np.linspace(bounds[1][0],bounds[1][1],num=stepnums[1]):
			for p2 in np.linspace(bounds[2][0],bounds[2][1],num=stepnums[2]):
				for p3 in np.linspace(bounds[3][0],bounds[3][1],num=stepnums[3]):
					vx = np.array([p0,p1,p2,p3])
					v = func(vx)
					if v<minv:
						minv = v
						minvx = vx
	import scipy.optimize

	#class Object(object):
	#	pass
	#result = Object()
	result = scipy.optimize.OptimizeResult()
	result.success = True
	result.x = minvx
	return result

