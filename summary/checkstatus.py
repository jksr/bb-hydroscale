import glob
import sys
import os

with open('finished.list') as f:
	lines = f.readlines()

finisheds = [ line[:4] for line in lines ]

tochecks = glob.glob('????')

for finished in finisheds:
	tochecks.remove(finished)

for tocheck in tochecks:
	if not os.path.isdir(tocheck):
		tochecks.remove(tocheck)

for tocheck in tochecks:
	#fnum = len([name for name in os.listdir('.') if os.path.isfile(name)])
	with open(tocheck+'/'+tocheck+'.out.list') as outf:
		outls = outf.readlines()
	finished = True
	for outl in outls:
		if len(glob.glob(tocheck+'/'+tocheck+'_'+outl.strip()+'*.energy')) == 0:
			finished = False
			break
		if os.path.getsize( glob.glob(tocheck+'/'+tocheck+'_'+outl.strip()+'*.energy')[0] ) < 300:
			finished = False
			break
	if finished:
		lines.append(tocheck+'\n')
		print ('new finished added: '+tocheck)


with open('finished.list','w') as f:
	f.writelines(lines)
